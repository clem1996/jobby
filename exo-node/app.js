// procédure installation node

// sudo apt-get update
// sudo apt-get install curl
// curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
// sudo apt-get install nodejs

// const http = require('http');
// http.createServer(function (req, res) {
//     res.writeHead(200, { 'Content-Type': 'text/plain' });
//     res.end('Hello World\n');
// }).listen(1337, '127.0.0.1'); // remove the ip argument from listen. otherwise it will bind only to this ip address. and you wont be able to connect if it runs on a server.
// console.log('Server running at http://127.0.0.1:1337/');

// var http = require('http');
// var url = require('url');
// http.createServer(function (req, res) {
//     res.writeHead(200, { 'Content-Type': 'text/plain; charset=UTF-8' });
//     var purl = url.parse(req.url, true);
//     if (purl.pathname == '/test') res.end('Test'); // there is response.write and response.end
//     // response.end('text') is response.write('text'), then response.end();
//     else res.end('Hello World\n');
// }).listen(1337);
// console.log('Server running at http://127.0.0.1:1337/');


// install express :
// cd mondossier
// npm init -y
// npm install --save express



const express = require('express') // on va chercher express et on l'assigne à une constante
const app = express() // notre application par défaut est maintenant express

// notre app GET la route '/' et renvoie une string 'Hello World'
// app.get('/', (req, res) => {
//     res.send('Allociné')
//     console.log(req.path)

// })

// app.get('/contact', (req, res) => {
//     res.send('contact form')
//     console.log(req.path)

// })

// app.get('/questionForm', (req, res) => {
//     res.send('question form')
//     console.log(req.path)

// })


// // notre app GET la route '/toto' et renvoie une string 'toto'
// app.get('/films', (req, res) => {
//     res.send('Films')
// })

// // notre app GET la route '/toto' et renvoie une string 'toto'
// app.get('/series', (req, res) => {
//     res.send('Séries')
// })

// // notre app GET la route '/toto' et renvoie une string 'toto'
// app.get('/serie/francaise', (req, res) => {
//     res.send('SérieFrançaise')
// })

// // notre app GET la route '/toto' et renvoie une string 'toto'
// app.get('/serie/anglaise', (req, res) => {
//     res.send('SerieAnglaise')
// })

// // notre app GET la route '/toto' et renvoie une string 'toto'
// app.get('/court-metrage', (req, res) => {
//     res.send('Court-métrage')
// })





app.listen(3000, () => console.log('app running on port 3000')) // notre application écoute en localhost(127.0.0.1) sur le port 3000
