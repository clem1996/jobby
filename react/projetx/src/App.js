import React from 'react';
import Home from './layouts/Home'
import superagent from 'superagent'

class App extends React.Component {

  state = {
    bearer: ''
  }
  componentDidMount = async () => {
    
    console.log('componentDidMount App')
    let myApiURl = 'http://api.projetx:8000/oAuth'
    let reponse = await superagent
      .get(myApiURl)
    // reponse.resultats
    this.setState({
      bearer: reponse.body
    })
  }
  render() {
    console.log('render App')

    if (this.state.bearer === '')
      return (
        <div>waiting token bearer</div>
      )

    return (
      <div className="App">
        {this.state.bearer}
        <Home bearer={this.state.bearer} />
      </div>
    )
  }
}

export default App;
