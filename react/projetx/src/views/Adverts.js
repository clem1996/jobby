import React from 'react';
import Advert from '../components/Advert'


class Adverts extends React.Component {
 
   
    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log('componentDidUpdate Adverts')
        // 4.2
        // 5.2
    }

    render() {
        // console.log('render Adverts')



        let rows = this.props.resultats.map((resultat, index) =>

            <tr key={`resultats${index}`}><td>
                <Advert
                    type={resultat.typeContrat}
                    intitule={resultat.intitule}
                    description={resultat.description.substring(0, 20) + '...'}

                />
            </td></tr>



        )



        // 2
        // changement d'etat ou de ses props 4.1
        // changement d'etat ou de ses props 5.1
        return (
            <div className="Adverts">
                Adverts

                <table border="1" width="100%">
                    <tbody>
                        {rows}
                    </tbody>

                </table>

            </div>
        )
    }
}

export default Adverts;