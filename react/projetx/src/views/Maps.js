import React from 'react';
import GoogleMapReact from 'google-map-react';
const AnyReactComponent = ({ text }) => <div>{text}</div>;

// AIzaSyAxCHfxbpynvqRn-lZcQmP2ZPTjoETMhj0
class Maps extends React.Component {

    // custom handler
    handleClickOnMap = () => {
        alert('ici')
    }
    render() {
        return (
            <div style={{ height: '100vh', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: 'AIzaSyAxCHfxbpynvqRn-lZcQmP2ZPTjoETMhj0' }}
                    defaultCenter={{
                        lat: 59.95,
                        lng: 30.33
                    }}
                    defaultZoom={11}
                >
                    <AnyReactComponent

                        lat={59.955413}
                        lng={30.337844}
                        text={<b style={{ fontSize: "18px" }} 
                        onClick={(evt)=>this.handleClickOnMap("lille")}

                        
                        >Lomme</b>}
                    />

                    <AnyReactComponent

                        lat={59.925413}
                        lng={30.337844}
                        text={<b style={{ fontSize: "18px" }}
                        onClick={(evt)=>this.handleClickOnMap("lomme")}

                         
                         >Lille</b>}
                    />
                </GoogleMapReact>

            </div>
        )
    }
}

export default Maps;