const datas = {
  "filtresPossibles": [
    {
      "agregation": [
        {
          "nbResultats": 2499,
          "valeurPossible": "CCE"
        },
        {
          "nbResultats": 62331,
          "valeurPossible": "CDD"
        },
        {
          "nbResultats": 193214,
          "valeurPossible": "CDI"
        },
        {
          "nbResultats": 80,
          "valeurPossible": "CDS"
        },
        {
          "nbResultats": 370,
          "valeurPossible": "DDI"
        },
        {
          "nbResultats": 231,
          "valeurPossible": "DIN"
        },
        {
          "nbResultats": 3692,
          "valeurPossible": "FRA"
        },
        {
          "nbResultats": 7216,
          "valeurPossible": "LIB"
        },
        {
          "nbResultats": 64009,
          "valeurPossible": "MIS"
        },
        {
          "nbResultats": 1297,
          "valeurPossible": "REP"
        },
        {
          "nbResultats": 3792,
          "valeurPossible": "SAI"
        },
        {
          "nbResultats": 149,
          "valeurPossible": "TTI"
        }
      ],
      "filtre": "typeContrat"
    },
    {
      "agregation": [
        {
          "nbResultats": 25098,
          "valeurPossible": "0"
        },
        {
          "nbResultats": 176863,
          "valeurPossible": "1"
        },
        {
          "nbResultats": 116583,
          "valeurPossible": "2"
        },
        {
          "nbResultats": 20336,
          "valeurPossible": "3"
        }
      ],
      "filtre": "experience"
    },
    {
      "agregation": [
        {
          "nbResultats": 190289,
          "valeurPossible": "0"
        },
        {
          "nbResultats": 13282,
          "valeurPossible": "9"
        },
        {
          "nbResultats": 135309,
          "valeurPossible": "X"
        }
      ],
      "filtre": "qualification"
    },
    {
      "agregation": [
        {
          "nbResultats": 259,
          "valeurPossible": "CC"
        },
        {
          "nbResultats": 156,
          "valeurPossible": "CI"
        },
        {
          "nbResultats": 244,
          "valeurPossible": "CU"
        },
        {
          "nbResultats": 319137,
          "valeurPossible": "E1"
        },
        {
          "nbResultats": 2658,
          "valeurPossible": "E2"
        },
        {
          "nbResultats": 18,
          "valeurPossible": "EE"
        },
        {
          "nbResultats": 191,
          "valeurPossible": "FA"
        },
        {
          "nbResultats": 4,
          "valeurPossible": "FJ"
        },
        {
          "nbResultats": 2345,
          "valeurPossible": "FS"
        },
        {
          "nbResultats": 247,
          "valeurPossible": "FT"
        },
        {
          "nbResultats": 8,
          "valeurPossible": "FU"
        },
        {
          "nbResultats": 165,
          "valeurPossible": "FV"
        },
        {
          "nbResultats": 814,
          "valeurPossible": "I1"
        },
        {
          "nbResultats": 12613,
          "valeurPossible": "NS"
        },
        {
          "nbResultats": 3,
          "valeurPossible": "PR"
        },
        {
          "nbResultats": 18,
          "valeurPossible": "PS"
        }
      ],
      "filtre": "natureContrat"
    }
  ],
  "resultats": [
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Tailleur-graveur / Tailleuse-graveuse",
      "competences": [
        {
          "code": "125598",
          "exigence": "S",
          "libelle": "Affûter des outils"
        },
        {
          "code": "125595",
          "exigence": "S",
          "libelle": "Effectuer une taille, une gravure ou un guillochage au sable"
        },
        {
          "code": "125136",
          "exigence": "S",
          "libelle": "Gravure de lettres et chiffres"
        },
        {
          "code": "125810",
          "exigence": "S",
          "libelle": "Reproduire à la main un motif sur un article"
        },
        {
          "code": "119214",
          "exigence": "S",
          "libelle": "Sélectionner des matériels, matériaux et outillages"
        },
        {
          "code": "125600",
          "exigence": "S",
          "libelle": "Techniques de ciselure"
        },
        {
          "code": "100610",
          "exigence": "S",
          "libelle": "Techniques de dessin"
        },
        {
          "code": "117952",
          "exigence": "S",
          "libelle": "Techniques de gravure"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : info@facade2000.fr",
        "courriel": "info@facade2000.fr",
        "nom": "FACADE 2000 - M. SARACOGLU MUSA"
      },
      "dateCreation": "2019-11-13T13:04:18+01:00",
      "deplacementCode": "2",
      "deplacementLibelle": "Ponctuels",
      "description": "Vos missions:\n- Réalisation et remise en état de gravures et de moulures sur les façades en pierre ou béton,\n- Travail en hauteur sur échafaudage ou sur nacelle,\n\nLe permis n'est pas obligatoire.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "FACADE 2000"
      },
      "experienceCommentaire": "en taille ou gravage",
      "experienceExige": "E",
      "experienceLibelle": "6 mois - en taille ou gravage",
      "formations": [],
      "id": "095WLCH",
      "intitule": "Tailleur-graveur / Tailleuse-graveuse",
      "langues": [],
      "lieuTravail": {
        "codePostal": "38610",
        "commune": "38179",
        "latitude": 45.18194444,
        "libelle": "38 - GIERES",
        "longitude": 5.791944444
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLCH"
      },
      "permis": [
        {
          "exigence": "S",
          "libelle": "B - Véhicule léger Souhaité"
        }
      ],
      "qualificationCode": "2",
      "qualificationLibelle": "Ouvrier spécialisé",
      "romeCode": "B1303",
      "romeLibelle": "Gravure - ciselure",
      "salaire": {
        "libelle": "Mensuel de 1524,00 Euros à 2000,00 Euros sur 12 mois"
      },
      "secteurActivite": "43",
      "secteurActiviteLibelle": "Travaux de peinture et vitrerie",
      "trancheEffectifEtab": "1 ou 2 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Moniteur / Monitrice d'auto-école",
      "competences": [
        {
          "code": "123049",
          "exigence": "S",
          "libelle": "Accompagner un élève dans l'apprentissage de la conduite"
        },
        {
          "code": "123877",
          "exigence": "S",
          "libelle": "Contrôler une maintenance"
        },
        {
          "code": "123048",
          "exigence": "S",
          "libelle": "Évaluer la conduite d'un élève"
        },
        {
          "code": "124590",
          "exigence": "S",
          "libelle": "Évaluer le nombre d'heure de code, de conduite ou de pilotage"
        },
        {
          "code": "123191",
          "exigence": "S",
          "libelle": "Former à la conduite d'un véhicule"
        },
        {
          "code": "108568",
          "exigence": "S",
          "libelle": "Règles de conduite et de sécurité routière"
        },
        {
          "code": "123050",
          "exigence": "S",
          "libelle": "Renseigner des documents de suivi et d'évaluation d'un élève"
        },
        {
          "code": "124589",
          "exigence": "S",
          "libelle": "Renseigner sur le déroulement des cours"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : ecf.leshaberges@orange.fr",
        "courriel": "ecf.leshaberges@orange.fr",
        "nom": "ECF - M. Xavier   BRECHE"
      },
      "dateCreation": "2019-11-13T13:04:01+01:00",
      "description": "Enseignement de la conduite pratique et théorique.\nPoste à pourvoir rapidement sur Vesoul : cours de conduite, cours de code, préparation aux examens ...\n\nVous possédez impérativement le BEPECASER ou titre pro.",
      "dureeTravailLibelle": "17H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps partiel",
      "entreprise": {
        "nom": "ECF"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "31802",
          "commentaire": "BPCASER ou titre professionnel",
          "domaineLibelle": "Monitorat auto-école",
          "exigence": "S",
          "niveauLibelle": "Bac ou équivalent"
        }
      ],
      "id": "095WLCG",
      "intitule": "Moniteur / Monitrice d'auto-école",
      "langues": [],
      "lieuTravail": {
        "codePostal": "70000",
        "commune": "70550",
        "latitude": 47.62222222,
        "libelle": "70 - VESOUL",
        "longitude": 6.155277777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLCG"
      },
      "permis": [
        {
          "exigence": "E",
          "libelle": "B - Véhicule léger Exigé"
        }
      ],
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "K2110",
      "romeLibelle": "Formation en conduite de véhicules",
      "salaire": {
        "commentaire": "suivant profil",
        "complement1": "suivant profil"
      },
      "secteurActivite": "85",
      "secteurActiviteLibelle": "Enseignement de la conduite",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Serveur / Serveuse de restaurant",
      "competences": [
        {
          "code": "104323",
          "exigence": "S",
          "libelle": "Accueillir le client à son arrivée au restaurant, l'installer à une table et lui présenter la carte"
        },
        {
          "code": "124015",
          "exigence": "S",
          "libelle": "Débarrasser une table"
        },
        {
          "code": "117933",
          "exigence": "S",
          "libelle": "Dresser les tables"
        },
        {
          "code": "124014",
          "exigence": "S",
          "libelle": "Nettoyer une salle de réception"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : vaness0306@hotmail.fr",
        "courriel": "vaness0306@hotmail.fr",
        "nom": "GACHET VANESSA - Mme Vanessa Gachet"
      },
      "dateCreation": "2019-11-13T13:03:45+01:00",
      "description": "Restaurant d'altitude, ouvert uniquement le midi\nPoste nourri, non logé\n",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Restaurant \"Chez Tartine\"",
        "nom": "GACHET VANESSA"
      },
      "experienceExige": "E",
      "experienceLibelle": "1 an",
      "formations": [],
      "id": "095WLCF",
      "intitule": "Serveur / Serveuse de restaurant",
      "langues": [],
      "lieuTravail": {
        "codePostal": "74120",
        "commune": "74099",
        "latitude": 45.85638889,
        "libelle": "74 - DEMI QUARTIER",
        "longitude": 6.617777777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLCF"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à garder le contrôle de soi pour agir efficacement face à des situations irritantes, imprévues et stressantes. Exemple : garder son calme",
          "libelle": "Gestion du stress"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "G1803",
      "romeLibelle": "Service en restauration",
      "salaire": {
        "commentaire": "selon compétences",
        "complement1": "selon compétences"
      },
      "secteurActivite": "56",
      "secteurActiviteLibelle": "Restauration traditionnelle",
      "trancheEffectifEtab": "Non défini",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 3 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Chef d'équipe de maintenance en froid et climatisation",
      "competences": [
        {
          "code": "124394",
          "exigence": "S",
          "libelle": "Corriger un dysfonctionnement"
        },
        {
          "code": "106743",
          "exigence": "S",
          "libelle": "Démarrer l'installation, ajuster les réglages (paramètres de fonctionnement, de régulation, températures, pressions, ...)"
        },
        {
          "code": "124395",
          "exigence": "S",
          "libelle": "Déterminer l'opération de remise en état d'une installation"
        },
        {
          "code": "106740",
          "exigence": "S",
          "libelle": "Positionner et fixer les groupes, condenseurs, tubes, câbles électriques, ... de l'installation frigorifique, de conditionnement d'air, ..."
        },
        {
          "code": "106742",
          "exigence": "S",
          "libelle": "Vérifier la conformité de l'installation, contrôler l'étanchéité et déterminer la charge de fluide frigorigène, tirer au vide l'installation et charger le circuit"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : strasbourg@samsic-emploi.fr",
        "courriel": "strasbourg@samsic-emploi.fr",
        "nom": "SAMSIC INTERIM ALSACE - Mme Myriam KALUZINSKI"
      },
      "dateCreation": "2019-11-13T13:03:44+01:00",
      "deplacementCode": "3",
      "deplacementLibelle": "Fréquents Départemental",
      "description": "Pour le compte de notre client, leader en Alsace dans le domaine du chauffage, du sanitaire, de la climatisation, nous recrutons :  un Responsable Équipe Maintenance CVC H/F - Secteur 67\n\nVos missions : au sein d'une agence du groupe et en étroite collaboration avec la direction d'agence, les services supports du groupe et les forces en présence sur l'agence,\n-  Supervision des opérations d'entretien préventifs et curatifs : préparation des plannings, sélection sous-traitants,\napprovisionnement, suivi humain, technique, administratif, financier ainsi que la rentabilité à l'exécution des prestations d'entretien et maintenance d'équipements selon cahiers des charges,\n- Management , accompagnement et encadrement d'une équipe de techniciens de maintenance composée de 10 personnes,\n- Pilotage des états d'avancement des visites d'entretiens préventives et curatives, réalisation des devis de réparations et d'améliorations.\n\nSalaire motivant, véhicule, téléphone, ticket restaurant, ",
      "dureeTravailLibelle": "38H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Entreprise spécialisée dans le recrutement depuis 1992, SAMSIC EMPLOI est un acteur incontournable en Alsace pour tous les besoins en intérim, CDD - CDI.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/k0LqjWBcBixor9lhGKJuMf4eiBLmvBMw.png",
        "nom": "SAMSIC INTERIM ALSACE",
        "url": "https://www.samsic-emploi.fr"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "E",
          "niveauLibelle": "Bac+2 ou équivalents"
        }
      ],
      "id": "095WLCD",
      "intitule": "Responsable Équipe maintenance en froid et climatisation (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "67000",
        "commune": "67482",
        "latitude": 48.57333333,
        "libelle": "67 - STRASBOURG",
        "longitude": 7.752222222
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLCD"
      },
      "qualificationCode": "9",
      "qualificationLibelle": "Cadre",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à être proactif, à initier, à imaginer des propositions nouvelles pour résoudre les problèmes identifiés ou pour améliorer une situation. Exemple : proposer des améliorations, être positif et constructif",
          "libelle": "Force de proposition"
        }
      ],
      "romeCode": "I1306",
      "romeLibelle": "Installation et maintenance en froid, conditionnement d'air",
      "salaire": {
        "commentaire": "SELON PROFIL",
        "complement1": "Véhicule",
        "complement2": "SELON PROFIL"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "1 ou 2 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Chef d'équipe de maintenance en froid et climatisation",
      "competences": [
        {
          "code": "124394",
          "exigence": "S",
          "libelle": "Corriger un dysfonctionnement"
        },
        {
          "code": "106743",
          "exigence": "S",
          "libelle": "Démarrer l'installation, ajuster les réglages (paramètres de fonctionnement, de régulation, températures, pressions, ...)"
        },
        {
          "code": "124395",
          "exigence": "S",
          "libelle": "Déterminer l'opération de remise en état d'une installation"
        },
        {
          "code": "106740",
          "exigence": "S",
          "libelle": "Positionner et fixer les groupes, condenseurs, tubes, câbles électriques, ... de l'installation frigorifique, de conditionnement d'air, ..."
        },
        {
          "code": "106742",
          "exigence": "S",
          "libelle": "Vérifier la conformité de l'installation, contrôler l'étanchéité et déterminer la charge de fluide frigorigène, tirer au vide l'installation et charger le circuit"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : strasbourg@samsic-emploi.fr",
        "courriel": "strasbourg@samsic-emploi.fr",
        "nom": "SAMSIC INTERIM ALSACE - Mme Myriam KALUZINSKI"
      },
      "dateCreation": "2019-11-13T13:03:44+01:00",
      "deplacementCode": "3",
      "deplacementLibelle": "Fréquents Départemental",
      "description": "Pour le compte de notre client, leader en Alsace dans le domaine du chauffage, du sanitaire, de la climatisation, nous recrutons :  un Responsable Équipe Maintenance CVC H/F - Secteur 67\n\nVos missions : au sein d'une agence du groupe et en étroite collaboration avec la direction d'agence, les services supports du groupe et les forces en présence sur l'agence,\n-  Supervision des opérations d'entretien préventifs et curatifs : préparation des plannings, sélection sous-traitants,\napprovisionnement, suivi humain, technique, administratif, financier ainsi que la rentabilité à l'exécution des prestations d'entretien et maintenance d'équipements selon cahiers des charges,\n- Management , accompagnement et encadrement d'une équipe de techniciens de maintenance composée de 10 personnes,\n- Pilotage des états d'avancement des visites d'entretiens préventives et curatives, réalisation des devis de réparations et d'améliorations.\n\nSalaire motivant, véhicule, téléphone, ticket restaurant, ",
      "dureeTravailLibelle": "38H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Entreprise spécialisée dans le recrutement depuis 1992, SAMSIC EMPLOI est un acteur incontournable en Alsace pour tous les besoins en intérim, CDD - CDI.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/k0LqjWBcBixor9lhGKJuMf4eiBLmvBMw.png",
        "nom": "SAMSIC INTERIM ALSACE",
        "url": "https://www.samsic-emploi.fr"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "E",
          "niveauLibelle": "Bac+2 ou équivalents"
        }
      ],
      "id": "095WLCC",
      "intitule": "Responsable Équipe maintenance en froid et climatisation (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "67000",
        "commune": "67482",
        "latitude": 48.57333333,
        "libelle": "67 - STRASBOURG",
        "longitude": 7.752222222
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLCC"
      },
      "qualificationCode": "9",
      "qualificationLibelle": "Cadre",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à être proactif, à initier, à imaginer des propositions nouvelles pour résoudre les problèmes identifiés ou pour améliorer une situation. Exemple : proposer des améliorations, être positif et constructif",
          "libelle": "Force de proposition"
        }
      ],
      "romeCode": "I1306",
      "romeLibelle": "Installation et maintenance en froid, conditionnement d'air",
      "salaire": {
        "commentaire": "SELON PROFIL",
        "complement1": "Véhicule",
        "complement2": "SELON PROFIL"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "1 ou 2 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Chef de chantier",
      "competences": [
        {
          "code": "118365",
          "exigence": "S",
          "libelle": "Affecter le personnel sur des postes de travail"
        },
        {
          "code": "117170",
          "exigence": "S",
          "libelle": "Coordonner les prestataires, fournisseurs, intervenants"
        },
        {
          "code": "120648",
          "exigence": "S",
          "libelle": "Planifier l'activité du personnel"
        },
        {
          "code": "123640",
          "exigence": "S",
          "libelle": "Présenter le chantier à un intervenant"
        },
        {
          "code": "118772",
          "exigence": "S",
          "libelle": "Suivre l'état d'avancement des travaux jusqu'à réception"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : audrey.ligas@veolia.com",
        "courriel": "audrey.ligas@veolia.com",
        "nom": "ASTERALIS - Mme AUDREY LIGAS"
      },
      "dateCreation": "2019-11-13T13:03:17+01:00",
      "description": "Missions :\nSous la responsabilité du Chargé d'Affaires, vos missions principales seront les suivantes:\n- Assure le suivi des opérations de manutentions et de transferts des ensembles mécaniques,\n- S'assure de la bonne application des différents modes opératoires, lors des démontages\n- S'assure du bon contrôle les pièces et du renseignement les formulaires de suivi,\n- Gère le planning d'activité du chantier, anticipe et planifie les taches \n- Renseigne et remonte les différents indicateurs nécessaires au bon fonctionnement du chantier (feuille de présence du personnel, cahier de chantier, tableau de bord  )\n- Gère les demandes d'achat (recherche les fournisseurs, demande les devis, renseigne les demandes d'achat, réceptionne  )\n\nProfil :\n- Formation initiale : Bac+2 Mécanique ou Electromécanique\n- Respecter les règles et les consignes de sécurité, sûreté et radioprotection,\n- CACES et-ou PR1 CC sont des plus",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Asteralis, filiale du groupe Veolia, accompagne ses clients de l'industrie nucléaire de l'assainissement et démantèlement à la gestion des déchets radioactifs. Acteur indépendant de la filière nucléaire, Asteralis propose des services de mesures et analyses, ingénierie et études, chantiers d'assainissements et de démantèlement, exploitation en gestion déléguée.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/acae412d47a24f0284629acbf1a24384.png",
        "nom": "ASTERALIS",
        "url": "http://asteralis.veolia.com/"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WLBZ",
      "intitule": "Chef de chantier",
      "langues": [],
      "lieuTravail": {
        "libelle": "Centre-Val de Loire"
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLBZ"
      },
      "qualificationCode": "8",
      "qualificationLibelle": "Agent de maîtrise",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à faire des choix pour agir, à prendre en charge son activité et à rendre compte, sans devoir être encadré de façon continue. Exemple : savoir faire des choix",
          "libelle": "Capacité de décision"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        },
        {
          "description": "Capacité à être proactif, à initier, à imaginer des propositions nouvelles pour résoudre les problèmes identifiés ou pour améliorer une situation. Exemple : proposer des améliorations, être positif et constructif",
          "libelle": "Force de proposition"
        }
      ],
      "romeCode": "F1202",
      "romeLibelle": "Direction de chantier du BTP",
      "salaire": {
        "libelle": "Annuel de 36000,00 Euros à 39000,00 Euros sur 13 mois"
      },
      "secteurActivite": "71",
      "secteurActiviteLibelle": "Analyses, essais et inspections techniques",
      "trancheEffectifEtab": "100 à 199 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Réceptionniste en hôtellerie",
      "competences": [
        {
          "code": "104174",
          "exigence": "S",
          "libelle": "Accueillir les clients à leur arrivée et effectuer les formalités administratives liées à leur séjour"
        },
        {
          "code": "104292",
          "exigence": "S",
          "libelle": "Enregistrer les réservations et actualiser le planning d'occupation des chambres, des appartements, des bungalows, ..."
        },
        {
          "code": "117666",
          "exigence": "S",
          "libelle": "Établir une facture"
        },
        {
          "code": "104173",
          "exigence": "S",
          "libelle": "Renseigner le client sur les prestations de l'établissement et les modalités de réservation"
        },
        {
          "code": "104293",
          "exigence": "S",
          "libelle": "Répondre aux demandes des clients durant leur séjour"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : msouaf@leshotelsdeparis.com",
        "courriel": "msouaf@leshotelsdeparis.com",
        "nom": "VILLA ROYALE**** - Mme Villa Royale Souaf"
      },
      "dateCreation": "2019-11-13T13:02:56+01:00",
      "description": "Pour un hôtel 4* de 32 chambres dans le 9 ème à Paris , vous serez en charge de l'accueil des clients, les renseigner du check-in au check-out, gestion des extranets, envoi des débiteurs, contrôle journalier de la caisse, clôture. Cycle de travail sur 15 jours : 4 shifts du matin 7h à 15h20, 4 shifts du soir de 12h10 à 20h30. La maîtrise du logiciel MEWS est un plus. Maîtrise de l'anglais indispensable plus espagnol ou italien.",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Hôtel 4 étoiles d'un groupe hôtelier parisien de 24 hôtels principalement situés à Paris",
        "nom": "VILLA ROYALE****"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WLBY",
      "intitule": "Réceptionniste en hôtellerie",
      "langues": [],
      "lieuTravail": {
        "codePostal": "75009",
        "commune": "75109",
        "latitude": 48.87833333,
        "libelle": "75 - PARIS 09",
        "longitude": 2.336944444
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLBY"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "G1703",
      "romeLibelle": "Réception en hôtellerie",
      "salaire": {
        "libelle": "Horaire de 10,03 Euros sur 12 mois"
      },
      "secteurActivite": "55",
      "secteurActiviteLibelle": "Hôtels et hébergement similaire",
      "trancheEffectifEtab": "10 à 19 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "SPA Praticien / Praticienne",
      "competences": [
        {
          "code": "102050",
          "exigence": "E",
          "libelle": "Appliquer les produits (masque, sérum, crème) et réaliser les soins du visage, du corps, de manucure ou de maquillage"
        },
        {
          "code": "102048",
          "exigence": "E",
          "libelle": "Déterminer le traitement adapté à la personne et présenter les produits, l'application des soins"
        },
        {
          "code": "124548",
          "exigence": "E",
          "libelle": "Entretenir un poste de travail"
        },
        {
          "code": "114145",
          "exigence": "E",
          "libelle": "Installer le client et surveiller la conformité de déroulement de soins machines, bains, spa, séances d'UV, de sauna, de hammam"
        },
        {
          "code": "121698",
          "exigence": "E",
          "libelle": "Proposer un service, produit adapté à la demande client"
        },
        {
          "code": "102059",
          "exigence": "E",
          "libelle": "Réaliser des soins du corps"
        },
        {
          "code": "102060",
          "exigence": "E",
          "libelle": "Réaliser des soins du visage"
        },
        {
          "code": "120130",
          "exigence": "S",
          "libelle": "Accueillir une clientèle"
        },
        {
          "code": "126129",
          "exigence": "S",
          "libelle": "Conseiller un client sur la mise en valeur de son image personnelle (apparence, personnalité, ...)"
        },
        {
          "code": "113810",
          "exigence": "S",
          "libelle": "Présenter et expliquer des techniques d'esthétique et de maquillage (démonstration, ...) à des clients"
        },
        {
          "code": "120138",
          "exigence": "S",
          "libelle": "Réaliser des maquillages"
        },
        {
          "code": "102054",
          "exigence": "S",
          "libelle": "Réaliser des soins d'épilation"
        },
        {
          "code": "102057",
          "exigence": "S",
          "libelle": "Réaliser des soins de pose de faux-cils, bijoux de peau, tatouages temporaires"
        },
        {
          "code": "102058",
          "exigence": "S",
          "libelle": "Réaliser des soins des mains, manucure, beauté des pieds"
        },
        {
          "code": "102061",
          "exigence": "S",
          "libelle": "Réaliser des tatouages permanents"
        },
        {
          "code": "102063",
          "exigence": "S",
          "libelle": "Utilisation de dermographe"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : julie.gruzza@gmail.com",
        "courriel": "julie.gruzza@gmail.com",
        "nom": "L UNIVERS DU SPA ET DE LA BEAUTE - Mme Julie Gruzza"
      },
      "dateCreation": "2019-11-13T13:02:45+01:00",
      "description": "Vous devez effectuer des massages à une clientèle mixte ainsi que :\ndes soins d'épilations, \ndes soins visages, \nde l'onglerie, \npour un contrat de 20H au démarrage allant sur un temps plein 35h après les 2 mois d'essai.\nplage amplitude maximale : horaire 10h 19h\nEtablissement de soins esthetiques avec SPA ( massage, hammam, espace balneo + gommage corps ...°   \nSalaire selon convention collective et diplômes ",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "L UNIVERS DU SPA ET DE LA BEAUTE"
      },
      "experienceExige": "E",
      "experienceLibelle": "1 an",
      "formations": [
        {
          "codeFormation": "42020",
          "commentaire": "BP ou CQP ou expérience massage",
          "domaineLibelle": "Massage esthétique",
          "exigence": "S",
          "niveauLibelle": "Bac ou équivalent"
        },
        {
          "codeFormation": "42032",
          "domaineLibelle": "Esthétique soin corporel",
          "exigence": "E",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "095WLBX",
      "intitule": "SPA Praticien / Praticienne H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "13400",
        "commune": "13005",
        "latitude": 43.29083333,
        "libelle": "13 - AUBAGNE",
        "longitude": 5.570833332
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLBX"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à s’adapter à des situations variées et à s’ajuster à des organisations, des collectifs de travail, des habitudes et des valeurs propres à l’entreprise. Exemple : être souple, agile, s’adapter à une situation imprévue",
          "libelle": "Capacité d’adaptation"
        },
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        }
      ],
      "romeCode": "D1208",
      "romeLibelle": "Soins esthétiques et corporels",
      "salaire": {
        "complement1": "Chèque repas",
        "complement2": "Mutuelle"
      },
      "secteurActivite": "70",
      "secteurActiviteLibelle": "Activités des sièges sociaux",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Débardeur forestier / Débardeuse forestière",
      "competences": [
        {
          "code": "100003",
          "exigence": "S",
          "libelle": "Conduire les engins agricoles vers le lieu de production (champs, forêt), de stockage, de livraison (fermes, coopératives)"
        },
        {
          "code": "117489",
          "exigence": "S",
          "libelle": "Débarder une grume"
        },
        {
          "code": "117492",
          "exigence": "S",
          "libelle": "Empiler des grumes"
        },
        {
          "code": "122704",
          "exigence": "S",
          "libelle": "Identifier le type d'intervention"
        },
        {
          "code": "116766",
          "exigence": "S",
          "libelle": "Procédures de maintenance de locaux"
        },
        {
          "code": "116765",
          "exigence": "S",
          "libelle": "Procédures de maintenance de matériel"
        },
        {
          "code": "116663",
          "exigence": "S",
          "libelle": "Réaliser l'entretien du matériel"
        }
      ],
      "contact": {
        "coordonnees1": "Tél. : 0558781655",
        "nom": "TRAVAUX FORESTIERS DUBERN - Mme DUSSOUL CLARISSE",
        "telephone": "0558781655"
      },
      "dateCreation": "2019-11-13T13:02:36+01:00",
      "description": "Vous utiliserez des machines récentes, vous devez être autonome. \nChantiers dans les landes, déplacements en fourgon de chantier. ",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "TRAVAUX FORESTIERS DUBERN"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WLBV",
      "intitule": "Porteur forestier (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "40115",
        "commune": "40046",
        "latitude": 44.39305556,
        "libelle": "40 - BISCARROSSE-LANDES",
        "longitude": -1.163888888
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLBV"
      },
      "permis": [
        {
          "exigence": "E",
          "libelle": "B - Véhicule léger Exigé"
        }
      ],
      "qualificationCode": "2",
      "qualificationLibelle": "Ouvrier spécialisé",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        }
      ],
      "romeCode": "A1101",
      "romeLibelle": "Conduite d'engins agricoles et forestiers",
      "salaire": {
        "commentaire": "Selon expérience",
        "complement1": "Selon expérience",
        "libelle": "Horaire de 1003 Euros"
      },
      "secteurActivite": "02",
      "secteurActiviteLibelle": "Services de soutien à l'exploitation forestière",
      "trancheEffectifEtab": "6 à 9 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Maçon / Maçonne",
      "competences": [
        {
          "code": "121482",
          "exigence": "S",
          "libelle": "Appliquer les mortiers"
        },
        {
          "code": "121517",
          "exigence": "S",
          "libelle": "Assembler des éléments d'armature de béton"
        },
        {
          "code": "122952",
          "exigence": "S",
          "libelle": "Monter les murs par maçonnage d'éléments portés"
        },
        {
          "code": "123667",
          "exigence": "S",
          "libelle": "Réaliser des enduits"
        },
        {
          "code": "103767",
          "exigence": "S",
          "libelle": "Terrasser et niveler la fondation"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : rosporden@triangle.fr",
        "courriel": "rosporden@triangle.fr",
        "nom": "CORNOUAILLE INTERIM - Mme MARY QUEVAT"
      },
      "dateCreation": "2019-11-13T13:02:31+01:00",
      "description": "Votre agence Cornouaille Intérim à Rosporden recrute pour son client basé à Concarneau, un Maçon N2 minimum H/F.\n\n \n\nVous serez chargé(e) de :\n\n \n\n- poser l''agglos,\n\n- réaliser des coffrages et des planchers,\n\n- la finition.\n\n \n\n39h semaine\n\nVéhicule de société selon niveau\n\nPanier repas\n\nLong terme enviseagable",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "La société Cornouaille Intérim groupe TRIANGLE INTÉRIM, entreprise à taille humaine classée parmi les 10 sociétés leaders de l'intérim en France, regroupe aujourd'hui un réseau d'une centaine d'agences réparties sur tout le territoire et emploie plus de 450 permanents et près de 15 000 intérimaires par jour.\nRejoindre Triangle, c'est bénéficier, sous conditions d'ancienneté, de ces différents avantages : C.E, participation, mutuelle et autres avantages sociaux (FASTT), accès à la formation",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/b7576c25d67842189eb5f1bedcc89e8d.png",
        "nom": "CORNOUAILLE INTERIM",
        "url": "https://www.triangle.fr/"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WLBS",
      "intitule": "Maçon / Maçonne",
      "langues": [],
      "lieuTravail": {
        "codePostal": "29181",
        "commune": "29039",
        "latitude": 47.87527778,
        "libelle": "29 - CONCARNEAU",
        "longitude": -3.918888888
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLBS"
      },
      "qualificationCode": "3",
      "qualificationLibelle": "Ouvrier qualifié (P1,P2)",
      "romeCode": "F1703",
      "romeLibelle": "Maçonnerie",
      "salaire": {
        "libelle": "Horaire de 10,55 Euros à 12,67 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "3 à 5 salariés",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 6 Mois"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "ape.56065@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Collaborateur / Collaboratrice d'architecte",
      "competences": [
        {
          "code": "118975",
          "exigence": "S",
          "libelle": "Outils bureautiques"
        },
        {
          "exigence": "S",
          "libelle": "Connaissances en bâtiment"
        },
        {
          "exigence": "S",
          "libelle": "gestion de planning"
        }
      ],
      "contact": {
        "coordonnees1": "7 RUE DE LA LIBERATION\nBP 70432",
        "coordonnees2": "56406 AURAY",
        "coordonnees3": "Courriel : ape.56065@pole-emploi.fr",
        "nom": "Pôle Emploi AURAY"
      },
      "dateCreation": "2019-11-13T13:01:43+01:00",
      "description": "Au sein d'un cabinet d'architecture d'intérieur et de design, vous assurez le passage des commandes et analysez les confirmations de commandes.\nVous assurez la planification des poses et le suivi de chantiers.\nDébutant/te accepté/e, une formation vous sera apportée.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "S",
          "niveauLibelle": "Bac ou équivalent"
        }
      ],
      "id": "095WLBL",
      "intitule": "Collaborateur / Collaboratrice d'architecte",
      "langues": [],
      "lieuTravail": {
        "codePostal": "56400",
        "commune": "56007",
        "latitude": 47.66777778,
        "libelle": "56 - AURAY",
        "longitude": -2.982499999
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLBL"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à s’adapter à des situations variées et à s’ajuster à des organisations, des collectifs de travail, des habitudes et des valeurs propres à l’entreprise. Exemple : être souple, agile, s’adapter à une situation imprévue",
          "libelle": "Capacité d’adaptation"
        },
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "F1104",
      "romeLibelle": "Dessin BTP et paysage",
      "salaire": {
        "libelle": "Horaire de 10,03 Euros"
      },
      "secteurActivite": "47",
      "secteurActiviteLibelle": "Commerce de détail de meubles",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Boulanger / Boulangère",
      "competences": [
        {
          "code": "124929",
          "exigence": "S",
          "libelle": "Boulage des pâtons"
        },
        {
          "code": "119368",
          "exigence": "S",
          "libelle": "Conduire une fermentation"
        },
        {
          "code": "124928",
          "exigence": "S",
          "libelle": "Diviser la masse de pâte en pâtons"
        },
        {
          "code": "101738",
          "exigence": "S",
          "libelle": "Sélectionner et doser les ingrédients (farine, levure, additifs, ...) pour la réalisation des pâtes à pain ou à viennoiserie, les mélanger et effectuer le pétrissage, l'abaisse"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : contact.dupaindesgateaux@orange.fr",
        "courriel": "contact.dupaindesgateaux@orange.fr",
        "nom": "L'AUTRE BOULANGERIE...DU PAIN, - M. CHRISTOPHE JEGOU"
      },
      "dateCreation": "2019-11-13T13:01:29+01:00",
      "description": "Au sein d'une équipe dynamique et en entreprise artisanale vous :\n- Préparez et réalisez des produits de boulangerie et de viennoiserie selon les règles d'hygiène et de sécurité alimentaires. \n- Pouvez effectuer la vente de produits de boulangerie, viennoiserie. \n- Pouvez gérer un commerce de détail alimentaire \n- Pouvez former les nouveaux équipiers.\n\nVous ne travaillez pas le lundi et pouvez négocier un salaire motivant en fonction de votre expérience.\nPoste à pourvoir dès que possible sur une base de 35h + 4 heures supplémentaires.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "L'AUTRE BOULANGERIE...DU PAIN,"
      },
      "experienceCommentaire": "en tant que boulanger",
      "experienceExige": "E",
      "experienceLibelle": "3 ans - en tant que boulanger",
      "formations": [
        {
          "codeFormation": "21538",
          "domaineLibelle": "Boulangerie",
          "exigence": "S",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "095WLBK",
      "intitule": "Boulanger / Boulangère",
      "langues": [],
      "lieuTravail": {
        "codePostal": "49240",
        "commune": "49015",
        "latitude": 47.50694444,
        "libelle": "49 - AVRILLE",
        "longitude": -0.588888888
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLBK"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "D1102",
      "romeLibelle": "Boulangerie - viennoiserie",
      "salaire": {
        "libelle": "Mensuel de 1521,22 Euros sur 12 mois"
      },
      "secteurActivite": "10",
      "secteurActiviteLibelle": "Boulangerie et boulangerie-pâtisserie",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "guillaume.micaelli@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Vendeur / Vendeuse en boulangerie-pâtisserie",
      "competences": [
        {
          "code": "120130",
          "exigence": "S",
          "libelle": "Accueillir une clientèle"
        },
        {
          "code": "102068",
          "exigence": "S",
          "libelle": "Encaisser le montant d'une vente"
        },
        {
          "code": "124545",
          "exigence": "S",
          "libelle": "Entretenir un espace de vente"
        },
        {
          "code": "124548",
          "exigence": "S",
          "libelle": "Entretenir un poste de travail"
        },
        {
          "code": "124547",
          "exigence": "S",
          "libelle": "Nettoyer du matériel ou un équipement"
        },
        {
          "code": "118366",
          "exigence": "S",
          "libelle": "Produits de boulangerie"
        },
        {
          "code": "121480",
          "exigence": "S",
          "libelle": "Produits de pâtisserie"
        },
        {
          "code": "121698",
          "exigence": "S",
          "libelle": "Proposer un service, produit adapté à la demande client"
        }
      ],
      "contact": {
        "coordonnees1": "batiment casaprom RUE marechal juin",
        "coordonnees2": "20407 BASTIA",
        "coordonnees3": "Courriel : guillaume.micaelli@pole-emploi.fr",
        "nom": "Pôle Emploi BASTIA"
      },
      "dateCreation": "2019-11-13T13:01:21+01:00",
      "description": "Vous assurerez la vente en boulangerie pâtisserie .\nmissions:\n-accueillir,conseiller et finaliser la vente de produits alimentaires \n-entretenir l espace vente et son poste de travail.\n",
      "dureeTravailLibelle": "30H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps partiel",
      "entreprise": {
        "nom": "L'EPICERIE DESIGN GOURMET"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WLBH",
      "intitule": "Vendeur / Vendeuse en boulangerie-pâtisserie",
      "langues": [],
      "lieuTravail": {
        "codePostal": "20200",
        "commune": "2B033",
        "latitude": 42.7,
        "libelle": "2B - BASTIA",
        "longitude": 9.449444444
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLBH"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "D1106",
      "romeLibelle": "Vente en alimentation",
      "salaire": {
        "commentaire": "possibilite h.s",
        "complement1": "possibilite h.s",
        "libelle": "Horaire de 10,23 Euros sur 12 mois"
      },
      "secteurActivite": "10",
      "secteurActiviteLibelle": "Boulangerie et boulangerie-pâtisserie",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 3 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Maçon / Maçonne",
      "competences": [
        {
          "code": "103070",
          "exigence": "S",
          "libelle": "Lecture de plan, de schéma"
        },
        {
          "code": "122952",
          "exigence": "S",
          "libelle": "Monter les murs par maçonnage d'éléments portés"
        },
        {
          "code": "103315",
          "exigence": "S",
          "libelle": "Prise d'aplomb et de niveau"
        },
        {
          "code": "123667",
          "exigence": "S",
          "libelle": "Réaliser des enduits"
        },
        {
          "code": "100596",
          "exigence": "S",
          "libelle": "Techniques de maçonnerie"
        }
      ],
      "contact": {
        "commentaire": "ou téléphoner au 06.01.00.65.90",
        "coordonnees1": "Courriel : dollet.raphael@sfr.fr",
        "courriel": "dollet.raphael@sfr.fr",
        "nom": "DOLLET RAPHAEL - M. Raphael DOLLET"
      },
      "dateCreation": "2019-11-13T13:01:17+01:00",
      "deplacementCode": "3",
      "deplacementLibelle": "Fréquents Départemental",
      "description": "Vous travaillerez sur des chantiers de rénovation (maçonnerie traditionnelle, carrelage...), principalement chez des particuliers.\nVous travaillerez sur les chantiers avec l'artisan, mais pourrez aussi travailler seul sur les chantiers.\nVous avez une première expérience en maçonnerie. Vous réalisez du travail soigné. ",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "DOLLET RAPHAEL"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [
        {
          "codeFormation": "22334",
          "domaineLibelle": "Maçonnerie",
          "exigence": "S",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "095WLBG",
      "intitule": "Maçon / Maçonne",
      "langues": [],
      "lieuTravail": {
        "codePostal": "27940",
        "commune": "27473",
        "latitude": 49.16444444,
        "libelle": "27 - PORT MORT + chantiers",
        "longitude": 1.415
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLBG"
      },
      "permis": [
        {
          "exigence": "E",
          "libelle": "B - Véhicule léger Exigé"
        }
      ],
      "qualificationCode": "3",
      "qualificationLibelle": "Ouvrier qualifié (P1,P2)",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à s’adapter à des situations variées et à s’ajuster à des organisations, des collectifs de travail, des habitudes et des valeurs propres à l’entreprise. Exemple : être souple, agile, s’adapter à une situation imprévue",
          "libelle": "Capacité d’adaptation"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "F1703",
      "romeLibelle": "Maçonnerie",
      "salaire": {
        "commentaire": "Selon qualification",
        "complement1": "Selon qualification"
      },
      "secteurActivite": "43",
      "secteurActiviteLibelle": "Travaux de maçonnerie générale et gros oeuvre de bâtiment",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 6 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Couvreur / Couvreuse",
      "competences": [
        {
          "code": "122128",
          "exigence": "E",
          "libelle": "Déposer une toiture"
        },
        {
          "code": "103655",
          "exigence": "E",
          "libelle": "Poser des ardoises"
        },
        {
          "code": "122208",
          "exigence": "E",
          "libelle": "Poser des tuiles"
        },
        {
          "code": "103797",
          "exigence": "E",
          "libelle": "Réaliser la pose d'éléments de couverture"
        },
        {
          "code": "122129",
          "exigence": "E",
          "libelle": "Remplacer des chevrons"
        }
      ],
      "dateCreation": "2019-11-13T13:01:12+01:00",
      "description": "PME spécialisée en couverture-zinguerie recherche un couvreur zingueur autonome et motivé, travaux en rénovation, tuiles, ardoises, zinc, bardage, isolation...\n35h - Paniers repas + mutuelle\n\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "CHF - CENTRE DE L'HABITAT FRANCAIS"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [
        {
          "codeFormation": "22454",
          "domaineLibelle": "Bâtiment second oeuvre",
          "exigence": "S",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "095WLBD",
      "intitule": "Couvreur / Couvreuse",
      "langues": [],
      "lieuTravail": {
        "codePostal": "44000",
        "commune": "44109",
        "latitude": 47.21722222,
        "libelle": "44 - NANTES",
        "longitude": -1.553888888
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WLBD"
      },
      "qualificationCode": "2",
      "qualificationLibelle": "Ouvrier spécialisé",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "F1610",
      "romeLibelle": "Pose et restauration de couvertures",
      "salaire": {
        "libelle": "Mensuel de 2400,00 Euros à 3000,00 Euros sur 12 mois"
      },
      "secteurActivite": "43",
      "secteurActiviteLibelle": "Travaux de couverture par éléments",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "entreprise.frc0053@pole-emploi.net"
      },
      "alternance": false,
      "appellationlibelle": "Serveur / Serveuse de restaurant",
      "competences": [
        {
          "code": "104323",
          "exigence": "S",
          "libelle": "Accueillir le client à son arrivée au restaurant, l'installer à une table et lui présenter la carte"
        },
        {
          "code": "124015",
          "exigence": "S",
          "libelle": "Débarrasser une table"
        },
        {
          "code": "117933",
          "exigence": "S",
          "libelle": "Dresser les tables"
        },
        {
          "code": "124014",
          "exigence": "S",
          "libelle": "Nettoyer une salle de réception"
        },
        {
          "code": "117932",
          "exigence": "S",
          "libelle": "Réaliser la mise en place de la salle et de l'office"
        }
      ],
      "complementExercice": "Possibilité de temps partiel",
      "contact": {
        "coordonnees1": "1 RUE Aimé Césaire\nCS 15169",
        "coordonnees2": "25405 AUDINCOURT",
        "coordonnees3": "Courriel : entreprise.frc0053@pole-emploi.net",
        "nom": "Pôle Emploi AUDINCOURT"
      },
      "dateCreation": "2019-11-13T13:00:41+01:00",
      "description": "Vous assistez le chef de rang lors du service, assurez  la prise de commande, ainsi que la propreté du restaurant vous veillez au confort et à la satisfaction du client.\nVous êtes  souriant, volontaire, motivé, dynamique et doté d'un bon esprit d'équipe.\nPossibilité de temps partiel.\n\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "LA BOUCHERIE"
      },
      "experienceCommentaire": "en service",
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté - en service",
      "formations": [],
      "id": "095WKZZ",
      "intitule": "Serveur / Serveuse de restaurant H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "25400",
        "commune": "25230",
        "latitude": 47.4975,
        "libelle": "25 - EXINCOURT",
        "longitude": 6.833055554
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZZ"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à s’adapter à des situations variées et à s’ajuster à des organisations, des collectifs de travail, des habitudes et des valeurs propres à l’entreprise. Exemple : être souple, agile, s’adapter à une situation imprévue",
          "libelle": "Capacité d’adaptation"
        },
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "G1803",
      "romeLibelle": "Service en restauration",
      "salaire": {
        "libelle": "Horaire de 10,18 Euros sur 12 mois"
      },
      "secteurActivite": "68",
      "secteurActiviteLibelle": "Location de terrains et d'autres biens immobiliers",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Aide-soignant / Aide-soignante",
      "competences": [
        {
          "code": "107662",
          "exigence": "E",
          "libelle": "Accompagner la personne dans les gestes de la vie quotidienne"
        },
        {
          "code": "107660",
          "exigence": "E",
          "libelle": "Mesurer les paramètres vitaux du patient/résident, contrôler les dispositifs et appareillages médicaux et transmettre les informations à l'infirmier"
        },
        {
          "code": "107661",
          "exigence": "E",
          "libelle": "Réaliser des soins d'hygiène corporelle, de confort et de prévention"
        },
        {
          "code": "116410",
          "exigence": "E",
          "libelle": "Repérer les modifications d'état du patient"
        },
        {
          "code": "107659",
          "exigence": "E",
          "libelle": "Surveiller l'état général du patient/résident, lui distribuer les médicaments et informer l'infirmier des manifestations anormales ou des risques de chutes, escarres, ..."
        },
        {
          "code": "107666",
          "exigence": "S",
          "libelle": "Manipulation d'équipement (lit médicalisé, lève malade, ...)"
        },
        {
          "code": "107667",
          "exigence": "S",
          "libelle": "Techniques de manipulation de patient"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : recrutement@amreso-bethel.fr",
        "courriel": "recrutement@amreso-bethel.fr",
        "nom": "ASS AMIS MAIS RETR SOINS OBERHAUSBERGE - Mme Emilie RETAILLEAU"
      },
      "dateCreation": "2019-11-13T13:00:24+01:00",
      "description": "Dans le cadre des remplacements de congés payés, nous recherchons des infirmier(e)s de juin à septembre 2019 pour chaque secteur d'activités (Pôle Gériatrie, Pôle Handicap, Pôle Oncologie).Vous aurez à prendre soin de personnes âgées dépendantes présentant une prise en charge diversifiée :\n- Contribuer à une prise en charge globale des personnes en lien avec les autres intervenants au sein d'une équipe pluridisciplinaire\n- Dispenser au quotidien, en collaboration et sous la responsabilité de l'infirmier, les soins visant à solliciter et stimuler les patients afin de conserver ou développer leur autonomie dans les gestes de la vie courante (lors de la toilette, de l'habillage, de la prise des repas, des déplacements )\n- Accueillir, informer et accompagner les familles\n- Favoriser et effectuer les transmissions ciblées (données, actions, résultats) et orales au sein de l'équipe\n\n",
      "dureeTravailLibelle": "35H Travail samedi et dimanche",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "La Maison de Santé Béthel, maison médicalisée située à Oberhausbergen, a pour mission d'accueillir et d'accompagner des personnes fragilisées tout au long de leur projet de vie et de leur parcours de soins. Plus de 300 collaborateurs passionnés et investis œuvrent au quotidien pour le bien-être de nos 333 patients et résidents. L'établissement est constitué en 4 Pôles d'activités : Pôle Personnes Âgées Dépendantes / EHPAD, Pôle SSR à orientation oncologique, Pôle Long Séjour, et Pôle Handicap.",
        "nom": "ASS AMIS MAIS RETR SOINS OBERHAUSBERGE",
        "url": "https://www.amreso-bethel.fr/recrutement"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "44004",
          "commentaire": "Aide soignant ou AMP",
          "domaineLibelle": "Aide médico-psychologique",
          "exigence": "E",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "095WKZY",
      "intitule": "Aide-soignant / Aide-soignante en EHPAD H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "67205",
        "commune": "67343",
        "latitude": 48.60666667,
        "libelle": "67 - OBERHAUSBERGEN",
        "longitude": 7.685277777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZY"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "J1501",
      "romeLibelle": "Soins d'hygiène, de confort du patient",
      "salaire": {
        "commentaire": "Selon grille FEHAP CCN51",
        "complement1": "Selon grille FEHAP CCN51"
      },
      "secteurActivite": "86",
      "secteurActiviteLibelle": "Activités hospitalières",
      "trancheEffectifEtab": "250 à 499 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "ape.32002@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Animateur / Animatrice de loisirs pour enfants",
      "competences": [
        {
          "code": "103885",
          "exigence": "S",
          "libelle": "Gestes d'urgence et de secours"
        },
        {
          "code": "103909",
          "exigence": "S",
          "libelle": "Informer les parents sur l'organisation de la structure et présenter le programme des activités aux enfants"
        },
        {
          "code": "103910",
          "exigence": "S",
          "libelle": "Organiser ou adapter la séance d'animation selon le déroulement de la journée"
        },
        {
          "code": "103911",
          "exigence": "S",
          "libelle": "Préparer l'espace d'animation et guider les participants tout au long de la séance"
        },
        {
          "code": "103922",
          "exigence": "S",
          "libelle": "Réaliser des interventions nécessitant une habilitation BAFA"
        },
        {
          "code": "100144",
          "exigence": "S",
          "libelle": "Règles d'hygiène et de sécurité"
        },
        {
          "code": "103913",
          "exigence": "S",
          "libelle": "Repérer les difficultés ou problèmes d'un enfant, intervenir ou informer le directeur, les parents"
        },
        {
          "code": "103912",
          "exigence": "S",
          "libelle": "Surveiller le déroulement de l'activité et veiller au respect des consignes de jeux, des règles de vie sociale"
        },
        {
          "code": "103918",
          "exigence": "S",
          "libelle": "Techniques d'éveil de l'enfant"
        },
        {
          "code": "103919",
          "exigence": "S",
          "libelle": "Techniques d'expression corporelle"
        },
        {
          "code": "100090",
          "exigence": "S",
          "libelle": "Techniques pédagogiques"
        }
      ],
      "complementExercice": "Temps annualisé.",
      "contact": {
        "coordonnees1": "1 BIS BOULEVARD POUMADERES",
        "coordonnees2": "32600 L ISLE JOURDAIN",
        "coordonnees3": "Courriel : ape.32002@pole-emploi.fr",
        "nom": "Pôle Emploi L'ISLE JOURDAIN"
      },
      "dateCreation": "2019-11-13T13:00:23+01:00",
      "description": "Sous la responsabilité du directeur de la structure, l'animateur H/F a pour mission :\n\n* Participer à l'élaboration et à la mise en oeuvre du projet pédagogique de la structure,\n* Assurer et animer l'accueil des enfants pendant le temps péri et extra-scolaire dans le cadre fixé par le projet pédagogique,\n* Mettre en oeuvre les projets d'activités tout en respectant le cadre sécuritaire et règlementaire,\n* Planifier et organiser les temps d'activités,\n* Assurer l'intégrité physique, psychologique et morale de l'enfant,\n* Assurer et contrôler l'hygiène des enfants et de la structure,\n\nVous intervenez essentiellement sur la pause méridienne de 12h à 14h avec possibilité d'accroissement d'horaires sur des remplacements et périodes de vacances scolaires.\nSession de formation pour l'obtention du BAFA en février possible et financée par la structure.",
      "dureeTravailLibelle": "7H20 Horaires annuels",
      "dureeTravailLibelleConverti": "Temps partiel",
      "entreprise": {
        "description": "Communauté de Communes de la Gascogne Toulousaine.",
        "nom": "COMMUNAUTE COMMUNES GASCOGNE TOULOUSAINE"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "44041",
          "commentaire": "BAFA / CQPou équiv.fortement appréci",
          "domaineLibelle": "Petite enfance",
          "exigence": "E",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "095WKZX",
      "intitule": "UN ANIMATEUR ALAE/ALSH  H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "32600",
        "commune": "32160",
        "latitude": 43.61361111,
        "libelle": "32 - L ISLE JOURDAIN",
        "longitude": 1.080833332
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZX"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à mobiliser une équipe e/ou des interlocuteurs et à les entrainer dans la poursuite d’objectifs partagés. Exemple : être un leader, rassembler",
          "libelle": "Capacité à fédérer"
        },
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "G1203",
      "romeLibelle": "Animation de loisirs auprès d'enfants ou d'adolescents",
      "salaire": {
        "libelle": "Horaire de 10,03 Euros"
      },
      "secteurActivite": "88",
      "secteurActiviteLibelle": "Accueil de jeunes enfants",
      "trancheEffectifEtab": "20 à 49 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 8 Mois"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "ape.78010@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Opérateur / Opératrice en télésurveillance",
      "competences": [
        {
          "code": "108842",
          "exigence": "E",
          "libelle": "Contrôler la conformité d'entrée et de sortie de personnes et de biens"
        },
        {
          "code": "108846",
          "exigence": "E",
          "libelle": "Renseigner les supports d'intervention et d'activité (rapports, mains courantes, registres de sécurité, déclaration, ...)"
        },
        {
          "code": "108844",
          "exigence": "E",
          "libelle": "Repérer les anomalies, incidents et informer les forces de l'ordre et les clients"
        },
        {
          "code": "104288",
          "exigence": "E",
          "libelle": "Surveiller les lieux, les biens et effectuer des rondes de prévention et de détection de risques"
        },
        {
          "code": "108843",
          "exigence": "S",
          "libelle": "Vérifier les accès, les lieux (fermeture, présence d'objets, de personnes, ...), les équipements et les systèmes de sécurité et de prévention"
        }
      ],
      "contact": {
        "coordonnees1": "2 bis BOULEVARD Calmette",
        "coordonnees2": "78200 MANTES LA JOLIE",
        "coordonnees3": "Courriel : ape.78010@pole-emploi.fr",
        "nom": "Pôle Emploi Mantes-la-Jolie"
      },
      "dateCreation": "2019-11-13T13:00:21+01:00",
      "deplacementCode": "1",
      "deplacementLibelle": "Jamais",
      "description": "vous gérez la permanence téléphonique, vous êtes à l'aise à l'oral .\nvous êtes à l'aide sur l'informatique, Vous relevez un maximum d'éléments et êtes réactif \n\nVous intervenez sur de la télésurveillance ainsi que de la vidéo-surveillance, \n\nSurveiller les lieux, les biens et effectuer des rondes vidéos préventives afin de détecter tous risques éventuels.\n- Repérer les anomalies, incidents et informer les forces de l'ordre et ou pompiers et les clients\n- Renseigner les supports d'intervention et d'activité (rapports, mains courantes, registres de sécurité,\ndéclaration, ...)\n\nL'œil averti, l'esprit concentré, au sein d'une station centrale de réception des alarmes, l'opérateur en télésurveillance voit tout et est très réactif. Il traite les informations reçues, et déclenche l'intervention des personnes habilitées en cas  d'alerte\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {},
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "42801",
          "commentaire": "cqp télésurveillance",
          "domaineLibelle": "Télésurveillance",
          "exigence": "E",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "095WKZW",
      "intitule": "Opérateur / Opératrice en télésurveillance   (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "78520",
        "commune": "78335",
        "latitude": 48.99333333,
        "libelle": "78 - LIMAY",
        "longitude": 1.735833333
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 2,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZW"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à garder le contrôle de soi pour agir efficacement face à des situations irritantes, imprévues et stressantes. Exemple : garder son calme",
          "libelle": "Gestion du stress"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        }
      ],
      "romeCode": "K2503",
      "romeLibelle": "Sécurité et surveillance privées",
      "salaire": {
        "libelle": "Mensuel de 1550,00 Euros à 1600,00 Euros sur 12 mois"
      },
      "secteurActivite": "80",
      "secteurActiviteLibelle": "Activités de sécurité privée",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "ape.75773@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Standardiste",
      "competences": [
        {
          "code": "120130",
          "exigence": "S",
          "libelle": "Accueillir une clientèle"
        },
        {
          "code": "120697",
          "exigence": "S",
          "libelle": "Assurer un accueil téléphonique"
        },
        {
          "code": "121711",
          "exigence": "S",
          "libelle": "Étudier une demande client"
        },
        {
          "code": "121288",
          "exigence": "S",
          "libelle": "Orienter les personnes selon leur demande"
        },
        {
          "code": "122330",
          "exigence": "S",
          "libelle": "Renseigner un client"
        }
      ],
      "contact": {
        "coordonnees1": "78 BOULEVARD Ney",
        "coordonnees2": "75018 PARIS 18",
        "coordonnees3": "Courriel : ape.75773@pole-emploi.fr",
        "nom": "Pôle Emploi Paris 18ème Ney"
      },
      "dateCreation": "2019-11-13T12:59:52+01:00",
      "description": "Vos missions:\n- Accueil physique et téléphonique en français et parfois en anglais ; \n- Gestion des plis, des colis, et des coursiers ;\n- Réservation de taxis ;\n- Vérification et réservation des salles de réunion ;\n- Diverses tâches administratives occasionnelles\nVous disposez d'une première expérience d'un ou deux ans dans un poste similaire ainsi que d'un bon niveau de culture générale. \nDynamique et enthousiaste, vous êtes doté(e) d'un excellent relationnel et d'un réel sens de l'accueil et du service.\nSouriant(e), volontaire et ponctuel(le), vous êtes également reconnu(e) pour votre discrétion.\nVous avez un niveau scolaire en anglais.\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "EDITIONS ALBIN MICHEL EDUCATION -"
      },
      "experienceCommentaire": "poste similaire",
      "experienceExige": "E",
      "experienceLibelle": "2 ans - poste similaire",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "S",
          "niveauLibelle": "Bac+2 ou équivalents"
        }
      ],
      "id": "095WKZS",
      "intitule": "Standardiste",
      "langues": [],
      "lieuTravail": {
        "codePostal": "75015",
        "commune": "75115",
        "latitude": 48.84138889,
        "libelle": "75 - PARIS 15",
        "longitude": 2.300277777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZS"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "M1601",
      "romeLibelle": "Accueil et renseignements",
      "salaire": {
        "complement1": "Chèque repas",
        "complement2": "Participation/action",
        "libelle": "Mensuel de 1668 Euros sur 13 mois"
      },
      "secteurActivite": "58",
      "secteurActiviteLibelle": "Édition de livres",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Métallier-serrurier industriel / Métallière-serrurière industrielle",
      "competences": [
        {
          "code": "106161",
          "exigence": "E",
          "libelle": "Former les matériaux par usinage, modelage, découpage, formage, sculpture, ..."
        },
        {
          "code": "120277",
          "exigence": "E",
          "libelle": "Régler les paramètres des machines et des équipements"
        },
        {
          "code": "106214",
          "exigence": "E",
          "libelle": "Reporter les cotes et mesures sur les matériaux et effectuer les tracés"
        },
        {
          "code": "123402",
          "exigence": "S",
          "libelle": "Installer des éléments de structures métalliques"
        },
        {
          "code": "123602",
          "exigence": "S",
          "libelle": "Marquer les éléments d'une structure métallique"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : lille2@welljob.fr",
        "courriel": "lille2@welljob.fr",
        "nom": "WELL JOB - Mme Céline  GANDILLET"
      },
      "dateCreation": "2019-11-13T12:59:32+01:00",
      "description": "L'Agence WELLJOB EMPLOI DE LILLE recrute un SERRURIER en ATELIER.\n\nMissions : \n- Lecture de plans\n- Fabrication de cadres\n- Soudure semi auto\n\nCompétences requises :\n-Rigoureux\n-Autonome\n-Respect de la clientèle\n-Savoir prendre des initiatives\n-Un bon esprit d'équipe",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "AGENCE WELLJOB EMPLOI DE LILLE \n54 Bis Boulevard de la Liberté\n59800 LILLE",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/aJRrFYgRrRAI0d6wyXUPFevjCOGo2rhH.png",
        "nom": "WELL JOB",
        "url": "https://www.welljob.fr/entreprise"
      },
      "experienceExige": "E",
      "experienceLibelle": "3 ans",
      "formations": [],
      "id": "095WKZQ",
      "intitule": "Métallier-serrurier industriel / Métallière-serrurière indu (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "59790",
        "commune": "59507",
        "latitude": 50.60472222,
        "libelle": "59 - RONCHIN",
        "longitude": 3.087777777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZQ"
      },
      "qualificationCode": "2",
      "qualificationLibelle": "Ouvrier spécialisé",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "H2911",
      "romeLibelle": "Réalisation de structures métalliques",
      "salaire": {
        "complement1": "Primes",
        "libelle": "Horaire de 11,00 Euros à 12,00 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 6 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Maçon / Maçonne",
      "competences": [
        {
          "code": "121482",
          "exigence": "S",
          "libelle": "Appliquer les mortiers"
        },
        {
          "code": "121517",
          "exigence": "S",
          "libelle": "Assembler des éléments d'armature de béton"
        },
        {
          "code": "122952",
          "exigence": "S",
          "libelle": "Monter les murs par maçonnage d'éléments portés"
        },
        {
          "code": "123667",
          "exigence": "S",
          "libelle": "Réaliser des enduits"
        },
        {
          "code": "103767",
          "exigence": "S",
          "libelle": "Terrasser et niveler la fondation"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : rosporden@triangle.fr",
        "courriel": "rosporden@triangle.fr",
        "nom": "CORNOUAILLE INTERIM - Mme MARY QUEVAT"
      },
      "dateCreation": "2019-11-13T12:59:18+01:00",
      "description": "Votre agence Cornouaille Intérim à Rosporden recrute pour son client basé à Concarneau, un Maçon N2 minimum H/F.\n\n \n\nVous serez chargé(e) de :\n\n \n\n- poser l''agglos,\n\n- réaliser des coffrages et des planchers,\n\n- la finition.\n\n \n\n39h semaine\n\nVéhicule de société selon niveau\n\nPanier repas\n\nLong terme enviseagable",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "La société Cornouaille Intérim groupe TRIANGLE INTÉRIM, entreprise à taille humaine classée parmi les 10 sociétés leaders de l'intérim en France, regroupe aujourd'hui un réseau d'une centaine d'agences réparties sur tout le territoire et emploie plus de 450 permanents et près de 15 000 intérimaires par jour.\nRejoindre Triangle, c'est bénéficier, sous conditions d'ancienneté, de ces différents avantages : C.E, participation, mutuelle et autres avantages sociaux (FASTT), accès à la formation",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/b7576c25d67842189eb5f1bedcc89e8d.png",
        "nom": "CORNOUAILLE INTERIM",
        "url": "https://www.triangle.fr/"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WKZP",
      "intitule": "Maçon / Maçonne",
      "langues": [],
      "lieuTravail": {
        "codePostal": "29140",
        "commune": "29241",
        "latitude": 47.96055556,
        "libelle": "29 - ROSPORDEN",
        "longitude": -3.834722221
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZP"
      },
      "qualificationCode": "3",
      "qualificationLibelle": "Ouvrier qualifié (P1,P2)",
      "romeCode": "F1703",
      "romeLibelle": "Maçonnerie",
      "salaire": {
        "libelle": "Horaire de 10,55 Euros à 12,67 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "3 à 5 salariés",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 6 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Poseur / Poseuse de canalisations",
      "competences": [
        {
          "code": "121795",
          "exigence": "S",
          "libelle": "Assembler des manchons, chaussettes, joints, vannes et robinets"
        },
        {
          "code": "121335",
          "exigence": "S",
          "libelle": "Creuser une fouille"
        },
        {
          "code": "121792",
          "exigence": "S",
          "libelle": "Protéger des manchons, chaussettes, joints, vannes et robinets"
        },
        {
          "code": "121334",
          "exigence": "S",
          "libelle": "Réaliser un lit de pose"
        },
        {
          "code": "121336",
          "exigence": "S",
          "libelle": "Régler un fond de forme"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : saminters@orange.fr",
        "courriel": "saminters@orange.fr",
        "nom": "SAS SAMINTER'S - Mme Aurélie DIEMER"
      },
      "dateCreation": "2019-11-13T12:58:53+01:00",
      "description": "Nous recherchons un technicien GAZ H/F\nAprès une période de formation, l'emploi aura en charge les interventions liées à la construction et à l'extension de nos réseaux, ainsi que les actes de maintenance préventives et correctives des installations de gaz naturel sur le territoire qui comprend environ 30 communes.\n\n \nIl réalise des actes techniques qui contribuent au développement et à l'image de l'entreprise en intervenant sur le réseau de distribution pour la pose de nouveaux réseaux, de branchements neufs ainsi que des travaux de renouvellement. Il participe également à la mise en service et à la maintenance des postes de détente et de comptage gaz.\n\n Profil recherché :\n Formation technique, idéalement dans le BTP\n Niveau CAP, BEP ou BAC PRO\nFormation spécifique au métier de technicien gaz assurée en interne\n Travail à l'extérieur et en équipe\n Permis B obligatoire\nMission de 18 mois à pourvoir dès que possible\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "SAS SAMINTER'S"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKZM",
      "intitule": "Poseur / Poseuse de canalisations",
      "langues": [],
      "lieuTravail": {
        "codePostal": "67140",
        "commune": "67021",
        "latitude": 48.40805556,
        "libelle": "67 - BARR",
        "longitude": 7.449722221
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZM"
      },
      "qualificationCode": "7",
      "qualificationLibelle": "Technicien",
      "romeCode": "F1705",
      "romeLibelle": "Pose de canalisations",
      "salaire": {
        "libelle": "Horaire de 10,50 Euros à 13,00 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 18 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Boulanger / Boulangère",
      "competences": [
        {
          "code": "124929",
          "exigence": "S",
          "libelle": "Boulage des pâtons"
        },
        {
          "code": "126290",
          "exigence": "S",
          "libelle": "Bouler et effectuer la tourne ou le façonnage des pâtons"
        },
        {
          "code": "119368",
          "exigence": "S",
          "libelle": "Conduire une fermentation"
        },
        {
          "code": "124928",
          "exigence": "S",
          "libelle": "Diviser la masse de pâte en pâtons"
        },
        {
          "code": "101738",
          "exigence": "S",
          "libelle": "Sélectionner et doser les ingrédients (farine, levure, additifs, ...) pour la réalisation des pâtes à pain ou à viennoiserie, les mélanger et effectuer le pétrissage, l'abaisse"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : recrutement@feuillette.fr",
        "courriel": "recrutement@feuillette.fr",
        "nom": "BOULANGERIE FEUILLETTE - Mme Manon HAMEAU"
      },
      "dateCreation": "2019-11-13T12:58:41+01:00",
      "description": "Vous serez en charge de la production de pain, viennoiserie et production salée.\n\nMissions de Boulanger/ère dans le cadre du respect des normes d'hygiène et sécurité : \n- Conduit le pétrissage \n- Pèse et façonne \n- Planifie la fermentation différée \n- Gère les cuissons \n- Analyse la qualité des productions \n- Confectionne des produits salés et sucrés\n\nExpérience production volumineuse serait un plus. \nHoraire type : 04h00 - 12h00 et 12h00 - 20h00\nHoraire variable du Lundi au Dimanche\n\nUn week-end par mois en repos.",
      "dureeTravailLibelle": "35H Travail samedi et dimanche",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "\" Plus qu'une Boulangerie ! \"\n\nCréée en 2009 par deux anciens pâtissiers, FEUILLETTE est une boulangerie-pâtisserie qui allie esthétisme et tradition pour fabriquer des produits originaux et de qualité.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/flmH5e888fdn9Xx1xDcLRv4AwoGo1dsS.png",
        "nom": "BOULANGERIE FEUILLETTE",
        "url": "http://www.feuillette.fr/"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [
        {
          "codeFormation": "21538",
          "domaineLibelle": "Boulangerie",
          "exigence": "E",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "095WKZL",
      "intitule": "Boulanger / Boulangère - Feuillette             (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "56100",
        "commune": "56121",
        "latitude": 47.74583333,
        "libelle": "56 - LORIENT",
        "longitude": -3.366388888
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZL"
      },
      "qualificationCode": "3",
      "qualificationLibelle": "Ouvrier qualifié (P1,P2)",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "D1102",
      "romeLibelle": "Boulangerie - viennoiserie",
      "salaire": {
        "commentaire": "Selon expérience / compétences",
        "complement1": "Mutuelle",
        "complement2": "Selon expérience / compétences"
      },
      "secteurActivite": "10",
      "secteurActiviteLibelle": "Boulangerie et boulangerie-pâtisserie",
      "trancheEffectifEtab": "20 à 49 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Attaché commercial / Attachée commerciale sédentaire",
      "competences": [
        {
          "code": "121427",
          "exigence": "S",
          "libelle": "Développer un portefeuille clients et prospects"
        },
        {
          "code": "117243",
          "exigence": "S",
          "libelle": "Établir un devis"
        },
        {
          "code": "102229",
          "exigence": "S",
          "libelle": "Intervenir auprès d'une clientèle d'entreprises"
        },
        {
          "code": "122339",
          "exigence": "S",
          "libelle": "Réaliser une étude technique et commerciale"
        },
        {
          "code": "122330",
          "exigence": "S",
          "libelle": "Renseigner un client"
        },
        {
          "code": "124537",
          "exigence": "S",
          "libelle": "Vérifier les conditions de réalisation d'une commande"
        },
        {
          "exigence": "S",
          "libelle": "génie thermique"
        },
        {
          "exigence": "S",
          "libelle": "maîtrise de l'informatique"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : eric.hoarau@lindab.fr",
        "courriel": "eric.hoarau@lindab.fr",
        "nom": "LINDAB FRANCE - M. Eric Hoarau"
      },
      "dateCreation": "2019-11-13T12:58:38+01:00",
      "description": "Au sein du service Commercial de l'agence Lindab à Bonneuil sur Marne (94387) et rattaché au Responsable d'Agence, vous êtes l'interlocuteur technique du client en agence et vous mettez tout en œuvre pour lui assurer le meilleur service (conseils, approvisionnements, devis, relances commerciales...). Ainsi, vous occupez un rôle central au sein de l'agence et travaillez en relation directe avec de multiples interlocuteurs de l'entreprise (commerciaux, fournisseurs, siège...).\nLa diversité des missions de ce poste nécessitera de votre part une bonne capacité à gérer la demande technique en parallèle de l'action commerciale, un excellent sens de l'organisation ainsi qu'une maîtrise générale des outils informatiques.\n",
      "dureeTravailLibelle": "37H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Le Groupe Lindab international développe, fabrique et commercialise des produits et des solutions complètes pour simplifier la construction et créer un meilleur confort thermique et acoustique à l'intérieur des bâtiments. Il propose trois divisions spécialisées.\n- Lindab Ventilation : systèmes et solution pour le renouvellement et le traitement de l'air par ventilation mécanique contrôlée (VMC).\n- Lindab Building Products : Composants et accessoires pour le traitement des eaux pluviales,",
        "nom": "LINDAB FRANCE"
      },
      "experienceExige": "E",
      "experienceLibelle": "3 ans",
      "formations": [],
      "id": "095WKZK",
      "intitule": "Attaché commercial / Attachée commerciale sédentaire  (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "94380",
        "commune": "94011",
        "latitude": 48.77416667,
        "libelle": "94 - BONNEUIL SUR MARNE",
        "longitude": 2.487499999
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZK"
      },
      "qualificationCode": "7",
      "qualificationLibelle": "Technicien",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à être proactif, à initier, à imaginer des propositions nouvelles pour résoudre les problèmes identifiés ou pour améliorer une situation. Exemple : proposer des améliorations, être positif et constructif",
          "libelle": "Force de proposition"
        }
      ],
      "romeCode": "D1401",
      "romeLibelle": "Assistanat commercial",
      "salaire": {
        "libelle": "Mensuel de 2200,00 Euros à 2500,00 Euros sur 12 mois"
      },
      "secteurActivite": "25",
      "secteurActiviteLibelle": "Fabrication d'autres articles métalliques",
      "trancheEffectifEtab": "50 à 99 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Collaborateur / Collaboratrice d'expertise comptable",
      "competences": [
        {
          "code": "117019",
          "exigence": "S",
          "libelle": "Contrôler l'application des méthodes et procédures de contrôle comptable"
        },
        {
          "code": "119405",
          "exigence": "S",
          "libelle": "Définir des procédures de contrôle comptable"
        },
        {
          "code": "120889",
          "exigence": "S",
          "libelle": "Gestion administrative"
        },
        {
          "code": "120888",
          "exigence": "S",
          "libelle": "Gestion comptable"
        },
        {
          "code": "117020",
          "exigence": "S",
          "libelle": "Proposer des améliorations des méthodes et procédures de contrôle comptable"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : contact@mnrhconseil.fr",
        "courriel": "contact@mnrhconseil.fr",
        "nom": "MN RH CONSEIL - Mme Nicol Maryse"
      },
      "dateCreation": "2019-11-13T12:58:16+01:00",
      "description": "Notre client est un cabinet d'expertise comptable comptant 50 collaborateurs environ. La clientèle est constituée de PME et PMI, des groupes mais aussi des start-ups. Pour accompagner le développement du bureau de Neuilly, il recherche : Un Collaborateur Comptable Débutant H/F Au sein d'un pôle expertise de 15 personnes, - vous assistez des chefs de missions dans la tenue quotidienne de leurs dossiers. - vous évoluez progressivement et participez à l'établissement de l'ensemble des déclarations fiscales ainsi qu'aux bilans. Selon votre évolution et votre profil, vous prenez en charge un portefeuille de clients. H/F - De formation BTS comptabilité, DCG, licence comptabilité, vous êtes débutant ou vous avez acquis une première expérience en cabinet. Poste basé à Métro Sablon- à pourvoir début septembre. CDI. Fixe, prime, RTT, TR. Un cadre de travail agréable et des équipes stables !",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "MN RH CONSEIL",
        "url": "https://www.mnrh-conseil.fr/"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKZG",
      "intitule": "Collaborateur Comptable Débutant (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "92200",
        "commune": "92051",
        "latitude": 48.88722222,
        "libelle": "92 - NEUILLY SUR SEINE",
        "longitude": 2.267499999
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZG"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "M1202",
      "romeLibelle": "Audit et contrôle comptables et financiers",
      "salaire": {
        "libelle": "Mensuel de 1700,00 Euros à 2000,00 Euros sur 12 mois"
      },
      "secteurActivite": "70",
      "secteurActiviteLibelle": "Conseil pour les affaires et autres conseils de gestion",
      "trancheEffectifEtab": "1 ou 2 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Opérateur / Opératrice d'exécution de façonnage",
      "competences": [
        {
          "code": "123337",
          "exigence": "S",
          "libelle": "Alimenter une machine industrielle en matière ou produit"
        },
        {
          "code": "102901",
          "exigence": "S",
          "libelle": "Réaliser les opérations manuelles de façonnage/routage"
        },
        {
          "code": "102902",
          "exigence": "S",
          "libelle": "Réceptionner les produits finis en sortie de machine et en contrôler la conformité"
        },
        {
          "code": "117634",
          "exigence": "S",
          "libelle": "Suivre l'approvisionnement"
        }
      ],
      "contact": {
        "coordonnees2": "260 AV AVENUE ARISTIDE BRIAND",
        "coordonnees3": "92220 BAGNEUX",
        "nom": "INSTANTANE - Mme nawel el kabir"
      },
      "dateCreation": "2019-11-13T12:58:06+01:00",
      "description": "-\tGérer la production sur les différentes machines de production (machine d'impression, de découpe  ) ;\n-\tGérer les impressions en tenant compte des instructions du responsable de production, des dates de chantier et des priorités planifiées ;\n-\tS'adapter aux dates de production en fonction des chantiers et des expositions ;\n-\tOrganiser le suivi et l'entretien du parc machines ;\n-\tGérer l'évolution du stock des consommables d'impression afin d'informer le responsable de production des commandes à prévoir ;\n-\tFaçonnage : découpe au cutter, collage au double face, conditionnement de non tissé, bâche, PVC, tissus, voile et tous autres matières ; Echenillage adhésifs\n-\tAutres activités : dé gainage des panneaux mélaminés, pose de rivets\n-\tPréparer et gérer le matériel d'accastillage : barres de fixation, barres de lestage, drisses,  \n-\tGérer les outillages d'atelier et de chantier\n-\tCharger/ décharger les camions,\n-\tLivrer sur site d'exposition ou chez les clients //dépose \n..",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Connaissances :\n- Maîtriser les logiciels bureautiques (Word, Excel, Power point ), Sage\n- Avoir une connaissance du monde de l'événementiel serait un plus,\n\nCompétences :\nElle/il doit être  \n- Polyvalente, \n- Volontaire,\n- Organisée,\n- Structurée,\n- Avoir l'esprit d'équipe\n\nNiveau de formation : BTS",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/sLcPTTbK8kMznS3SbmGr4B7Hc8Q7rDR7.png",
        "nom": "INSTANTANE",
        "url": "http://www.instantane.net"
      },
      "experienceExige": "E",
      "experienceLibelle": "6 mois",
      "formations": [],
      "id": "095WKZF",
      "intitule": "Opérateur / Opératrice d'exécution de façonnage     (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "92220",
        "commune": "92007",
        "latitude": 48.79777778,
        "libelle": "92 - BAGNEUX",
        "longitude": 2.3125
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZF"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à s’adapter à des situations variées et à s’ajuster à des organisations, des collectifs de travail, des habitudes et des valeurs propres à l’entreprise. Exemple : être souple, agile, s’adapter à une situation imprévue",
          "libelle": "Capacité d’adaptation"
        },
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à aller chercher au-delà de ce qui est donné à voir, à s’ouvrir sur la nouveauté et à investiguer pour comprendre et agir de façon appropriée. Exemple : avoir l’envie d’apprendre",
          "libelle": "Curiosité"
        }
      ],
      "romeCode": "E1304",
      "romeLibelle": "Façonnage et routage",
      "salaire": {
        "libelle": "Mensuel de 1799,00 Euros à 1801,00 Euros sur 12 mois"
      },
      "secteurActivite": "73",
      "secteurActiviteLibelle": "Activités des agences de publicité",
      "trancheEffectifEtab": "6 à 9 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Cadre de santé d'unité de soins",
      "competences": [
        {
          "code": "121813",
          "exigence": "S",
          "libelle": "Concevoir et mettre en place des protocoles de soins"
        },
        {
          "code": "120281",
          "exigence": "S",
          "libelle": "Contrôler l'application de procédures Qualité, Hygiène, Sécurité et Environnement (QHSE)"
        },
        {
          "code": "121814",
          "exigence": "S",
          "libelle": "Contrôler l'application des protocoles de soins"
        },
        {
          "code": "119337",
          "exigence": "S",
          "libelle": "Mettre en place des actions de gestion de ressources humaines"
        },
        {
          "code": "123903",
          "exigence": "S",
          "libelle": "Planifier l'activité d'un service"
        },
        {
          "code": "117590",
          "exigence": "S",
          "libelle": "Réaliser la gestion administrative du personnel"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : recrutement@amreso-bethel.fr",
        "courriel": "recrutement@amreso-bethel.fr",
        "nom": "ASS AMIS MAIS RETR SOINS OBERHAUSBERGE - M. JEROME PIERRON"
      },
      "dateCreation": "2019-11-13T12:57:50+01:00",
      "description": "Rattaché(e) à la Direction, vous aurez pour principales missions de :\n- Décliner la politique d'établissement au sein des services encadrés tout en étant au plus près des équipes et des patients dans un souci de qualité des prestations,\n- Concevoir et mettre en œuvre le projet de soin des services encadrés en lien avec l'équipe médicale,\n- Assurer le management des 2 équipes SSR Oncologie (quarantaine de collaborateurs) et USLD (trentaine de collaborateurs)",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "La Maison de Santé Béthel, maison médicalisée située à Oberhausbergen, a pour mission d'accueillir et d'accompagner des personnes fragilisées tout au long de leur projet de vie et de leur parcours de soins. Plus de 300 collaborateurs passionnés et investis œuvrent au quotidien pour le bien-être de nos 333 patients et résidents. L'établissement est constitué en 4 Pôles d'activités : Pôle Personnes Âgées Dépendantes / EHPAD, Pôle SSR à orientation oncologique, Pôle Long Séjour, et Pôle Handicap.",
        "nom": "ASS AMIS MAIS RETR SOINS OBERHAUSBERGE",
        "url": "https://www.amreso-bethel.fr/recrutement"
      },
      "experienceCommentaire": "sanitaire ou médico-social",
      "experienceExige": "E",
      "experienceLibelle": "2 ans - sanitaire ou médico-social",
      "formations": [
        {
          "codeFormation": "99999",
          "commentaire": "DE infirmier + Cadre de santé",
          "exigence": "E",
          "niveauLibelle": "Bac+3, Bac+4 ou équivalents"
        }
      ],
      "id": "095WKZB",
      "intitule": "infirmier  SSR orientation oncologique H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "67205",
        "commune": "67343",
        "latitude": 48.60666667,
        "libelle": "67 - OBERHAUSBERGEN",
        "longitude": 7.685277777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKZB"
      },
      "qualificationCode": "9",
      "qualificationLibelle": "Cadre",
      "romeCode": "J1502",
      "romeLibelle": "Coordination de services médicaux ou paramédicaux",
      "salaire": {
        "commentaire": "selon convention FEHAP 51",
        "complement1": "selon convention FEHAP 51"
      },
      "secteurActivite": "86",
      "secteurActiviteLibelle": "Activités hospitalières",
      "trancheEffectifEtab": "250 à 499 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Électricien / Électricienne d'équipement",
      "competences": [
        {
          "code": "121777",
          "exigence": "S",
          "libelle": "Fixer des éléments basse tension"
        },
        {
          "code": "123597",
          "exigence": "S",
          "libelle": "Positionner une armoire électrique de locaux domestiques ou tertiaires"
        },
        {
          "code": "121778",
          "exigence": "S",
          "libelle": "Raccorder des éléments basse tension"
        },
        {
          "code": "123598",
          "exigence": "S",
          "libelle": "Raccorder une armoire électrique aux équipements de locaux domestiques ou tertiaires"
        },
        {
          "code": "103495",
          "exigence": "S",
          "libelle": "Réaliser et poser des chemins de câbles et des conduits électriques en apparent ou en encastré"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : niort.interim@orange.fr",
        "courriel": "niort.interim@orange.fr",
        "nom": "NIORT INTERIM - Mme Priscille LE DILY"
      },
      "dateCreation": "2019-11-13T12:57:13+01:00",
      "description": "- Travaux électriques chez des particuliers, pose de prise, luminaires, installations de tableau électrique",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/UgagDoUwSzd7XdRWBJ4vJDrPmwhSugBl.png",
        "nom": "NIORT INTERIM",
        "url": "https://www.niort-interim.fr"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKYW",
      "intitule": "Électricien / Électricienne d'équipement",
      "langues": [],
      "lieuTravail": {
        "codePostal": "79000",
        "commune": "79191",
        "latitude": 46.325,
        "libelle": "79 - NIORT",
        "longitude": -0.462222222
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKYW"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "F1602",
      "romeLibelle": "Électricité bâtiment",
      "salaire": {
        "libelle": "Horaire de 10,03 Euros à 11,00 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 1 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Opérateur / Opératrice en impression numérique et découpe adhésif/vinyle",
      "competences": [
        {
          "code": "124623",
          "exigence": "E",
          "libelle": "Sélectionner des documents et images dans une base de données"
        },
        {
          "code": "102948",
          "exigence": "E",
          "libelle": "Traiter textes et images en fonction des modes et des supports d'impression"
        },
        {
          "code": "102725",
          "exigence": "S",
          "libelle": "Concevoir la mise en page et enrichir le document (intégration texte, image, correction, ...) selon les règles typographiques et la charte graphique"
        },
        {
          "code": "102945",
          "exigence": "S",
          "libelle": "Définir les étapes de réalisation de la commande (mise en page, imposition, flashage, ...) et contrôler le support fourni"
        },
        {
          "code": "102946",
          "exigence": "S",
          "libelle": "Saisir le texte ou réaliser la maquette à partir du manuscrit, pré-projet, brouillon"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : nawel@instantane.net",
        "courriel": "nawel@instantane.net",
        "nom": "INSTANTANE - Mme nawel EL KABIR"
      },
      "dateCreation": "2019-11-13T12:56:54+01:00",
      "description": "-\tGérer l'impression de la production sur les différentes machines d'impression\n-\tGérer l'évolution du stock des consommables d'impression afin d'établir les demandes d'achats de matières premières\n-\tOrganiser le suivi et l'entretien du parc machines \n-\tGérer la production en tenant compte des dates de chantier et des priorités planifiées;\n-\tS'adapter aux dates des expositions.\n-\tIntervenir en soutien, si nécessaire : contre collage, façonnage, emballage, chargement camion, éventuellement pose et dépose de signalétique",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Connaissances :\n- Maîtriser les logiciels bureautiques (Word, Excel, Power point ), Sage\n- Avoir une connaissance du monde de l'événementiel serait un plus,\n- Connaître les machines d'impression Vutek \n\nCompétences :\nElle/il doit être \n- Polyvalente, \n- Volontaire,\n- Organisée,\n- Structurée,\n- Avoir l'esprit d'équipe",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/sLcPTTbK8kMznS3SbmGr4B7Hc8Q7rDR7.png",
        "nom": "INSTANTANE",
        "url": "http://www.instantane.net"
      },
      "experienceExige": "E",
      "experienceLibelle": "1 an",
      "formations": [],
      "id": "095WKYV",
      "intitule": "Opérateur / Opératrice en impression numérique et découpe a (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "92220",
        "commune": "92007",
        "latitude": 48.79777778,
        "libelle": "92 - BAGNEUX",
        "longitude": 2.3125
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKYV"
      },
      "qualificationCode": "7",
      "qualificationLibelle": "Technicien",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à garder le contrôle de soi pour agir efficacement face à des situations irritantes, imprévues et stressantes. Exemple : garder son calme",
          "libelle": "Gestion du stress"
        },
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        }
      ],
      "romeCode": "E1306",
      "romeLibelle": "Prépresse",
      "salaire": {
        "complement1": "Chèque repas",
        "complement2": "Mutuelle",
        "libelle": "Mensuel de 1900,00 Euros à 2200,00 Euros sur 12 mois"
      },
      "secteurActivite": "73",
      "secteurActiviteLibelle": "Activités des agences de publicité",
      "trancheEffectifEtab": "6 à 9 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Agent / Agente technique d'élevage",
      "competences": [
        {
          "code": "123897",
          "exigence": "S",
          "libelle": "Accompagner la mise en oeuvre d'actions préventives et curatives"
        },
        {
          "code": "100133",
          "exigence": "S",
          "libelle": "Identifier les priorités d'intervention (contrôles, relevés, ...) en fonction de la planification annuelle et déterminer le matériel approprié"
        },
        {
          "code": "100135",
          "exigence": "S",
          "libelle": "Relever les données de performance individuelle des animaux (quantité et qualité du lait, prise de poids, ...)"
        },
        {
          "code": "120509",
          "exigence": "S",
          "libelle": "Repérer et signaler des anomalies"
        },
        {
          "code": "123108",
          "exigence": "S",
          "libelle": "Vérifier le contenu d'un registre de suivi de production ou d'élevage"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : aef22@aef22.fr",
        "courriel": "aef22@aef22.fr",
        "nom": "ANEFA 22 - Mme ANEFA 22"
      },
      "dateCreation": "2019-11-13T12:56:42+01:00",
      "description": "Exploitation avicole située à Lohuec (et un site sur Plougras) recherche un(e)agent(e) agricole polyvalent(e)avec une expérience en aviculture pour seconder l'exploitant en toute autonomie sur les deux sites. Vous vous occuperez du suivi de l'élevage, de l'arrivée des lots jusqu'à leur départ, gèrerez le vide-sanitaire, entretiendrez les bâtiments...). La conduite du tracteur serait un plus mais pas indispensable. Prévoir de travailler un week-end par mois. Possibilité de prendre ses repas sur place. Salaire selon profil avec des primes sur résultats. Comité d'entreprise.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "ANEFA 22"
      },
      "experienceCommentaire": "Souhaitée",
      "experienceExige": "E",
      "experienceLibelle": "1 an - Souhaitée",
      "formations": [],
      "id": "095WKYR",
      "intitule": "Agent polyvalent d'exploitation OAV123045-22 (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "22160",
        "commune": "22132",
        "latitude": 48.46027778,
        "libelle": "22 - LOHUEC",
        "longitude": -3.521666666
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKYR"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "A1302",
      "romeLibelle": "Contrôle et diagnostic technique en agriculture",
      "salaire": {
        "commentaire": "Selon profil",
        "complement1": "Selon profil"
      },
      "secteurActivite": "01",
      "secteurActiviteLibelle": "Élevage de volailles",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Secrétaire médical / médicale",
      "competences": [
        {
          "code": "107353",
          "exigence": "S",
          "libelle": "Accueillir et renseigner le patient sur les horaires de réception, les possibilités de rendez-vous, le déroulement de l'examen"
        },
        {
          "code": "107201",
          "exigence": "S",
          "libelle": "Actualiser le dossier médical du patient"
        },
        {
          "code": "123899",
          "exigence": "S",
          "libelle": "Organiser le planning des activités"
        },
        {
          "code": "124388",
          "exigence": "S",
          "libelle": "Réaliser des démarches médico administratives"
        },
        {
          "code": "124221",
          "exigence": "S",
          "libelle": "Tenir à jour les dossiers médico-administratifs des patients"
        }
      ],
      "contact": {
        "coordonnees1": "https://jobs.smartrecruiters.com/Eurofins/743999699425257-secretaire-medicale-en-laboratoire-de-biologie-medicale",
        "nom": "centre de biologie médicale Eurofins 69 - Mme GERALDINE CHEMIN",
        "urlPostulation": "https://jobs.smartrecruiters.com/Eurofins/743999699425257-secretaire-medicale-en-laboratoire-de-biologie-medicale"
      },
      "dateCreation": "2019-11-13T12:56:24+01:00",
      "description": "Dans le cadre d'un remplacement de congé maternité, le laboratoire recherche sa (son) Secrétaire Médicale.\n\nIl/elle accueille, enregistre les dossiers et édite les résultats d'analyses des patients. Il/elle est également amené(e) à gérer la facturation et les impayés des patients, correspondants ou autres collaborateurs.\n\nQualifications\nSAVOIR :\nBonne connaissance des terminologies médicales, examens biologiques, pathologies chroniques et incidences biologiques\nSAVOIR-FAIRE :\nOrganisation dans la méthode de travail / Bonne maîtrise de l'outil informatique\nSAVOIR-ÊTRE :\nDynamisme / Travail en équipe / Amabilité / Patience\nInformations complémentaires\nNIVEAU D'ÉTUDES REQUIS : Diplôme de Secrétaire médicale ou équivalence\nEXPÉRIENCE REQUISE : Débutant(e)s acceptés.\nLa rémunération sera fixée selon l'expérience.\n\nCe poste est à pourvoir début d'année 2020.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "laboratoire de médipôle hôpital privé résulte du   regroupement de 3 laboratoires : celui de la clinique du Tonkin, Eurofins Lyon 3 et eurofins Villeurbanne",
        "nom": "centre de biologie médicale Eurofins 69",
        "url": "https://www.eurofins.com"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKYP",
      "intitule": "Secrétaire médicale en Laboratoire de Biologie Médicale (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "97320",
        "commune": "97311",
        "latitude": 5.503888888,
        "libelle": "973 - ST LAURENT DU MARONI",
        "longitude": -54.02888889
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKYP"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "M1609",
      "romeLibelle": "Secrétariat et assistanat médical ou médico-social",
      "salaire": {
        "commentaire": "compétitive",
        "complement1": "compétitive"
      },
      "secteurActivite": "86",
      "secteurActiviteLibelle": "Laboratoires d'analyses médicales",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Commercial / Commerciale auprès des particuliers",
      "competences": [
        {
          "code": "125815",
          "exigence": "S",
          "libelle": "Conseiller un client"
        },
        {
          "code": "119945",
          "exigence": "S",
          "libelle": "Identifier les besoins d'un client"
        },
        {
          "code": "121417",
          "exigence": "S",
          "libelle": "Méthodes de plan de prospection"
        },
        {
          "code": "120526",
          "exigence": "S",
          "libelle": "Mettre en place des actions de prospection"
        },
        {
          "code": "121421",
          "exigence": "S",
          "libelle": "Présenter des produits et services"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : agentco@ipheos.com",
        "courriel": "agentco@ipheos.com",
        "nom": "JCST - M. Jean-Christophe HALLYNCK"
      },
      "dateCreation": "2019-11-13T12:56:23+01:00",
      "description": "Vous prospectez une cible de clientèle de professionnels (institut de beauté, esthéticienne à domicile, pharmacie et para pharmacie, parfumerie, onglerie, salon de coiffure).",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "description": "IPHEOS Cosmétiques est une marque de produits cosmétiques hautement efficaces, naturels (la plupart BIO), fabriqués en France et qui s'inscrivent dans une démarche écoresponsable.\n\nNous avons 2 départements, un pour la distribution de notre gamme auprès des professionnels (via des Agents Commerciaux) et l'autre pour la vente aux particuliers par le biais de la Vente Directe.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/a359d17995f74a18880d1d2f2430b241.png",
        "nom": "JCST",
        "url": "http://www.ipheos.com"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "S",
          "niveauLibelle": "Bac ou équivalent"
        }
      ],
      "id": "095WKYN",
      "intitule": "agent commercial en cosmetiques                         (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "56000",
        "commune": "56260",
        "latitude": 47.655,
        "libelle": "56 - VANNES",
        "longitude": -2.761666666
      },
      "natureContrat": "Emploi non salarié",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKYN"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à être proactif, à initier, à imaginer des propositions nouvelles pour résoudre les problèmes identifiés ou pour améliorer une situation. Exemple : proposer des améliorations, être positif et constructif",
          "libelle": "Force de proposition"
        },
        {
          "description": "Capacité à maintenir son effort jusqu’à l’achèvement complet d’une tâche, quels que soient les imprévus et les obstacles de réalisation rencontrés. Exemple : faire preuve de volonté, d’assiduité, de régularité, rebondir après une erreur",
          "libelle": "Persévérance"
        }
      ],
      "romeCode": "D1403",
      "romeLibelle": "Relation commerciale auprès de particuliers",
      "salaire": {
        "complement1": "Primes"
      },
      "secteurActivite": "47",
      "secteurActiviteLibelle": "Vente à domicile",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "CCE",
      "typeContratLibelle": "Profession commerciale"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Débardeur forestier / Débardeuse forestière",
      "competences": [
        {
          "code": "122822",
          "exigence": "S",
          "libelle": "Abattre un arbre"
        },
        {
          "code": "100003",
          "exigence": "S",
          "libelle": "Conduire les engins agricoles vers le lieu de production (champs, forêt), de stockage, de livraison (fermes, coopératives)"
        },
        {
          "code": "122579",
          "exigence": "S",
          "libelle": "Débiter un arbre selon son usage"
        },
        {
          "code": "122704",
          "exigence": "S",
          "libelle": "Identifier le type d'intervention"
        },
        {
          "code": "116766",
          "exigence": "S",
          "libelle": "Procédures de maintenance de locaux"
        },
        {
          "code": "116765",
          "exigence": "S",
          "libelle": "Procédures de maintenance de matériel"
        },
        {
          "code": "116663",
          "exigence": "S",
          "libelle": "Réaliser l'entretien du matériel"
        },
        {
          "code": "122564",
          "exigence": "S",
          "libelle": "Sécuriser un équipement"
        }
      ],
      "contact": {
        "coordonnees1": "Tél. : 0558781655",
        "nom": "TRAVAUX FORESTIERS DUBERN - Mme DUSSOUL CLARISSE",
        "telephone": "0558781655"
      },
      "dateCreation": "2019-11-13T12:56:22+01:00",
      "deplacementCode": "4",
      "deplacementLibelle": "Quotidiens Départemental",
      "description": "Vous serez chauffeur(se) abatteuse, vous êtes autonome. Chantiers dans les landes, un fourgon d'atelier vous permet de vous rendre sur les chantiers. \nPoste à pourvoir de suite.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "TRAVAUX FORESTIERS DUBERN"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WKYM",
      "intitule": "Chauffeur d'abatteuse forestière (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "40115",
        "commune": "40046",
        "latitude": 44.39305556,
        "libelle": "40 - BISCARROSSE-LANDES",
        "longitude": -1.163888888
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKYM"
      },
      "qualificationCode": "2",
      "qualificationLibelle": "Ouvrier spécialisé",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        }
      ],
      "romeCode": "A1101",
      "romeLibelle": "Conduite d'engins agricoles et forestiers",
      "salaire": {
        "commentaire": "Selon expérience",
        "complement1": "Selon expérience",
        "libelle": "Horaire de 10,03 Euros"
      },
      "secteurActivite": "02",
      "secteurActiviteLibelle": "Services de soutien à l'exploitation forestière",
      "trancheEffectifEtab": "6 à 9 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Manoeuvre de chantier",
      "competences": [],
      "contact": {
        "coordonnees1": "Courriel : audrey.ligas@veolia.com",
        "courriel": "audrey.ligas@veolia.com",
        "nom": "ASTERALIS - Mme AUDREY LIGAS"
      },
      "dateCreation": "2019-11-13T12:55:51+01:00",
      "description": "Missions :\nSous la responsabilité du Chef de Chantier, vos missions principales seront les suivantes:\n- Assure les opérations de manutention et de transfert des ensembles mécaniques,\n- Démonte les pièces mécaniques, suivant les différents modes opératoires,\n- Contrôle les pièces, renseignement les formulaires de suivi,\n- Manutentionne et range les pièces,\n- Prend les photographies des pièces et de leur conditionnement.\n\nProfil :\n- Formation initiale : mécanique ou électromécanique\n- Faire preuve d'une attitude interrogative ainsi, d'une démarche rigoureuse et prudente,\n- Respecter les règles et les consignes de sécurité, sûreté et radioprotection,\n- Considérer la polyvalence comme une opportunité de développement,\n- Avoir un bon relationnel pour travailler en équipe,\n- La détention d'habilitations CACES et/ou PR1 CC sont des plus.\nDisposer d'une culture «nucléaire» et avoir une formation suffisante pour intervenir en zone contrôlée sont des plus.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Asteralis, filiale du groupe Veolia, accompagne ses clients de l'industrie nucléaire de l'assainissement et démantèlement à la gestion des déchets radioactifs. Acteur indépendant de la filière nucléaire, Asteralis propose des services de mesures et analyses, ingénierie et études, chantiers d'assainissements et de démantèlement, exploitation en gestion déléguée.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/acae412d47a24f0284629acbf1a24384.png",
        "nom": "ASTERALIS",
        "url": "http://asteralis.veolia.com/"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKYL",
      "intitule": "Manoeuvre de chantier",
      "langues": [],
      "lieuTravail": {
        "libelle": "Centre-Val de Loire"
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKYL"
      },
      "qualificationCode": "2",
      "qualificationLibelle": "Ouvrier spécialisé",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à aller chercher au-delà de ce qui est donné à voir, à s’ouvrir sur la nouveauté et à investiguer pour comprendre et agir de façon appropriée. Exemple : avoir l’envie d’apprendre",
          "libelle": "Curiosité"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "F1704",
      "romeLibelle": "Préparation du gros oeuvre et des travaux publics",
      "salaire": {
        "libelle": "Annuel de 22000,00 Euros à 25000,00 Euros sur 13 mois"
      },
      "secteurActivite": "71",
      "secteurActiviteLibelle": "Analyses, essais et inspections techniques",
      "trancheEffectifEtab": "100 à 199 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 7 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Serveur / Serveuse de restaurant",
      "competences": [
        {
          "code": "104323",
          "exigence": "S",
          "libelle": "Accueillir le client à son arrivée au restaurant, l'installer à une table et lui présenter la carte"
        },
        {
          "code": "124015",
          "exigence": "S",
          "libelle": "Débarrasser une table"
        },
        {
          "code": "117933",
          "exigence": "S",
          "libelle": "Dresser les tables"
        },
        {
          "code": "124014",
          "exigence": "S",
          "libelle": "Nettoyer une salle de réception"
        },
        {
          "code": "117932",
          "exigence": "S",
          "libelle": "Réaliser la mise en place de la salle et de l'office"
        }
      ],
      "dateCreation": "2019-11-13T12:55:50+01:00",
      "description": "Dans le cadre d'un accroissement d'activités pour les fêtes de fin d'année, nous recherchons des serveurs/serveuses pour des vacations d'extra (midi et/ou soir, week-end et jours fériés), horaires continus et/ou coupure.Postes à pourvoir de suite.\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "Domaine et golf de Vaugouard"
      },
      "experienceExige": "E",
      "experienceLibelle": "6 mois",
      "formations": [],
      "id": "095WKYK",
      "intitule": "Serveur / Serveuse de restaurant",
      "langues": [],
      "lieuTravail": {
        "codePostal": "45210",
        "commune": "45148",
        "latitude": 48.10416667,
        "libelle": "45 - FONTENAY SUR LOING",
        "longitude": 2.774166666
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKYK"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "G1803",
      "romeLibelle": "Service en restauration",
      "salaire": {
        "libelle": "Horaire de 10,03 Euros à 12,00 Euros sur 12 mois"
      },
      "secteurActivite": "56",
      "secteurActiviteLibelle": "Restauration traditionnelle",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 1 Mois"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "ape.51436@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Téléconseiller / Téléconseillère",
      "competences": [
        {
          "code": "120697",
          "exigence": "S",
          "libelle": "Assurer un accueil téléphonique"
        },
        {
          "code": "121420",
          "exigence": "S",
          "libelle": "Contacter un client ou un prospect"
        },
        {
          "code": "121421",
          "exigence": "S",
          "libelle": "Présenter des produits et services"
        },
        {
          "code": "126253",
          "exigence": "S",
          "libelle": "Présenter ou identifier l'objet de l'appel (commande, vente, information, réclamation, assistance, ...)"
        },
        {
          "code": "124611",
          "exigence": "S",
          "libelle": "Réaliser un suivi d'appel"
        },
        {
          "code": "121862",
          "exigence": "S",
          "libelle": "Techniques de vente par téléphone"
        },
        {
          "code": "122492",
          "exigence": "S",
          "libelle": "Vérifier l'identité et les coordonnées de l'interlocuteur"
        }
      ],
      "contact": {
        "coordonnees1": "67 RUE DU MONT D ARENE\nBP 80",
        "coordonnees2": "51873 REIMS",
        "coordonnees3": "Courriel : ape.51436@pole-emploi.fr",
        "nom": "Pôle Emploi REIMS MONT D'ARENE"
      },
      "dateCreation": "2019-11-13T12:55:49+01:00",
      "description": "Plusieurs postes de téléconseillers(ères)  : appels entrants\n- Appels entrants et Back Office\n- Société de Carte Prépayée de titres cadeaux sécurisés\n\n- Être à l'aise avec l'outil informatique \n\nPlusieurs CDD disponibles à pourvoir de suite.\n\n\n\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "COMDATA"
      },
      "experienceCommentaire": "en téléconseil",
      "experienceExige": "E",
      "experienceLibelle": "3 mois - en téléconseil",
      "formations": [],
      "id": "095WKYJ",
      "intitule": "Téléconseiller(ère) appels entrants H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "51050",
        "commune": "51454",
        "latitude": 49.26527778,
        "libelle": "51 - REIMS",
        "longitude": 4.02861111
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKYJ"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "D1408",
      "romeLibelle": "Téléconseil et télévente",
      "salaire": {
        "libelle": "Mensuel de 1521,22 Euros sur 12 mois"
      },
      "secteurActivite": "82",
      "secteurActiviteLibelle": "Activités de centres d'appels",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 6 Mois"
    },
    {
      "alternance": false,
      "appellationlibelle": "Conseiller / Conseillère de clientèle bancaire",
      "competences": [],
      "dateActualisation": "2019-11-13T12:55:52+01:00",
      "dateCreation": "2019-11-13T12:55:48+01:00",
      "description": "Vous êtes animé(e) par la culture du résultat et par un sens prononcé du service client. Votre rigueur et organisation garantiront la qualité du suivi et la gestion de chaque dossier client. Vos qualités d'initiative et d'anticipation vous permettront de surprendre vos clients en leur ouvrant des perspectives de placement. Votre aisance relationnelle et vos capacités de conviction seront déterminantes dans la fidèlisation de vos clients. Vous êtes diplômé(e) : Bac3 avec 2/3 ans min. d'expérience de conseiller bancaire particulier généraliste ou d'un Master 1. Si votre profil est retenu, nous vous inviterons à passer un entretien vidéo différé afin d'en apprendre plus sur votre profil. Vous serez invité(e) par email et/ou SMS à vous connecter en ligne à une plateforme dédiée pour répondre à quelques questions. LI-DRHU",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 2 à 3 An(s)",
      "formations": [],
      "id": "3642876",
      "intitule": "Conseiller bancaire confirmé H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "47300",
        "commune": "47323",
        "latitude": 44.406581879,
        "libelle": "47 - VILLENEUVE SUR LOT",
        "longitude": 0.707830012
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1331783102?v=E59CDFFCD6D61832C2A5917C767FA28B936FB485&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642876"
      },
      "qualificationCode": "X",
      "romeCode": "C1206",
      "romeLibelle": "Gestion de clientèle bancaire",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Barman / Barmaid",
      "competences": [],
      "contact": {
        "coordonnees1": "Courriel : angersleb@gmail.com",
        "courriel": "angersleb@gmail.com",
        "nom": "TONTON FOCH - Mme Elodie Stephan"
      },
      "dateCreation": "2019-11-13T12:55:20+01:00",
      "description": "Tonton Foch recrute un(e) barman(maid) pour rejoindre l'équipe!\nQualité numéro 1 : le smile!\nProfil dynamique, organisé et professionnel.\nProduits de qualité, sourcing local et artisanal pour la bière, vin de petits producteurs, belle carte de cocktails classiques et créations.\nExpérience nécessaire de 6 mois au bar.\nPoste en CDI sur 39h. 2 jours de repos consécutifs. Prime sur objectifs.",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "TONTON FOCH",
        "url": "https://www.tontonfoch.com"
      },
      "experienceExige": "E",
      "experienceLibelle": "6 mois",
      "formations": [],
      "id": "095WKYD",
      "intitule": "Barman / Barmaid",
      "langues": [],
      "lieuTravail": {
        "codePostal": "49000",
        "commune": "49007",
        "latitude": 47.47277778,
        "libelle": "49 - ANGERS",
        "longitude": -0.555555555
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKYD"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        }
      ],
      "romeCode": "G1801",
      "romeLibelle": "Café, bar brasserie",
      "salaire": {
        "commentaire": "Selon profil et expérience",
        "complement1": "Selon profil et expérience"
      },
      "secteurActivite": "56",
      "secteurActiviteLibelle": "Restauration traditionnelle",
      "trancheEffectifEtab": "3 à 5 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "entreprise.aqu0105@pole-emploi.net"
      },
      "alternance": false,
      "appellationlibelle": "Chef de caisses",
      "competences": [
        {
          "code": "118069",
          "exigence": "S",
          "libelle": "Réaliser le comptage des fonds de caisses"
        }
      ],
      "contact": {
        "coordonnees1": "ZA DU HAUT LEVEQUE 15 AVENUE LEONARD DE VINCI\nCS 60015",
        "coordonnees2": "33608 PESSAC",
        "coordonnees3": "Courriel : entreprise.aqu0105@pole-emploi.net",
        "nom": "Pôle Emploi PESSAC"
      },
      "dateCreation": "2019-11-13T12:55:10+01:00",
      "description": "VOUS TRAVAILLEREZ POUR UNE MOYENNE SURFACE.\nVOUS ASSUREREZ LES ENCAISSEMENTS ET SEREZ POLYVALENT(E) SUR LA MISE EN RAYONS.\nVOUS SECONDEREZ LA DIRECTION SUR LES TACHES D'ORGANISATION DES CAISSES ET ASSUREREZ LES CLOTURES JOURNALIERES DE CAISSES ET DU MAGASIN.\n\nLE MAGASIN EST OUVERT DU LUNDI MATIN AU DIMANCHE MIDI .\nAMPLITUDE HORAIRES : 7H - 20H - ROULEMENTS PAR DEMI-JOURNEE + DIMANCHE MATIN.\n",
      "dureeTravailLibelle": "30H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps partiel",
      "experienceCommentaire": "EN CAISSE",
      "experienceExige": "E",
      "experienceLibelle": "1 an - EN CAISSE",
      "formations": [],
      "id": "095WKXZ",
      "intitule": "CHEF DE CAISSE  H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "33600",
        "commune": "33318",
        "latitude": 44.80583333,
        "libelle": "33 - PESSAC",
        "longitude": -0.632222221
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXZ"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "romeCode": "D1508",
      "romeLibelle": "Encadrement du personnel de caisses",
      "salaire": {
        "commentaire": "SELON EXPERIENCE",
        "complement1": "SELON EXPERIENCE",
        "libelle": "Horaire de 10,03 Euros"
      },
      "secteurActivite": "47",
      "secteurActiviteLibelle": "Supermarchés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Aide-soignant / Aide-soignante",
      "competences": [
        {
          "code": "107662",
          "exigence": "S",
          "libelle": "Accompagner la personne dans les gestes de la vie quotidienne"
        },
        {
          "code": "107660",
          "exigence": "S",
          "libelle": "Mesurer les paramètres vitaux du patient/résident, contrôler les dispositifs et appareillages médicaux et transmettre les informations à l'infirmier"
        },
        {
          "code": "107661",
          "exigence": "S",
          "libelle": "Réaliser des soins d'hygiène corporelle, de confort et de prévention"
        },
        {
          "code": "116410",
          "exigence": "S",
          "libelle": "Repérer les modifications d'état du patient"
        },
        {
          "code": "107659",
          "exigence": "S",
          "libelle": "Surveiller l'état général du patient/résident, lui distribuer les médicaments et informer l'infirmier des manifestations anormales ou des risques de chutes, escarres, ..."
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : recrutement@amreso-bethel.fr",
        "courriel": "recrutement@amreso-bethel.fr",
        "nom": "ASS AMIS MAIS RETR SOINS OBERHAUSBERGE - Mme RETAILLEAU"
      },
      "dateCreation": "2019-11-13T12:55:09+01:00",
      "description": "VOTRE MISSION  :\n- Prendre soin d'adultes handicapés ou victimes d'affections neuro-dégénératives ou accidentés de la vie, présentant une prise en charge diversifiée.\n- Participer activement à l'élaboration du projet de vie personnalisé, s'attacher à une prise en charge globale et individualisée en étant attentif aux besoins de chaque patient, en lien avec les autres intervenants au sein d'une équipe pluridisciplinaire\n- Accueillir, informer et accompagner les familles.\n- Favoriser et effectuer les transmissions ciblées (données, actions, résultats) et orales au sein de l'équipe.\n\nATOUTS POUR NOTRE PERSONNEL :\n- Matériel de travail moderne, adapté et facilitateur au quotidien \n- Plannings annualisés, possibilité de consultation des plannings à distance,\n- Cours de yoga, self-service disponible, salles de repos climatisées dédiées au personnel,\n- Comité d'entreprise avec de nombreux avantages culturels et sociaux\n- Formation tout au long de la carrière, possibilités de mobilité interne ",
      "dureeTravailLibelle": "35H Travail samedi et dimanche",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "La Maison de Santé Béthel, maison médicalisée située à Oberhausbergen, a pour mission d'accueillir et d'accompagner des personnes fragilisées tout au long de leur projet de vie et de leur parcours de soins. Plus de 300 collaborateurs passionnés et investis œuvrent au quotidien pour le bien-être de nos 333 patients et résidents. L'établissement est constitué en 4 Pôles d'activités : Pôle Personnes Âgées Dépendantes / EHPAD, Pôle SSR à orientation oncologique, Pôle Long Séjour, et Pôle Handicap.",
        "nom": "ASS AMIS MAIS RETR SOINS OBERHAUSBERGE"
      },
      "experienceCommentaire": "secteur du handicap",
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté - secteur du handicap",
      "formations": [
        {
          "codeFormation": "44004",
          "commentaire": "Aide soignant ou AMP",
          "domaineLibelle": "Aide médico-psychologique",
          "exigence": "E",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "095WKXY",
      "intitule": "Aide-Soignant au Pôle Handicap H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "67205",
        "commune": "67343",
        "latitude": 48.60666667,
        "libelle": "67 - OBERHAUSBERGEN",
        "longitude": 7.685277777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXY"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "J1501",
      "romeLibelle": "Soins d'hygiène, de confort du patient",
      "salaire": {
        "commentaire": "convention collective FEHAP 51",
        "complement1": "convention collective FEHAP 51"
      },
      "secteurActivite": "86",
      "secteurActiviteLibelle": "Activités hospitalières",
      "trancheEffectifEtab": "250 à 499 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Plongeur / Plongeuse en restauration",
      "competences": [
        {
          "code": "126380",
          "exigence": "S",
          "libelle": "Batterie de cuisine"
        },
        {
          "code": "124629",
          "exigence": "S",
          "libelle": "Entretenir un outil ou matériel"
        },
        {
          "code": "124548",
          "exigence": "S",
          "libelle": "Entretenir un poste de travail"
        },
        {
          "code": "104263",
          "exigence": "S",
          "libelle": "Essuyer et ranger la vaisselle, la verrerie, les ustensiles de cuisine"
        },
        {
          "code": "104262",
          "exigence": "S",
          "libelle": "Réaliser la plonge"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : ybattaglia2@edenis.fr",
        "courriel": "ybattaglia2@edenis.fr",
        "nom": "LE GRAND MARQUISAT - Mme Yolande BATTAGLIA"
      },
      "dateCreation": "2019-11-13T12:55:07+01:00",
      "description": "Travail en EHPAD, Service du petit déjeuner en chambre, aide au service de midi et du soir. Plonge du petit déjeuner, du repas de midi et du soir. Travail un week-end sur deux avec des jours de repos en semaine. Il est nécessaire que vous soyez disponible de 7h30 à  20h30. ",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Public = personnes âgées.\nEHPAD accueillant 80 résidents dont 18 en unité protégée.\nAssociation loi 1901 (18 résidences). Convention Collective Unique.",
        "nom": "LE GRAND MARQUISAT"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKXX",
      "intitule": "Plongeur / Plongeuse en restauration",
      "langues": [],
      "lieuTravail": {
        "codePostal": "31170",
        "commune": "31557",
        "latitude": 43.58444444,
        "libelle": "31 - TOURNEFEUILLE",
        "longitude": 1.34361111
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXX"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        }
      ],
      "romeCode": "G1605",
      "romeLibelle": "Plonge en restauration",
      "salaire": {
        "libelle": "Mensuel de 1526,50 Euros à 1526,50 Euros sur 12 mois"
      },
      "secteurActivite": "87",
      "secteurActiviteLibelle": "Hébergement médicalisé pour personnes âgées",
      "trancheEffectifEtab": "50 à 99 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 6 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": true,
      "appellationlibelle": "Cuisinier / Cuisinière",
      "competences": [
        {
          "code": "124919",
          "exigence": "S",
          "libelle": "Cuire des viandes, poissons ou légumes"
        },
        {
          "code": "124933",
          "exigence": "S",
          "libelle": "Doser des ingrédients culinaires"
        },
        {
          "code": "124947",
          "exigence": "S",
          "libelle": "Éplucher des légumes et des fruits"
        },
        {
          "code": "124934",
          "exigence": "S",
          "libelle": "Mélanger des produits et ingrédients culinaires"
        },
        {
          "code": "104207",
          "exigence": "S",
          "libelle": "Préparer les viandes et les poissons (brider, barder, vider, trancher, ...)"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : yvan.reboul@wanadoo.fr",
        "courriel": "yvan.reboul@wanadoo.fr",
        "nom": "NENNI MA FOI - M. YAN REBOUL"
      },
      "dateCreation": "2019-11-13T12:55:05+01:00",
      "description": "Vous souhaitez préparer votre CAP de CUISINIER par le biais de l'apprentissage - Vous sortez idéalement d'une 3 e des collèges ",
      "dureeTravailLibelle": "35H Autre",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "NENNI MA FOI"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "S",
          "niveauLibelle": "3ème achevée ou Brevet"
        }
      ],
      "id": "095WKXW",
      "intitule": "Cuisinier / Cuisinière",
      "langues": [],
      "lieuTravail": {
        "codePostal": "90000",
        "commune": "90010",
        "latitude": 47.6375,
        "libelle": "90 - BELFORT",
        "longitude": 6.862777777
      },
      "natureContrat": "Contrat apprentissage",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXW"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "romeCode": "G1602",
      "romeLibelle": "Personnel de cuisine",
      "salaire": {
        "commentaire": "% du smic suivant l'âge",
        "complement1": "% du smic suivant l'âge"
      },
      "secteurActivite": "56",
      "secteurActiviteLibelle": "Restauration traditionnelle",
      "trancheEffectifEtab": "3 à 5 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 24 Mois"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technicien / Technicienne d'entretien industriel",
      "competences": [],
      "dateActualisation": "2019-11-13T12:54:47+01:00",
      "dateCreation": "2019-11-13T12:54:46+01:00",
      "description": "DALKIA PMiG (Pilotage et Maintenance Intra-Groupe), filiale de Dalkia dédiée aux prestations de pilotage et de maintenance multi-techniques sur le périmètre tertiaire des sites industriels d'EDF, recrute un Technicien multiservices/Petit Entretien Technique H/F intervenant sur le Centre Nucléaire de Production d'Électricité de Cruas (07). (poste sédentaire) Sous la responsabilité du Responsable de site Multitechnique de Dalkia PMiG, vous effectuez les opérations suivantes : réalisation des petits travaux sur les installations de Plomberie et d'Electricité de proximité, suivre et mettre à jour votre activité opérationnelledans le logiciel outil de GMAO du site, assurer l'accompagnement des prestataires extérieurs, être force de proposition sur les solutions d'amélioration et/ou de mise aux normes des installations, respecter les modes opératoires des sites industriels d'EDF, être un acteur de la politique SANTE SECURITE de Dalkia PMiG dans la zone d'intervention. Profil Idéalement diplômé(e) d'un Bac pro ou d'un CAP/BEP dans les domaines de l'électricité, de la plomberie ou du CVC. Vous avez une expérience significative dans un poste similaire. Vous êtes capable de suivre et respecter les procédures de sécurité et de qualité. Vous avez déjà utilisé un logiciel de Gestion de la Maintenance Assistée par Ordinateur (GMAO). Vous êtes titulaire d'une habilitation électrique type : H0 - B0. Autonome et réactif (ve), vous avez un bon relationnel et le souci du service client. Vous êtes capable de vous adapter à toutes situations et proposer un service de qualité. Postulez Candidature spontanée Votre espace candidat FAQ recrutement",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3642839",
      "intitule": "Technicien Travaux de Petit Entretien Technique (PET) - H/F (Ref. 53613)",
      "langues": [],
      "lieuTravail": {
        "libelle": "France"
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1308127220?v=EC74DA737899C5C4DC194C07870E9C0FF978A95B&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642839"
      },
      "qualificationCode": "X",
      "romeCode": "I1310",
      "romeLibelle": "Maintenance mécanique industrielle",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Conseiller commercial / Conseillère commerciale en assurances",
      "competences": [],
      "dateActualisation": "2019-11-13T12:54:39+01:00",
      "dateCreation": "2019-11-13T12:54:38+01:00",
      "description": "Vous avez une 1ère expérience commerciale probante (vente, relation client). Vous êtes titulaire d'une formation de niveau Bac2/3 dans le domaine commercial ou vous avez un niveau similaire justifié par votre expérience professionnelle dans le domaine banque et/ou assurance. Votre force de conviction, vos capacités relationnelles, votre dynamisme, votre sens du service et votre disposition d'écoute active vous permettent de développer une relation client durable. Vous avez une excellente expression orale ainsi qu'une bonne pratique de l'outil informatique. Ce sont des atouts indispensables pour réussir votre mission. Vous détenez un sens développé du travail en équipe.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 1 An(s)",
      "formations": [],
      "id": "3642834",
      "intitule": "Conseiller commercial (F/H) (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "07200",
        "commune": "07019",
        "latitude": 44.571170807,
        "libelle": "07 - AUBENAS",
        "longitude": 4.392539978
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1318841194?v=70ABDE49A2AB25AB38882640062C926D5AC49908&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642834"
      },
      "qualificationCode": "X",
      "romeCode": "C1102",
      "romeLibelle": "Conseil clientèle en assurances",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Conseiller commercial / Conseillère commerciale en assurances",
      "competences": [],
      "dateActualisation": "2019-11-13T12:54:38+01:00",
      "dateCreation": "2019-11-13T12:54:38+01:00",
      "description": "Vous avez une 1ère expérience commerciale probante (vente, relation client). Vous êtes titulaire d'une formation de niveau Bac2/3 dans le domaine commercial ou vous avez un niveau similaire justifié par votre expérience professionnelle dans le domaine banque et/ou assurance. Votre force de conviction, vos capacités relationnelles, votre dynamisme, votre sens du service et votre disposition d'écoute active vous permettent de développer une relation client durable. Vous avez une excellente expression orale ainsi qu'une bonne pratique de l'outil informatique. Ce sont des atouts indispensables pour réussir votre mission. Vous détenez un sens développé du travail en équipe.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 1 An(s)",
      "formations": [],
      "id": "3642833",
      "intitule": "Conseiller commercial (F/H) (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "13104",
        "commune": "13004",
        "latitude": 43.676651001,
        "libelle": "13 - ARLES",
        "longitude": 4.627799988
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1318841193?v=79BE0F572C8FAD1B9473E3656F508097BDFFE68B&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642833"
      },
      "qualificationCode": "X",
      "romeCode": "C1102",
      "romeLibelle": "Conseil clientèle en assurances",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Conseiller commercial / Conseillère commerciale en assurances",
      "competences": [],
      "dateActualisation": "2019-11-13T12:54:36+01:00",
      "dateCreation": "2019-11-13T12:54:35+01:00",
      "description": "Profil : - Vous avez une 1ère expérience commerciale probante (vente, relation client). - Vous êtes titulaire d'une formation de niveau Bac2/3 dans le domaine commercial ou vous avez un niveau similaire justifié par votre expérience professionnelle dans le domaine banque et/ou assurance. - Votre force de conviction, vos capacités relationnelles, votre dynamisme, votre sens du service et votre disposition d'écoute active vous permettent de développer une relation client durable. - Vous avez une excellente expression orale ainsi qu'une bonne pratique de l'outil informatique. Ce sont des atouts indispensables pour réussir votre mission. - Vous détenez un sens développé du travail en équipe.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 1 An(s)",
      "formations": [],
      "id": "3642830",
      "intitule": "Conseiller commercial (F/H) (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "95200",
        "commune": "95585",
        "latitude": 48.996311188,
        "libelle": "95 - SARCELLES",
        "longitude": 2.379460096
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1310237005?v=B7B12A2FD7E68D83FADD10C204F8F279A8AD5457&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642830"
      },
      "qualificationCode": "X",
      "romeCode": "C1102",
      "romeLibelle": "Conseil clientèle en assurances",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Conseiller commercial / Conseillère commerciale en assurances",
      "competences": [],
      "dateActualisation": "2019-11-13T12:54:31+01:00",
      "dateCreation": "2019-11-13T12:54:31+01:00",
      "description": "Profil : - Vous avez une 1ère expérience commerciale probante (vente, relation client). - Vous êtes titulaire d'une formation de niveau Bac2/3 dans le domaine commercial ou vous avez un niveau similaire justifié par votre expérience professionnelle dans le domaine banque et/ou assurance. - Votre force de conviction, vos capacités relationnelles, votre dynamisme, votre sens du service et votre disposition d'écoute active vous permettent de développer une relation client durable. - Vous avez une excellente expression orale ainsi qu'une bonne pratique de l'outil informatique. Ce sont des atouts indispensables pour réussir votre mission.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 1 An(s)",
      "formations": [],
      "id": "3642826",
      "intitule": "Conseiller commercial (F/H) (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "30200",
        "commune": "30028",
        "latitude": 44.196399689,
        "libelle": "30 - BAGNOLS SUR CEZE",
        "longitude": 4.658239841
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1278469144?v=89A35AC06FBAEF53C30A4866424F864C994A934D&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642826"
      },
      "qualificationCode": "X",
      "romeCode": "C1102",
      "romeLibelle": "Conseil clientèle en assurances",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Attaché commercial / Attachée commerciale en immobilier",
      "competences": [],
      "dateActualisation": "2019-11-13T12:54:30+01:00",
      "dateCreation": "2019-11-13T12:54:30+01:00",
      "description": "Le poste :Que vous soyez expérimenté ou débutant en immobilier, si vous avez la fibre commerciale, votre profil nous intéresse SAFTI vous forme et vous accompagne dans votre projet.Vous travaillez depuis chez vous.Votre activité se concentre essentiellement sur le terrain : de la prise de mandat à la signature de l'acte de vente chez le notaire.Vous bénéficiez d'une formation sur-mesure et d'un accompagnement personnalisé tout au long de votre activité. Nous vous garantissons la mise à disposition d'outils exclusifs et innovants pour atteindre vos objectifs de réussite.SAFTI vous propose une rémunération avantageuse : de 70 à 99% des honoraires d'agence sur les transactions réalisées. Vous pouvez également développer une équipe et percevoir des revenus additionnels.Exigences :Vous souhaitez conjuguer indépendance, professionnalisme et proximité ? Débutant ou expérimenté en immobilier, SAFTI s'adapte à vos exigences et accompagne votre projet.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3642824",
      "intitule": "Commercial Immobilier (M/F) (H/F)",
      "langues": [],
      "lieuTravail": {
        "commune": "75000",
        "latitude": 48.863838196,
        "libelle": "75 - Paris (Dept.)",
        "longitude": 2.344630957
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333863903?v=D6E9C26402DB4D2BED221D0F1A32638B197487D5&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642824"
      },
      "qualificationCode": "X",
      "romeCode": "C1504",
      "romeLibelle": "Transaction immobilière",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Responsable comptabilité",
      "competences": [],
      "dateActualisation": "2019-11-13T12:54:19+01:00",
      "dateCreation": "2019-11-13T12:54:19+01:00",
      "description": "Dans les grandes lignes, vous assurez le suivi au quotidien des dossiers de nos clients, dont vous avez la responsabilité opérationnelle. Dans le cadre de notre développement et rattaché(e) au Manager, vous intervenez sur un portefeuille à formes juridiques et secteurs d'activités variés, avec une large autonomie dans vos travaux. Vous avez pour missions principales : - l'élaboration des projets d'arrêtés de comptes, - la réalisation des déclarations fiscales, - la préparation du bilan imagé, - la réalisation de missions exceptionnelles. Vous aimez le contact avec la clientèle ? Tant mieux, car nous comptons sur vous pour assurer du conseil auprès de nos clients et être à leur écoute pour les accompagner dans leur développement.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3642818",
      "intitule": "Responsable de Dossiers Comptable H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "25300",
        "commune": "25462",
        "latitude": 46.939350128,
        "libelle": "25 - PONTARLIER",
        "longitude": 6.321859837
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1326159760?v=5DD5704C6A7C0041C55FCD7DA6F7A933D6102F34&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642818"
      },
      "qualificationCode": "X",
      "romeCode": "M1206",
      "romeLibelle": "Management de groupe ou de service comptable",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "ape.13165@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Métallier-serrurier industriel / Métallière-serrurière industrielle",
      "competences": [
        {
          "code": "106161",
          "exigence": "S",
          "libelle": "Former les matériaux par usinage, modelage, découpage, formage, sculpture, ..."
        },
        {
          "code": "123402",
          "exigence": "S",
          "libelle": "Installer des éléments de structures métalliques"
        },
        {
          "code": "120277",
          "exigence": "S",
          "libelle": "Régler les paramètres des machines et des équipements"
        },
        {
          "code": "106214",
          "exigence": "S",
          "libelle": "Reporter les cotes et mesures sur les matériaux et effectuer les tracés"
        }
      ],
      "contact": {
        "commentaire": "sous référence FBTP13",
        "coordonnees1": "88 RUE DE LA REPUBLIQUE",
        "coordonnees2": "13400 AUBAGNE",
        "coordonnees3": "Courriel : ape.13165@pole-emploi.fr",
        "nom": "Pôle Emploi AUBAGNE"
      },
      "dateCreation": "2019-11-13T12:54:11+01:00",
      "description": "Respect du port des EPI\n Bonne gestion du temps et respect des délais imposés\n Soucieux des règles de sécurité\n Soucieux de l'entretien du matériel, des véhicules et engins dont il a la responsabilité\n Montage d'auvents et structures métalliques\nTravaux de soudure pour réparation\nPetits travaux de 2nd œuvre apprécié\nRespect du règlement et de sa hiérarchie",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "experienceExige": "E",
      "experienceLibelle": "3 ans",
      "formations": [],
      "id": "095WKXR",
      "intitule": "Métallier-serrurier industriel / Métallière-serrurière industrielle",
      "langues": [],
      "lieuTravail": {
        "codePostal": "13400",
        "commune": "13005",
        "latitude": 43.29083333,
        "libelle": "13 - AUBAGNE",
        "longitude": 5.570833332
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXR"
      },
      "permis": [
        {
          "exigence": "E",
          "libelle": "B - Véhicule léger Exigé"
        },
        {
          "exigence": "S",
          "libelle": "C - Poids lourd Souhaité"
        }
      ],
      "qualificationCode": "4",
      "qualificationLibelle": "Ouvrier qualifié (P3,P4,OHQ)",
      "romeCode": "H2911",
      "romeLibelle": "Réalisation de structures métalliques",
      "salaire": {
        "libelle": "Mensuel de 1800 Euros sur 12 mois"
      },
      "secteurActivite": "43",
      "secteurActiviteLibelle": "Travaux de maçonnerie générale et gros oeuvre de bâtiment",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "ape.31015@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Vendeur / Vendeuse en boulangerie-pâtisserie",
      "competences": [
        {
          "code": "120130",
          "exigence": "S",
          "libelle": "Accueillir une clientèle"
        },
        {
          "code": "123793",
          "exigence": "S",
          "libelle": "Contrôler l'état de conservation d'un produit périssable"
        },
        {
          "code": "125971",
          "exigence": "S",
          "libelle": "Définir des besoins en approvisionnement"
        },
        {
          "code": "117532",
          "exigence": "S",
          "libelle": "Disposer des produits sur le lieu de vente"
        },
        {
          "code": "124545",
          "exigence": "S",
          "libelle": "Entretenir un espace de vente"
        },
        {
          "code": "124548",
          "exigence": "S",
          "libelle": "Entretenir un poste de travail"
        },
        {
          "code": "119000",
          "exigence": "S",
          "libelle": "Gestion des stocks et des approvisionnements"
        },
        {
          "code": "124547",
          "exigence": "S",
          "libelle": "Nettoyer du matériel ou un équipement"
        },
        {
          "code": "119860",
          "exigence": "S",
          "libelle": "Préparer les commandes"
        },
        {
          "code": "121698",
          "exigence": "S",
          "libelle": "Proposer un service, produit adapté à la demande client"
        },
        {
          "code": "126444",
          "exigence": "S",
          "libelle": "Ranger du matériel"
        },
        {
          "code": "123792",
          "exigence": "S",
          "libelle": "Retirer un produit impropre à la vente"
        },
        {
          "code": "116932",
          "exigence": "S",
          "libelle": "Suivre l'état des stocks"
        },
        {
          "code": "100340",
          "exigence": "S",
          "libelle": "Techniques de vente"
        }
      ],
      "complementExercice": "travail le dimanche matin",
      "contact": {
        "coordonnees1": "25 BD VICTOR HUGO\nCS 70334",
        "coordonnees2": "31776 COLOMIERS",
        "coordonnees3": "Courriel : ape.31015@pole-emploi.fr",
        "nom": "Pôle Emploi COLOMIERS"
      },
      "dateCreation": "2019-11-13T12:54:05+01:00",
      "description": "URGENT Boulangerie Traditionnelle recherche vendeur/Vendeuse.\n\nVous aurez en charge la vente du pain, des pâtisseries et de la restauration rapide.\nVous travaillerez du Lundi au vendredi de 11h30 à 14h30 et le dimanche de 8h30 à 13h30 avec 1 jour de repos Le samedi\n\nUne expérience significative de vente en boulangerie serait un plus.\nSalaire en fonction du profil. Mutuelle entreprise.\n\nVous présenter à la Boulangerie avec un CV, idéalement entre 11H et 15H.",
      "dureeTravailLibelle": "20H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps partiel",
      "entreprise": {
        "description": "Boulangerie traditionnelle",
        "nom": "LA FOUGASSE"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKXP",
      "intitule": "Vendeur boulangerie-pâtisserie-restauration rapide rapide H/F 20H",
      "langues": [],
      "lieuTravail": {
        "codePostal": "31770",
        "commune": "31149",
        "latitude": 43.61277778,
        "libelle": "31 - COLOMIERS",
        "longitude": 1.335833333
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXP"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "romeCode": "D1106",
      "romeLibelle": "Vente en alimentation",
      "salaire": {
        "complement1": "Mutuelle",
        "libelle": "Horaire de 10,03 Euros"
      },
      "secteurActivite": "10",
      "secteurActiviteLibelle": "Boulangerie et boulangerie-pâtisserie",
      "trancheEffectifEtab": "10 à 19 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Chef comptable",
      "competences": [
        {
          "code": "101573",
          "exigence": "S",
          "libelle": "Analyse comptable et financière"
        },
        {
          "code": "101360",
          "exigence": "S",
          "libelle": "Comptabilité générale"
        },
        {
          "code": "126334",
          "exigence": "S",
          "libelle": "Comptabilité tiers"
        },
        {
          "code": "117986",
          "exigence": "S",
          "libelle": "Concevoir un tableau de bord"
        },
        {
          "code": "120708",
          "exigence": "S",
          "libelle": "Établir des déclarations fiscales et sociales"
        },
        {
          "code": "121739",
          "exigence": "S",
          "libelle": "Réaliser un bilan comptable"
        },
        {
          "code": "124377",
          "exigence": "S",
          "libelle": "Réaliser une gestion administrative"
        },
        {
          "code": "124318",
          "exigence": "S",
          "libelle": "Réaliser une gestion comptable"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : aseigneuret@cfnord.com",
        "courriel": "aseigneuret@cfnord.com",
        "nom": "CAP FACE NORD - Mme Allison SEIGNEURET"
      },
      "dateCreation": "2019-11-13T12:53:58+01:00",
      "description": "CAP FACE NORD, cabinet de recrutement et de chasse, présent à Aix, Lyon et à Paris, propose des offres d'emploi en comptabilité, finance et ressources humaines sur le territoire national.\nA propos de notre client : Notre client, cabinet d'expertise comptable de taille intermédiaire, situé à Nîmes, recrute un Chef de Mission en CDI.\n\nPoste\n\nAu sein de ce cabinet structuré, vous aurez pour mission d'assurer le suivi de votre portefeuille client.\nVous assurerez la responsabilité opérationnelle (réaliser les déclarations, préparer les bilans, ...) et superviserez vos assistants.\n\nEn véritable caméléon, vous gérerez également la relation client en les renseignant au mieux et en étant à l'écoute des opportunités commerciales. \n\nVotre rigueur vous permettra d'assurer le suivi administratif.\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "CAP FACE NORD, cabinet de recrutement et de chasse, présent à Aix-en-Provence, Lyon, et Paris, propose des offres d'emploi en comptabilité, finance et ressources humaines sur le territoire national.",
        "nom": "CAP FACE NORD",
        "url": "http://www.cfnord.com"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WKXN",
      "intitule": "Chef comptable",
      "langues": [],
      "lieuTravail": {
        "codePostal": "30000",
        "commune": "30189",
        "latitude": 43.83694444,
        "libelle": "30 - NIMES",
        "longitude": 4.36
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXN"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à faire des choix pour agir, à prendre en charge son activité et à rendre compte, sans devoir être encadré de façon continue. Exemple : savoir faire des choix",
          "libelle": "Capacité de décision"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        }
      ],
      "romeCode": "M1206",
      "romeLibelle": "Management de groupe ou de service comptable",
      "salaire": {
        "libelle": "Annuel de 28000,00 Euros à 29000,00 Euros sur 12 mois"
      },
      "secteurActivite": "70",
      "secteurActiviteLibelle": "Conseil pour les affaires et autres conseils de gestion",
      "trancheEffectifEtab": "1 ou 2 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Chef de service logistique",
      "competences": [
        {
          "code": "119858",
          "exigence": "S",
          "libelle": "Contrôler la réception des commandes"
        },
        {
          "code": "123552",
          "exigence": "S",
          "libelle": "Contrôler une opération logistique"
        },
        {
          "code": "124009",
          "exigence": "S",
          "libelle": "Coordonner des opérations"
        },
        {
          "code": "120648",
          "exigence": "S",
          "libelle": "Planifier l'activité du personnel"
        },
        {
          "code": "124578",
          "exigence": "S",
          "libelle": "Planifier le traitement des commandes"
        }
      ],
      "contact": {
        "coordonnees1": "https://jobs.smartrecruiters.com/Eurofins/743999699410848-responsable-service-logistique-h-f-cdi",
        "nom": "EUROFINS BIOFFICE - Mme GERALDINE CHEMIN",
        "urlPostulation": "https://jobs.smartrecruiters.com/Eurofins/743999699410848-responsable-service-logistique-h-f-cdi"
      },
      "dateCreation": "2019-11-13T12:53:52+01:00",
      "description": "Nous recherchons pour notre service logistique, un manager pour organiser, améliorer et développer nos activités de réception, stockage et distribution de produits pharmaceutiques. Au sein d'une unité en forte croissance et au croisement des différentes équipes de la société, vous serez sous la responsabilité du Directeur de la Business Unit Clinical Trial Supply.\n\nVous aurez en charge le management de l'équipe logistique, la gestion et la modernisation des activités logistiques ainsi que la proposition et la mise en place de nouveaux services de supply chain.\n\nPlus particulièrement, les missions sont :\n\n- Gestion et amélioration des activités logistiques:\n\nCoordonner et organiser les flux de produits inter services\nCoordonner optimiser les activités de réception et de distribution\nAssurer la bonne gestion des stocks et leur traçabilité\nAméliorer les outils, évaluer les besoins et proposer de nouveaux services\n\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Avec plus de 35.000 collaborateurs dans 800 laboratoires et 47 pays, Eurofins fournit des prestations d'analyse aux industries pharmaceutiques, alimentaires et dans le domaine de l'environnement. A la pointe des derniers développements en biotechnologie, la société en forte croissance, leader dans son domaine d'activité, est cotée à la Bourse de Paris.",
        "nom": "EUROFINS BIOFFICE",
        "url": "https://www.eurofins.com"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKXM",
      "intitule": "RESPONSABLE SERVICE LOGISTIQUE (H/F) - CDI",
      "langues": [],
      "lieuTravail": {
        "codePostal": "34731",
        "commune": "34255",
        "latitude": 43.69222222,
        "libelle": "34 - ST GELY DU FESC",
        "longitude": 3.806111111
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXM"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "N1303",
      "romeLibelle": "Intervention technique d'exploitation logistique",
      "salaire": {
        "commentaire": "compétitive",
        "complement1": "compétitive"
      },
      "secteurActivite": "86",
      "secteurActiviteLibelle": "Laboratoires d'analyses médicales",
      "trancheEffectifEtab": "20 à 49 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Développeur / Développeuse full-stack",
      "competences": [],
      "dateActualisation": "2019-11-13T12:53:48+01:00",
      "dateCreation": "2019-11-13T12:53:47+01:00",
      "description": "Sous la responsabilité du Directeur des Systèmes d'Information et au sein d'une équipe dynamique, vous concevez et développez les applicatifs logiciels en mode projet sur l'ensemble du système d'information du Groupe pour un client ou une entreprise. Pour ce faire, vous : Analysez et modélisez les besoins utilisateurs, Développez les applicatifs web en C# .net / .net core, Angular et HTML 5 / JavaScript, Maintenez et faîtes évoluer les applicatifs au fil de l'évolution des besoins, Réalisez des revues de code croisées avec les autres développeurs de l'équipe, Participez à l'intégration continue avec la plateforme Azure for Devops et au déploiement sur les différents environnements de recette, de tests & de production, Accompagnez la recette et traitez les retours (bug fixes), Formez les utilisateurs, Assurez le support utilisateur de niveau 3 (expertise technique), Participez à la veille technologique. Issu(e) d'une formation supérieure en informatique, DUT/BTS, école d'ingénieur ou université, vous justifiez d'une expérience significative dans le développement d'applications web et d'API en méthodologie Agile.Polyvalent(e), vous maitrisez les bonnes pratiques de développement (SOLID, IoC, ) et les technologies web ou objet telles que C#, .Net, SQL Server (Procédures stockées, Transact SQL), Javascript, HTML 5, CSS, Web Services, ASP .net web API, ASP .net MVC, WCF, REST / Json, Design Patterns.Doté(e) de grandes qualités de communication et d'analyse, vous prenez en charge les projets de façon itérative en cycles courts dans une démarche Agile.Curieux(se), vous êtes force de proposition et reconnu(e) pour votre pragmatisme et votre disponibilité. De plus, vous maitrisez l'anglais aussi bien à l'oral qu'à l'écrit.Vous vous reconnaissez, vous avez envie de travailler dans une équipe à taille humaine en mode projet avec des personnes passionnées par leur métier Nous vous attendons Avec plus de 1100 personnes, 55 agences en Europe, 5500 partenaires dans le monde et 202 millions d'euros de chiffre d'affaires, le Groupe Sterne est un acteur majeur du transport à forte valeur ajoutée. De la course urbaine au transport express et international, le Groupe Sterne fonde sa stratégie sur sa capacité à apporter à ses clients une prestation sur-mesure, répondant à leurs besoins et enjeux. Dans le cadre de la refonte complète du système d'information et de la digitalisation des processus des entreprises du Groupe, nous recrutons des Développeurs Full Stack H/F pour les sites de Bordeaux (33), Saint-Ouen l'Aumône (95) et Chilly-Mazarin (91).",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3642791",
      "intitule": "Développeur Full Stack C# .Net (95) - H/F",
      "langues": [
        {
          "exigence": "E",
          "libelle": "Anglais"
        }
      ],
      "lieuTravail": {
        "latitude": 48.996456146,
        "libelle": "95 - Val d'Oise",
        "longitude": 2.241127014
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865967?v=FA1F4E05CEDF4C9F8C41A11635C4ADFE0A697319&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642791"
      },
      "qualificationCode": "X",
      "romeCode": "M1805",
      "romeLibelle": "Études et développement informatique",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Attaché commercial / Attachée commerciale en immobilier",
      "competences": [],
      "dateActualisation": "2019-11-13T12:53:46+01:00",
      "dateCreation": "2019-11-13T12:53:46+01:00",
      "description": "Le poste :Que vous soyez expérimenté ou débutant en immobilier, si vous avez la fibre commerciale, votre profil nous intéresse SAFTI vous forme et vous accompagne dans votre projet.Vous travaillez depuis chez vous.Votre activité se concentre essentiellement sur le terrain : de la prise de mandat à la signature de l'acte de vente chez le notaire.Vous bénéficiez d'une formation sur-mesure et d'un accompagnement personnalisé tout au long de votre activité. Nous vous garantissons la mise à disposition d'outils exclusifs et innovants pour atteindre vos objectifs de réussite.SAFTI vous propose une rémunération avantageuse : de 70 à 99% des honoraires d'agence sur les transactions réalisées. Vous pouvez également développer une équipe et percevoir des revenus additionnels.Exigences :Vous souhaitez conjuguer indépendance, professionnalisme et proximité ? Débutant ou expérimenté en immobilier, SAFTI s'adapte à vos exigences et accompagne votre projet.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3642789",
      "intitule": "Commercial Immobilier (M/F) (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 43.40819931,
        "libelle": "31 - Garonne (Haute)",
        "longitude": 1.138939977
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333863820?v=B16167439337421302BE6033BECCBAE87BF1E399&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642789"
      },
      "qualificationCode": "X",
      "romeCode": "C1504",
      "romeLibelle": "Transaction immobilière",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Responsable administratif et financier / administrative et financière",
      "competences": [],
      "dateActualisation": "2019-11-13T12:53:37+01:00",
      "dateCreation": "2019-11-13T12:53:37+01:00",
      "description": "Idéalement diplômé(e) d'un Bac3/5 en école de commerce ou expertise-comptable, vous avez au moins 5 ans d'expérience professionnelle sur un poste similaire. Vous êtes reconnu(e) pour vos qualités de rigueur, d'esprit d'équipe, management participatif. La maîtrise de l'Anglais est impérative : lu, écrit, parlé. Si vous souhaitez en savoir plus sur le poste et mon client \"postulez\" je me ferai une joie de répondre à toutes vos interrogations  Si vous ne répondez pas à tous ces critères, transmettez-moi votre candidature qui sera étudiée avec attention et en toute confidentialité. Contactez-moi au 03 28 14 17 14, par mail à sarahmoraschettifedfinance.fr ou via mon profil https://www.linkedin.com/in/sarah-moraschetti-6533a4129/",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 5 An(s)",
      "formations": [],
      "id": "3642778",
      "intitule": "Adjoint Responsable Administratif et Financier F/H (H/F)",
      "langues": [
        {
          "exigence": "E",
          "libelle": "Anglais"
        }
      ],
      "lieuTravail": {
        "libelle": "France"
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1331782776?v=5E3B87EE782E9D7A5DBE4611E006FF83D4A3B1E0&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642778"
      },
      "qualificationCode": "X",
      "romeCode": "M1205",
      "romeLibelle": "Direction administrative et financière",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Couvreur zingueur / Couvreuse zingueuse",
      "competences": [
        {
          "code": "122128",
          "exigence": "E",
          "libelle": "Déposer une toiture"
        },
        {
          "code": "103655",
          "exigence": "E",
          "libelle": "Poser des ardoises"
        },
        {
          "code": "122208",
          "exigence": "E",
          "libelle": "Poser des tuiles"
        },
        {
          "code": "103797",
          "exigence": "E",
          "libelle": "Réaliser la pose d'éléments de couverture"
        },
        {
          "code": "122129",
          "exigence": "S",
          "libelle": "Remplacer des chevrons"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : lille2@welljob.fr",
        "courriel": "lille2@welljob.fr",
        "nom": "WELL JOB - Mme Céline  GANDILLET"
      },
      "dateCreation": "2019-11-13T12:53:36+01:00",
      "description": "L'agence Welljob Emploi Lille recherche des couvreurs zingueurs H/F.\n\nVos missions :\n- Dresser l'inventaire des matériaux et outils nécessaires et les acheminer sur le chantier,\n- S'assurer de la sécurité du chantier,\n- Prévoir la gestion des gravats,\n- Déposer la couverture ancienne le cas échéant,\n- Poser les matériaux de couverture,\n- Façonner les pièces métalliques, découper et poser les matériaux après traçage,\n- Disposer les éléments de couverture et les dispositifs des évacuations des eaux pluviales,\n\nVotre profil :\nNiveau 2 ou 3\nAutonome et rigoureux sur la pose et dépose de couverture\nTitulaire d'un diplôme en couverture",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "AGENCE WELLJOB EMPLOI DE LILLE \n54BIS BOULEVARD DE LA LIBERTE\n59800 LILLE",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/aJRrFYgRrRAI0d6wyXUPFevjCOGo2rhH.png",
        "nom": "WELL JOB",
        "url": "https://www.welljob.fr/entreprise"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WKXK",
      "intitule": "Couvreur zingueur / Couvreuse zingueuse        (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "59000",
        "commune": "59350",
        "latitude": 50.63194444,
        "libelle": "59 - LILLE",
        "longitude": 3.0575
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXK"
      },
      "qualificationCode": "3",
      "qualificationLibelle": "Ouvrier qualifié (P1,P2)",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "F1610",
      "romeLibelle": "Pose et restauration de couvertures",
      "salaire": {
        "complement1": "Primes",
        "libelle": "Mensuel de 1530,00 Euros à 2200,00 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 8 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Plongeur / Plongeuse en restauration",
      "competences": [
        {
          "code": "126380",
          "exigence": "S",
          "libelle": "Batterie de cuisine"
        },
        {
          "code": "124629",
          "exigence": "S",
          "libelle": "Entretenir un outil ou matériel"
        },
        {
          "code": "124548",
          "exigence": "S",
          "libelle": "Entretenir un poste de travail"
        },
        {
          "code": "104263",
          "exigence": "S",
          "libelle": "Essuyer et ranger la vaisselle, la verrerie, les ustensiles de cuisine"
        },
        {
          "code": "104262",
          "exigence": "S",
          "libelle": "Réaliser la plonge"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : adjoint.gambetta@orpea.net",
        "courriel": "adjoint.gambetta@orpea.net",
        "nom": "RESIDENCE GAMBETTA - Mme Laure OLLIER"
      },
      "dateCreation": "2019-11-13T12:53:36+01:00",
      "description": "Sous la responsabilité du Chef de cuisine, vous aurez pour missions principales :\n-\tRespecter les normes et procédures d'hygiène (méthode H.A.C.C.P.)\n-\tProcéder au lavage de la vaisselle et des ustensiles de cuisine\n-\tNettoyer les matériels (dont le lave-vaisselle) dans le respect des procédures définies\n-\tDébarrasser et nettoyer les charriots de service et de distribution des repas\n-\tS'assurer du bon fonctionnement de la machine à laver la vaisselle et de son entretien dans le respect des protocoles de nettoyage \n\nProfil :\n-\tVous détenez l'esprit de service et des qualités relationnelles\n-\tVous possédez une bonne maitrise des règles HACCP\n\nType d'emploi : CDD\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Située au cœur de la ville de Lyon, la Maison de retraite ORPEA Gambetta, accueille des personnes âgées autonomes, semi-valides, dépendantes et désorientées ou atteintes de la maladie d'Alzheimer (et pathologies apparentées) dans deux unités de vie protégées.",
        "nom": "RESIDENCE GAMBETTA",
        "url": "https://www.orpea.com"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKXJ",
      "intitule": "Plongeur / Plongeuse en restauration",
      "langues": [],
      "lieuTravail": {
        "codePostal": "69007",
        "commune": "69387",
        "latitude": 45.73083333,
        "libelle": "69 - LYON 07",
        "longitude": 4.840277777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXJ"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "romeCode": "G1605",
      "romeLibelle": "Plonge en restauration",
      "salaire": {
        "libelle": "Mensuel de 1522,00 Euros à 1600,00 Euros sur 12 mois"
      },
      "secteurActivite": "87",
      "secteurActiviteLibelle": "Hébergement médicalisé pour personnes âgées",
      "trancheEffectifEtab": "50 à 99 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 35 Mois"
    },
    {
      "alternance": false,
      "appellationlibelle": "Directeur / Directrice de supermarché",
      "competences": [],
      "dateActualisation": "2019-11-13T12:53:32+01:00",
      "dateCreation": "2019-11-13T12:53:32+01:00",
      "description": "Chez Schiever, nos directeurs de magasin sont des managers participatifs, des gestionnaires hors pair et des commerçants créatifs. Concrètement, qu'est-ce que cela signifie ?Manager participatif : votre réussite passe par celle de vos collaborateursAvec une équipe à taille humaine, vous savez être fédérateur, être à l'écoute de vos équipes, accompagner et former les managers de demainGestionnaire hors pair : votre agilité avec les chiffres fait de vous un expert en gestionAcrobate du compte d'exploitation, agile dans l'analyse chiffrée, as de la stratégie : en clair, vous avez la complète maîtrise des comptes d'exploitation de votre point de vente, du chiffre d'affaires aux frais de personnel.Commerçant créatif : votre leitmotiv c'est d'être un facilitateur de vieAmbassadeur du bien-manger, vous participez à la mise en place de l'animation et de la théâtralisation. Innovant et enthousiaste, vous contribuez à développer une autre idée de la grande distribution.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3642775",
      "intitule": "Directeur de supermarché H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "58800",
        "commune": "58083",
        "latitude": 47.232528687,
        "libelle": "58 - CORBIGNY",
        "longitude": 3.823549986
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1329269918?v=527B2ADDE82D8D800DC2683FE8F8F51CB261A8B6&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642775"
      },
      "qualificationCode": "X",
      "romeCode": "D1504",
      "romeLibelle": "Direction de magasin de grande distribution",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "ape.92053@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Employé / Employée de libre-service",
      "competences": [
        {
          "code": "121228",
          "exigence": "S",
          "libelle": "Contrôler le balisage et l'étiquetage des produits en rayon"
        },
        {
          "code": "125971",
          "exigence": "S",
          "libelle": "Définir des besoins en approvisionnement"
        },
        {
          "code": "117532",
          "exigence": "S",
          "libelle": "Disposer des produits sur le lieu de vente"
        },
        {
          "code": "119860",
          "exigence": "S",
          "libelle": "Préparer les commandes"
        },
        {
          "code": "121230",
          "exigence": "S",
          "libelle": "Réaliser le balisage et l'étiquetage des produits en rayon"
        },
        {
          "code": "117528",
          "exigence": "S",
          "libelle": "Réaliser un inventaire"
        },
        {
          "code": "117547",
          "exigence": "S",
          "libelle": "Réceptionner un produit"
        },
        {
          "code": "116932",
          "exigence": "S",
          "libelle": "Suivre l'état des stocks"
        },
        {
          "code": "126124",
          "exigence": "S",
          "libelle": "Utilisation d'appareils de lecture optique de codes-barres (pistolet, flasheur, ...)"
        },
        {
          "code": "119851",
          "exigence": "S",
          "libelle": "Vérifier la conformité de la livraison"
        }
      ],
      "contact": {
        "coordonnees1": "17 RUE du Président Kruger",
        "coordonnees2": "92400 COURBEVOIE",
        "coordonnees3": "Courriel : ape.92053@pole-emploi.fr",
        "nom": "Pôle Emploi Courbevoie"
      },
      "dateCreation": "2019-11-13T12:53:30+01:00",
      "description": "Vos principales missions :\n* effectuer la mise en rayon des produits frais\n*  contrôler la conformité de votre rayon en terme de balisage, de propreté, \n* gérer les  stocks .\n* passer des commandes\n* réaliser un inventaire.\n\n                                                      ******* Prise de poste à 05h30 le matin *******************\n\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "VALTHILDE"
      },
      "experienceCommentaire": "comme ELS dans grande distribution",
      "experienceExige": "E",
      "experienceLibelle": "6 mois - comme ELS dans grande distribution",
      "formations": [],
      "id": "095WKXH",
      "intitule": "Employé libre service rayon frais H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "92250",
        "commune": "92035",
        "latitude": 48.905,
        "libelle": "92 - LA GARENNE COLOMBES",
        "longitude": 2.24361111
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXH"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "D1507",
      "romeLibelle": "Mise en rayon libre-service",
      "salaire": {
        "complement1": "Primes",
        "libelle": "Mensuel de 1600,00 Euros à 1700,00 Euros sur 12 mois"
      },
      "secteurActivite": "47",
      "secteurActiviteLibelle": "Supermarchés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Responsable comptabilité",
      "competences": [],
      "dateActualisation": "2019-11-13T12:53:27+01:00",
      "dateCreation": "2019-11-13T12:53:27+01:00",
      "description": "Dans les grandes lignes, vous assurez le suivi au quotidien des dossiers de nos clients, dont vous avez la responsabilité opérationnelle. Dans le cadre de notre développement et rattaché(e) au Directeur d'agence, vous intervenez sur un portefeuille à formes juridiques et secteurs d'activités variés, avec une large autonomie dans vos travaux. Vous avez pour missions principales : - l'élaboration des projets d'arrêtés de comptes, - la réalisation des déclarations fiscales, - la préparation du bilan imagé, - la réalisation de missions exceptionnelles. Vous aimez le contact avec la clientèle ? Tant mieux, car nous comptons sur vous pour assurer du conseil auprès de nos clients et être à leur écoute pour les accompagner dans leur développement.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3642774",
      "intitule": "Responsable de Dossiers Comptable H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "21000",
        "commune": "21231",
        "latitude": 47.327209473,
        "libelle": "21 - DIJON",
        "longitude": 5.043990135
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1326159761?v=604D9A5721D24ADD62987B761DAA5DE1F87D4FCB&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642774"
      },
      "qualificationCode": "X",
      "romeCode": "M1206",
      "romeLibelle": "Management de groupe ou de service comptable",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Assistant commercial / Assistante commerciale",
      "competences": [
        {
          "exigence": "E",
          "libelle": "bon contact"
        },
        {
          "exigence": "E",
          "libelle": "capacité d'adaptation et de travail en équipe"
        },
        {
          "exigence": "E",
          "libelle": "qualités rédactionnelles"
        },
        {
          "exigence": "E",
          "libelle": "réactivité"
        },
        {
          "exigence": "E",
          "libelle": "rigueur, organisation"
        },
        {
          "exigence": "S",
          "libelle": "bonne humeur"
        },
        {
          "exigence": "S",
          "libelle": "expérience dans une structure sportive de football"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : candidature@staderennais.fr",
        "courriel": "candidature@staderennais.fr",
        "nom": "SRFC - Mme Elodie Crocq"
      },
      "dateCreation": "2019-11-13T12:53:14+01:00",
      "description": "Descriptif du poste\n\nAdministration des ventes avec suivi complet du back office & des commandes en relation directe avec tous les services de la société : bon de commande, facturation, relance client\n\nAssistanat du directeur commercial :\n-Préparation, mise en forme & envoi des propositions commerciales\n-Suivi administratif des contrats commerciaux (clients fournisseurs, prospect  )\n-Préparation & suivi des calendriers & plannings du personnel du service en relation avec la direction juridique & RH\n-Collaboration aux travaux concernant la création d'outils, de fichiers servant à la commercialisation des produits existants & à venir\n\nProfil requis :\n-3 ans d'expérience dans un service commercial\n-Maîtrise du pack office & autres logiciels spécialisés\n-Connaissances en CRM & en data\n-Anglais\n-Diplôme Bac +2 à 5 (IUT GEA, BTS action co, école de commerce )\n\nAutres :\n-CDI\n-rattaché.e au directeur commercial et marketing\n-Salaire : à définir selon profil et expérience",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "SRFC"
      },
      "experienceExige": "E",
      "experienceLibelle": "3 ans",
      "formations": [],
      "id": "095WKXF",
      "intitule": "Assistant.e commercial en charge administration des ventes  (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "35000",
        "commune": "35238",
        "latitude": 48.11416667,
        "libelle": "35 - RENNES",
        "longitude": -1.680833332
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXF"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "D1401",
      "romeLibelle": "Assistanat commercial",
      "salaire": {
        "libelle": "Mensuel de 1521,22 Euros sur 12 mois"
      },
      "secteurActivite": "93",
      "secteurActiviteLibelle": "Activités de clubs de sports",
      "trancheEffectifEtab": "100 à 199 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Assistant / Assistante comptable",
      "competences": [],
      "dateActualisation": "2019-11-13T12:53:21+01:00",
      "dateCreation": "2019-11-13T12:53:14+01:00",
      "description": "De formation comptable, équivalent BTS, d'un niveau minimum BAC 2/3, vous justifiez d'une première expérience en comptabilité. Vous aimez travailler en équipe, disposez d'un bon relationnel et avez l'esprit d'initiative. Autonomie, rigueur, discrétion et sens de l'organisation sont des qualités requises pour réussir dans ce poste.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3642763",
      "intitule": "Comptable auxiliaire F/H (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.730300903,
        "libelle": "France",
        "longitude": 2.272870064
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1312273470?v=85BD4586BACC49D005AC2A2399BC2EC5B554599C&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642763"
      },
      "qualificationCode": "X",
      "romeCode": "M1203",
      "romeLibelle": "Comptabilité",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Menuisier / Menuisière",
      "competences": [
        {
          "code": "124985",
          "exigence": "S",
          "libelle": "Assembler des sous-ensembles par procédé mécanique"
        },
        {
          "code": "120068",
          "exigence": "S",
          "libelle": "Débiter des pièces de rotin ou de bois"
        },
        {
          "code": "125576",
          "exigence": "S",
          "libelle": "Poser des éléments de quincaillerie"
        },
        {
          "code": "120066",
          "exigence": "S",
          "libelle": "Réaliser des gabarits de fabrication"
        },
        {
          "code": "125750",
          "exigence": "S",
          "libelle": "Réaliser les opérations de décoration d'une pièce artisanale"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : niort.interim@orange.fr",
        "courriel": "niort.interim@orange.fr",
        "nom": "NIORT INTERIM - Mme Priscille LE DILY"
      },
      "dateCreation": "2019-11-13T12:53:12+01:00",
      "description": "- Menuisier poseur\n - Pose de menuiserie, portails, pergola",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/UgagDoUwSzd7XdRWBJ4vJDrPmwhSugBl.png",
        "nom": "NIORT INTERIM",
        "url": "https://www.niort-interim.fr"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKXD",
      "intitule": "Menuisier / Menuisière",
      "langues": [],
      "lieuTravail": {
        "codePostal": "85000",
        "commune": "85191",
        "latitude": 46.66972222,
        "libelle": "85 - LA ROCHE SUR YON",
        "longitude": -1.427777777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXD"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "H2206",
      "romeLibelle": "Réalisation de menuiserie bois et tonnellerie",
      "salaire": {
        "libelle": "Horaire de 10,03 Euros à 11,00 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 1 Mois"
    },
    {
      "alternance": false,
      "appellationlibelle": "Consultant / Consultante réseaux informatiques",
      "competences": [],
      "dateActualisation": "2019-11-13T12:53:09+01:00",
      "dateCreation": "2019-11-13T12:53:08+01:00",
      "description": "Randstad, leader en France de l'intérim spécialisé et du recrutement en CDI de cadres et agents de maîtrise. Les consultants du Département Construction & Bâtiment vous proposent des opportunités de carrière Randstad, 1er groupe mondial en ressources humaines, recrute ses futurs consultants commerciaux pour son réseau d'agences. Commercial avant tout, vous êtes chasseur de clients et de talents. Vous êtes dans une démarche de conquête permanente de nouveaux marchés dans un environnement de services aux entreprises. Véritable intermédiaire de l'emploi, vous êtes l'expert métier sur votre bassin d'emploi auprès de vos clients. Vous leur proposez les solutions RH adaptées à leur besoin : recrutement de collaborateurs en CDI / CDD / intérim. Vous assurez également la gestion administrative inhérente à l'ensemble de vos activités et travaillez en binôme avec un autre consultant commercial de votre spécialité. Plus qu'une expérience ou un niveau d'études, c'est votre personnalité qui fait la différence. Pour demande du client de profils confirmés : Avec une expérience confirmée d'au moins 5 ans dans le domaine commercial, vous aimez la diversité des tâches, l'action, la multiplicité des contacts et des situations. Votre expérience et vos résultats vous permettront d'évoluer rapidement au sein de notre groupe. Pour demande client sans spécification d'expérience confirmée : Vous aimez la diversité des tâches, l'action, la multiplicité des contacts et des situations. Notre parcours d'intégration personnalisé, nos équipes, nos process et nos outils innovants vous accompagneront dans votre succès. Vous pourrez ainsi révéler tout votre potentiel et faire partie des 80% de nos managers issus de la promotion interne. Package de rémunération : - Fixe sur 13 mois - Variable - 23 jours de RTT - Tickets restaurant, mutuelle, Comité d'Entreprise, etc.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Randstad"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 5 An(s)",
      "formations": [],
      "id": "3642760",
      "intitule": "Consultant F/H (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 47.749488831,
        "libelle": "France",
        "longitude": 7.339779854
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1302532092?v=F1A6945C4CE3740CCBB2F39FDCE3E69C0D6F4420&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642760"
      },
      "qualificationCode": "X",
      "romeCode": "M1806",
      "romeLibelle": "Conseil et maîtrise d'ouvrage en systèmes d'information",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Électricien / Électricienne photovoltaïque",
      "competences": [
        {
          "code": "121777",
          "exigence": "S",
          "libelle": "Fixer des éléments basse tension"
        },
        {
          "code": "123597",
          "exigence": "S",
          "libelle": "Positionner une armoire électrique de locaux domestiques ou tertiaires"
        },
        {
          "code": "121778",
          "exigence": "S",
          "libelle": "Raccorder des éléments basse tension"
        },
        {
          "code": "123598",
          "exigence": "S",
          "libelle": "Raccorder une armoire électrique aux équipements de locaux domestiques ou tertiaires"
        },
        {
          "code": "103495",
          "exigence": "S",
          "libelle": "Réaliser et poser des chemins de câbles et des conduits électriques en apparent ou en encastré"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : recrutement@vsb-energies.fr",
        "courriel": "recrutement@vsb-energies.fr",
        "nom": "VSB ENERGIES NOUVELLES - Mme JORDANE NOGAREDE"
      },
      "dateCreation": "2019-11-13T12:53:05+01:00",
      "description": "Chargé(e) d'exploitation Photovoltaïque - Nîmes (CDI Temps Plein) \n \nMISSIONS \n \nRattaché(e) au Responsable Exploitation Photovoltaïque, vous aurez pour principales missions de : - Surveiller à distance le fonctionnement du parc et participer aux astreintes tournantes ; - Contrôler la bonne exécution des contrats de maintenance ; - Préparer les rapports d'exploitation et analyser leurs pertinences ; - Assurer la gestion HSEQ des sites conformément à la réglementation en vigueur ; - Réaliser des expertises techniques et des analyses de pertinence ; - Représente VSB auprès des Clients du pôle Exploitation. A ce titre, organise, conduit ou participe aux réunions avec les Clients et assure la communication et le suivi des dossiers. \n \n (Liste non exhaustive)",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Avec environ 80 collaborateurs répartis sur toute la France, VSB énergies nouvelles rassemble toutes les compétences et expertises techniques dédiées au développement de projets, financement, construction et exploitation de parcs éoliens, centrales solaires et hydroélectriques.",
        "nom": "VSB ENERGIES NOUVELLES",
        "url": "http://vsb-energies.fr/"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKXC",
      "intitule": "Ingénieur Exploitation Photovoltaïque  (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "30000",
        "commune": "30189",
        "latitude": 43.83694444,
        "libelle": "30 - NIMES",
        "longitude": 4.36
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXC"
      },
      "qualificationCode": "9",
      "qualificationLibelle": "Cadre",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        }
      ],
      "romeCode": "F1602",
      "romeLibelle": "Électricité bâtiment",
      "salaire": {
        "libelle": "Annuel de 33000,00 Euros à 36000,00 Euros sur 12 mois"
      },
      "secteurActivite": "71",
      "secteurActiviteLibelle": "Ingénierie, études techniques",
      "trancheEffectifEtab": "10 à 19 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Jardinier / Jardinière paysagiste",
      "competences": [],
      "dateActualisation": "2019-11-13T12:53:02+01:00",
      "dateCreation": "2019-11-13T12:53:01+01:00",
      "description": "Nous recherchons actuellement sur le secteur de MARIGNANE et environs un jardinier et/ou paysagiste pour effectuer les prestations suivantes au domicile de nos clients : - Désherber / Ratisser / Ramasser - Tailler / Arroser / Tondre / Planter / Semer - Traiter les pelouses / Traiter arbres et plantes / Entretenir les massifs et balcons - Débroussailler / Brûler Nous proposons un CDI à temps plein Nous fournissons un véhicule de service avec carte essence, le matériel de jardinage, les équipements de sécurité, un téléphone portable professionnel Leader sur le marché des services à la personne depuis 1997, HOME SERVICES propose des prestations de qualité répondant à plusieurs référentiels exigeants comme NF Service. Répartis sur 8 établissements et 4 départements (Bouches du Rhône, Var, Vaucluse, Alpes maritimes), nous employons actuellement plus de 800 personnes et servons plus de 4000 clients dont le taux de satisfaction s'élève à 97%. En pleine croissance, nous sommes en constant recrutement et de nombreux postes sont à pourvoir au sein de nos établissements Nous recherchons une personne sérieuse et investie, faisant preuve d'autonomie et d'adaptation Permis B exigé Diplôme dans le secteur du jardinage/paysagisme exigé",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3642758",
      "intitule": "JARDINIER (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 43.270019531,
        "libelle": "France",
        "longitude": 5.381690025
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1296086019?v=E6524BEFCFA862AB412C43323B64BE4F38D04604&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3642758"
      },
      "permis": [
        {
          "exigence": "E",
          "libelle": "B - Véhicule léger"
        }
      ],
      "qualificationCode": "X",
      "romeCode": "A1203",
      "romeLibelle": "Aménagement et entretien des espaces verts",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Infirmier / Infirmière de service hospitalier",
      "competences": [
        {
          "code": "107737",
          "exigence": "E",
          "libelle": "Organiser le plan de soins infirmiers selon les besoins des patients et préparer le chariot de soins ou la trousse médicale"
        },
        {
          "code": "107738",
          "exigence": "E",
          "libelle": "Réaliser les soins infirmiers, communiquer avec le patient (ressenti, douleur, ...) et actualiser le dossier de soins infirmiers (incidents, modifications d'état clinique, ...)"
        },
        {
          "code": "107740",
          "exigence": "E",
          "libelle": "Réaliser ou contrôler les soins d'hygiène, de confort et apporter une aide au patient (lever, marche, soins post opératoires...)"
        },
        {
          "code": "107739",
          "exigence": "E",
          "libelle": "Surveiller l'état clinique du patient (constantes, fonctions d'élimination, comportement, ...) et informer l'équipe soignante/médicale sur l'évolution de l'état clinique"
        },
        {
          "code": "107736",
          "exigence": "S",
          "libelle": "Cerner l'état du patient (clinique, psychologique) et consigner les informations recueillies dans le dossier médical"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : recrutement@amreso-bethel.fr",
        "courriel": "recrutement@amreso-bethel.fr",
        "nom": "ASS AMIS MAIS RETR SOINS OBERHAUSBERGE - Mme Emilie RETAILLEAU"
      },
      "dateCreation": "2019-11-13T12:52:53+01:00",
      "description": "Nous sommes à la recherche d'un(e) IDE en EHPAD. \nVos missions :\n- participer activement à l'élaboration du projet de vie personnalisé, s'attacher à une prise en charge globale et individualisée en étant attentif aux besoins de chaque patient\n- organiser et effectuer de façon autonome l'ensemble des soins infirmiers (injections, pansements, prise de sang, prise des constantes, préparation et administration des prescriptions médicales, soins d'urgence )\n- prévenir, dépister et traiter la maladie\n- aider la personne soignée à maintenir et recouvrir son indépendance et son autonomie autant que possible, à développer son potentiel de santé, à soulager sa souffrance, à vivre ses derniers moments\n- prévenir l'altération des patients : rééducation vésicale et anale, surveillance de l'alimentation, prévention des chutes\n- accueillir, informer et accompagner les familles\nHoraires : selon un cycle, weekends par roulement",
      "dureeTravailLibelle": "35H Horaires variables",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "La Maison de Santé Béthel, maison médicalisée située à Oberhausbergen, a pour mission d'accueillir et d'accompagner des personnes fragilisées tout au long de leur projet de vie et de leur parcours de soins. Plus de 300 collaborateurs passionnés et investis œuvrent au quotidien pour le bien-être de nos 333 patients et résidents. L'établissement est constitué en 4 Pôles d'activités : Pôle Personnes Âgées Dépendantes / EHPAD, Pôle SSR à orientation oncologique, Pôle Long Séjour, et Pôle Handicap.",
        "nom": "ASS AMIS MAIS RETR SOINS OBERHAUSBERGE"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "43448",
          "commentaire": "DE Infirmier en EHPAD",
          "domaineLibelle": "Infirmier",
          "exigence": "E",
          "niveauLibelle": "Bac+3, Bac+4 ou équivalents"
        }
      ],
      "id": "095WKXB",
      "intitule": "Infirmier Diplômé d'Etat en EHPAD H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "67205",
        "commune": "67343",
        "latitude": 48.60666667,
        "libelle": "67 - OBERHAUSBERGEN",
        "longitude": 7.685277777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKXB"
      },
      "qualificationCode": "8",
      "qualificationLibelle": "Agent de maîtrise",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à garder le contrôle de soi pour agir efficacement face à des situations irritantes, imprévues et stressantes. Exemple : garder son calme",
          "libelle": "Gestion du stress"
        },
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "J1506",
      "romeLibelle": "Soins infirmiers généralistes",
      "salaire": {
        "commentaire": "Selon grille FEHAP CCN51",
        "complement1": "Selon grille FEHAP CCN51"
      },
      "secteurActivite": "86",
      "secteurActiviteLibelle": "Activités hospitalières",
      "trancheEffectifEtab": "250 à 499 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Professeur / Professeure de mathématiques",
      "competences": [
        {
          "code": "108816",
          "exigence": "S",
          "libelle": "Mathématiques"
        },
        {
          "code": "108412",
          "exigence": "S",
          "libelle": "Préparer les cours et établir la progression pédagogique"
        },
        {
          "code": "108478",
          "exigence": "S",
          "libelle": "Programme de l'Éducation Nationale"
        },
        {
          "code": "108490",
          "exigence": "S",
          "libelle": "Suivre et conseiller les élèves dans l'organisation du travail personnel"
        },
        {
          "code": "100090",
          "exigence": "S",
          "libelle": "Techniques pédagogiques"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : marion.borel@anacours.fr",
        "courriel": "marion.borel@anacours.fr",
        "nom": "ANACOURS - Mme Marion Borel"
      },
      "dateCreation": "2019-11-13T12:52:46+01:00",
      "description": "Anacours, expert du soutien scolaire en France, recrute un(e) enseignant(e) en MATHEMATIQUES pour donner des cours particuliers à domicile à ARAMON (30390), pour un élève en classe de TERMINALE.\n\nVous avez validé un niveau Bac+3 minimum et vous souhaitez intégrer une équipe pédagogique motivée et engagée dans la réussite scolaire de nos élèves, et ainsi acquérir une expérience enrichissante au sein d'un organisme reconnu pour la qualité de ses cours et la relation de proximité avec ses enseignants.\n\nVos Avantages :\n- Une rémunération nette et claire,\n- Un paiement 2 fois dans le mois,\n- Une possibilité de suivre de nombreux élèves,\n- Offres de cours disponibles en ligne,\n- Une expérience professionnelle dans l'enseignement valorisante.\n- En fonction de votre emploi du temps, vous sélectionnez des cours particuliers à domicile qui correspondent à vos disponibilités : en semaine en fin de journée, le mercredi toute la journée ou le week-end.",
      "dureeTravailLibelle": "1H30 Horaires normaux",
      "dureeTravailLibelleConverti": "Temps partiel",
      "entreprise": {
        "description": "Anacours, spécialiste du soutien scolaire depuis plus de 15 ans, propose des cours particuliers dans de nombreuses matières et pour tous les niveaux : primaire, secondaire, supérieur, prépas et compte aujourd'hui parmi les leaders du secteur.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/Uig3tfjdkuljbeUW8q4l3jMGZy2DVUTA.png",
        "nom": "ANACOURS",
        "url": "https://www.anacours.com/"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "E",
          "niveauLibelle": "Bac+3, Bac+4 ou équivalents"
        }
      ],
      "id": "095WKWZ",
      "intitule": "Professeur / Professeure de mathématiques",
      "langues": [],
      "lieuTravail": {
        "codePostal": "30390",
        "commune": "30012",
        "latitude": 43.89111111,
        "libelle": "30 - ARAMON",
        "longitude": 4.680833332
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWZ"
      },
      "qualificationCode": "8",
      "qualificationLibelle": "Agent de maîtrise",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "K2107",
      "romeLibelle": "Enseignement général du second degré",
      "salaire": {
        "libelle": "Horaire de 19,00 Euros à 19,00 Euros sur 12 mois"
      },
      "secteurActivite": "85",
      "secteurActiviteLibelle": "Formation continue d'adultes",
      "trancheEffectifEtab": "3 à 5 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 7 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Électricien / Électricienne tertiaire",
      "competences": [
        {
          "code": "121777",
          "exigence": "E",
          "libelle": "Fixer des éléments basse tension"
        },
        {
          "code": "123597",
          "exigence": "E",
          "libelle": "Positionner une armoire électrique de locaux domestiques ou tertiaires"
        },
        {
          "code": "121778",
          "exigence": "E",
          "libelle": "Raccorder des éléments basse tension"
        },
        {
          "code": "123598",
          "exigence": "E",
          "libelle": "Raccorder une armoire électrique aux équipements de locaux domestiques ou tertiaires"
        },
        {
          "code": "103495",
          "exigence": "E",
          "libelle": "Réaliser et poser des chemins de câbles et des conduits électriques en apparent ou en encastré"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : lille2@welljob.fr",
        "courriel": "lille2@welljob.fr",
        "nom": "WELL JOB - Mme Céline  GANDILLET"
      },
      "dateCreation": "2019-11-13T12:52:33+01:00",
      "description": "L'agence Welljob Emploi Lille recherche des électriciens tertiaires (H/F)\n\nVos missions :\nTirage de câbles\nPose d'appareillage\nPose de goulottes\nTravaux sur des logements\nIncorporation et filerie\n\nVotre profil :\nNiveau 2 ou 3\nAutonome et réactif\nDiplôme en électricité\nHabilitations électriques à jour",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "AGENCE WELLJOB EMPLOI DE LILLE \n54 Bis Boulevard de la Liberté\n59800 LILLE",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/aJRrFYgRrRAI0d6wyXUPFevjCOGo2rhH.png",
        "nom": "WELL JOB",
        "url": "https://www.welljob.fr/entreprise"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WKWX",
      "intitule": "Electricien / Electricienne tertiaire",
      "langues": [],
      "lieuTravail": {
        "codePostal": "59200",
        "commune": "59599",
        "latitude": 50.7225,
        "libelle": "59 - TOURCOING",
        "longitude": 3.160277777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWX"
      },
      "qualificationCode": "3",
      "qualificationLibelle": "Ouvrier qualifié (P1,P2)",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "F1602",
      "romeLibelle": "Électricité bâtiment",
      "salaire": {
        "complement1": "Primes",
        "libelle": "Mensuel de 1550,00 Euros à 2850,00 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 5 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Employé / Employée de ménage",
      "competences": [
        {
          "code": "104167",
          "exigence": "S",
          "libelle": "Caractéristiques des produits d'entretien"
        },
        {
          "code": "107948",
          "exigence": "S",
          "libelle": "Dépoussiérer les sols, les tapis, les meubles, les objets et aérer, désodoriser les pièces"
        },
        {
          "code": "124543",
          "exigence": "S",
          "libelle": "Entretenir des locaux"
        },
        {
          "code": "120699",
          "exigence": "S",
          "libelle": "Entretenir le linge de maison et les vêtements de la personne"
        },
        {
          "code": "102096",
          "exigence": "S",
          "libelle": "Règles d'hygiène et de propreté"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : maisonetservicesantilles@gmail.com",
        "courriel": "maisonetservicesantilles@gmail.com",
        "nom": "MAISON ET SERVICES - Mme MAISON ET SERVICES"
      },
      "dateCreation": "2019-11-13T12:52:26+01:00",
      "deplacementCode": "4",
      "deplacementLibelle": "Quotidiens Départemental",
      "description": "Vous intervenez au domicile des différents clients particuliers de l'entreprise. Vous réalisez les tâches ménagères. Le nombre d'heure réalisé est évolutif en fonction des demandes des clients.",
      "dureeTravailLibelle": "24H Horaires variables",
      "dureeTravailLibelleConverti": "Temps partiel",
      "entreprise": {
        "nom": "MAISON ET SERVICES"
      },
      "experienceExige": "E",
      "experienceLibelle": "6 mois",
      "formations": [],
      "id": "095WKWW",
      "intitule": "Employé / Employée de ménage",
      "langues": [],
      "lieuTravail": {
        "codePostal": "97229",
        "commune": "97231",
        "latitude": 14.53805556,
        "libelle": "972 - LES TROIS ILETS / SUD",
        "longitude": -61.03444444
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWW"
      },
      "permis": [
        {
          "exigence": "S",
          "libelle": "B - Véhicule léger Souhaité"
        }
      ],
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "romeCode": "K1304",
      "romeLibelle": "Services domestiques",
      "salaire": {
        "libelle": "Horaire de 10,03 Euros"
      },
      "secteurActivite": "96",
      "secteurActiviteLibelle": "Autres services personnels n.c.a.",
      "trancheEffectifEtab": "20 à 49 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 6 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Serveur / Serveuse de restaurant",
      "competences": [],
      "dateCreation": "2019-11-13T12:52:24+01:00",
      "description": "ALPE D'HUEZ - HIVER 2019/20 - BAR - RESTAURANT - SOLARIUM \nPied de piste à forte fréquentation - 200 places en terrasse + transats\nRecherche SERVEURS(E)\n\nÉquipe de 9 personnes - Cuisine de type Brasserie - Service continu \n\nDe l'accueil des clients à l'encaissement\nMaitrise du PAD (logiciel Pointex)\nService au plateau\nDéneigement\nMise en place de la salle et de la terrasse (200 places)\nNettoyage et entretien des locaux\nremplacements ponctuels au bar et comme runner\n\nANGLAIS CORRECT DEMANDE\n\nQualités requises :\n- excellente présentation\n- grosse résistance physique\n- patience\n- rapidité\n\nExpérience souhaitée en saison au même poste\n\nPOSTE LOGE EN CHAMBRE INDIVIDUELLE ET NOURRI\nCONTRAT SAISONNIER DE 5 MOIS \n\nSALAIRE A NÉGOCIER SELON EXPÉRIENCE",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "LA TAVERNE DES BERGERS"
      },
      "experienceExige": "E",
      "experienceLibelle": "1 an",
      "formations": [],
      "id": "095WKWV",
      "intitule": "Serveur / Serveuse de restaurant       (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "38750",
        "commune": "38191",
        "latitude": 45.0825,
        "libelle": "38 - HUEZ",
        "longitude": 6.059166666
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 2,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWV"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "romeCode": "G1803",
      "romeLibelle": "Service en restauration",
      "salaire": {
        "libelle": "Mensuel de 2000,00 Euros à 2800,00 Euros sur 12 mois"
      },
      "secteurActivite": "56",
      "secteurActiviteLibelle": "Restauration traditionnelle",
      "typeContrat": "SAI",
      "typeContratLibelle": "Contrat travail saisonnier - 4 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Commercial / Commerciale auprès des particuliers",
      "competences": [
        {
          "code": "125815",
          "exigence": "S",
          "libelle": "Conseiller un client"
        },
        {
          "code": "119945",
          "exigence": "S",
          "libelle": "Identifier les besoins d'un client"
        },
        {
          "code": "121417",
          "exigence": "S",
          "libelle": "Méthodes de plan de prospection"
        },
        {
          "code": "120526",
          "exigence": "S",
          "libelle": "Mettre en place des actions de prospection"
        },
        {
          "code": "121421",
          "exigence": "S",
          "libelle": "Présenter des produits et services"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : agentco@ipheos.com",
        "courriel": "agentco@ipheos.com",
        "nom": "JCST - M. Jean-Christophe HALLYNCK"
      },
      "dateCreation": "2019-11-13T12:52:14+01:00",
      "description": "Vous prospectez une cible de clientèle de professionnels (institut de beauté, esthéticienne à domicile, pharmacie et para pharmacie, parfumerie, onglerie, salon de coiffure).",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "description": "IPHEOS Cosmétiques est une marque de produits cosmétiques hautement efficaces, naturels (la plupart BIO), fabriqués en France et qui s'inscrivent dans une démarche écoresponsable.\n\nNous avons 2 départements, un pour la distribution de notre gamme auprès des professionnels (via des Agents Commerciaux) et l'autre pour la vente aux particuliers par le biais de la Vente Directe.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/a359d17995f74a18880d1d2f2430b241.png",
        "nom": "JCST",
        "url": "http://www.ipheos.com"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "S",
          "niveauLibelle": "Bac ou équivalent"
        }
      ],
      "id": "095WKWT",
      "intitule": "agent commercial en cosmetiques                         (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "58000",
        "commune": "58194",
        "latitude": 46.9925,
        "libelle": "58 - NEVERS",
        "longitude": 3.156666666
      },
      "natureContrat": "Emploi non salarié",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWT"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à être proactif, à initier, à imaginer des propositions nouvelles pour résoudre les problèmes identifiés ou pour améliorer une situation. Exemple : proposer des améliorations, être positif et constructif",
          "libelle": "Force de proposition"
        },
        {
          "description": "Capacité à maintenir son effort jusqu’à l’achèvement complet d’une tâche, quels que soient les imprévus et les obstacles de réalisation rencontrés. Exemple : faire preuve de volonté, d’assiduité, de régularité, rebondir après une erreur",
          "libelle": "Persévérance"
        }
      ],
      "romeCode": "D1403",
      "romeLibelle": "Relation commerciale auprès de particuliers",
      "salaire": {
        "complement1": "Primes"
      },
      "secteurActivite": "47",
      "secteurActiviteLibelle": "Vente à domicile",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "CCE",
      "typeContratLibelle": "Profession commerciale"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Chef de secteur distribution",
      "competences": [
        {
          "code": "119855",
          "exigence": "S",
          "libelle": "Agencer un espace de vente"
        },
        {
          "code": "102359",
          "exigence": "S",
          "libelle": "Coordonner l'activité d'une équipe"
        },
        {
          "code": "122595",
          "exigence": "S",
          "libelle": "Organiser des animations commerciales"
        },
        {
          "code": "121632",
          "exigence": "S",
          "libelle": "Organiser une unité de vente"
        },
        {
          "code": "121934",
          "exigence": "S",
          "libelle": "Superviser la gestion des rayons"
        }
      ],
      "contact": {
        "coordonnees1": "https://jobs.smartrecruiters.com/somfygroup/743999699409765-responsable-secteur-distribution-professionnelle-h-f-",
        "nom": "SOMFY SAS - M. Camille Deschamps",
        "urlPostulation": "https://jobs.smartrecruiters.com/somfygroup/743999699409765-responsable-secteur-distribution-professionnelle-h-f-"
      },
      "dateCreation": "2019-11-13T12:52:05+01:00",
      "description": "Pour assurer notre développement, nous recherchons un(e) :\n\nResponsable Secteur Distribution Professionnelle (H/F)\n\nBusiness Unit Bâtiments Neufs Résidentiels et Tertiaires\n\nDans le cadre de l'ouverture du canal Distribution Professionnelle en France, sous la marque SOMFY, et rattaché(e) au Responsable national des ventes distribution professionnelle nous sommes amenés à renforcer une équipe commerciale qui a pour ambition de développer un volume d'affaire représentant 5 à 7% du CA de la BA France.\n\nVous contribuez à notre business plan en développant les Distributeurs référencés de votre secteur via les missions principales suivantes :  \n\n- Vous animez les comptoirs des différentes enseignes, par la présentation de nos produits et la mise en scène de nos innovations, la mise en place de supports techniques à la vente, et le relais de nos actions commerciales / promotionnelles. Vous veillez dans ce cadre à l'optimisation de vos tournées.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Somfy se concentre depuis plus de 50 ans sur un métier : l automatisation des ouvertures et des fermetures de la maison et des bâtiments et est aujourd hui au cœur des enjeux de la maison intelligente. Animée par une forte culture de l'innovation, Somfy crée des solutions qui contribuent à l'amélioration des cadres de vie des habitants et s engage à les rendre accessibles au plus grand nombre, en répondant à leurs attentes de confort, de sécurité et d'économie d'énergie.",
        "nom": "SOMFY SAS",
        "url": "https://www.somfy.com/portail/index.cfm"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKWS",
      "intitule": "Responsable Secteur Distribution Professionnelle (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "95000",
        "commune": "95500",
        "latitude": 49.05083333,
        "libelle": "95 - PONTOISE",
        "longitude": 2.100833333
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWS"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "D1509",
      "romeLibelle": "Management de département en grande distribution",
      "salaire": {
        "commentaire": "compétitive",
        "complement1": "compétitive"
      },
      "secteurActivite": "27",
      "secteurActiviteLibelle": "Fabrication de moteurs, génératrices et transformateurs électriques",
      "trancheEffectifEtab": "500 à 999 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Formateur / Formatrice ressources humaines",
      "competences": [
        {
          "code": "121621",
          "exigence": "S",
          "libelle": "Actualiser des outils de formation / pédagogiques"
        },
        {
          "code": "121620",
          "exigence": "S",
          "libelle": "Concevoir des outils de formation / pédagogiques"
        },
        {
          "code": "122898",
          "exigence": "S",
          "libelle": "Définir les méthodes et outils pédagogiques d'une formation"
        },
        {
          "code": "123128",
          "exigence": "S",
          "libelle": "Encadrer des stagiaires dans leurs missions"
        },
        {
          "code": "126285",
          "exigence": "S",
          "libelle": "Techniques de formation individuelle"
        }
      ],
      "contact": {
        "coordonnees1": "https://jobs.smartrecruiters.com/Eurofins/743999699378677-stagiaire-ressources-humaines-h-f-",
        "nom": "EUROFINS BIOFFICE - Mme GERALDINE CHEMIN",
        "urlPostulation": "https://jobs.smartrecruiters.com/Eurofins/743999699378677-stagiaire-ressources-humaines-h-f-"
      },
      "dateCreation": "2019-11-13T12:51:48+01:00",
      "description": "Sous la responsabilité de la Responsable Ressources Humaines, vous interviendrez sur différents sujets RH.\n\nVos missions seront les suivantes :\n\nMettre à jour les affichages obligatoires ;\nParticiper au recensement des besoins de formation pour l'année 2020 ;\nParticiper au processus de recrutement ;\nSuivi des travailleurs en situation de handicap ;\nMise en place du Règlement intérieur ;\nParticipation à la mise en place des élections professionnelles ;\nRéaliser des tâches de gestion administrative du personnel : rédaction d'avenants, courriers, etc. ;\nSujets RH transverses tel que la mise en place d'une GPEC.\nQualifications\nSi ...\n\nVous recherchez un stage dans le cadre d'un cursus de formation dans le domaine des Ressources Humaines ;\nVous faites preuve de rigueur ;\nVous êtes dynamique et curieux ;\nVous souhaitez travailler sur des sujets de développement RH variés\n... Alors envoyez nous votre CV et Lettre de motivation !",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Avec plus de 35.000 collaborateurs dans 800 laboratoires et 47 pays, Eurofins fournit des prestations d'analyse aux industries pharmaceutiques, alimentaires et dans le domaine de l'environnement. A la pointe des derniers développements en biotechnologie, la société en forte croissance, leader dans son domaine d'activité, est cotée à la Bourse de Paris.",
        "nom": "EUROFINS BIOFFICE",
        "url": "https://www.eurofins.com"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKWR",
      "intitule": "Formateur / Formatrice ressources humainesumaines (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "33000",
        "commune": "33063",
        "latitude": 44.83777778,
        "libelle": "33 - BORDEAUX",
        "longitude": -0.579444443
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWR"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "K2111",
      "romeLibelle": "Formation professionnelle",
      "salaire": {
        "commentaire": "compétitive",
        "complement1": "compétitive"
      },
      "secteurActivite": "86",
      "secteurActiviteLibelle": "Laboratoires d'analyses médicales",
      "trancheEffectifEtab": "20 à 49 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": true,
      "appellationlibelle": "Serveur / Serveuse de restaurant",
      "competences": [
        {
          "code": "104323",
          "exigence": "S",
          "libelle": "Accueillir le client à son arrivée au restaurant, l'installer à une table et lui présenter la carte"
        },
        {
          "code": "124015",
          "exigence": "S",
          "libelle": "Débarrasser une table"
        },
        {
          "code": "117933",
          "exigence": "S",
          "libelle": "Dresser les tables"
        },
        {
          "code": "124014",
          "exigence": "S",
          "libelle": "Nettoyer une salle de réception"
        },
        {
          "code": "117932",
          "exigence": "S",
          "libelle": "Réaliser la mise en place de la salle et de l'office"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : yvan.reboul@wanadoo.fr",
        "courriel": "yvan.reboul@wanadoo.fr",
        "nom": "NENNI MA FOI - M. YVAN REBOUL"
      },
      "dateCreation": "2019-11-13T12:51:37+01:00",
      "description": "Vous souhaitez préparer un CAP SERVICE EN SALLE par le biais de l'apprentissage - vous sortez idéalement d'une classe de 3 e des collèges - ",
      "dureeTravailLibelle": "35H Autre",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "NENNI MA FOI"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "S",
          "niveauLibelle": "3ème achevée ou Brevet"
        }
      ],
      "id": "095WKWQ",
      "intitule": "Serveur / Serveuse de restaurant",
      "langues": [],
      "lieuTravail": {
        "codePostal": "90000",
        "commune": "90010",
        "latitude": 47.6375,
        "libelle": "90 - BELFORT",
        "longitude": 6.862777777
      },
      "natureContrat": "Contrat apprentissage",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWQ"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "romeCode": "G1803",
      "romeLibelle": "Service en restauration",
      "salaire": {
        "commentaire": "% du smic suivant l'age",
        "complement1": "% du smic suivant l'age"
      },
      "secteurActivite": "56",
      "secteurActiviteLibelle": "Restauration traditionnelle",
      "trancheEffectifEtab": "3 à 5 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 24 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Technicien / Technicienne paie",
      "competences": [],
      "contact": {
        "coordonnees1": "Courriel : compta-fi.nancy@expectra.fr",
        "courriel": "compta-fi.nancy@expectra.fr",
        "nom": "EXPECTRA - M. Melanie LEFEVRE"
      },
      "dateCreation": "2019-11-13T12:51:08+01:00",
      "deplacementCode": "1",
      "deplacementLibelle": "Jamais",
      "description": "Intégré au sein du service financier de l'entreprise, vous êtes en charge de la réalisation des paies sur le logiciel CEGID jusqu'au déclaration sociale via la DSN.\nVous assurez l'administration du personnel avec la gestion des arrêts maladie, des déclarations d'accidents du travail éventuels, les entrées/sorties et les formations éventuelles.\nA l'aise avec les remontées chiffrés vous tenez un reporting social sur Excel.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Expectra, leader en France de l'intérim spécialisé et du recrutement en CDI de cadres et agents de maîtrise.\nLes consultants du Département Comptabilité & Finance vous proposent des opportunités de carrière.",
        "nom": "EXPECTRA"
      },
      "experienceExige": "S",
      "experienceLibelle": "5 ans",
      "formations": [
        {
          "codeFormation": "33054",
          "domaineLibelle": "Ressources humaines",
          "exigence": "S",
          "niveauLibelle": "Bac+2 ou équivalents"
        }
      ],
      "id": "095WKWM",
      "intitule": "Technicien / Technicienne paie",
      "langues": [],
      "lieuTravail": {
        "codePostal": "54110",
        "commune": "54159",
        "latitude": 48.625,
        "libelle": "54 - DOMBASLE SUR MEURTHE",
        "longitude": 6.349722221
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWM"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "M1203",
      "romeLibelle": "Comptabilité",
      "salaire": {
        "libelle": "Annuel de 25000,00 Euros à 28000,00 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "1 ou 2 salariés",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 91 Jour(s)"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Menuisier / Menuisière",
      "competences": [
        {
          "code": "124985",
          "exigence": "E",
          "libelle": "Assembler des sous-ensembles par procédé mécanique"
        },
        {
          "code": "120068",
          "exigence": "E",
          "libelle": "Débiter des pièces de rotin ou de bois"
        },
        {
          "code": "125576",
          "exigence": "E",
          "libelle": "Poser des éléments de quincaillerie"
        },
        {
          "code": "120066",
          "exigence": "E",
          "libelle": "Réaliser des gabarits de fabrication"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : lille2@welljob.fr",
        "courriel": "lille2@welljob.fr",
        "nom": "WELL JOB - Mme Mégane François"
      },
      "dateCreation": "2019-11-13T12:51:01+01:00",
      "deplacementCode": "3",
      "deplacementLibelle": "Fréquents Départemental",
      "description": "L'agence Welljob Emploi Lille recherche un menuisier H/F.\n\nVos missions :\npose d'éléments de menuiserie (bois, pvc ou alu selon compétences)\npose chez des professionnels et particuliers\npose de fenêtres, portes, plinthes...\n\nVotre profil :\n\nVous disposez d'un diplôme en menuiserie poseur\nVous aimez le travail d'équipe\nVous êtes disponible rapidement",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "AGENCE WELLJOB EMPLOI DE LILLE\n54 Bis Boulevard de la Liberté\n59800 LILLE",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/aJRrFYgRrRAI0d6wyXUPFevjCOGo2rhH.png",
        "nom": "WELL JOB",
        "url": "https://www.welljob.fr/entreprise"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WKWK",
      "intitule": "Menuisier / Menuisière    (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "59150",
        "commune": "59650",
        "latitude": 50.70111111,
        "libelle": "59 - WATTRELOS",
        "longitude": 3.213333333
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWK"
      },
      "qualificationCode": "3",
      "qualificationLibelle": "Ouvrier qualifié (P1,P2)",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "H2206",
      "romeLibelle": "Réalisation de menuiserie bois et tonnellerie",
      "salaire": {
        "libelle": "Mensuel de 1550,00 Euros à 2500,00 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 5 Mois"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "smhen.38060@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Esthéticien / Esthéticienne",
      "competences": [
        {
          "code": "120130",
          "exigence": "S",
          "libelle": "Accueillir une clientèle"
        },
        {
          "code": "102050",
          "exigence": "S",
          "libelle": "Appliquer les produits (masque, sérum, crème) et réaliser les soins du visage, du corps, de manucure ou de maquillage"
        },
        {
          "code": "102052",
          "exigence": "S",
          "libelle": "Caractéristiques des parfums"
        },
        {
          "code": "102048",
          "exigence": "S",
          "libelle": "Déterminer le traitement adapté à la personne et présenter les produits, l'application des soins"
        },
        {
          "code": "124548",
          "exigence": "S",
          "libelle": "Entretenir un poste de travail"
        },
        {
          "code": "121250",
          "exigence": "S",
          "libelle": "Merchandising / Marchandisage"
        },
        {
          "code": "101343",
          "exigence": "S",
          "libelle": "Principes de la relation client"
        },
        {
          "code": "121698",
          "exigence": "S",
          "libelle": "Proposer un service, produit adapté à la demande client"
        }
      ],
      "complementExercice": "travail le week-end",
      "contact": {
        "coordonnees1": "Immeuble Pic de Belledonne 22 AVENUE BENOIT FRACHON\nCS 91314",
        "coordonnees2": "38403 ST MARTIN D HERES",
        "coordonnees3": "Courriel : smhen.38060@pole-emploi.fr",
        "nom": "Pôle Emploi SAINT MARTIN D'HERES"
      },
      "dateCreation": "2019-11-13T12:50:47+01:00",
      "description": "*** POSTE A POURVOIR DE SUITE ***\n\nPROFIL: \nVous avez une formation dans le domaine de l'esthétique ou avez déjà une expérience réussie de l'animation dans ce domaine. Vous aimez le contact client, le conseil, la vente.\n\nRÔLE / FINALITÉ:\nMettre en œuvre les techniques et les produits ayant pour but d'entretenir et\nd'embellir la peau.\n\nACTIVITÉS ET RESPONSABILITÉS:\n- Procéder à des soins sur le visage et sur le corps\n- Vendre et conseiller la cliente en institut et parfumerie\n- Accueillir les clients\n- Être disponible pour les clients\n- Écouter et identifier le besoin \n- Assurer un environnement de qualité\n- Respect de la politique instituts et des éléments de reporting\n- Participer et respecter la politique commerciale\n- Participer à la vie du magasin\n\nFORMATION:\n- Suivre les formations proposées par le service instituts\n\nDisponibilité impérative sur un bloc week-end (soit vendredi/samedi/dimanche pour les magasins ouverts le dimanche, soit jeudi/vendredi/samedi).\n\nCDD en vue de CDI",
      "dureeTravailLibelle": "35H Travail le samedi",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {},
      "experienceCommentaire": "sur la fonction",
      "experienceExige": "E",
      "experienceLibelle": "3 mois - sur la fonction",
      "formations": [
        {
          "codeFormation": "42032",
          "domaineLibelle": "Esthétique soin corporel",
          "exigence": "E",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "095WKWF",
      "intitule": "CONSEILLER(E)/ESTHETICIEN(NE) H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "38260",
        "commune": "38130",
        "latitude": 45.39361111,
        "libelle": "38 - LA COTE ST ANDRE",
        "longitude": 5.260555555
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWF"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "D1208",
      "romeLibelle": "Soins esthétiques et corporels",
      "salaire": {
        "commentaire": "+ commission",
        "complement1": "+ commission",
        "libelle": "Mensuel de 1522,00 Euros sur 12 mois"
      },
      "secteurActivite": "46",
      "secteurActiviteLibelle": "Commerce de gros (commerce interentreprises) de parfumerie et de produits de beauté",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 6 Mois"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "ape.95132@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Téléprospecteur / Téléprospectrice",
      "competences": [
        {
          "code": "101343",
          "exigence": "S",
          "libelle": "Principes de la relation client"
        }
      ],
      "conditionExercice": "HORAIRES 10H30-13H00 / 17H30-20H00",
      "contact": {
        "coordonnees1": "93 RUE DE LA MARNE",
        "coordonnees2": "95220 HERBLAY",
        "coordonnees3": "Courriel : ape.95132@pole-emploi.fr",
        "nom": "Pôle Emploi Herblay"
      },
      "dateCreation": "2019-11-13T12:50:41+01:00",
      "deplacementCode": "1",
      "deplacementLibelle": "Jamais",
      "description": "Rattaché à notre agence de Montigny-les-Cormeilles (95) et à partir de fichiers prospects qualifiés, vous prenez des rendez-vous auprès d'une clientèle de particuliers pour nos commerciaux. Formation en interne prévue.\nVous avez une grande aisance téléphonique, vous aimez convaincre et vous aimez les challenges. Possibilité d'évolution.\nHoraires du lundi au vendredi de 10H30 à 13H00 et de 17H30 à 20H00\nVous aurez pour missions la détection de projets d'amélioration de l'habitat auprès d'une clientèle de particuliers et la prise de rendez-vous pour nos équipes commerciales terrain.\n\n***** Poste à pourvoir de suite et ouvert aux débutants, dès lors que vous avez une bonne élocution. *****",
      "dureeTravailLibelle": "25H Autre",
      "dureeTravailLibelleConverti": "Temps partiel",
      "entreprise": {},
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKWC",
      "intitule": "Téléprospecteur / Téléprospectrice",
      "langues": [],
      "lieuTravail": {
        "codePostal": "95370",
        "commune": "95424",
        "latitude": 48.99388889,
        "libelle": "95 - MONTIGNY LES CORMEILLES",
        "longitude": 2.194999999
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKWC"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "romeCode": "D1408",
      "romeLibelle": "Téléconseil et télévente",
      "salaire": {
        "commentaire": "chq cadeaux + vac",
        "complement1": "Primes",
        "complement2": "chq cadeaux + vac",
        "libelle": "Mensuel de 1100,00 Euros à 1454,00 Euros sur 12 mois"
      },
      "secteurActivite": "47",
      "secteurActiviteLibelle": "Commerce de détail d'autres équipements du foyer",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Commercial / Commerciale auprès des particuliers",
      "competences": [
        {
          "code": "125815",
          "exigence": "S",
          "libelle": "Conseiller un client"
        },
        {
          "code": "119945",
          "exigence": "S",
          "libelle": "Identifier les besoins d'un client"
        },
        {
          "code": "121417",
          "exigence": "S",
          "libelle": "Méthodes de plan de prospection"
        },
        {
          "code": "120526",
          "exigence": "S",
          "libelle": "Mettre en place des actions de prospection"
        },
        {
          "code": "121421",
          "exigence": "S",
          "libelle": "Présenter des produits et services"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : agentco@ipheos.com",
        "courriel": "agentco@ipheos.com",
        "nom": "JCST - M. Jean-Christophe HALLYNCK"
      },
      "dateCreation": "2019-11-13T12:50:13+01:00",
      "description": "Vous prospectez une cible de clientèle de professionnels (institut de beauté, esthéticienne à domicile, pharmacie et para pharmacie, parfumerie, onglerie, salon de coiffure).",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "description": "IPHEOS Cosmétiques est une marque de produits cosmétiques hautement efficaces, naturels (la plupart BIO), fabriqués en France et qui s'inscrivent dans une démarche écoresponsable.\n\nNous avons 2 départements, un pour la distribution de notre gamme auprès des professionnels (via des Agents Commerciaux) et l'autre pour la vente aux particuliers par le biais de la Vente Directe.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/a359d17995f74a18880d1d2f2430b241.png",
        "nom": "JCST",
        "url": "http://www.ipheos.com"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "S",
          "niveauLibelle": "Bac ou équivalent"
        }
      ],
      "id": "095WKVY",
      "intitule": "agent commercial en cosmetiques                         (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "59000",
        "commune": "59350",
        "latitude": 50.63194444,
        "libelle": "59 - LILLE",
        "longitude": 3.0575
      },
      "natureContrat": "Emploi non salarié",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKVY"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à être proactif, à initier, à imaginer des propositions nouvelles pour résoudre les problèmes identifiés ou pour améliorer une situation. Exemple : proposer des améliorations, être positif et constructif",
          "libelle": "Force de proposition"
        },
        {
          "description": "Capacité à maintenir son effort jusqu’à l’achèvement complet d’une tâche, quels que soient les imprévus et les obstacles de réalisation rencontrés. Exemple : faire preuve de volonté, d’assiduité, de régularité, rebondir après une erreur",
          "libelle": "Persévérance"
        }
      ],
      "romeCode": "D1403",
      "romeLibelle": "Relation commerciale auprès de particuliers",
      "salaire": {
        "complement1": "Primes"
      },
      "secteurActivite": "47",
      "secteurActiviteLibelle": "Vente à domicile",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "CCE",
      "typeContratLibelle": "Profession commerciale"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Éducateur / Éducatrice d'activités physiques",
      "competences": [
        {
          "code": "103927",
          "exigence": "E",
          "libelle": "Concevoir la séance selon le niveau du public et préparer le matériel ou les équipements"
        },
        {
          "code": "103926",
          "exigence": "E",
          "libelle": "Évaluer le niveau de départ du pratiquant et déterminer les objectifs sportifs"
        },
        {
          "code": "103928",
          "exigence": "E",
          "libelle": "Présenter l'exercice aux pratiquants et apporter un appui technique"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : candidature@ge-apa-sante.com",
        "courriel": "candidature@ge-apa-sante.com",
        "nom": "GROUPEMENT D'EMPLOYEURS ACTIVITE PHYSI - Mme Maylis Robert"
      },
      "dateCreation": "2019-11-13T12:50:03+01:00",
      "deplacementCode": "3",
      "deplacementLibelle": "Fréquents Départemental",
      "description": "Vous travaillerez en relation directe avec la Coordinatrice, la Directrice et les Professionnels du GE dans la mise en œuvre, le suivi et le développement des actions du GE.\nVotre mission sera :\n- d'enseigner l'APA- Santé auprès de personnes âgées, personnes atteintes de handicap mental, psychique et maladies chroniques (établissements et structures membres du GE) \n- de s'investir pour pérenniser les actions APA - Santé en cours au sein du GE\n- de démarcher, rencontrer, développer de nouvelles actions APA Santé au sein du GE\n- de participer à des événements professionnels pour représenter le GE (manifestations, journées d'Activités Physique, forum ...)\n\n- vous travaillez et avancez sur une pratique commune avec d'autres professionnels en APA\n- vous êtes intégré(e) aux structures dans lesquelles vous intervenez (travail pluridisciplinaire)\n- vous travaillez avec différentes structures et pathologies",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Association loi 1901 à but non lucratif, créée en Octobre 2011.\nIl emploie des « Enseignants en Activité Physique Adaptée - Santé », qu'il met à disposition de ses membres adhérents. Ces derniers sont des etblts médico-sociaux, sanitaires, associations de patients, accueillant des personnes malades, fragiles ou en situation de handicap (handicap mental, moteur, personnes âgées, personnes atteintes de cancer, diabète, maladies cardio-respiratoires, VIH, obésité, maladies rénales,).",
        "nom": "GROUPEMENT D'EMPLOYEURS ACTIVITE PHYSI",
        "url": "http://www.ge-apa-sante.com"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "15454",
          "commentaire": "Licence STAPS APA-S Minimum",
          "domaineLibelle": "Activité physique et sportive",
          "exigence": "E",
          "niveauLibelle": "Bac+3, Bac+4 ou équivalents"
        }
      ],
      "id": "095WKVW",
      "intitule": "Enseignant(e) APA-S    (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "33000",
        "commune": "33063",
        "latitude": 44.83777778,
        "libelle": "33 - BORDEAUX",
        "longitude": -0.579444443
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKVW"
      },
      "qualificationCode": "7",
      "qualificationLibelle": "Technicien",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "G1204",
      "romeLibelle": "Éducation en activités sportives",
      "salaire": {
        "complement1": "Mutuelle",
        "libelle": "Mensuel de 1700 Euros à 1800 Euros sur 12 mois"
      },
      "secteurActivite": "85",
      "secteurActiviteLibelle": "Autres enseignements",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Éducateur spécialisé / Éducatrice spécialisée",
      "competences": [],
      "contact": {
        "coordonnees1": "Courriel : secretariat.central@villagedufier.org",
        "courriel": "secretariat.central@villagedufier.org",
        "nom": "LE VILLAGE DU FIER - M. EPDA LE VILLAGE DU FIER"
      },
      "dateCreation": "2019-11-13T12:49:53+01:00",
      "deplacementCode": "3",
      "deplacementLibelle": "Fréquents Départemental",
      "description": "EPDA Le Village du Fier, Etablissement relevant de la Protection de l'enfance, recrute 1 éducateur(trice) spécialisé(e) à 100 % pour un service éducatif accueillant 6 adolescents de 15 à 18 ans situé à Annecy-le-Vieux. Diplôme d'Educateur / Éducatrice spécialisé(e) ou moniteur éducateur exigé. Permis de conduire B obligatoire. \nCDD de 3 mois . Titularisation dans la Fonction Publique Hospitalière possible. Prise de poste au plus vite. Réponse au plus tard le 04/12/2019.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Merci d'adresser votre lettre de motivation et CV au plus vite à:\nMme Magali Guérin, directrice adjointe§\n- par courrier à: EDPA Le Village du Fier -BP 26- Argonay- 74371 Pringy cedex\n-ou par mail: secretariat.central@villagedufier.org",
        "nom": "LE VILLAGE DU FIER"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WKVV",
      "intitule": "Educateur spécialisé / Educatrice spécialisée   (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "74370",
        "commune": "74019",
        "latitude": 45.94527778,
        "libelle": "74 - ARGONAY",
        "longitude": 6.140277777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKVV"
      },
      "permis": [
        {
          "exigence": "E",
          "libelle": "B - Véhicule léger Exigé"
        }
      ],
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        }
      ],
      "romeCode": "K1207",
      "romeLibelle": "Intervention socioéducative",
      "salaire": {
        "commentaire": "Grille Fonct Publ Hospital",
        "complement1": "Grille Fonct Publ Hospital"
      },
      "secteurActivite": "87",
      "secteurActiviteLibelle": "Hébergement social pour enfants en difficultés",
      "trancheEffectifEtab": "200 à 249 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 3 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Aide d'élevage en production laitière",
      "competences": [
        {
          "code": "124335",
          "exigence": "S",
          "libelle": "Désinfecter et décontaminer un équipement"
        },
        {
          "code": "122572",
          "exigence": "S",
          "libelle": "Déterminer les rations alimentaires selon les besoins physiques et physiologiques des animaux"
        },
        {
          "code": "124543",
          "exigence": "S",
          "libelle": "Entretenir des locaux"
        },
        {
          "code": "100015",
          "exigence": "S",
          "libelle": "Normes environnementales"
        },
        {
          "code": "126362",
          "exigence": "S",
          "libelle": "Préparer la traite (regroupement et préparation des animaux)"
        },
        {
          "code": "100304",
          "exigence": "S",
          "libelle": "Procédure de traite"
        },
        {
          "code": "122860",
          "exigence": "S",
          "libelle": "Récolter le produit d'un élevage"
        },
        {
          "code": "100144",
          "exigence": "S",
          "libelle": "Règles d'hygiène et de sécurité"
        },
        {
          "code": "123075",
          "exigence": "S",
          "libelle": "Regrouper des animaux"
        },
        {
          "code": "100088",
          "exigence": "S",
          "libelle": "Techniques d'approche et de manipulation des animaux"
        }
      ],
      "complementExercice": "Ponctuellement le week end",
      "contact": {
        "coordonnees1": "Courriel : aef22@aef22.fr",
        "courriel": "aef22@aef22.fr",
        "nom": "ANEFA Cotes d'Armor  - Mme ANEFA"
      },
      "dateCreation": "2019-11-13T12:49:50+01:00",
      "description": "Exploitation située à Lohuec recherche un(e) agent(e) d'élevage laitier. Vous interviendrez sur tous les ateliers (traite, alimentation, paillage, soins, surveillance de l'élevage) en binôme avec l'exploitant. Ce poste s'adresse à un(e) candidat(e) ayant une formation agricole et/ou une expérience.- Prévoir de travailler de temps en temps le week-end",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "ANEFA Cotes d'Armor"
      },
      "experienceCommentaire": "Souhaitée",
      "experienceExige": "E",
      "experienceLibelle": "1 an - Souhaitée",
      "formations": [
        {
          "codeFormation": "21056",
          "domaineLibelle": "Elevage",
          "exigence": "S",
          "niveauLibelle": "Bac ou équivalent"
        }
      ],
      "id": "095WKVT",
      "intitule": "Agent d'élevage laitier  (H/F) OVL123041-22",
      "langues": [],
      "lieuTravail": {
        "codePostal": "22160",
        "commune": "22132",
        "latitude": 48.46027778,
        "libelle": "22 - LOHUEC",
        "longitude": -3.521666666
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKVT"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "A1403",
      "romeLibelle": "Aide d'élevage agricole et aquacole",
      "salaire": {
        "commentaire": "Selon CCN polyculture élevage",
        "complement1": "Selon CCN polyculture élevage"
      },
      "secteurActivite": "01",
      "secteurActiviteLibelle": "Élevage de vaches laitières",
      "trancheEffectifEtab": "1 ou 2 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "aleroussillon.38052@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Ingénieur / Ingénieure génie civil",
      "competences": [
        {
          "code": "118637",
          "exigence": "S",
          "libelle": "Analyser les besoins du client"
        },
        {
          "code": "118639",
          "exigence": "S",
          "libelle": "Analyser les données économiques du projet"
        },
        {
          "code": "113547",
          "exigence": "S",
          "libelle": "Définir la faisabilité et la rentabilité d'un projet"
        },
        {
          "code": "122764",
          "exigence": "S",
          "libelle": "Définir un avant-projet"
        },
        {
          "code": "104376",
          "exigence": "S",
          "libelle": "Élaborer des solutions techniques et financières"
        }
      ],
      "contact": {
        "coordonnees1": "8 RUE ANATOLE FRANCE\nCS 70034 ROUSSILLON",
        "coordonnees2": "38150 ROUSSILLON",
        "coordonnees3": "Courriel : aleroussillon.38052@pole-emploi.fr",
        "nom": "Pôle Emploi ROUSSILLON"
      },
      "dateCreation": "2019-11-13T12:49:29+01:00",
      "description": "Au sein d'une équipe commune, vous rejoignez la section \"Génie Civil\" qui a pour missions : piloter la maintenance génie civil des bâtiments industriels et nucléaires, contribuer à la sûreté des installations par la gestion de la sectorisation incendie, piloter les travaux génie civil nécessaires à la prolongation de la durée de vie du patrimoine industriel. Vous êtes affecté/e à l'une des ces missions. Vos activités : contribuer à l'atteinte de la performance de la CNPE dans les domaines de la sécurité des intervenants, de la sûreté des installations et du respect des plannings travaux, piloter la programme des travaux de maintenance sur la base des résultats des visites effectuées au titre des programmes de maintenance préventive, assure la gestion technique et la maintenance des éléments de sectorisation et de confinement statique, préparer la réalisation des activités, animer et surveiller les entreprises prestataires. Vous exercez à Belleville, Dampierre, Nogent, Penly, St Laurent",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {},
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "22024",
          "domaineLibelle": "Génie civil",
          "exigence": "S",
          "niveauLibelle": "Bac+2 ou équivalents"
        }
      ],
      "id": "095WKVQ",
      "intitule": "CHARGE D'AFFAIRES EN GENIE CIVIL - NUCLEAIRE2019",
      "langues": [],
      "lieuTravail": {
        "libelle": "Belleville-Dampierre-Nogent-Penly"
      },
      "natureContrat": "CDI de chantier ou d'opération",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKVQ"
      },
      "permis": [
        {
          "exigence": "S",
          "libelle": "B - Véhicule léger Souhaité"
        }
      ],
      "qualificationCode": "8",
      "qualificationLibelle": "Agent de maîtrise",
      "romeCode": "F1106",
      "romeLibelle": "Ingénierie et études du BTP",
      "salaire": {
        "libelle": "Annuel de 25000 Euros à 28000 Euros sur 12 mois"
      },
      "secteurActivite": "35",
      "secteurActiviteLibelle": "Production d'électricité",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Ouvrier / Ouvrière serriste",
      "competences": [
        {
          "code": "119210",
          "exigence": "S",
          "libelle": "Assurer une maintenance de premier niveau"
        },
        {
          "code": "114645",
          "exigence": "S",
          "libelle": "Configurer et paramétrer un équipement ou un système électronique"
        },
        {
          "code": "124888",
          "exigence": "S",
          "libelle": "Entretenir des équipements"
        },
        {
          "code": "122581",
          "exigence": "S",
          "libelle": "Entretenir une plantation"
        },
        {
          "code": "122704",
          "exigence": "S",
          "libelle": "Identifier le type d'intervention"
        },
        {
          "code": "125978",
          "exigence": "S",
          "libelle": "Préparer le matériel, les matériaux et les outillages"
        },
        {
          "code": "122730",
          "exigence": "S",
          "libelle": "Préparer les sols et les plantations (épandage, semis, récolte, ...)"
        },
        {
          "code": "102695",
          "exigence": "S",
          "libelle": "Réaliser la maintenance de premier niveau des machines et équipements"
        },
        {
          "code": "123033",
          "exigence": "S",
          "libelle": "Récolter un produit agricole"
        },
        {
          "code": "123046",
          "exigence": "S",
          "libelle": "Surveiller l'état d'une plantation"
        }
      ],
      "dateCreation": "2019-11-13T12:49:26+01:00",
      "description": "Vous effectuerez essentiellement la récolte au sein de serres de culture de tomates.\nTravail du lundi au vendredi (jours fériés inclus), horaire de journée.\n\nRecrutement utilisant la Méthode de Recrutement par Simulation MRS\nMerci de respecter le mode de candidature pour obtenir votre invitation à l'information collective.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "RECRUTEMENT SERRES TOMATES"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKVP",
      "intitule": "Ouvrier / Ouvrière serriste",
      "langues": [],
      "lieuTravail": {
        "codePostal": "40110",
        "commune": "40197",
        "latitude": 44.03305555,
        "libelle": "40 - MORCENX LA NOUVELLE",
        "longitude": -0.914722222
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKVP"
      },
      "qualificationCode": "2",
      "qualificationLibelle": "Ouvrier spécialisé",
      "romeCode": "A1414",
      "romeLibelle": "Horticulture et maraîchage",
      "salaire": {
        "libelle": "Horaire de 10,03 Euros"
      },
      "secteurActivite": "01",
      "secteurActiviteLibelle": "Culture de légumes, de melons, de racines et de tubercules",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 9 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Pilote qualité en système qualité en industrie",
      "competences": [
        {
          "code": "114647",
          "exigence": "S",
          "libelle": "Analyser les non-conformités et déterminer des mesures correctives"
        },
        {
          "code": "124842",
          "exigence": "S",
          "libelle": "Contrôler des données qualité"
        },
        {
          "code": "124680",
          "exigence": "S",
          "libelle": "Contrôler la conformité d'application de procédures qualité"
        },
        {
          "code": "124843",
          "exigence": "S",
          "libelle": "Déterminer les évolutions et améliorations d'une démarche qualité"
        },
        {
          "code": "123873",
          "exigence": "S",
          "libelle": "Mettre en place des procédures qualité"
        }
      ],
      "contact": {
        "coordonnees1": "https://jobs.smartrecruiters.com/somfygroup/743999699405739-pilote-assurance-qualite-developpement-des-produits-achetes-mecanique-h-f",
        "nom": "SOMFY SAS - M. Camille Deschamps",
        "urlPostulation": "https://jobs.smartrecruiters.com/somfygroup/743999699405739-pilote-assurance-qualite-developpement-des-produits-achetes-mecanique-h-f"
      },
      "dateCreation": "2019-11-13T12:49:24+01:00",
      "description": "-\tRattaché au responsable achat projet, vous mettez en œuvre avec l'ensemble des équipes impliquées, le processus d'Assurance Qualité nécessaire à la réception de produits conformes à l'ensemble des exigences spécifiées, dans les phases de développement de nos produits.\n-\tVous pilotez le développement des produits achetés en mettant en œuvre la planification Qualité associée (APQP).\n-\tVous savez gérer un projet de Co-Développement ou ODM fournisseur dans un contexte international (Asie - USA)\n-\tVous animez les réunions de faisabilités et exprimez les exigences Qualité correspondantes\n-\tVous vous assurez de la robustesse de conception et du produit développé par le fournisseur\n-\tVous vous assurez de la robustesse du procédé de fabrication fournisseur et de sa capacité à livrer des pièces conformes en vie série\n-\tVous mettez en place les moyens de contrôle à réception",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Somfy se concentre depuis plus de 50 ans sur un métier : l automatisation des ouvertures et des fermetures de la maison et des bâtiments et est aujourd hui au cœur des enjeux de la maison intelligente. Animée par une forte culture de l'innovation, Somfy crée des solutions qui contribuent à l'amélioration des cadres de vie des habitants et s engage à les rendre accessibles au plus grand nombre, en répondant à leurs attentes de confort, de sécurité et d'économie d'énergie.",
        "nom": "SOMFY SAS",
        "url": "https://www.somfy.com/portail/index.cfm"
      },
      "experienceExige": "E",
      "experienceLibelle": "5 ans",
      "formations": [],
      "id": "095WKVN",
      "intitule": "PILOTE ASSURANCE QUALITE DEVELOPPEMENT DES PRODUITS ACHETES (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "74300",
        "commune": "74081",
        "latitude": 46.06027778,
        "libelle": "74 - CLUSES",
        "longitude": 6.57861111
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKVN"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "H1502",
      "romeLibelle": "Management et ingénierie qualité industrielle",
      "salaire": {
        "commentaire": "compétitive",
        "complement1": "compétitive"
      },
      "secteurActivite": "27",
      "secteurActiviteLibelle": "Fabrication de moteurs, génératrices et transformateurs électriques",
      "trancheEffectifEtab": "500 à 999 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Charcutier / Charcutière",
      "competences": [
        {
          "code": "123522",
          "exigence": "S",
          "libelle": "Conditionner un produit alimentaire"
        },
        {
          "code": "124545",
          "exigence": "S",
          "libelle": "Entretenir un espace de vente"
        },
        {
          "code": "124548",
          "exigence": "S",
          "libelle": "Entretenir un poste de travail"
        },
        {
          "code": "124547",
          "exigence": "S",
          "libelle": "Nettoyer du matériel ou un équipement"
        },
        {
          "code": "124451",
          "exigence": "S",
          "libelle": "Procédures de conditionnement"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : compta@saschb.fr",
        "courriel": "compta@saschb.fr",
        "nom": "CHB - M. Guglielmi"
      },
      "dateCreation": "2019-11-13T12:49:13+01:00",
      "description": "Vous effectuez la préparation de commandes des produits crus de porc d'après le cahier des charges de l'entreprise, selon la commande du client, et dans le respect des normes d'hygiène et de sécurité alimentaire. \nVous approvisionnez également le stock selon le FIFO, vous vérifier les éléments de traçabilité.\nVous travaillez à partir de 7h le matin, du lundi au vendredi, jusqu'à 20h30 le soir en période estivale.",
      "dureeTravailLibelle": "42H30 Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "CHB"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WKVM",
      "intitule": "Charcutier / Charcutière",
      "langues": [],
      "lieuTravail": {
        "codePostal": "28330",
        "commune": "28027",
        "latitude": 48.13861111,
        "libelle": "28 - LA BAZOCHE GOUET",
        "longitude": 0.980555554
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKVM"
      },
      "qualificationCode": "4",
      "qualificationLibelle": "Ouvrier qualifié (P3,P4,OHQ)",
      "romeCode": "D1103",
      "romeLibelle": "Charcuterie - traiteur",
      "salaire": {
        "libelle": "Mensuel de 2300 Euros sur 12 mois"
      },
      "secteurActivite": "01",
      "secteurActiviteLibelle": "Élevage de porcins",
      "trancheEffectifEtab": "50 à 99 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Adjoint / Adjointe au responsable de rayon produits alimentaires",
      "competences": [
        {
          "code": "120130",
          "exigence": "E",
          "libelle": "Accueillir une clientèle"
        },
        {
          "code": "102357",
          "exigence": "E",
          "libelle": "Adapter le plan d'implantation des articles"
        },
        {
          "code": "123793",
          "exigence": "E",
          "libelle": "Contrôler l'état de conservation d'un produit périssable"
        },
        {
          "code": "123624",
          "exigence": "E",
          "libelle": "Contrôler la conformité d'un produit"
        },
        {
          "code": "116396",
          "exigence": "E",
          "libelle": "Contrôler la mise en rayon des articles"
        },
        {
          "code": "125971",
          "exigence": "E",
          "libelle": "Définir des besoins en approvisionnement"
        },
        {
          "code": "119860",
          "exigence": "E",
          "libelle": "Préparer les commandes"
        },
        {
          "code": "121932",
          "exigence": "E",
          "libelle": "Réaliser la coupe de produits frais"
        },
        {
          "code": "117468",
          "exigence": "E",
          "libelle": "Réaliser la mise en rayon"
        },
        {
          "code": "122330",
          "exigence": "E",
          "libelle": "Renseigner un client"
        },
        {
          "code": "123792",
          "exigence": "E",
          "libelle": "Retirer un produit impropre à la vente"
        },
        {
          "code": "116932",
          "exigence": "E",
          "libelle": "Suivre l'état des stocks"
        },
        {
          "code": "117488",
          "exigence": "E",
          "libelle": "Superviser le stockage des produits"
        },
        {
          "code": "119851",
          "exigence": "E",
          "libelle": "Vérifier la conformité de la livraison"
        },
        {
          "code": "119912",
          "exigence": "S",
          "libelle": "Analyser les résultats des ventes"
        },
        {
          "code": "101716",
          "exigence": "S",
          "libelle": "Chaîne du froid"
        },
        {
          "code": "102358",
          "exigence": "S",
          "libelle": "Concevoir les opérations commerciales selon les marges, le chiffre d'affaires et les prévisions"
        },
        {
          "code": "102359",
          "exigence": "S",
          "libelle": "Coordonner l'activité d'une équipe"
        },
        {
          "code": "126213",
          "exigence": "S",
          "libelle": "Effectuer des relevés de prix auprès de la concurrence"
        },
        {
          "code": "124629",
          "exigence": "S",
          "libelle": "Entretenir un outil ou matériel"
        },
        {
          "code": "121060",
          "exigence": "S",
          "libelle": "Former du personnel à des procédures et techniques"
        },
        {
          "code": "102363",
          "exigence": "S",
          "libelle": "Techniques de découpe de produits frais"
        },
        {
          "code": "100340",
          "exigence": "S",
          "libelle": "Techniques de vente"
        }
      ],
      "dateCreation": "2019-11-13T12:49:11+01:00",
      "deplacementCode": "1",
      "deplacementLibelle": "Jamais",
      "description": "Le second de rayon anime et contribue à la bonne marche du rayon Fruits & Légumes et Poissonnerie en s'impliquant directement dans la vie du rayon (implantation, approvisionnement, conseil )  tout en assurant l'organisation du travail de l'équipe et la satisfaction de la clientèle.\n\nSous la responsabilité du chef de rayon, vous serez notamment en charge de :\n \tParticiper à la fidélisation de nos clients : renseigner, orienter, conseiller la clientèle\n \tRendre le rayon attractif et accessible pour contribuer au développement des ventes \n \tAssurer la mise en rayon des marchandises livrées en tenant compte des consignes d'implantation et de  présentation \n \tAvec le responsable de rayon, participer à l'animation, la formation et à l'intégration des collaborateurs\n \tMettre en place l'offre Poissonnerie (faire le glaçage des bancs poissons et faire les contrôles des températures)\n\nVous intégrez un véritable parcours de formation.\nRémunération fixe + primes mensuelles",
      "dureeTravailLibelle": "38H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "GRAND FRAIS F&L"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WKVL",
      "intitule": "Adjoint(e) au responsable de rayon produits alimentaires  (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "67640",
        "commune": "67137",
        "latitude": 48.49055556,
        "libelle": "67 - FEGERSHEIM",
        "longitude": 7.680277777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKVL"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "D1502",
      "romeLibelle": "Management/gestion de rayon produits alimentaires",
      "salaire": {
        "complement1": "Primes",
        "libelle": "Mensuel de 1868,00 Euros sur 12 mois"
      },
      "secteurActivite": "46",
      "secteurActiviteLibelle": "Commerce de gros (commerce interentreprises) de fruits et légumes",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Électricien / Électricienne bâtiment tertiaire",
      "competences": [
        {
          "code": "121777",
          "exigence": "E",
          "libelle": "Fixer des éléments basse tension"
        },
        {
          "code": "123597",
          "exigence": "E",
          "libelle": "Positionner une armoire électrique de locaux domestiques ou tertiaires"
        },
        {
          "code": "121778",
          "exigence": "E",
          "libelle": "Raccorder des éléments basse tension"
        },
        {
          "code": "123598",
          "exigence": "E",
          "libelle": "Raccorder une armoire électrique aux équipements de locaux domestiques ou tertiaires"
        },
        {
          "code": "103495",
          "exigence": "E",
          "libelle": "Réaliser et poser des chemins de câbles et des conduits électriques en apparent ou en encastré"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : lille2@welljob.fr",
        "courriel": "lille2@welljob.fr",
        "nom": "WELL JOB - Mme Céline  GANDILLET"
      },
      "dateCreation": "2019-11-13T12:48:16+01:00",
      "description": "L'agence WELLJOB EMPLOI DE LILLE recrute un ELECTRICIEN BATIMENT TERTIAIRE H/F.\n\nMissions :\nVous aurez pour missions sous la responsabilité du chef d'équipe:\n- La lecture du schéma du réseau électrique.\n- La mise en place des aménagements pour l'installation d'un réseau ou sa rénovation.\n- La mise en place d'appareillages et de luminaires\n- Le câblage depuis la source d'énergie.\n- Le raccordement et dérivation du flux électrique.\n- La réalisation d'une phase de test et de mesure.\nProfil : \nHabilitations électriques obligatoires.\nMission en intérim à pourvoir dès que possible.\n\nExpérience Vous justifiez d'une expérience significative réussie  sur un poste similaire.",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "AGENCE WELLJOB EMPLOI DE LILLE \n54 Bis Boulevard de la Liberté\n59800 LILLE",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/aJRrFYgRrRAI0d6wyXUPFevjCOGo2rhH.png",
        "nom": "WELL JOB",
        "url": "https://www.welljob.fr/entreprise"
      },
      "experienceExige": "E",
      "experienceLibelle": "3 ans",
      "formations": [],
      "id": "095WKVD",
      "intitule": "Electricien / Electricienne bâtiment tertiaire",
      "langues": [],
      "lieuTravail": {
        "codePostal": "59155",
        "commune": "59220",
        "latitude": 50.59888889,
        "libelle": "59 - FACHES THUMESNIL",
        "longitude": 3.07361111
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKVD"
      },
      "qualificationCode": "2",
      "qualificationLibelle": "Ouvrier spécialisé",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        },
        {
          "description": "Capacité à être proactif, à initier, à imaginer des propositions nouvelles pour résoudre les problèmes identifiés ou pour améliorer une situation. Exemple : proposer des améliorations, être positif et constructif",
          "libelle": "Force de proposition"
        }
      ],
      "romeCode": "F1602",
      "romeLibelle": "Électricité bâtiment",
      "salaire": {
        "complement1": "Primes",
        "libelle": "Horaire de 10,97 Euros à 14,22 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 6 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Secrétaire médical / médicale",
      "competences": [
        {
          "code": "107353",
          "exigence": "S",
          "libelle": "Accueillir et renseigner le patient sur les horaires de réception, les possibilités de rendez-vous, le déroulement de l'examen"
        },
        {
          "code": "107201",
          "exigence": "S",
          "libelle": "Actualiser le dossier médical du patient"
        },
        {
          "code": "123899",
          "exigence": "S",
          "libelle": "Organiser le planning des activités"
        },
        {
          "code": "124388",
          "exigence": "S",
          "libelle": "Réaliser des démarches médico administratives"
        },
        {
          "code": "124221",
          "exigence": "S",
          "libelle": "Tenir à jour les dossiers médico-administratifs des patients"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : docteur.f.madar@orange.fr",
        "courriel": "docteur.f.madar@orange.fr",
        "nom": "CABINET MADAR ET ASSOCIES - M. fabrice madar"
      },
      "dateCreation": "2019-11-13T12:48:03+01:00",
      "description": "Un cabinet dentaire à Sèvres recherche un(e) secrétaire médical(e) rigoureux(se), organisé(e), qui sache accueillir les patients, les renseigner, noter les rendez-vous, organiser les plannings, télétransmettre les actes  de la sécurité sociale...\nVous maitrisez l'informatique et vous aimez le contact.\nFormation assurée.",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "CABINET MADAR ET ASSOCIES"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKTY",
      "intitule": "Secrétaire médical / médicale (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "92310",
        "commune": "92072",
        "latitude": 48.82305555,
        "libelle": "92 - SEVRES",
        "longitude": 2.210833333
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKTY"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "M1609",
      "romeLibelle": "Secrétariat et assistanat médical ou médico-social",
      "salaire": {
        "libelle": "Mensuel de 2048,15 Euros sur 12 mois"
      },
      "secteurActivite": "86",
      "secteurActiviteLibelle": "Pratique dentaire",
      "trancheEffectifEtab": "6 à 9 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Professeur / Professeure à domicile",
      "competences": [
        {
          "code": "108475",
          "exigence": "S",
          "libelle": "Développer la démarche pédagogique et enseigner les savoirs fondamentaux (français, mathématiques, sciences, ...)"
        },
        {
          "code": "118230",
          "exigence": "S",
          "libelle": "Français"
        },
        {
          "code": "108816",
          "exigence": "S",
          "libelle": "Mathématiques"
        },
        {
          "code": "108412",
          "exigence": "S",
          "libelle": "Préparer les cours et établir la progression pédagogique"
        },
        {
          "code": "108478",
          "exigence": "S",
          "libelle": "Programme de l'Éducation Nationale"
        },
        {
          "code": "108490",
          "exigence": "S",
          "libelle": "Suivre et conseiller les élèves dans l'organisation du travail personnel"
        },
        {
          "code": "100090",
          "exigence": "S",
          "libelle": "Techniques pédagogiques"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : marion.borel@anacours.fr",
        "courriel": "marion.borel@anacours.fr",
        "nom": "ANACOURS - Mme Marion Borel"
      },
      "dateCreation": "2019-11-13T12:48:03+01:00",
      "description": "Anacours, expert du soutien scolaire en France, recrute un(e) intervenant(e) en supervision des devoirs à domicile à BRIGNOUD (38190), pour un élève en classe de 5EME.\n\nVous avez validé un niveau Bac+3 minimum et vous souhaitez intégrer une équipe pédagogique motivée et engagée dans la réussite scolaire de nos élèves, et ainsi acquérir une expérience enrichissante au sein d'un organisme reconnu pour la qualité de ses cours et la relation de proximité avec ses enseignants.\n\nVos Avantages :\n- Une rémunération nette et claire,\n- Un paiement 2 fois dans le mois,\n- Une possibilité de suivre de nombreux élèves,\n- Offres de cours disponibles en ligne,\n- Une expérience professionnelle dans l'enseignement valorisante.\n- En fonction de votre emploi du temps, vous sélectionnez des cours particuliers à domicile qui correspondent à vos disponibilités : en semaine en fin de journée, le mercredi toute la journée ou le week-end.",
      "dureeTravailLibelle": "1H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps partiel",
      "entreprise": {
        "description": "Anacours, spécialiste du soutien scolaire depuis plus de 15 ans, propose des cours particuliers dans de nombreuses matières et pour tous les niveaux : primaire, secondaire, supérieur, prépas et compte aujourd'hui parmi les leaders du secteur.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/Uig3tfjdkuljbeUW8q4l3jMGZy2DVUTA.png",
        "nom": "ANACOURS",
        "url": "https://www.anacours.com/"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "E",
          "niveauLibelle": "Bac+3, Bac+4 ou équivalents"
        }
      ],
      "id": "095WKTX",
      "intitule": "Professeur / Professeure à domicile",
      "langues": [],
      "lieuTravail": {
        "codePostal": "38190",
        "commune": "38547",
        "latitude": 45.23805556,
        "libelle": "38 - VILLARD BONNOT",
        "longitude": 5.888333333
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKTX"
      },
      "qualificationCode": "8",
      "qualificationLibelle": "Agent de maîtrise",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "K2107",
      "romeLibelle": "Enseignement général du second degré",
      "salaire": {
        "libelle": "Horaire de 16,00 Euros à 16,00 Euros sur 12 mois"
      },
      "secteurActivite": "85",
      "secteurActiviteLibelle": "Formation continue d'adultes",
      "trancheEffectifEtab": "3 à 5 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 7 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Chef de projet informatique",
      "competences": [
        {
          "code": "109797",
          "exigence": "S",
          "libelle": "Analyser et définir les besoins de l'entreprise, des utilisateurs en matière d'organisation et de systèmes d'information et de télécoms"
        },
        {
          "code": "109798",
          "exigence": "S",
          "libelle": "Concevoir le schéma directeur à partir des orientations fixées par la direction et des besoins des services utilisateurs et superviser les modalités de mise en oeuvre"
        },
        {
          "code": "118023",
          "exigence": "S",
          "libelle": "Coordonner les différentes étapes d'un projet"
        },
        {
          "code": "113325",
          "exigence": "S",
          "libelle": "Piloter un projet"
        },
        {
          "code": "109800",
          "exigence": "S",
          "libelle": "Procéder au choix de réalisation, de traitement en interne ou par sous-traitance et en contrôler la conformité de réalisation"
        }
      ],
      "contact": {
        "coordonnees1": "https://jobs.smartrecruiters.com/Edenred/743999699430584-chef-de-projet-h-f",
        "nom": "EDENRED FRANCE - M. Clément-Matthieu MOAL",
        "urlPostulation": "https://jobs.smartrecruiters.com/Edenred/743999699430584-chef-de-projet-h-f"
      },
      "dateCreation": "2019-11-13T12:47:39+01:00",
      "description": "Le chef de projet H/F est le point de contact privilégié de PWCE (en particulier les Business Owners qui sont ses interlocuteurs principaux).\nVous êtes le/la garant(e) de la bonne communication entre les équipes E2F et les différents stakeholders PWCE, afin d'assurer une totale transparence sur la roadmap E2F.\nVous serez amené(e) à intervenir sur les petites, moyennes et grosses évolutions fonctionnelles ou techniques.\n\nScope : Back office, Middle Office, Front office e-commerce et application mobile.\n\nLes missions principales sont : \n\nCadrer, coordonner et suivre des demandes d'évolutions\nAnalyser des demandes d'évolution\nRecueillir les besoins des utilisateurs, cadrer des besoins business, aider à la rédaction des expressions de besoins\nCadrer et chiffrer avec les équipes produit et IT E2F\nAider à la préparation des GPB\nParticiper aux ateliers métier\nOrganiser, coordonner et suivre des développements\nSuivre des plannings\nOrganiser et animer des comités de suivi\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Edenred, inventeur de Ticket Restaurant® et leader mondial des services prépayés aux entreprises, conçoit et gère des solutions qui permettent de gérer : Avantages aux salariés (Ticket Restaurant®,...) / Frais professionnels (Ticket Travel Pro®,...) / Motivation et récompenses (Ticket Kadéos®,...) / Programmes sociaux publics (Ticket CESU, Ticket Service®)\nFort de 700 collaborateurs, Edenred France déploie ses solutions auprès de 120 000 clients, 6,7 millions d'utilisateurs et 380 000 affiliés.",
        "nom": "EDENRED FRANCE",
        "url": "http://www.edenred.fr"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKTS",
      "intitule": "Chef de projet H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "92300",
        "commune": "92044",
        "latitude": 48.89333333,
        "libelle": "92 - LEVALLOIS PERRET",
        "longitude": 2.287777777
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKTS"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "M1803",
      "romeLibelle": "Direction des systèmes d'information",
      "salaire": {
        "commentaire": "compétitive",
        "complement1": "compétitive"
      },
      "secteurActivite": "66",
      "secteurActiviteLibelle": "Autres activités auxiliaires de services financiers, hors assurance et caisses de retraite, n.c.a.",
      "trancheEffectifEtab": "500 à 999 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Chargé / Chargée de développement culturel",
      "competences": [
        {
          "code": "123182",
          "exigence": "S",
          "libelle": "Animer une réunion"
        },
        {
          "code": "118949",
          "exigence": "S",
          "libelle": "Apporter un appui à des partenaires institutionnels"
        },
        {
          "code": "117427",
          "exigence": "S",
          "libelle": "Communication externe"
        },
        {
          "code": "121793",
          "exigence": "S",
          "libelle": "Concevoir un plan d'action de projet"
        },
        {
          "code": "104283",
          "exigence": "S",
          "libelle": "Environnement culturel et touristique"
        },
        {
          "code": "120889",
          "exigence": "S",
          "libelle": "Gestion administrative"
        },
        {
          "code": "120246",
          "exigence": "S",
          "libelle": "Gestion de projet"
        },
        {
          "code": "118942",
          "exigence": "S",
          "libelle": "Identifier de nouveaux axes d'intervention"
        },
        {
          "code": "118938",
          "exigence": "S",
          "libelle": "Participer à l'élaboration d'un projet de développement local"
        },
        {
          "code": "118943",
          "exigence": "S",
          "libelle": "Présenter un projet à des acteurs locaux"
        },
        {
          "code": "100541",
          "exigence": "S",
          "libelle": "Techniques de communication"
        },
        {
          "code": "100090",
          "exigence": "S",
          "libelle": "Techniques pédagogiques"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : s.thevenet@cc-hautpoitou.fr",
        "courriel": "s.thevenet@cc-hautpoitou.fr",
        "nom": "COMMUNAUTE DE COMMUNES DU HAUT POITOU - M. SEBASTIEN THEVENET"
      },
      "dateCreation": "2019-11-13T12:47:30+01:00",
      "description": "Dans le cadre d'un remplacement temporaire, sous l'autorité du DGA \"Service à la population\", la Communauté de communes du Haut-Poitou recrute un(e) Coordinateur/trice \"Culture\", par référence au cadre d'emplois des Attachés territoriaux (catégorie A), avec pour axes principaux :\n-\tMettre en œuvre les projets définis au titre de la politique culturelle telle que décidée par les élus communautaires ;\n-\tAnimer les partenariats, tant en interne qu'en externe (Ecoles de musique, ...).\n\nMissions principales : 1° ) Contribuer à l'élaboration et la mise en œuvre de la politique culturelle (décliner les orientations du projet politique culturel, conseiller les élus notamment dans le cadre de la Commission \"Culture\", mettre en oeuvre la programmation définie,...) - 2°) Impulser, piloter et évaluer les projets culturels à portée intercommunale (instruire les demandes de subventions, accompagner élus et porteurs de projets, programmer les événements culturels...).\n\n",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Etablissement relevant de la fonction publique territoriale, la Communauté de communes du Haut-Poitou est née, au 1er janvier 2017, de la fusion des Communautés de communes du Mirebalais, du Neuvillois et du Vouglaisien.\nIntervenant sur un périmètre de 27 communes et environ 42.000 habitants, notre établissement est idéalement situé, à proximité immédiate de l'agglomération de Poitiers et du Futuroscope.",
        "nom": "COMMUNAUTE DE COMMUNES DU HAUT POITOU",
        "url": "http://www.cc-hautpoitou.fr/"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [
        {
          "codeFormation": "14254",
          "commentaire": "Métiers de la culture",
          "domaineLibelle": "Sciences humaines",
          "exigence": "S",
          "niveauLibelle": "Bac+3, Bac+4 ou équivalents"
        },
        {
          "codeFormation": "14254",
          "commentaire": "Métiers de la culture",
          "domaineLibelle": "Sciences humaines",
          "exigence": "S",
          "niveauLibelle": "Bac+5 et plus ou équivalents"
        }
      ],
      "id": "095WKTQ",
      "intitule": "Chargé / Chargée de développement culturel (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "86170",
        "commune": "86177",
        "latitude": 46.685,
        "libelle": "86 - NEUVILLE DE POITOU",
        "longitude": 0.244999999
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKTQ"
      },
      "qualificationCode": "9",
      "qualificationLibelle": "Cadre",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à s’adapter à des situations variées et à s’ajuster à des organisations, des collectifs de travail, des habitudes et des valeurs propres à l’entreprise. Exemple : être souple, agile, s’adapter à une situation imprévue",
          "libelle": "Capacité d’adaptation"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        }
      ],
      "romeCode": "K1802",
      "romeLibelle": "Développement local",
      "salaire": {
        "libelle": "Mensuel de 1900 Euros à 2100 Euros sur 12 mois"
      },
      "secteurActivite": "84",
      "secteurActiviteLibelle": "Administration publique générale",
      "trancheEffectifEtab": "Non défini",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 5 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Secrétaire médicosocial / médicosociale",
      "competences": [
        {
          "code": "126265",
          "exigence": "S",
          "libelle": "Modalités d'accueil"
        },
        {
          "code": "118975",
          "exigence": "S",
          "libelle": "Outils bureautiques"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : mp.ogheard@vivre-asso.com",
        "courriel": "mp.ogheard@vivre-asso.com",
        "nom": "VIVRE ASSO - Mme MARIE PIERRE OGHEARD"
      },
      "dateCreation": "2019-11-13T12:47:15+01:00",
      "description": "Dans un établissement qui accueillent des personnes en situation de handicap (sur rendez-vous ) , la secrétaire médico-sociale  réalise l'accueil téléphonique et physique des personnes  en lien avec l'équipe des travailleurs sociaux et la chef de service. Elle rédige des courriers et gère le tableau de suivi de l'activité ( saisie de données) en lien avec le chef de service .\n\nHoraires de travail de 9h à 13h - 14h/18h du lundi au jeudi et 17h le vendredi",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "VIVRE ASSO"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKTM",
      "intitule": "Secrétaire médicosocial / médicosociale",
      "langues": [],
      "lieuTravail": {
        "codePostal": "92320",
        "commune": "92020",
        "latitude": 48.80111111,
        "libelle": "92 - CHATILLON",
        "longitude": 2.28861111
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKTM"
      },
      "qualificationCode": "4",
      "qualificationLibelle": "Ouvrier qualifié (P3,P4,OHQ)",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        }
      ],
      "romeCode": "M1609",
      "romeLibelle": "Secrétariat et assistanat médical ou médico-social",
      "salaire": {
        "libelle": "Mensuel de 1830,00 Euros à 1830,00 Euros sur 12 mois"
      },
      "secteurActivite": "88",
      "secteurActiviteLibelle": "Action sociale sans hébergement n.c.a.",
      "trancheEffectifEtab": "10 à 19 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 15 Jour(s)"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Chargé / Chargée d'affaires en industrie",
      "competences": [],
      "contact": {
        "coordonnees1": "http://offresgroupecoriance.fr/consult.php?offre=2cfa15735675036955&job=185",
        "urlPostulation": "http://offresgroupecoriance.fr/consult.php?offre=2cfa15735675036955&job=185"
      },
      "dateCreation": "2019-11-13T12:46:47+01:00",
      "description": "Rattaché au chef d'agence, le Chargé d'Affaires (H/F) assurera la coordination de l'exécution du contrat de DSP (Délégation de Service Public) dont il porte la responsabilité. Ainsi, il aura pour missions principales de :\n\n-\tAssurer la coordination de l'exécution des contrats de Délégation de Services Publiques sur son secteur,\n-\tEntretenir les relations avec les Délégants et les clients auprès desquels il devra accompagner les offres jusqu'à la signature de la police ou du contrat, et assurer un suivi sur la durée des contrats,\n-\tDéfinir et mettre en œuvre un plan de développement,\n-\tS'assurer de la coordination des différents services pour la réalisation de projets,\n-\tRéaliser l'analyse financière des projets de développement et assurer leur reporting interne.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "CORIANCE"
      },
      "experienceExige": "E",
      "experienceLibelle": "5 ans",
      "formations": [
        {
          "codeFormation": "99999",
          "exigence": "S",
          "niveauLibelle": "Bac+5 et plus ou équivalents"
        }
      ],
      "id": "095WKTH",
      "intitule": "Chargé d'Affaires en CDD de 6 mois   CA-13 (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "13080",
        "commune": "13001",
        "latitude": 43.52777778,
        "libelle": "13 - AIX EN PROVENCE",
        "longitude": 5.445555555
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKTH"
      },
      "qualificationCode": "7",
      "qualificationLibelle": "Technicien",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        }
      ],
      "romeCode": "H1102",
      "romeLibelle": "Management et ingénierie d'affaires",
      "salaire": {
        "commentaire": "35 000",
        "complement1": "35 000"
      },
      "secteurActivite": "35",
      "secteurActiviteLibelle": "Production et distribution de vapeur et d'air conditionné",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 6 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Responsable service relation clientèle",
      "competences": [
        {
          "code": "118275",
          "exigence": "S",
          "libelle": "Améliorer une procédure qualité"
        },
        {
          "code": "119317",
          "exigence": "S",
          "libelle": "Contrôler l'application d'une procédure"
        },
        {
          "code": "102359",
          "exigence": "S",
          "libelle": "Coordonner l'activité d'une équipe"
        },
        {
          "code": "119106",
          "exigence": "S",
          "libelle": "Réaliser un suivi d'activité"
        }
      ],
      "contact": {
        "coordonnees1": "https://taleez.com/apply/3kl110k",
        "nom": "MUTUELLE VIA SANTE - Mme AURELIE PAIN",
        "urlPostulation": "https://taleez.com/apply/3kl110k"
      },
      "dateCreation": "2019-11-13T12:46:33+01:00",
      "description": "Il y a ceux qui rêvent d'être manager et ceux qui postulent ! Nous sommes à la recherche de jeunes talents qui aiment encadrer, motiver une équipe, coordonnez le travail, résoudre des situations ! Le challenge ? Se présenter en 60 secondes ! Pour ce faire, suivez le lien : https://taleez.com/apply/3kl110k\n\nConcrètement, on attendra de vous que vous :\n- Organisiez et coordonnez l'activité de l'équipe\n- Répartissiez les missions de votre équipe tout en veillant à l'atteinte de leurs résultats\n- Interveniez en qualité de support, expertise et conseil de l'équipe sur des dossiers spécifiques\n- Reportiez à votre direction le suivi d'activité de votre équipe\nVous faites preuve d'une appétence et capacité à maîtriser des activités de relation client téléphoniques. Véritable leader, vous avez des aptitudes de management de proximité. Soyez en sûr, votre sens pédagogue, votre rigueur ainsi que votre esprit d'équipe vous permettront de réussir vos missions.",
      "dureeTravailLibelle": "34H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps partiel",
      "entreprise": {
        "description": "VIASANTE Mutuelle est le nouveau pole de santé mutualiste au sein du groupe AG2R La mondiale. Comptant dans les  5 premiers acteurs mutualistes français de la santé. VIASANTE Mutuelle (1 million de personnes protégées) recherche un responsable d'unité relation client (H/F) en CDI  dans le cadre d'une création de poste.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/a7d30295eff34ae584aff98cc16defcb.png",
        "nom": "MUTUELLE VIA SANTE",
        "url": "http://www.mutuelle-viasante.fr/recrutement"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKTF",
      "intitule": "Responsable service relation clientèle      (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "24000",
        "commune": "24322",
        "latitude": 45.18416667,
        "libelle": "24 - PERIGUEUX",
        "longitude": 0.718055554
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKTF"
      },
      "qualificationCode": "7",
      "qualificationLibelle": "Technicien",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à mobiliser une équipe e/ou des interlocuteurs et à les entrainer dans la poursuite d’objectifs partagés. Exemple : être un leader, rassembler",
          "libelle": "Capacité à fédérer"
        },
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "M1704",
      "romeLibelle": "Management relation clientèle",
      "salaire": {
        "libelle": "Annuel de 24000,00 Euros à 30000,00 Euros sur 12 mois"
      },
      "secteurActivite": "65",
      "secteurActiviteLibelle": "Autres assurances",
      "trancheEffectifEtab": "50 à 99 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Conducteur / Conductrice de véhicules Super Lourds",
      "competences": [
        {
          "code": "120376",
          "exigence": "E",
          "libelle": "Contrôler l'état de fonctionnement du véhicule"
        },
        {
          "code": "124795",
          "exigence": "E",
          "libelle": "Définir un itinéraire en fonction des consignes de livraison"
        },
        {
          "code": "110473",
          "exigence": "E",
          "libelle": "Organiser ou contrôler le chargement des marchandises dans le véhicule"
        },
        {
          "code": "120377",
          "exigence": "E",
          "libelle": "Réaliser les opérations d'attelage"
        },
        {
          "code": "110474",
          "exigence": "E",
          "libelle": "Vérifier la présence et la conformité des documents de bord et de transport"
        },
        {
          "exigence": "S",
          "libelle": "caces"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : barbara.monchicourt@qapa.com",
        "courriel": "barbara.monchicourt@qapa.com",
        "nom": "QAPA INTERIM - Mme Barbara MONCHICOURT"
      },
      "dateCreation": "2019-11-13T12:46:28+01:00",
      "deplacementCode": "4",
      "deplacementLibelle": "Quotidiens Régional",
      "description": "Nous recherchons pour notre client, entreprise familiale de location de transports avec conducteur et de logistique française,  un Conducteur de véhicules Super Lourds (H/F) pour effectuer de la livraison de produits surgelés pour la grande distribution à Cestas.\n\nVous disposez d'une expérience dans la grande distribution et gérer vous-même le chargement de la cargaison ne vous fait pas peur ? N'hésitez plus, cette offre est faite pour vous !\n\nMissions Conducteur :\n- gérer le chargement et le déchargement du camion avec un transpalette électrique,\n- remplir les documents administratifs adéquats,\n\nProfil recherché :\n- savoir gérer son temps et prévoir son itinéraire,\n- connaître les règles de sécurité,\n\n\n\nPrécisions :\n- travail 6 jours par semaine avec un repos dans la semaine (jour de repos négociable)\n\n- travail de jour et de nuit possible, avec roulement toutes les quinzaines.",
      "dureeTravailLibelle": "39H Travail le samedi",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Qapa est la première agence d'intérim digital, et recrute de nombreux profils pour des missions de toutes durées et partout en France. Rejoignez-nous !",
        "nom": "QAPA INTERIM",
        "url": "http://www.qapa.fr"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKTC",
      "intitule": "Conducteur / Conductrice de véhicules Super Lourds (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "33130",
        "commune": "33039",
        "latitude": 44.80777778,
        "libelle": "33 - BEGLES",
        "longitude": -0.548888888
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 3,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKTC"
      },
      "permis": [
        {
          "exigence": "E",
          "libelle": "EC - Poids lourd + remorque Exigé"
        }
      ],
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à s’adapter à des situations variées et à s’ajuster à des organisations, des collectifs de travail, des habitudes et des valeurs propres à l’entreprise. Exemple : être souple, agile, s’adapter à une situation imprévue",
          "libelle": "Capacité d’adaptation"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        }
      ],
      "romeCode": "N4101",
      "romeLibelle": "Conduite de transport de marchandises sur longue distance",
      "salaire": {
        "complement1": "Primes",
        "libelle": "Horaire de 10,3 Euros à 10,39 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 1 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Dessinateur / Dessinatrice en chaudronnerie",
      "competences": [
        {
          "code": "104408",
          "exigence": "S",
          "libelle": "Constituer et faire évoluer les nomenclatures des plans, dossiers de définition"
        },
        {
          "code": "104421",
          "exigence": "S",
          "libelle": "Étudier et concevoir des pièces, sous-ensembles ou ensembles"
        },
        {
          "code": "104419",
          "exigence": "S",
          "libelle": "Identifier la demande et réaliser les ébauches, schémas de pièces, systèmes, sous-ensembles ou ensembles"
        },
        {
          "code": "104407",
          "exigence": "S",
          "libelle": "Réaliser et faire évoluer les schémas, les plans de détails, de sous-ensembles ou d'ensembles"
        },
        {
          "code": "104420",
          "exigence": "S",
          "libelle": "Réaliser les relevés dimensionnels de pièces, sous-ensembles ou ensembles"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : aurelia.fouquet@adecco.fr",
        "courriel": "aurelia.fouquet@adecco.fr",
        "nom": "ADECCO - Mme Aurélia FOUQUET"
      },
      "dateCreation": "2019-11-13T12:46:02+01:00",
      "description": "L'agence Adecco de La Ferté Bernard recrute pour un de ses clients, un dessinateur industriel (H/F):\n\nAu sein d'une entreprise de chaudronnerie et structures métalliques, vous devrez dessiner des produits sur un logiciel 3D puis créer les phases de découpage en fonction des pièces demandées.\n\nVous aurez en charge:\n- Dessiner les produits de chaudronnerie\n- Analyser la faisabilité des pièces\n- Elaborer les devis\n- Créer et suivre la production des pièces en atelier\n- Mettre en plan les pièces et les ensembles\n- ...\n\nVous serez amener à vous déplacer sur les chantiers pour faire de la prise de cotation.\n\nDe formation BAC à BAC +3, vous devez justifier d'une expérience réussie sur un poste similaire.\n\nLa connaissance du module tôlerie de Solidworks est indispensable.\n\nLe poste est à pourvoir au plus vite pour un contrat à durée indéterminée.\n\nSalaire à négocier selon profil.",
      "dureeTravailLibelle": "39H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/pYVc0hhtMGO8PVjxkcvzHFo3tqEGLgHD.png",
        "nom": "ADECCO",
        "url": "http://www.adecco.fr"
      },
      "experienceExige": "E",
      "experienceLibelle": "3 ans",
      "formations": [],
      "id": "095WKSY",
      "intitule": "Dessinateur / Dessinatrice en chaudronnerie",
      "langues": [],
      "lieuTravail": {
        "codePostal": "72400",
        "commune": "72093",
        "latitude": 48.17,
        "libelle": "72 - CORMES",
        "longitude": 0.704722222
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKSY"
      },
      "qualificationCode": "7",
      "qualificationLibelle": "Technicien",
      "romeCode": "H1203",
      "romeLibelle": "Conception et dessin produits mécaniques",
      "salaire": {
        "libelle": "Annuel de 20500,00 Euros à 24000,00 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "6 à 9 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Comptable",
      "competences": [
        {
          "code": "120886",
          "exigence": "E",
          "libelle": "Logiciels comptables"
        },
        {
          "code": "117136",
          "exigence": "E",
          "libelle": "Saisir les factures"
        },
        {
          "code": "117140",
          "exigence": "S",
          "libelle": "Codifier un mandat"
        },
        {
          "code": "117141",
          "exigence": "S",
          "libelle": "Codifier un titre"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : sarl-trv@orange.fr",
        "courriel": "sarl-trv@orange.fr",
        "nom": "TRV - Mme veronique reymond"
      },
      "dateCreation": "2019-11-13T12:45:53+01:00",
      "description": "COMPTABILITE JUSQU AU BILAN TVA SAISIE FACTURE ACHAT ET VENTES SUR LOGICIEL SAP",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "ENTREPRISE DE TRAVAUX AGRICOLES ET FORESTIERES",
        "nom": "TRV"
      },
      "experienceExige": "E",
      "experienceLibelle": "2 ans",
      "formations": [],
      "id": "095WKSX",
      "intitule": "Comptable",
      "langues": [],
      "lieuTravail": {
        "codePostal": "38830",
        "commune": "38439",
        "latitude": 45.37472222,
        "libelle": "38 - CRETS EN BELLEDONNE",
        "longitude": 6.048055555
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKSX"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "M1203",
      "romeLibelle": "Comptabilité",
      "salaire": {
        "libelle": "Mensuel de 1800,00 Euros à 1950,00 Euros sur 12 mois"
      },
      "secteurActivite": "02",
      "secteurActiviteLibelle": "Services de soutien à l'exploitation forestière",
      "trancheEffectifEtab": "1 ou 2 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "ape.51436@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Agent / Agente d'accueil",
      "competences": [
        {
          "code": "120130",
          "exigence": "S",
          "libelle": "Accueillir une clientèle"
        },
        {
          "code": "120872",
          "exigence": "S",
          "libelle": "Contrôler l'accès et la circulation des personnes"
        },
        {
          "code": "121288",
          "exigence": "S",
          "libelle": "Orienter les personnes selon leur demande"
        },
        {
          "code": "101506",
          "exigence": "S",
          "libelle": "Règles et consignes de sécurité"
        }
      ],
      "contact": {
        "coordonnees1": "67 RUE DU MONT D ARENE\nBP 80",
        "coordonnees2": "51873 REIMS",
        "coordonnees3": "Courriel : ape.51436@pole-emploi.fr",
        "nom": "Pôle Emploi REIMS MONT D'ARENE"
      },
      "dateCreation": "2019-11-13T12:45:42+01:00",
      "description": "Dans un cinéma : \nTâches principales :  - Assurer l'activité de contrôleur(se)\n- s'assurer que les spectateurs entrant en salle sont bien munis d'un billet\n- gérer les chassés croisés de spectateurs dans l'établissement\n- informer et orienter le public\n- veiller à la propreté de l'établissement et à la sécurité du public\n\nProfil recherché : Titulaire d'une formation en sécurité (CAP,CQP ou titre professionnel)  , intérêt pour le cinéma,  Le SSIAP et la pratique de l'anglais sont appréciés\nTravail en soirée (jusque 1h le week-end), les week-end, jours fériés et vacances scolaires ( 2 jours de repos dans la semaine)   ",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "experienceCommentaire": "expérience souhaitée dans commerce",
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté - expérience souhaitée dans commerce",
      "formations": [
        {
          "codeFormation": "42822",
          "domaineLibelle": "Surveillance protection gardiennage",
          "exigence": "E",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "095WKST",
      "intitule": "Contrôleur (se) de billets H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "51050",
        "commune": "51454",
        "latitude": 49.26527778,
        "libelle": "51 - REIMS",
        "longitude": 4.02861111
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKST"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "M1601",
      "romeLibelle": "Accueil et renseignements",
      "salaire": {
        "libelle": "Mensuel de 1544,84 Euros sur 12 mois"
      },
      "secteurActivite": "59",
      "secteurActiviteLibelle": "Projection de films cinématographiques",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Auxiliaire de vie",
      "competences": [
        {
          "code": "107662",
          "exigence": "S",
          "libelle": "Accompagner la personne dans les gestes de la vie quotidienne"
        },
        {
          "code": "125971",
          "exigence": "S",
          "libelle": "Définir des besoins en approvisionnement"
        },
        {
          "code": "107661",
          "exigence": "S",
          "libelle": "Réaliser des soins d'hygiène corporelle, de confort et de prévention"
        },
        {
          "code": "107920",
          "exigence": "S",
          "libelle": "Réaliser pour la personne des courses, la préparation des repas, des formalités administratives, ..."
        },
        {
          "code": "107923",
          "exigence": "S",
          "libelle": "Suivre l'état de santé de la personne, relever les modifications de comportement, états dépressifs, ... et informer les interlocuteurs concernés (famille, médecin, ...)"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : anais.poveda@vitalliance.fr",
        "courriel": "anais.poveda@vitalliance.fr",
        "nom": "VITALLIANCE - Mme Anaïs POVEDA"
      },
      "dateCreation": "2019-11-13T12:45:41+01:00",
      "description": "Vitalliance, structure d'aide à domicile spécialisée dans l'accompagnement des personnes en situation de handicap, recherche deux auxiliaires de vie pour accompagner un monsieur tétraplégique et trachéotomisé à Massy.\n\nCréneaux d'intervention : plusieurs soirs par semaine de 21h30 à 22h30, ainsi que certains matins de 07h15 à 08h15.\n\nTâches a effectuées : aide à la toilette, aide au transfert manuel, habillage.\n\nPoste à pourvoir dès que possible.",
      "dureeTravailLibelle": "15H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps partiel",
      "entreprise": {
        "description": "Service professionnel d'aide à domicile présent sur toute l'Ile de France et spécialisé dans l'accompagnement aux personnes lourdement handicapées.",
        "nom": "VITALLIANCE"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKSR",
      "intitule": "Auxiliaire de vie",
      "langues": [],
      "lieuTravail": {
        "codePostal": "91300",
        "commune": "91377",
        "latitude": 48.73055555,
        "libelle": "91 - MASSY",
        "longitude": 2.276388888
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKSR"
      },
      "qualificationCode": "5",
      "qualificationLibelle": "Employé non qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "K1302",
      "romeLibelle": "Assistance auprès d'adultes",
      "salaire": {
        "libelle": "Horaire de 10,03 Euros à 10,05 Euros sur 12 mois"
      },
      "secteurActivite": "88",
      "secteurActiviteLibelle": "Aide à domicile",
      "trancheEffectifEtab": "100 à 199 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Analyste-programmeur / Analyste-programmeuse informatique",
      "competences": [
        {
          "code": "109846",
          "exigence": "E",
          "libelle": "Concevoir et développer les programmes et applications informatiques"
        },
        {
          "code": "121055",
          "exigence": "E",
          "libelle": "Déterminer des mesures correctives"
        },
        {
          "code": "113317",
          "exigence": "E",
          "libelle": "Développer une application en lien avec une base de données"
        },
        {
          "code": "121398",
          "exigence": "S",
          "libelle": "Analyser des problèmes techniques"
        },
        {
          "code": "109791",
          "exigence": "S",
          "libelle": "Informatique de gestion"
        },
        {
          "code": "104549",
          "exigence": "S",
          "libelle": "Informatique industrielle"
        },
        {
          "code": "109884",
          "exigence": "S",
          "libelle": "Langage informatique WLangage"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : cpetel@altaconcept.com",
        "courriel": "cpetel@altaconcept.com",
        "nom": "ALTA CONCEPT - Mme Christine PETEL"
      },
      "dateCreation": "2019-11-13T12:45:27+01:00",
      "deplacementCode": "2",
      "deplacementLibelle": "Ponctuels National",
      "description": "Dans le cadre de son expansion ALTA CONCEPT recrute un analyste programmeur pour compléter l'équipe existante. La connaissance du developpement WINDEV est requise. Les missions attendues couvrent l'analyse fonctionnelle, le developpement, la redaction  des tutos utilisateurs,  la maintenance des projets ,la mise en place et le recettage.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "ALTA CONCEPT est éditeur d'un ERP et intégrateur de ses propres solutions informatiques.\n100% des clients d'ALTA CONCEPT sont des TPE/PME agroalimentaires.\nPlus de 120 sites ont été installés sur l'ensemble du territoire français.\nDepuis 2001, ALTA CONCEPT s'est construit une image de qualité par la performance de ses réalisations en milieu agroalimentaire.\nNotre ERP (logiciel) a été conçu spécifiquement pour les sociétés agroalimentaires.",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/lgzwCAmQBfSBox8wZg8mxpKWCGD8SNWA.png",
        "nom": "ALTA CONCEPT",
        "url": "http://www.altaconcept.com"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKSQ",
      "intitule": "Analyste-programmeur / Analyste-programmeuse informatique",
      "langues": [],
      "lieuTravail": {
        "codePostal": "72000",
        "commune": "72181",
        "latitude": 48.00416667,
        "libelle": "72 - LE MANS",
        "longitude": 0.196944444
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKSQ"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à travailler et à se coordonner avec les autres au sein de l’entreprise en confiance et en transparence pour réaliser les objectifs fixés. Exemple : aider ses collègues, savoir demander de l’aide",
          "libelle": "Travail en équipe"
        },
        {
          "description": "Capacité à aller chercher au-delà de ce qui est donné à voir, à s’ouvrir sur la nouveauté et à investiguer pour comprendre et agir de façon appropriée. Exemple : avoir l’envie d’apprendre",
          "libelle": "Curiosité"
        },
        {
          "description": "Capacité à maintenir son effort jusqu’à l’achèvement complet d’une tâche, quels que soient les imprévus et les obstacles de réalisation rencontrés. Exemple : faire preuve de volonté, d’assiduité, de régularité, rebondir après une erreur",
          "libelle": "Persévérance"
        }
      ],
      "romeCode": "M1805",
      "romeLibelle": "Études et développement informatique",
      "salaire": {
        "complement1": "Primes",
        "complement2": "Participation/action",
        "libelle": "Annuel de 22000,00 Euros à 28000,00 Euros sur 12 mois"
      },
      "secteurActivite": "62",
      "secteurActiviteLibelle": "Conseil en systèmes et logiciels informatiques",
      "trancheEffectifEtab": "20 à 49 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Monteur / Monteuse en plomberie chauffage",
      "competences": [
        {
          "code": "122213",
          "exigence": "E",
          "libelle": "Installer des équipements de chauffage"
        },
        {
          "code": "122970",
          "exigence": "E",
          "libelle": "Poser des éléments sanitaires"
        },
        {
          "code": "121605",
          "exigence": "E",
          "libelle": "Poser des tuyauteries"
        },
        {
          "code": "123695",
          "exigence": "E",
          "libelle": "Réaliser des travaux de raccordement aux appareils de chauffage et éléments sanitaires"
        },
        {
          "code": "113669",
          "exigence": "E",
          "libelle": "Réaliser un diagnostic de panne ou de dysfonctionnement d'installation"
        },
        {
          "code": "123301",
          "exigence": "E",
          "libelle": "Réparer une pièce défectueuse"
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : lille2@welljob.fr",
        "courriel": "lille2@welljob.fr",
        "nom": "WELL JOB - Mme Céline  GANDILLET"
      },
      "dateCreation": "2019-11-13T12:45:24+01:00",
      "description": "L'Agence WELLJOB EMPLOI DE LILLE recrute pour l'un de ses clients : un MONTEUR EN PLOMBERIE CHAUFFAGE (H/F).\n\nMissions :\nVous assurez les travaux de plomberie et d'installations de chauffage et de\n\nclimatisation au sein d'une équipe travaux pilotée par notre chargé d'affaires. (Installations individuelles et collectives).\n\nPlomberie : travail du cuivre, réseaux, installations sanitaires ...\n\nMonteur Chauffagiste : réalisation de chaufferies (collectives)\n\nClimatisation : installations détente directe, VRV, production eau glacée ...\n\nPlomberie : travail du cuivre, réseaux, installations sanitaires ...\n\nMonteur Chauffagiste : réalisation de chaufferies (collectives) Climatisation : installations détente directe, VRV, production eau glacée ...",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "AGENCE WELLJOB EMPLOI DE LILLE \n54Bis Boulevard de la liberté\n59800 LILLE",
        "logo": "https://entreprise.pole-emploi.fr/static/img/logos/aJRrFYgRrRAI0d6wyXUPFevjCOGo2rhH.png",
        "nom": "WELL JOB",
        "url": "https://www.welljob.fr/entreprise"
      },
      "experienceExige": "E",
      "experienceLibelle": "3 ans",
      "formations": [],
      "id": "095WKSN",
      "intitule": "Monteur / Monteuse en plomberie chauffage       (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "59491",
        "commune": "59009",
        "latitude": 50.62277778,
        "libelle": "59 - VILLENEUVE D ASCQ",
        "longitude": 3.144166666
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKSN"
      },
      "qualificationCode": "2",
      "qualificationLibelle": "Ouvrier spécialisé",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à transmettre clairement des informations, à échanger, à écouter activement, à réceptionner des informations et messages et à faire preuve d’ouverture d’esprit. Exemple : être à l’écoute, être attentif aux autres",
          "libelle": "Sens de la communication"
        },
        {
          "description": "Capacité à planifier, à prioriser, à anticiper des actions en tenant compte des moyens, des ressources, des objectifs et du calendrier pour les réaliser. Exemple : planifier, ordonner ses actions par priorité",
          "libelle": "Sens de l’organisation"
        },
        {
          "description": "Capacité à respecter les règles et codes de l’entreprise; à réaliser des tâches en suivant avec précision les procédures et instructions fournies ; à transmettre des informations avec exactitude. Exemple : être ponctuel, respecter les engagements, résister à la distraction",
          "libelle": "Rigueur"
        }
      ],
      "romeCode": "F1603",
      "romeLibelle": "Installation d'équipements sanitaires et thermiques",
      "salaire": {
        "complement1": "Primes",
        "libelle": "Horaire de 10,97 Euros à 13,10 Euros sur 12 mois"
      },
      "secteurActivite": "78",
      "secteurActiviteLibelle": "Activités des agences de travail temporaire",
      "trancheEffectifEtab": "0 salarié",
      "typeContrat": "MIS",
      "typeContratLibelle": "Mission intérimaire - 6 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Chef de produit",
      "competences": [
        {
          "code": "121363",
          "exigence": "S",
          "libelle": "Analyser la concurrence"
        },
        {
          "code": "118637",
          "exigence": "S",
          "libelle": "Analyser les besoins du client"
        },
        {
          "code": "120782",
          "exigence": "S",
          "libelle": "Analyser un marché"
        },
        {
          "code": "121559",
          "exigence": "S",
          "libelle": "Définir les caractéristiques d'un produit"
        },
        {
          "code": "117327",
          "exigence": "S",
          "libelle": "Réaliser un plan marketing"
        }
      ],
      "contact": {
        "coordonnees1": "https://jobs.smartrecruiters.com/Edenred/743999699400849-chef-de-produit-ticket-fleetpro-h-f",
        "nom": "EDENRED FRANCE - M. Clément-Matthieu MOAL",
        "urlPostulation": "https://jobs.smartrecruiters.com/Edenred/743999699400849-chef-de-produit-ticket-fleetpro-h-f"
      },
      "dateCreation": "2019-11-13T12:45:21+01:00",
      "description": "Au sein de notre Direction Marketing basé à Malakoff, nous recherchons en CDI un:\n\n \n\n                              CHEF DE PRODUIT TICKET FLEETPRO H/F\n\n \n\nLe Chef de Produit participe à l'élaboration du plan marketing et est force de proposition sur les actions à mettre en place, suit chaque action initiée, en mesure la performance et propose des actions correctives.\n\n \n\nAinsi vos missions seront:\n\nGérer les campagnes de marketing direct\n\nMet en œuvre les actions de marketing opérationnel\n\nMet en place les actions de fidélisation clients\n\nAider à la gestion du budget marketing \n\nRespecter la politique et la charte de communication du groupe Edenred\n\nMettre à jour et enrichir le contenu du site Internet et relaier de nouvelles fonctionnalités siège\n\nEffectuer une veille concurrentielle active\n\nOrganiser les manifestations événementielles (salons, relations publiques )\n\nMettre en œuvre le plan média",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "Edenred, inventeur de Ticket Restaurant® et leader mondial des services prépayés aux entreprises, conçoit et gère des solutions qui permettent de gérer : Avantages aux salariés (Ticket Restaurant®,...) / Frais professionnels (Ticket Travel Pro®,...) / Motivation et récompenses (Ticket Kadéos®,...) / Programmes sociaux publics (Ticket CESU, Ticket Service®)\nFort de 700 collaborateurs, Edenred France déploie ses solutions auprès de 120 000 clients, 6,7 millions d'utilisateurs et 380 000 affiliés.",
        "nom": "EDENRED FRANCE",
        "url": "http://www.edenred.fr"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKSM",
      "intitule": "Chef de Produit Ticket FleetPro H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "92240",
        "commune": "92046",
        "latitude": 48.81722222,
        "libelle": "92 - MALAKOFF",
        "longitude": 2.299166666
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKSM"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "romeCode": "M1703",
      "romeLibelle": "Management et gestion de produit",
      "salaire": {
        "commentaire": "compétitive",
        "complement1": "compétitive"
      },
      "secteurActivite": "66",
      "secteurActiviteLibelle": "Autres activités auxiliaires de services financiers, hors assurance et caisses de retraite, n.c.a.",
      "trancheEffectifEtab": "500 à 999 salariés",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "accessibleTH": false,
      "agence": {
        "courriel": "ape.31354@pole-emploi.fr"
      },
      "alternance": false,
      "appellationlibelle": "Assistant administratif et commercial / Assistante administrative et commerciale",
      "competences": [
        {
          "code": "121646",
          "exigence": "S",
          "libelle": "Enregistrer les données d'une commande"
        },
        {
          "code": "117243",
          "exigence": "S",
          "libelle": "Établir un devis"
        },
        {
          "code": "120889",
          "exigence": "S",
          "libelle": "Gestion administrative"
        },
        {
          "code": "120888",
          "exigence": "S",
          "libelle": "Gestion comptable"
        },
        {
          "code": "121688",
          "exigence": "S",
          "libelle": "Logiciel de gestion clients"
        },
        {
          "code": "102226",
          "exigence": "S",
          "libelle": "Normes rédactionnelles"
        },
        {
          "code": "118975",
          "exigence": "S",
          "libelle": "Outils bureautiques"
        },
        {
          "code": "124240",
          "exigence": "S",
          "libelle": "Réaliser un suivi des dossiers clients, fournisseurs"
        },
        {
          "code": "123835",
          "exigence": "S",
          "libelle": "Réaliser un suivi statistique de l'activité d'un site web"
        },
        {
          "code": "122477",
          "exigence": "S",
          "libelle": "Transmettre des données techniques et commerciales"
        },
        {
          "code": "140955",
          "exigence": "S",
          "libelle": "Utilisation d'outils collaboratifs (planning partagé, web conférence, réseau social d'entreprise, ...)"
        },
        {
          "code": "124537",
          "exigence": "S",
          "libelle": "Vérifier les conditions de réalisation d'une commande"
        }
      ],
      "contact": {
        "coordonnees1": "187 AV JACQUES DOUZANS\nCS 60315",
        "coordonnees2": "31605 MURET",
        "coordonnees3": "Courriel : ape.31354@pole-emploi.fr",
        "nom": "Pôle Emploi MURET"
      },
      "dateCreation": "2019-11-13T12:45:19+01:00",
      "deplacementCode": "1",
      "deplacementLibelle": "Jamais",
      "description": "Nous recherchons pour le compte de notre entreprise en pleine croissance, un(e) assistant(e) polyvalent(e) dont les missions sont les suivantes:\n**Gestions administrative quotidienne (courriers, suivi de dossiers, classement, archivage)\n**Facturation et saisie comptable basique\n**Mise à jour du site internet de l'entreprise\n**Optimiser la promotion commerciale des produits et de l'entreprise sur les différents canaux ( réseaux sociaux, sites        publicitaires,etc..)\n**Etiquetage des produits et prise de photos\nUne bonne maîtrise des outils bureautiques et des outils internet est requise\nVous faites preuve d'initiative et êtes force de proposition dans le cadre de nos actions internes, vous êtes autonome et réactif(ve) et doté(e) d'une bonne aisance relationnelle\nVous travaillez du lundi au vendredi (horaires à définir)\nUne formation interne est prévue ",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "nom": "CARBOISSOLD"
      },
      "experienceCommentaire": "en gestion administrative",
      "experienceExige": "E",
      "experienceLibelle": "2 ans - en gestion administrative",
      "formations": [
        {
          "codeFormation": "35054",
          "domaineLibelle": "Secrétariat assistanat",
          "exigence": "S",
          "niveauLibelle": "Bac ou équivalent"
        }
      ],
      "id": "095WKSL",
      "intitule": "assistant(e) polyvalent(e)  (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "31390",
        "commune": "31107",
        "latitude": 43.29722222,
        "libelle": "31 - CARBONNE",
        "longitude": 1.219166666
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKSL"
      },
      "qualificationCode": "6",
      "qualificationLibelle": "Employé qualifié",
      "qualitesProfessionnelles": [
        {
          "description": "Capacité à prendre en charge son activité sans devoir être encadré de façon continue. Exemple : travailler efficacement sans responsable",
          "libelle": "Autonomie"
        },
        {
          "description": "Capacité à faire des choix pour agir, à prendre en charge son activité et à rendre compte, sans devoir être encadré de façon continue. Exemple : savoir faire des choix",
          "libelle": "Capacité de décision"
        },
        {
          "description": "Capacité à réagir rapidement face à des évènements et à des imprévus, en hiérarchisant les actions, en fonction de leur degré d’urgence / d’importance. Exemple : faire preuve de dynamisme, vivacité, énergie, comprendre vite",
          "libelle": "Réactivité"
        }
      ],
      "romeCode": "D1401",
      "romeLibelle": "Assistanat commercial",
      "salaire": {
        "commentaire": "négociable selon profil",
        "complement1": "négociable selon profil",
        "libelle": "Horaire de 10,03 Euros"
      },
      "secteurActivite": "46",
      "secteurActiviteLibelle": "Commerce de gros (commerce interentreprises) de bois et de matériaux de construction",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 6 Mois"
    },
    {
      "accessibleTH": false,
      "alternance": false,
      "appellationlibelle": "Ouvrier polyvalent / Ouvrière polyvalente d'entretien des bâtiments",
      "competences": [
        {
          "code": "123621",
          "exigence": "S",
          "libelle": "Contrôler une installation électrique"
        },
        {
          "code": "106659",
          "exigence": "S",
          "libelle": "Diagnostiquer une panne sur une installation (éclairage, chauffage, sanitaires)"
        },
        {
          "code": "106661",
          "exigence": "S",
          "libelle": "Réparer ou remplacer les poignées, vitres, rails, ... de portes, fenêtres, ..."
        }
      ],
      "contact": {
        "coordonnees1": "Courriel : recrutement@sg2a.fr",
        "courriel": "recrutement@sg2a.fr",
        "nom": "SG2A - Mme SG2A SG2A"
      },
      "dateCreation": "2019-11-13T12:45:08+01:00",
      "description": "SG2A, recherche un agent d'entretien et de maintenance (AEM) H / F, sur les aires d'accueil des gens du voyage du secteur de Guingamp et Paimpol  (22).\n\nSous la responsabilité du coordonnateur de travaux, l'AEM assure l'entretien des espaces verts (tontes du gazon, tailles des haies, arrosages et nettoyages...) et la maintenance du bâtiment (plomberie, électricité, maçonnerie, serrurerie ) des sites qui lui sont affectés, tout en respectant les règles d'hygiène et de sécurité. \nIl établit des comptes rendus journalier d'activité à son responsable.\n\nSens de l'organisation, autonomie et bon relationnel sont des qualités nécessaires pour exercer ces fonctions.\n\nConnaissances dans les domaines de la maintenance du bâtiment et de l'entretien des espaces verts ou un diplôme équivalent. Permis B exigé.\n\nCDD à temps plein, six jours par semaine, du lundi au samedi, avec astreintes.",
      "dureeTravailLibelle": "35H Horaires normaux",
      "dureeTravailLibelleConverti": "Temps plein",
      "entreprise": {
        "description": "SG2A L'HACIENDA, société de gestion des aires d'accueil des gens du voyage présente sur le territoire national depuis 15 ans et acteur majeur dans le domaine de la gestion des aires d accueil des gens du voyage se diversifie et s'ouvre notamment à la gestion des campings.",
        "nom": "SG2A"
      },
      "experienceExige": "D",
      "experienceLibelle": "Débutant accepté",
      "formations": [],
      "id": "095WKSJ",
      "intitule": "Agent d'entretien et de maintenance                   (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "22200",
        "commune": "22070",
        "latitude": 48.5625,
        "libelle": "22 - GUINGAMP",
        "longitude": -3.151111111
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "1",
        "partenaires": [],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/095WKSJ"
      },
      "qualificationCode": "3",
      "qualificationLibelle": "Ouvrier qualifié (P1,P2)",
      "romeCode": "I1203",
      "romeLibelle": "Maintenance des bâtiments et des locaux",
      "salaire": {
        "libelle": "Mensuel de 1522,00 Euros à 1528,50 Euros sur 12 mois"
      },
      "secteurActivite": "55",
      "secteurActiviteLibelle": "Terrains de camping et parcs pour caravanes ou véhicules de loisirs",
      "trancheEffectifEtab": "100 à 199 salariés",
      "typeContrat": "CDD",
      "typeContratLibelle": "Contrat à durée déterminée - 3 Mois"
    },
    {
      "alternance": false,
      "appellationlibelle": "Fraiseur / Fraiseuse",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:08+01:00",
      "dateCreation": "2019-11-13T12:45:08+01:00",
      "description": "METIER INTERIM ET CDI MONTAIGU recrute pour notre client spécialisé l'aménagement intérieur pour les secteurs de l'aéronautique et du ferroviaire un fraiseur H/F. Au sein de l'atelier commandes numériques, en tant qu'opérateur-opératrice usinage, vos principales activités sont les suivantes : - Assurer la production de pièces de petites et moyennes séries - Réaliser les opérations de fraisage (matériaux composites) - Régler, préparer et conduire la/les machine(s) - Assurer le contrôle et la traçabilité des pièces - Conditionner les pièces - Réaliser la déclaration de production dans SAP - Assurer la maintenance de premier niveau de son matériel Vous êtes en contact avec les programmeurs, le technicien méthodes et le technicien qualité de l'atelier. Au quotidien, vous appliquez les règles de sécurité et de propreté. Salaire : selon profil  primes panier  13 e mois Temps de travail hebdomadaire : 39h Travail en 2x8 soit 5h-13h ou 13h-21h du lundi au jeudi et le vendredi : 5h-12h ou 12h-19h - Avantages sociaux : 13ème mois, intéressement, participation, prime de vacances et avantages CE Poste à pourvoir en CDI. Ce poste vous intéresse Merci de postuler en ligne ou vous présenter en agence munit de votre CV. CAP / BEP Fraiseur avec 1ere expérience requiseMétier Intérim & CDI est un réseau de 10 agences dans l'Ouest de la France. Nous tissons avec nos collaborateurs intérimaires une relation de confiance et de fidélité.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Metier Interim"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641202",
      "intitule": "Fraiseur (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 46.558670044,
        "libelle": "85 - Vendée",
        "longitude": -1.356292963
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865951?v=2AD1EB796701464E81A6711D32F88F7D25985952&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641202"
      },
      "qualificationCode": "X",
      "romeCode": "H2903",
      "romeLibelle": "Conduite d'équipement d'usinage",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Chef de secteur commerce",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:07+01:00",
      "dateCreation": "2019-11-13T12:45:07+01:00",
      "description": "Notre client, un acteur majeur dans le secteur des affichages publicitaires et de la communication extérieure est à la recherche d'un Chef de secteur (H/F). Permis 2ans obligatoire.MISSIONS DU CHEF DE SECTEUR:- Assurer l'organisation et l'encadrement d'une équipe et d'un parc de mobiliers urbains- Coordonner les opérations de montage et de maintenance en relation avec l'Adjoint TechniquePROFIL DU CHEF DE SECTEUR:- Expérience de 1 à 3 ans- Chargé d'affaire / technicien VRD- Habilitation électrique appréciée- Maîtrise de l'outil informatique- Permis 2ans obligatoireAVANTAGES DU CHEF DE SECTEUR:- Prime QAPA déblocable en fin de mission (voir conditions sur le site)Rémunération en fonction du profil Compétences requises ou optionnelles : Permis B (voiture)",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 1 à 3 An(s)",
      "formations": [],
      "id": "3641198",
      "intitule": "Chef de secteur (H/F) - Marseille - QAPA",
      "langues": [],
      "lieuTravail": {
        "latitude": 43.533737183,
        "libelle": "13 - Bouches du Rhône",
        "longitude": 5.095419884
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333862797?v=C710A02893D988A3D8B4A829E7F7DF19A3297744&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641198"
      },
      "qualificationCode": "X",
      "romeCode": "D1509",
      "romeLibelle": "Management de département en grande distribution",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Dessinateur-projeteur / Dessinatrice-projeteuse en ameublement",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:06+01:00",
      "dateCreation": "2019-11-13T12:45:06+01:00",
      "description": "Vous êtes un(e) concepteur(rice) d'aménagement Aboutir Emploi Rochefort n'attend plus que vous Votre mission : Réalisation des plans de fabrication (aménagements du carré, modules des coques et menuiserie et composite) Renseigner et gérer les nomenclatures des ensembles conçus (sous logiciel IFS) Validation des plans avec les Chefs de Projet et le service Méthodes Envoi des plans aux sous traitants indiqués par le service Achats Echanges et validation avec le fournisseur Suivi de la livraison des pièces / équipements Suivi du montage et validation avec le service Production Mise à jour des éventuelles modifications du prototype ou de la série pour l'amélioration continue. Poste à pourvoir sur La Rochelle à compter du 2/12/2019. Vous maîtrisez les logiciels suivants ; logiciels :Solid Works 2017 // Audros (PLM - gestion documentaire) // IFS (ERP) compétences professionnelles attendues : Maitrise du logiciel CAO 3D - Solid Works Connaissance en menuiserie (choix des matériaux et assemblages) Connaissance des matériaux composite et échantillonnages Connaissance des modes de fabrication des différentes pièces composite Connaissance du nautisme et de sa réglementation Anglais : échanges avec fournisseurs - rédaction de plans et documents en anglaisAboutir Emploi est un partenaire de travail temporaire local, avec une présence régionale, et l'enseigne est elle-même rattachée à un réseau national d'indépendants locaux nommé « Réséo ». (voir Réséo.fr, 150 agences en France et étranger) L'enseigne Aboutir Emploi a eu 10 ans en novembre 2015. Depuis 2005, cette enseigne a été créée par Jean Marc Boutineau (pratiquant le métier depuis 1986) pour être proche de vous (entreprises utilisatrices), réactive. Nous disposons des agences sur La Rochelle, Rochefort, Poitiers, Cognac, Saint Jean de Beugné et Niort.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641197",
      "intitule": "CONCEPTEUR / PROJETEUR AMENAGEMENT (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 45.834991455,
        "libelle": "17 - Charente Maritime",
        "longitude": -0.76207602
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865669?v=1AD0F289037AFD6D4D1728F1C3A8E385664B7630&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641197"
      },
      "qualificationCode": "X",
      "romeCode": "H2209",
      "romeLibelle": "Intervention technique en ameublement et bois",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Menuisier poseur / Menuisière poseuse de fermetures",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:06+01:00",
      "dateCreation": "2019-11-13T12:45:06+01:00",
      "description": "L'agence Interaction de Fougères recherche pour son client, spécialisée dans la fourniture et la pose de menuiseries et fermetures en neuf et rénovation auprès de particuliers, basé à Fougères un menuisier poseur H/F Vous serez chargé de la gestion d'une équipe de menuisiers. Vous assurez la pose de menuiserie (fenêtres, portes, ) sur des chantiers de particuliers principalement. Vous faites également le suivi de chantier. Poste à pourvoir en CDI au plus vite.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Interaction De"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641196",
      "intitule": "Menuisier poseur H/F",
      "langues": [],
      "lieuTravail": {
        "libelle": "France"
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1334075424?v=33D5846B8D9D7D0690089D02C331CEDACBD5C822&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641196"
      },
      "qualificationCode": "X",
      "romeCode": "F1607",
      "romeLibelle": "Pose de fermetures menuisées",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Baby-sitter",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:06+01:00",
      "dateCreation": "2019-11-13T12:45:06+01:00",
      "description": "Nous recherchons un/une baby-sitter pour garder 2 enfants de 2 et 5 ans, être disponible du lundi au vendredi hors mercredi 16h30 à 18h30 dès que possible. Vous gérez l'accompagnement et/ou la sortie d'école, le petit déjeuner, l'habillage, le repas, les activités, le goûter, le bain, la mise en tenue de nuit. Poste à pourvoir au plus vite en CDI à temps partiel, avec la possibilité de cumuler plusieurs missions. Les kilomètres parcourus avec les enfants sont remboursés. Nous offrons également une carte avantages type comité d'entreprise, ainsi qu'une participation à la mutuelle santé de l'entreprise.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 2 à 5 An(s)",
      "formations": [],
      "id": "3641195",
      "intitule": "Garde d'enfants ST AUBIN S/ MER (H/F)",
      "langues": [],
      "lieuTravail": {
        "libelle": "France"
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1334075713?v=86DA15D561A0B15EACD516FB62914CF390B6FB69&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641195"
      },
      "qualificationCode": "X",
      "romeCode": "K1303",
      "romeLibelle": "Assistance auprès d'enfants",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Manager / Manageuse de caisses",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:05+01:00",
      "dateCreation": "2019-11-13T12:45:05+01:00",
      "description": "Vous aimez être dans le feu de l'action, vous savez tirer le meilleur de vos équipes et, après une première expérience réussie dans le management d'équipe, vous brûlez de faire vos preuves dans une entreprise en pleine croissance, alors ces responsabilités sont faites pour vous : · Faites grandir la flamme au sein de votre équipe en développant les compétences de chacun ; · Gérez à chaud le quotidien du restaurant (caisses, plannings, approvisionnements) ; · Enflammez les ventes ; · Transmettez les bons réflexes hygiène et sécurité BURGER KING® à vos équipes. Vous avez : - Le sens des responsabilités - L'envie de faire grandir une équipe - Le goût du challenge - L'envie de mettre le feu à votre carrière Nous avons : - Des équipes de 30 personnes à enflammer - Un développement ambitieux - Un parcours de formation sur mesure - Des opportunités dans toute la France BURGER KING® est la seconde chaîne de burger au monde avec près de 17.000 restaurants dans plus de 100 pays. Après 15 ans d'absence, la marque est revenue en France avec un ambitieux plan de développement et déjà plus de 200 restaurants ouverts en 4 ans. Fort d'un succès commercial inédit et d'une croissance dopée par l'acquisition de Quick, BURGER KING® est aujourd'hui le second acteur du marché de la restauration rapide en France. Cette position de challenger sera renforcée dans les années à venir par l'ouverture d'une centaine de nouveaux restaurants par an. Le seuil des 500 restaurants sera atteint en 2020. La marque bénéficie à la fois d'une aura incomparable en France et d'une personnalité unique, permettant un marketing unique et décalé qui fait son succès commercial au quotidien. Pour accompagner son développement BURGER KING® est aujourd'hui en quête active de talents à la recherche d'opportunités et de défis à relever. »",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641193",
      "intitule": "MANAGER H/F ANCENIS",
      "langues": [],
      "lieuTravail": {
        "latitude": 47.351799011,
        "libelle": "49 - Maine et Loire",
        "longitude": -0.599422991
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865724?v=1BB0C2D0676E112B8AA5C7CF4C62B6FDE4D645BA&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641193"
      },
      "qualificationCode": "X",
      "romeCode": "D1508",
      "romeLibelle": "Encadrement du personnel de caisses",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Gestionnaire paie",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:05+01:00",
      "dateCreation": "2019-11-13T12:45:05+01:00",
      "description": "En tant que Gestionnaire de paie start-up, vous serez en charge de créer et développer ce pôle et vos missions consisteront à : Etablir et contrôler les bulletins de paie des intérimaires (de 0 à 5000 et ) Etablir les contrats de travail des intérimaires Gérer l'administration du personnel (congés payés, absences, maladie, accident du travail, maternité, congé sans solde, etc) Effectuer les formalités d'entrée et les formalités de sortie du personnel Effectuer les recherches techniques nécessaires pour assurer l'application des textes législatifs et la veille juridique Informations sur ce poste : Poste en CDI Localisé sur Paris 11ème (Philippe Auguste - ligne 2) Poste à pourvoir immédiatement Le salaire : 30-35KEUR brut fixe annuel, tickets restaurant de 8EUR, RTT Vous disposez d'une formation supérieure en ressources humaines. Vous justifiez d'une expérience de minimum 3 ans en paie au sein d'une agence d'intérim. Par ailleurs vous connaissez un peu l'univers des start-up. En tant que Gestionnaire de paies start-up, vous êtes dynamique, autonome et vous disposez de fortes facultés d'adaptation, de communication et d'encadrement d'équipe. Ce poste est fait pour vous  « Voluntae, votre réussite est notre volonté » VOLUNTAE, expert des métiers comptables et financiers en cabinet ou en entreprise, recrute pour une start-up d'une soixantaine de personnes qui a mis en place une plateforme d'intermédiation dans le domaine des services. Dans le cadre de son développement, nous recherchons un Gestionnaire de paie start-up H/F.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 3 An(s)",
      "formations": [],
      "id": "3641192",
      "intitule": "Gestionnaire de paie start-up (H/F)",
      "langues": [],
      "lieuTravail": {
        "commune": "75000",
        "latitude": 48.863838196,
        "libelle": "75 - Paris (Dept.)",
        "longitude": 2.344630957
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333866102?v=68086FD000384CFBA93A107BF637F5150DCF6BC3&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641192"
      },
      "qualificationCode": "X",
      "romeCode": "M1203",
      "romeLibelle": "Comptabilité",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Responsable administratif / administrative de gestion",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:03+01:00",
      "dateCreation": "2019-11-13T12:45:03+01:00",
      "description": "Nous recrutons dans le cadre d'un nouveau contrat pour le compte d'un grand groupe industriel aéronautique. Tu seras rattaché(e) au Responsable de Flux du site en question et tu seras en charge d'assurer la réalisation de tes missions dans les délais et la qualité définis ensemble. Véritable responsable des stocks, tu seras en charge de l'organisation des inventaires et de l'optimisation des capacités de stockage sur site. Tu es ainsi également le garant(e) de la fiabilité des stocks.Tu possèdes une forte sensibilité à la sécurité, la qualité et l'amélioration. Tu as fait de belles études (ou pas). Tu possèdes une réelle aisance dans le cadre de gestion d'activités industrielles ou logistiques. Tu aimes suivre et analyser les stocks, proposer et mettre en place des solutions d'amélioration du mode de gestion des pièces, tu es à l'aise avec les outils informatiques (notamment sur SAP). Tu as une forte sensibilité à la coopération et au travail en équipe, un sens du service client particulièrement développé et une capacité à t'adapter aux modes opératoires attendus Tu as envie de vivre une aventure humaine dans une entreprise centrée sur l'homme, au management ambitieux, à l'expertise innovante Parce que notre ambition est de devenir une communauté de compétences, unique et innovante, Parce que participer à la performance de l'industrie et manager des femmes et des hommes a du sens : REJOINS-NOUS  Poste à pourvoir immédiatementGestionnaire administratif de Stocks F/H - Saint-Marcel (27) : viens travailler avec nous  GT Logistics c'est 1100 salariés, 80 MEUR de CA,  de 35 sites et une forte croissance dans la sous-traitance industrielle. Gestionnaire de Stocks : nous recherchons de nouveaux talents, pour faire vivre et développer nos activités au coeur des usines de nos clients. Tu es un(e) as du travail collaboratif, tu es passionné(e) par l'industrie, tu es naturellement rigoureux(se) et engagé(e), et tu ne travailles pas encore avec nous",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641187",
      "intitule": "Gestionnaire Administratif de Stocks F/H - Saint-Marcel (27) (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 49.111816406,
        "libelle": "27 - Eure",
        "longitude": 1.047379971
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865783?v=67079B54B4CC50D6E964EBD7884855A690E27872&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641187"
      },
      "qualificationCode": "X",
      "romeCode": "M1205",
      "romeLibelle": "Direction administrative et financière",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Comptable taxateur / taxatrice d'étude notariale",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:02+01:00",
      "dateCreation": "2019-11-13T12:45:02+01:00",
      "description": "Au sein d'un Office Notarial à taille humaine , rattaché à la direction, vous aurez en charge la comptabilité de la société : - Gestion de la comptabilité client/fournisseur - Gestion des comptes clients - Gestion de la taxation des actes en lien avec les collaborateurs rédacteurs - Contact avec les tiers Cette liste de tâches est non-exhaustive et sera ajustée en fonction des compétences du candidat retenu. De formation Bac 2 minimum en comptabilité/taxation, vous justifiez idéalement d'une première expérience réussie sur un poste similaire. Vous êtes à l'aise avec l'informatique (Pack office, Internet). Votre dynamisme, votre autonomie ainsi que votre capacité à prendre des initiatives vous assurent réussite à ce poste. Poste à pourvoir en CDI. Accompagnement assurée. Date de prise de poste : dès que possible. Base de 35 heures hebdomadaires (modulables). Fourchette rémunération annuelle brute : 24kEUR à 40kEUR Rémunération discutable en fonction du profil et de l'expérience. Merci d'envoyer votre CV à l'adresse mail suivante OU via notre site marque SBC. Votre candidature sera étudiée en toute confidentialité. Sans nouvelles de notre part sous quinzaine, veuillez considérer que votre candidature n'est pas retenue. SBC Recrutement, spécialisé dans les métiers du Droit et de la Comptabilité, recherche pour un de ses clients, Etude de Notaires à taille humaine située à proximité de Cavignac (33) un Comptable Taxateur H/F en CDI.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Sbc."
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641184",
      "intitule": "COMPTABLE TAXATEUR (H/F) - CDI",
      "langues": [],
      "lieuTravail": {
        "latitude": 44.91065979,
        "libelle": "33 - Gironde",
        "longitude": -0.665018022
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333863834?v=2B7A204B30E39E5DA6B8323D4A777D9E7F9F3395&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641184"
      },
      "qualificationCode": "X",
      "romeCode": "M1203",
      "romeLibelle": "Comptabilité",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Conducteur / Conductrice de grue mobile",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:01+01:00",
      "dateCreation": "2019-11-13T12:45:01+01:00",
      "description": "Nous recrutons pour le compte d'un de nos clients un conducteur de grue mobile. Votre mission sera de réaliser des opérations de levage par le biais d'une grue mobile. A cela s'ajoute des missions complémentaires : effectuer l'élingage des charges, réaliser des opérations d'entretiens sur la grue et analyser votre environnement sur le chantier (calage de la grue ainsi que la typologie des charges à soulever). Il faudra respecter les normes de qualité et de sécurité sur les chantiers. Nous sommes également ouvert à la candidature de conducteur ayant besoin d'effectuer un recyclage du caces R383Votre rigueur , votre sérieux ainsi que votre respect des règles de sécurité et qualité constitueront un atout primordiale pour votre candidature.Le Groupe DOMINO (280 collaborateurs, 60 agences en France, Pologne, Portugal & Suisse, 130 millions EUR de CA) confirme depuis 20 ans sa présence et son développement sur le marché des solutions RH (intérim, recrutement, formation, conseil). Membre fondateur du réseau DOMITIS (www.reseaudomitis.com), DOMINO accède à de nombreux marchés nationaux.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Groupe Domino"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641181",
      "intitule": "CONDUCTEUR DE GRUE MOBILE (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 47.409152985,
        "libelle": "44 - Loire Atlantique",
        "longitude": -1.542898059
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333866128?v=1BDC6E535F63D05CA443EB5C4BFA92167215381B&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641181"
      },
      "qualificationCode": "X",
      "romeCode": "N1104",
      "romeLibelle": "Manoeuvre et conduite d'engins lourds de manutention",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Supply chain manager",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:00+01:00",
      "dateCreation": "2019-11-13T12:45:00+01:00",
      "description": "Au sein de la Direction Supply Chain, vous prenez en charge une équipe de 3 personnes et pilotez les stocks et la logistique de Distribution dans un contexte dynamique de très forte croissance. Vous intervenez sur des canaux de distribution variés ( GMS, GSB, e-commerce.). Pour cela, vos principales missions seront : - Le Management des prestataires logistiques, - Renforcer le pilotage des stocks, - La gestion de projets Supply aval, - Développer l'amélioration continue au sein de la Supply, - Piloter les EDI, - Travail à l'échelle européenne avec les homologues des différents pays pour l'harmonisation des process et le développement des best practices, - Reporting et analyses diverses. Liste de tâches non exhaustive.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641180",
      "intitule": "Supply Chain Manager F/H (H/F)",
      "langues": [],
      "lieuTravail": {
        "commune": "75000",
        "latitude": 48.863838196,
        "libelle": "75 - Paris (Dept.)",
        "longitude": 2.344630957
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1334074890?v=59897550A6D1301B9723750404AA23C8FFF9E5FA&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641180"
      },
      "qualificationCode": "X",
      "romeCode": "N1301",
      "romeLibelle": "Conception et organisation de la chaîne logistique",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technicien / Technicienne support technique",
      "competences": [],
      "dateActualisation": "2019-11-13T12:45:00+01:00",
      "dateCreation": "2019-11-13T12:45:00+01:00",
      "description": "Au sein d'une entreprise multinationale française spécialisée dans la sous-traitance de services, notre client recherche un technicien support N1 à Boulogne.A ce titre, ci -dessous les missions du technicien support :- Traitement des tickets Ivanti : Accepter, mettre à jour, clôturer - Attribution du matériel- Renouvellement du matériel- Reprise du matériel- Vous serez rattaché au site de Boulogne mais serez en charge d'intervenir sur les sites de la région parisienne.Les compétences attendues :- Vous avez une première expérience en tant que technicien support- Bonne présentation- Bon savoir être- Permis B requisLes avantages :- Tickets restaurant- Véhicule de fonction fourni- Travailler dans un groupe à forte renommée - Taux horaire fixe  10% de congés payés  10% d'indemnité de fin de mission- Prime QAPASi ce poste vous intéresse, n'hésitez pas à postuler.006735 Compétences requises ou optionnelles : Permis B (voiture)",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641179",
      "intitule": "Technicien support Niv 1 (H/F) - Boulogne-Billancourt",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.825828552,
        "libelle": "92 - Hauts de Seine",
        "longitude": 2.245857
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333862845?v=7FF7376F5BAA4B9D59A52E09A559419AEE9E134D&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641179"
      },
      "qualificationCode": "X",
      "romeCode": "H1101",
      "romeLibelle": "Assistance et support technique client",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Responsable de réseaux d'assainissement",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:59+01:00",
      "dateCreation": "2019-11-13T12:44:59+01:00",
      "description": "L'agence INTERACTION Fougères recherche pour son client un Chef d'équipe VRD - Assainissement H/F. Sous la responsabilité du conducteur de travaux, vous réalisez et coordonnez la réalisation des travaux d'aménagement urbain et d'assainissement. Vous maitrisez les techniques de VRD (pose de bordures, de pavages et de mobiliers.) et disposez de bonne connaissance en assainissement. Vos compétences managériales vous permettront d'encadrer une équipe composée de 3 à 7 ouvriers.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Interaction"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641177",
      "intitule": "Chef d'équipe VRD - Assainissement H/F",
      "langues": [],
      "lieuTravail": {
        "libelle": "40 - Landes"
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1334075418?v=BF20CCBF25546DE335B1D6375848A8BAF3F166E9&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641177"
      },
      "qualificationCode": "X",
      "romeCode": "K2302",
      "romeLibelle": "Management et inspection en environnement urbain",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Comptable",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:59+01:00",
      "dateCreation": "2019-11-13T12:44:59+01:00",
      "description": "Au sein d'un cabinet d'expertise comptable à taille humaine, en collaboration avec l'équipe en place, vos missions seront les suivantes : - La gestion d'un portefeuille type TPE/PME (Tenue  Révision) - 30 à 50 dossiers - Montage de liasse fiscale - Préparation du bilan (encadrée par l'Expert Comptable) - Rdv clients ponctuels Cette liste de tâches est non-exhaustive. Diplômé du DCG/DSCG, vous justifiez idéalement d'une première expérience similaire réussie. Expert-Comptable Stagiaire accepté. Profil débutant accepté sous réserve d'avoir un diplôme type DCG/DSCG. Vous êtes à l'aise avec l'informatique (Pack office, Internet). Poste à pourvoir en CDI. Date de prise de poste : dès que possible. Base 35 heures modulables. Rémunération brute mensuelle : 1800EUR à 3000EUR Rémunération discutable en fonction du profil et de l'expérience. Merci d'envoyer votre CV à l'adresse mail suivante OU via notre site marque SBC. Votre candidature sera étudiée en toute confidentialité. Sans nouvelles de notre part sous quinzaine, veuillez considérer que votre candidature n'est pas retenue. SBC Recrutement, spécialisé dans les métiers du Droit et de l'Expertise Comptable, recherche pour un de ses clients, cabinet d'expertise comptable situé à proximité de Valence (26), un Collaborateur Comptable H/F en CDI.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Sbc."
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641176",
      "intitule": "COLLABORATEUR COMPTABLE (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 44.684532166,
        "libelle": "26 - Drôme",
        "longitude": 5.070162773
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865859?v=38B7210094925023C9B38C925A8A92CB17DFF131&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641176"
      },
      "qualificationCode": "X",
      "romeCode": "M1203",
      "romeLibelle": "Comptabilité",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Assistant / Assistante administration des ventes",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:58+01:00",
      "dateCreation": "2019-11-13T12:44:58+01:00",
      "description": "Gitec recrute pour l'un de ses clients, basé à Cormeilles en Parisis (95), un(e) Assistant(e) Commercial(e) ADV ITALIEN dans le cadre d'un CDI. Le poste est à pourvoir immédiatement. Vos missions principales seront de : - Saisir, suivre et facturer les commandes sur les différentes zones - Organiser et gérer les transports (opérations de dédouanement, établissement des documents liés à l'exportation, rattachement des justificatifs douaniers, suivi des livraisons) - Assurer l'accueil téléphonique - Saisir lesoffres de prix et assurer le suivi clients - Gérer les envois d'échantillons et de catalogues - Assurer la gestion des tableaux de bordVous êtes titulaire d'un Bac2 en gestion. Vous avez une première expérience en ADV. Vous parlez italien couramment et avez des notions d'espagnol.GITEC est une société spécialisée dans le recrutement Tertiaire, IT et Ingénierie, GITEC. GITEC propose des opportunités d'emploi en Intérim ou en CDD/CDI. GITEC est avant tout une société à taille humaine, avec six agences, six équipes, qui s'appuient sur la proximité, l'écoute et la réactivité.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Gitec"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641173",
      "intitule": "Assistant Commercial ADV ITALIEN (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.996456146,
        "libelle": "95 - Val d'Oise",
        "longitude": 2.241127014
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865662?v=4489AA65B37A70494A2868BE5C9B5571E5B02E27&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641173"
      },
      "qualificationCode": "X",
      "romeCode": "D1401",
      "romeLibelle": "Assistanat commercial",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technicien / Technicienne de maintenance industrielle",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:58+01:00",
      "dateCreation": "2019-11-13T12:44:58+01:00",
      "description": "METIER INTERIM ET CDI MONTAIGU recrute pour notre client spécialisé dans la maintenance Industrielle, la Mécano-Soudure un technicien de maintenance H/F Vous intervenez comme prestataire de service chez les clients pour réaliser principalement des opérations de maintenance curative et préventive sur des équipements industriels de production. Plus précisément, vous : - venez en support technique chez les clients pour accompagner les équipes en poste, - diagnostiquez les pannes et assurez les réparations, - assurez le remplacement des organes défectueux ou usés, - assurez en complément le déplacement de machines. Poste à pourvoir en CDI Horaires adaptables: travail en 2x8 ou en journée Heures supplémentaires majorées à 25%. Récupérées au delà de 39H par semaine Zone d'intervention: 1H30 maximum autour de l'entreprise Salaire : à étudier selon expérience Ce poste vous intéresse, merci de postuler en ligne ou vous présenter en agence munit de votre CV. Minimum 2 ans d'expérience sur poste similaire Autonome sur les parties mécanique et électricité La maîtrise en pneumatique et en hydraulique serait un plus Les habilitations électriques et le CACES nacelle et/ou chariot élévateur serait un plus (formations assurées néanmoins en interne) Métier Intérim et CDI est un réseau de 10 agences dans l'Ouest de la France. Nous tissons avec nos collaborateurs intérimaires une relation de confiance et de fidélité.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 2 An(s)",
      "formations": [],
      "id": "3641172",
      "intitule": "Technicien de maintenance (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 46.558670044,
        "libelle": "85 - Vendée",
        "longitude": -1.356292963
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865882?v=5254C215ADB9C72970482CA950FA5BA61302B99F&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641172"
      },
      "qualificationCode": "X",
      "romeCode": "I1304",
      "romeLibelle": "Installation et maintenance d'équipements industriels et d'exploitation",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": true,
      "appellationlibelle": "Orthophoniste",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:57+01:00",
      "dateCreation": "2019-11-13T12:44:57+01:00",
      "description": "Le descriptif de poste : Nous recrutons, pour l'un de nos clients, une Association médico-sociale, située dans Paris présentant différents pôles et structures, un Orthophoniste - H/F, dans le cadre d'un CDI à temps plein. Présentation de l'établissement : Un Institut pour déficient auditif, accueillant des enfants, situé à Paris Intra Muros. Soins : -Accompagnement de Jeunes enfants sourds avec pour la plupart un implant cochléaire. -Possibilité de déficience intellectuelle légère et moyenne ; -Présentation du poste : Rattaché à la Direction, au sein de l'équipe pluridisciplinaire, vous êtes amené à contribuer au diagnostic et à la prise en charge des troubles de la sphère du langage. -Participer activement au projet de vie des enfants pris en charges au sein d'une équipe pluridisciplinaire. -Présentation des missions principales : - Réaliser les bilans orthophoniques et les suivis ; - Suivre les enfants ; - Travailler sur langage oral, écrit ; - Mettre en place d'éventuelles prises en charges plus créatives pour troubles des apprentissages. Travail au sein de l'institut et accompagnement au sein des établissements scolaires pour certains enfants . Rattachement hiérarchique : la Direction. Conditions du poste : Rémunération indicative : Convention collective : 66  prime, prise en charge de la mutuelle, TR , possibilité de Formation à la LSF.La qualification requise : Diplôme d'État d'Orthophonie. Les compétences demandées : Une expérience en Institution ou auprès d'enfants est demandée ; Un intérêt pour le travail de réflexion d'équipe pluridisciplinaire est demandé. Au-delà des compétences techniques inhérentes à votre fonction, vous faîtes preuve de : Rigueur et ponctualité ; Ouverture d'esprit ; Bon relationnel ; Capacités d'adaptation ; Sens du travail en équipe ; Dynamisme. Nous attendons votre candidature au plus vite, sur l'adresse mail : medicareadomino-hc.com Nous restons à votre disposition au 06 80 87 38 53 ou au 06 84 65 94 45.Le Groupe DOMINO (200 collaborateurs, 40 agences en France, Pologne, Portugal & Suisse, 112 millions EUR de CA) confirme depuis 19 ans sa présence et son développement sur le marché des solutions RH (intérim, recrutement, formation, conseil). Membre fondateur du réseau DOMITIS (www.reseaudomitis.com), DOMINO accède à de nombreux marchés nationaux.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Medicareadomino-hc"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641169",
      "intitule": "Orthophoniste - H/F temps plein/OTS/PARIS",
      "langues": [],
      "lieuTravail": {
        "commune": "75000",
        "latitude": 48.863838196,
        "libelle": "75 - Paris (Dept.)",
        "longitude": 2.344630957
      },
      "natureContrat": "Contrat apprentissage",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333864030?v=C8074BB5EC356A81CB5D59768423DF8BBC8EDC08&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641169"
      },
      "qualificationCode": "X",
      "romeCode": "J1406",
      "romeLibelle": "Orthophonie",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technico-commercial / Technico-commerciale",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:56+01:00",
      "dateCreation": "2019-11-13T12:44:56+01:00",
      "description": "Sous la responsabilité du Responsable de secteur, vous aurez en charge de : - Prospecter toutes les entreprises cibles afin de développer votre portefeuille client - Identifier tous les potentiels prospects - Fidéliser un portefeuille existant - Organiser et optimiser votre planning de rendez-vous - Gérer en toute autonomie votre business et vos reportings Votre secteur : 13 et 86 Vous avez une expérience significative dans le domaine de l'Industrie ou du BTP, vous avez le sens de l'organisation, un excellent relationnel et savez travailler en toute autonomie.CONNECTT recherche pour l'un de ses clients dans le domaine de l'Industrie.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Connectt"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641166",
      "intitule": "TECHNICO-COMMERCIALE 13 et 86 (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 43.533737183,
        "libelle": "13 - Bouches du Rhône",
        "longitude": 5.095419884
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333864090?v=C8D580318CE9D6200F46F8B18363DB0E9CEB61DB&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641166"
      },
      "qualificationCode": "X",
      "romeCode": "D1407",
      "romeLibelle": "Relation technico-commerciale",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Agent / Agente de sécurité",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:54+01:00",
      "dateCreation": "2019-11-13T12:44:54+01:00",
      "description": "Barrière recherche un Talent : Agent de Sécurité - Contrôleur aux entrées(H/F) Barrière, c'est 33 Casinos, 18 Hôtels, plus de 120 Restaurants et Bars, 15 Spas, 3 Golfs, 2 Tennis club, 1 Balnéo, 1 Thalasso. Barrière, c'est aussi 50 métiers incarnés par près de 7000 collaborateurs. Et par vous aussi demain Véritable acteur du service Sécurité, vous intervenez dans les missions relatives au contrôle et à la protection des personnes, à la sûreté des lieux et à la sécurité incendie. Vous êtes en charge: - d'accueillir, orienter et sélectionner la clientèle - de surveiller le casino et ses extérieurs - d'intervenir en cas d'acte de malveillance - de contrôler les entréesPoste 35 heures hebdomadaires alternant travail de nuit et de journée et nécessitant une disponibilité les weekend et jours fériés. Vous justifiez d'une expérience significative dans la sécurité et êtes titulaire d'un CQP à jour. Vous êtes reconnu pour votre sens relationnel, votre diplomatie et votre esprit d'équipe, Votre \"plus\" Vous êtes titulaire du SSIAP 1 ou 2 Travailler chez Barrière : Partager un esprit. Faire vivre des moments exceptionnels à nos clients. Voilà la mission de nos 7000 collaborateurs, passionnés par leur métier.Casino Barrière de Dinard",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641159",
      "intitule": "Agent de Sécurité - Contrôleur aux entrées H/F",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.201869965,
        "libelle": "35 - Ille et Vilaine",
        "longitude": -1.72755301
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865979?v=7B5CBCE2CBBC5A27F14119583E4FB046EFBDC1BA&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641159"
      },
      "qualificationCode": "X",
      "romeCode": "K2503",
      "romeLibelle": "Sécurité et surveillance privées",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technicien applicateur / Technicienne applicatrice hygiéniste",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:53+01:00",
      "dateCreation": "2019-11-13T12:44:53+01:00",
      "description": "Après une formation, vous aurez pour mission principale de contribuer en milieu urbain, à la protection des bâtiments et des volumes contre les nuisibles. Dans ce cadre, vos tâches sont les suivantes : - Préparation du matériel, équipement et sécurisation du périmètre d'intervention - Repérage et constatation des nuisibles - Dépôt d'appâts sur les lieux de l'infestation - Traitement contre les nuisibles - Renseignement des supports de suivi d'intervention Ces tâches ne sont pas exhaustives mais peuvent évoluer selon le profil du candidat.Le permis B (VL) est indispensable. Vous êtes motivé(e), sérieux (se) et avez envie d'être formé(e) et d'apprendre un nouveau métier Nous sommes fait pour nous rencontrer  Dynamisme, sens de l'écoute et esprit d'équipe seront vos atouts de réussite. Débutant(e) accepté(e). ELIS PREVENTION NUISIBLES nouvelle filiale d'ELIS groupe multiservice recherche pour son agence basée à Bondoufle : 1 Technicien(ne) Applicateur/rice Hygiéniste. Nous proposons un service complet de prévention (dératisation, désinsectisation, désinfection) mais aussi de lutte contre les nuisibles.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641156",
      "intitule": "Technicien 3D H/F",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.580608368,
        "libelle": "91 - Essonne",
        "longitude": 2.323250055
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865857?v=F983C0EB52F0E50219FA7AC62CC8328855E28B54&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641156"
      },
      "qualificationCode": "X",
      "romeCode": "K2305",
      "romeLibelle": "Salubrité et traitement de nuisibles",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Responsable comptabilité",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:52+01:00",
      "dateCreation": "2019-11-13T12:44:52+01:00",
      "description": "Dans les grandes lignes, il s'agira d'assurer la gestion de vos dossiers clients, de l'accompagnement des clients dans leur développement à l'identification de potentielles nouvelles missions. Vous élaborez le suivi quotidien de vos clients, tenue, révision et élaboration des déclarations fiscales jusqu'au projet de bilan avec la supervision du manager. Vous avez pour missions principales : - l'élaboration des projets d'arrêtés de comptes, - la réalisation des déclarations fiscales, - la préparation du bilan imagé, - la réalisation de missions exceptionnelles. Vous aimez le contact avec la clientèle ? Tant mieux, car nous comptons sur vous pour repérer des opportunités et valoriser l'offre In Extenso auprès de clients potentiels, et pour développer un réseau professionnel solide.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641154",
      "intitule": "Responsable de Dossiers Comptable H/F",
      "langues": [],
      "lieuTravail": {
        "codePostal": "67500",
        "commune": "67180",
        "latitude": 48.781768799,
        "libelle": "67 - HAGUENAU",
        "longitude": 7.657740116
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1334075653?v=1257543F97998A3FDF8CA74E5FEF05B9F01FABE5&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641154"
      },
      "qualificationCode": "X",
      "romeCode": "M1206",
      "romeLibelle": "Management de groupe ou de service comptable",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Chef de produit",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:52+01:00",
      "dateCreation": "2019-11-13T12:44:52+01:00",
      "description": "Expectra, leader en France de l'intérim spécialisé et du recrutement en CDI de cadres et agents de maîtrise. Les consultants du Département Commercial & Marketing vous proposent des opportunités de carrière.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Expectra"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641152",
      "intitule": "CHEF DE PRODUIT MARKETING (F/H) - Juvisy-sur-Orge (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.580608368,
        "libelle": "91 - Essonne",
        "longitude": 2.323250055
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333864839?v=7E200E13FEE20F0AF17B4A947953364A5F96DBAE&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641152"
      },
      "qualificationCode": "X",
      "romeCode": "M1703",
      "romeLibelle": "Management et gestion de produit",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technicien / Technicienne logistique",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:51+01:00",
      "dateCreation": "2019-11-13T12:44:51+01:00",
      "description": "Le/La technicien(ne) Logistique Supply Chain a pour mission de s'assurer que tous les moyens de son UOP sont en place pour livrer l'ensemble des clients dans les délais. Il/Elle est le lien direct avec les CT et/ou client, la logistique de production, les transporteurs et les douanes, les fournisseurs et sous-traitants. MISSIONS : - Valider les ARC clients en tenant compte des capacités de son UOP avec assistance - Répondre aux demandes de délais (nouvelles et relances) avec assistance - Relancer les fournisseurs lors de l'absence du logisticien approvisionnement - Relancer les sous-traitants - Editer les bordereaux de préparation pour le service expédition - Renseigner les indicateurs définis (OTD.) - Si besoin, concevoir et rédiger des courriers - Selon le secteur, saisir les bordereaux de livraison - Être en relation directe avec les CT et/ou la logistique client pour les problèmes de livraisons, les programmes d'approvisionnements. - Tirer les flux en fonction des besoins clients - Assurer la relation avec les ateliers (attentes, urgences.) pour respecter les délais - Assurer les passations des demandes d'achats après analyse des besoins. - Être en relation direct avec les fournisseurs, sous-traitants français et étrangers pour les problèmes de livraison, les programmes d'approvisionnements - Assurer la relation avec les ateliers (attentes, urgences.) en optimisant les délais - Être en relation directe avec les CT et/ou client export et France pour les problèmes de livraisons, les programmes d'approvisionnements avec assistance.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641150",
      "intitule": "TECHNICIEN LOGISTIQUE SUPPLY CHAIN (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "72400",
        "commune": "72132",
        "latitude": 48.135871887,
        "libelle": "72 - LA FERTE BERNARD",
        "longitude": 0.600929976
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1334075538?v=A80D981D58371952A1C55248C49D7962825C1056&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641150"
      },
      "qualificationCode": "X",
      "romeCode": "N1303",
      "romeLibelle": "Intervention technique d'exploitation logistique",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Expert / Experte support technique",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:50+01:00",
      "dateCreation": "2019-11-13T12:44:50+01:00",
      "description": "Nous recherchons pour l'un de nos clients, un expert technique VUEJS, pour accompagner une équipe sur VUEJSCompétences techniques VUEJS - Expert - ImpératifREACT - Confirmé - ImportantANGULAR - Confirmé - SouhaitableConnaissances linguistiques Français Professionnel (Impératif)Description détaillée - Expertise sur la conception d'applications SPA : architecture applicative : découpage du code source, granularité des composants, communication avec les parties serveurs (communication REST, gestion de l'authentification utilisateur et de ses droits d'accès, règles CORS, gestion des états, gestion des transactions)- Conseils sur l'utilisation et la mise en uvre de ces concepts avec Vue.js et les technologies et l'outillage javascript- Expertise sur les chaînes de construction du code SPA : transpilation, minification, - Expertise sur la sécurisation des développements (préventions des attaques XSS, CSRF, vol de cookie, ) sur ces technologies - Expertise méthodologique liée à ces technologies- Cadrage de l'utilisation (quels outils, quelles options, quelles règles à respecter) et une documentation d'utilisation pas à pas pour les développeurs- Des composants prêts à l'emploi pour les futurs développeurs- Les livrables attendus sont :- un document comportant les différents conseils et recommandations à l'utilisation de Vue.js et l'outillage associé.- un document analysant les risques liés à la sécurité et proposant des bonnes pratiques pour couvrir ces risques.- un guide pratique de développement Vue.js adapté aux choix et au contexte client- des composants développés sous forme de code source commenté avec leur procédure de constructionExpérience de 6 à 7 ans minimum.Définition du profil INFRASTRUCTURE / SUPPORTIl assure un rôle de conseil, d'assistance, d'information de formation et d'alerte. Il peut intervenir directement sur tout ou partie d'un projet qui relève de son domaine d'expertise.Il effectue un travail de veille technologique sur son domaine et propose des évolutions qu'il juge nécessaires.Il est l'interlocuteur reconnu des experts externes (fournisseurs, partenaires).Nécessite un niveau d'expérience minimum de  de 10 ans.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 6 à 7 An(s)",
      "formations": [],
      "id": "3641149",
      "intitule": "Expert technique VueJS / Freelance (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 44.91065979,
        "libelle": "33 - Gironde",
        "longitude": -0.665018022
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333862833?v=B931698200B0DB149F606063BCBFAF2C2BA506B8&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641149"
      },
      "qualificationCode": "X",
      "romeCode": "H1101",
      "romeLibelle": "Assistance et support technique client",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Responsable des projets organisation",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:50+01:00",
      "dateCreation": "2019-11-13T12:44:50+01:00",
      "description": "Au sein de cette équipe pluridisciplinaire, vous apportez une expertise urbaine et architecturale ainsi que conseil aux élus et aux services. - En maîtrise d'ouvrage directe ou en AMO, vous assurez les missions suivantes :- programmation et conception au stade esquisse de projets d'espaces publics ;- réalisation de notices d'insertion urbaine et paysagère ou d'éléments de programme pour des projets sous maîtrise d'ouvrage Lorient Agglomération ;- réalisation d'esquisse ou d'avant-projet de plan masse pour des opérations d'aménagements en s'appuyant sur différentes dimensions : diagnostic et analyse du site, foncier, orientations réglementaires et contraintes techniques, potentiel commercial ;- montage et pilotage opérationnel d'opérations d'aménagement sous maîtrise d'ouvrage communautaire (parcs d'activités économiques) et études préalables et conception de projets d'aménagement (lotissement ZAC) en vue de leur mise en œuvre par les communes de l'agglomération.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641148",
      "intitule": "Resp. projets opérations aménagement, projets urbains h/f",
      "langues": [],
      "lieuTravail": {
        "codePostal": "56100",
        "commune": "56121",
        "latitude": 47.750068665,
        "libelle": "56 - LORIENT",
        "longitude": -3.366290092
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1334074876?v=D075AEBED02E8FF96530454A93B8D1F699350E29&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641148"
      },
      "qualificationCode": "X",
      "romeCode": "M1402",
      "romeLibelle": "Conseil en organisation et management d'entreprise",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Conducteur / Conductrice machiniste",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:49+01:00",
      "dateCreation": "2019-11-13T12:44:49+01:00",
      "description": "METIER INTERIM ET CDI ST PHILBERT DE GRAND LIEU recrute pour son client spécialisé dans les crêpes et les galettes industrielles un(e) conducteur de machine (H/F) Vos missions seront les suivantes : - Gestion de la ligne de production - Management - Montage - Démontage - Nettoyage - Changement de format Poste du lundi au vendredi en 28 .Nous recherchons une personne dynamique, autonome et motivée  Vous devez être disponible de suite et sur du long terme.Métier Intérim et CDI est un réseau de 10 agences dans l'Ouest de la France. Nous tissons avec nos collaborateurs intérimaires une relation de confiance et de fidélité.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641145",
      "intitule": "Conducteur machine (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 47.409152985,
        "libelle": "44 - Loire Atlantique",
        "longitude": -1.542898059
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333864049?v=AA8C724F444B7E799688F3D5E83AD5F1F0C7A332&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641145"
      },
      "qualificationCode": "X",
      "romeCode": "N4103",
      "romeLibelle": "Conduite de transport en commun sur route",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technicien / Technicienne de hot line en informatique",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:49+01:00",
      "dateCreation": "2019-11-13T12:44:49+01:00",
      "description": "Dans le cadre du renforcement de nos équipes, CTS recherche un technicien support polyvalent qui participera au projet sur des aspects techniques et fonctionnels. Vous aurez pour principales missions :  L'assistance au paramétrage de la solution, en relation avec les développeurs/agences Web  Le support aux utilisateurs d'une suite logicielle  Le suivi des demandes d'assistance et des incidents dans une démarche d'amélioration de la qualité documentaire et de la qualité de service  La maintenance des documentations  Gestion de parc, l'installation, le paramétrage et la maintenance des équipements du parc informatique (stations de travail, périphériques) Compétences  Assistance technique et fonctionnelle  Prise en main du fonctionnement d'une suite logicielle et de son environnement métier  Connaissances techniques en MySQL, XML et idéalement PHP Vous avez un Bac  2 minimum, d'une formation informatique  Vous justifiez d'au moins une expérience réussie sur un poste similaire  Vous savez définir et gérer des priorités d'interventions  Doté d'un bon sens relationnel, vous avez le sens du service client et êtes organisé et rigoureux. Vous avez un bon esprit d'équipe. Vous êtes curieux et motivé. Vous appréciez le travail en équipe, et vous avez la volonté de monter en compétences sur des projets à forte valeur ajoutée Rejoignez-nous  Issue sur un regroupement des Bureaux d'Etudes de PME industrielles, CTS intervient sur l'ensemble du cycle de vie des projets de ses partenaires et clients. CTS s'appuie sur des équipes mobiles et structurées dotées de solides compétences en ingénierie mécanique électrique et électronique, process industriel, gestion de projet et systèmes d'informations. Forgé par son expérience au sein de grands programmes industriels, et à l'épreuve de la réactivité nécessaire des PME en ingénierie et production, le groupe est capable de répondre aux besoins de ses clients en solutions globales. CTS a pour vocation de rassembler l'ensemble des compétences indispensables à la réalisation de programmes de grande envergure aussi bien sur le territoire français qu'à l'international. Nous intervenons au travers de nos équipes auprès des grands donneurs d'ordres dans les domaines de l'Energie, l'Aéronautique, l'automobile, Informatique et Telecom sur des programmes variés.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641142",
      "intitule": "Technicien support informatique (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 43.40819931,
        "libelle": "31 - Garonne (Haute)",
        "longitude": 1.138939977
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333864103?v=533875ADF6F2535101C4C64E115DB965DE39D1BC&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641142"
      },
      "qualificationCode": "X",
      "romeCode": "I1401",
      "romeLibelle": "Maintenance informatique et bureautique",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Auditeur comptable et financier / Auditrice comptable et financière",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:48+01:00",
      "dateCreation": "2019-11-13T12:44:48+01:00",
      "description": "Vous êtes auditeur ou auditrice financier dans un cabinet comptable et vous avez envie et besoin de prendre le temps de conseiller. Vous avez l'habitude de d'accompagner vos collaborateurs dans le cadre de la gestion du portefeuille. Vous planifiez vos missions, vous réalisez le contrôle interne et l'analyse des risques via l'audit, vous rédigez des rapports et des synthèses  bref, vous occupez un poste similaire mais vous avez besoin d'être plus proche de vos clients, de les conseiller et d'accompagner leur pilotage d'activité   Notre client accompagne des PME et des grands groupes avec la même envie de rendre un travail de qualité, soigné tout en prenant le temps de conseiller et d'accompagner autant que possible. Vous travaillerez dans une ambiance agréable où les dirigeants sont soucieux du bien être des collaborateurs et collaboratrices. Vous êtes issu(e) d'une formation type DSCG ou master CCA ou expert comptable ou école supérieure de commerce. Vous avez une 1ère expérience avérée sur un poste similaire. Vous avez des valeurs et le besoin que l'humain soit au centre de votre accompagnement. Postulez, votre candidature sera confidentielle. Cabinet conseil, ACTO CONSULTING est une solution RH globale pour l'entreprise et l'emploi en région. ACTO CONSULTING partage les valeurs fortes du réseau : proximité, efficacité, réactivité et disponibilité. Notre cabinet intervient dans : Recrutement, Ingénierie de formation, Entretiens Professionnels, de Positionnement, Testing, Évaluation des comportements professionnels, Tests d'aptitude, Audit RH, Marketing RH",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 1 An(s)",
      "formations": [],
      "id": "3641140",
      "intitule": "Auditeur financier F/H (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 45.901981354,
        "libelle": "87 - Vienne (Haute)",
        "longitude": 1.03944695
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865708?v=B88B924BB299B94AFE594EBB82D5030AABC66409&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641140"
      },
      "qualificationCode": "X",
      "romeCode": "M1202",
      "romeLibelle": "Audit et contrôle comptables et financiers",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Mécanicien / Mécanicienne poids lourds",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:47+01:00",
      "dateCreation": "2019-11-13T12:44:47+01:00",
      "description": "Nous recherchons pour l'un de nos clients, un(e) mécanicien(ne) poids lourd/ utilitaire/ bus. Le poste est à pourvoir en CDI le plus rapidement possible sur l'agglomération rochelaise. Horaires classiques de journée du lundi au vendredi, avec un samedi matin travaillé toutes les trois semaines. Rémunération à convenir selon profil Si vous êtes intéressé(e), merci de nous contacter au 05.46.45.43.94 et de nous envoyer votre CV par mail à larochelleachronos-interim.frPermis b souhaité. Expérience minimum exigée de 2 ans.Chronos intérim, premier réseaux d'agences d'emploi sous forme de société coopérative et participative Avec Chronos Intérim, vous bénéficiez d'un accompagnement unique, car pour nous, vous n'êtes pas un numéro. Vous intégrez une entreprise dynamique, fière de ses talents",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Chronos Intérim"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 2 An(s)",
      "formations": [],
      "id": "3641136",
      "intitule": "MÉCANICIEN POIDS LOURD (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 45.834991455,
        "libelle": "17 - Charente Maritime",
        "longitude": -0.76207602
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333863962?v=0693B75F05DB554F986910D48CF99DC2509C3FA8&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641136"
      },
      "permis": [
        {
          "exigence": "S",
          "libelle": "B - Véhicule léger"
        }
      ],
      "qualificationCode": "X",
      "romeCode": "I1604",
      "romeLibelle": "Mécanique automobile et entretien de véhicules",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technicien / Technicienne de maintenance industrielle",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:46+01:00",
      "dateCreation": "2019-11-13T12:44:46+01:00",
      "description": "CAPACTUEL, agence d'emploi de proximité, spécialisée dans le secteur de l'industrie, recherche pour son client, spécialiste de la menuiserie aluminium, un technicien de maintenance H/F : Vous serez en charge de la maintenance préventive, curative, améliorative des machines et interviendrez également au niveau des locaux, si besoin. CDI 35h à pourvoir dès que possible Horaires de nuit fixes : 22h30 - 6h00 (formation à prévoir de journée pendant 2 à 3 mois) Rémunération : entre 1 800EUR et 2 200EUR brut mensuel fixe selon expérience  majoration des heures de nuit de 15%  prime panier de nuit de 15.05EUR brut/jour  prime de participation  mutuelle.Votre profil : Vous êtes issu(e) d'une formation Bac à Bac2 en maintenance industrielle et avez une première expérience dans le milieu industriel. Vous êtes réactif(ve), autonome et savez vous adapter rapidement, envoyez nous votre candidature.CAPACTUEL Travail Temporaire et Recrutement direct CDD-CDI, est une agence d'emploi de proximité spécialiste du recrutement dans le secteur du Tertiaire et de l'Industrie",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Capactuel"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641133",
      "intitule": "Technicien de maintenance industrielle (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 44.91065979,
        "libelle": "33 - Gironde",
        "longitude": -0.665018022
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333863917?v=264D22E70072EBDEB72304BE183C57CFF2D7FDB8&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641133"
      },
      "qualificationCode": "X",
      "romeCode": "I1304",
      "romeLibelle": "Installation et maintenance d'équipements industriels et d'exploitation",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technicien / Technicienne d'études en automatisme",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:45+01:00",
      "dateCreation": "2019-11-13T12:44:45+01:00",
      "description": "Expectra, leader en France de l'intérim spécialisé et du recrutement en CDI de cadres et agents de maîtrise.Les consultants du Département Ingénierie vous proposent des opportunités de carrière.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Expectra"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641130",
      "intitule": "TECHNICIEN BE ELECTROTECHNIQUE (F/H) (H/F)",
      "langues": [],
      "lieuTravail": {
        "commune": "75000",
        "latitude": 48.863838196,
        "libelle": "75 - Paris (Dept.)",
        "longitude": 2.344630957
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333864500?v=2AC45E2A3A137A60B06C59E782024277D46CC913&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641130"
      },
      "qualificationCode": "X",
      "romeCode": "H1208",
      "romeLibelle": "Intervention technique en études et conception en automatisme",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Développeur / Développeuse informatique",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:45+01:00",
      "dateCreation": "2019-11-13T12:44:45+01:00",
      "description": "Mission en développement Scala-Une architecture Micro-Services développés en Scala -Spring Reactor, ReactiveX ou Akka Streams-Architecture reactive-Éléments de l'architecture développé en GO et en Kotlin -Front-end sous forme d'une application Angular 6 utilisant avec RxJS.-PostgreSQL et MongoDB, REDIS-Bus de message RabbitMQ / Kafka -Cloud AWS et On Premise (VMWare)-Linux / IntelliJ, le travail est organisé-Paradigmes fonctionnels (expressions lambda, types immutables, etc.)-Méthode agile-Intégration continue GitFlow -Tests auto et manuels-Déploiements automatisés (Jenkins/Terraform)",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641128",
      "intitule": "Développeur Scala / Freelance - Bagnolet (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.936069489,
        "libelle": "France",
        "longitude": 2.364880085
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333862834?v=2C88D06BEE77F2C636CD7139AFE3EE9796690DFC&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641128"
      },
      "qualificationCode": "X",
      "romeCode": "M1805",
      "romeLibelle": "Études et développement informatique",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Ingénieur chargé / Ingénieure chargée d'affaires du BTP",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:44+01:00",
      "dateCreation": "2019-11-13T12:44:44+01:00",
      "description": "ARTUS INTERIM CDD CDI à ALENCON recherche pour l'un de ses client : CHARGE D'AFFAIRES PROCESS CONFIRME (H/F) Sous la direction du responsable de service, vous êtes le garant de la pertinence technique et financière des offres transmises aux clients. Vos missions : - Vous analysez les demandes des clients en établissant si besoin leur cahier des charges - Vous établissez les offres techniques et commerciales - Vous réalisez le support technique à la vente à la demande du service commercial - Vous lancez les projets après commandes Pour réaliser vos missions, vous vous appuyer sur l'ensemble des compétences de l'entreprise (dessinateurs, assistants, chargés d'affaires). Doté(e) d'une expérience confirmée en gestion de projets process, vous êtes familier des problématiques de pompage, d'échanges thermiques, de tuyauterie, de chantier, de qualité. Vous maîtrisez les techniques de chiffrage, de gestion de projet, les normes, réglementations et pratiques des clients. Ingénieur(e) de formation, vous avez des notions en génie des procédés dans l'un des domaines suivants : traitement des eaux de source et minérales, autres boissons, laitier, agro-alimentaire.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Artus Interim"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641124",
      "intitule": "INGÉNIEUR CHARGE D'AFFAIRES PROCESS (H/F)",
      "langues": [],
      "lieuTravail": {
        "codePostal": "61000",
        "commune": "61001",
        "latitude": 48.430648804,
        "libelle": "61 - ALENCON",
        "longitude": 0.083619997
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1334074937?v=2535CF74EE9F98B05142D2BE8F2310E9A5CB5D9C&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641124"
      },
      "qualificationCode": "X",
      "romeCode": "F1106",
      "romeLibelle": "Ingénierie et études du BTP",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Comptable",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:43+01:00",
      "dateCreation": "2019-11-13T12:44:43+01:00",
      "description": "Au sein de cabinet, vous aurez un poste crucial puisque vous intervenez essentiellement : - Sur de la révision comptable, - La vérification de la saisie comptable, - L'élaboration des liasses fiscales, - La préparation du Bilan et du Résultat Fiscal. Votre portefeuille comprendra divers types de dossiers (BNC, BIC, IS, LCI, SARL, Groupement de Sociétés ) De formation comptable (idéalement DCG), vous justifiez de 3 ans d'expérience professionnelle et vous maîtrisez le Pack Office et le logiciel QUADRA. Rigoureux, polyvalent, vous souhaitez vous investir sur du long terme au sein d'un cabinet. SBC Intérim & Recrutement, société en conseil et recrutement spécialisé dans la Comptabilité et Gestion, recherche pour l'un de ses client, important cabinet comptable de la région, un Collaborateur Comptable H/F.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Sbc Intérim & Recrutement"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 3 An(s)",
      "formations": [],
      "id": "3641121",
      "intitule": "Collaboratrice comptable (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 43.533737183,
        "libelle": "13 - Bouches du Rhône",
        "longitude": 5.095419884
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865736?v=9D8AB0CF4C592F41E26EF73879BE35AA03EC502C&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641121"
      },
      "qualificationCode": "X",
      "romeCode": "M1203",
      "romeLibelle": "Comptabilité",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technicien / Technicienne de maintenance process",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:43+01:00",
      "dateCreation": "2019-11-13T12:44:43+01:00",
      "description": "Rattaché au Chef d'Atelier, vous garantissez un taux de service optimal des équipements. Vos missions principales sont : - Diagnostiquer et traiter les dysfonctionnements (automates, électriques, mécaniques, pneumatiques ..) - Remettre en état les matériels avec le souci permanent d'assurer leur redémarrage dans les meilleurs délais en respectant les normes et les consignes de sécurité. - Réaliser les opérations de maintenance préventive et corrective des installations durant leur phase d'arrêt, de sa propre initiative ou de manière concertée avec l'encadrement. - Contribuer à l'optimisation du fonctionnement des lignes en proposant des actions de modification, d'adaptation et d'amélioration du matériel. - Participer à la mise en œuvre de solutions nouvelles - Conseiller les utilisateurs et les intervenants sur le fonctionnement des machines et sur le process. - Mettre à jour la documentation technique. - Contribuer au bon emploi du stock de pièces de rechange et a son suivi sur la GMAO (anticipation des besoins, rangement et conservation, ) . - Réaliser spontanément, ou à la demande de l'encadrement, la maintenance préventive et les dépannages de premier niveau des fonctionnalités de l'ensemble des installations générales dans le respect des normes et consignes de sécurité. - S'informer sur les opérations de maintenance réalisées par les prestataires extérieurs. - Mettre en œuvre des modifications décidées sur l'ensemble des installations générales, relevant de ses attributions - Effectuer le suivi qualité des lignes automatisées et réaliser toutes les modifications nécessaires à l'amélioration de la qualité de la préparation",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641119",
      "intitule": "Technicien de Maintenance basé dans le 77 H/F",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.668079376,
        "libelle": "77 - Seine et Marne",
        "longitude": 2.835798025
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1334074864?v=FAB8E4492EB97428AC22624182DEE488A384766D&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641119"
      },
      "qualificationCode": "X",
      "romeCode": "I1304",
      "romeLibelle": "Installation et maintenance d'équipements industriels et d'exploitation",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Livreur / Livreuse",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:43+01:00",
      "dateCreation": "2019-11-13T12:44:43+01:00",
      "description": "- Réaliser les livraisons de nos produits SUSHI SHOP auprès de nos clients en respectant notre engagement en matière de délais de livraison et en matière de qualité des produits - Participer, entre les livraisons, à l'entretien de l'espace intérieur et extérieur de la Boutique - Participer à la préparation des sacs de livraison - Réceptionner et ranger les marchandises - Réaliser toutes autres tâches nécessaires au bon déroulement de la boutique.-maîtriser le code de la route -avoir son BSR ou permis B -avoir une conduite irréprochable en scooter -avoir l'habitude de conduire un scooter en ville (idéalement Paris) -respecter les délais de livraison imposés (rapidité) -assurer une qualité d'accueil à l'ensemble de nos clients, dans la boutique comme lors des livraisons à domicileDepuis sa création en 1998, Sushi Shop est devenu le leader du marché de la livraison et de la vente à emporter de sushi et la 1ère enseigne de ce type à se développer en franchise dès 2006. Sushi Shop dispose d'un réseau de plus de 155 points de vente, des Shops et des Corners (stands à sushis implantés en grandes et moyennes surfaces) en France et à l'étranger. L'univers Sushi Shop, ses recettes innovantes, la qualité de ses produits, son savoir- faire, son professionnalisme sont les atouts qui permettent à Sushi Shop de gagner chaque jour en notoriété et d'étendre son implantation en France, en Europe avec l'ouverture de points de vente en Belgique, Italie, Espagne, Luxembourg, Suisse, Angleterre, Allemagne, mais aussi au Moyen-Orient.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641118",
      "intitule": "Livreur polyvalent H/F AVIGNON",
      "langues": [],
      "lieuTravail": {
        "latitude": 43.974594116,
        "libelle": "84 - Vaucluse",
        "longitude": 5.086143017
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865650?v=F3D467978655A55A150B46424CEF6FD8258E6C1C&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641118"
      },
      "qualificationCode": "X",
      "romeCode": "N4105",
      "romeLibelle": "Conduite et livraison par tournées sur courte distance",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Juriste droit public",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:42+01:00",
      "dateCreation": "2019-11-13T12:44:42+01:00",
      "description": "Au sein de la direction juridique France, sous la direction du responsable juridique, vous êtes en charge de l'accompagnement juridique et du support au développement des directions opérationnelles et fonctionnelles. Vous intervenez également sur des projets transverses pilotés par la direction juridique France. Vos missions principales seront les suivantes : - appréhender les éléments clefs des opportunités commerciales et structurer les transactions de la manière la plus avantageuse du point de vue juridique et commercial pour l'ensemble des contrats de la commande publique - participer à l'analyse des cahiers des charges, à l'élaboration des offres et de la stratégie de négociation avec les équipes projet - identifier et sensibiliser les équipes projet et les dirigeants de l'entreprise sur les risques compliance, contractuels, financiers et opérationnels, - participer à la résolution des litiges en étant force de proposition, - soutenir, promouvoir et mettre en œuvre les initiatives de la direction juridique",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641117",
      "intitule": "Juriste droit public des affaires min. 5 ans (F/H) (H/F)",
      "langues": [],
      "lieuTravail": {
        "libelle": "France"
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1334074891?v=26CB7A21ED3066632B4139D3F118D9678DAEC3A6&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641117"
      },
      "qualificationCode": "X",
      "romeCode": "K1903",
      "romeLibelle": "Défense et conseil juridique",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Développeur / Développeuse informatique",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:42+01:00",
      "dateCreation": "2019-11-13T12:44:42+01:00",
      "description": "TEKsystems recherche pour son client dans le cadre d'une mission un Développeur JAVA confirmé. Mission : Sur un projet majeur vous allez intervenir sur la mise en place d'une application.Mission : - Assurer larelation avec le client et prendre le lead avec les équipes transverses - Suivre les travaux des développeurs : Revue de code et bonnes pratiques de développement- Assurer la configuration des serveurs pour le déploiement automatique via JENKINS- Réaliser les livraisons sur les différents environnements - Participer à la rédaction la documentation technique - Concevoir et réaliser le modèle de donnéesCompétences techniques : JEE, SPRING, WEBSERVICES REST, HIBERNATE, JENKINS, POSTGRES, SPRING BATCH, HTML, CSSSouhaitables : BOOTSTRAP, JAVASCRIPT, JQUERY",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Teksystems"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641114",
      "intitule": "URGENT recherche TECHLEAD JAVA / Freelance - Vincennes (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.801467896,
        "libelle": "94 - Val de Marne",
        "longitude": 2.426923037
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333862734?v=C08E2D0F1AA3989C64C75A96DE187CD783531657&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641114"
      },
      "qualificationCode": "X",
      "romeCode": "M1805",
      "romeLibelle": "Études et développement informatique",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Métreur / Métreuse",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:41+01:00",
      "dateCreation": "2019-11-13T12:44:41+01:00",
      "description": "Le Métreur assure le relevé des mesures et les conceptions de nos chantiers auprès d'une clientèle de particuliers. Le Métreur prend des dimensions, rempli des feuilles de métrages, fait des croquis à main levée et transmet des commandes aux différentes usines par système informatique, etc  Formation et accompagnement assurés avant la prise de poste en autonomie.Vous êtes titulaire d'un diplôme de niveau CAP/BEP Menuiserie ou d'un Bac Technique. Vous êtes technicien dans les fermetures du bâtiment même avec une faible expérience dans la menuiserie. Vous êtes poseur de menuiseries/fermetures ayant déjà effectué des relevés de mesures. K par K (960 salariés, 50 000 clients par an) est l'un des spécialistes de la rénovation de menuiserie sur mesure auprès des particuliers. Enseigne de service, nous installons des gammes de produits qui allient performances techniques, acoustiques et esthétiques.",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [
        {
          "codeFormation": "22435",
          "domaineLibelle": "Menuiserie",
          "exigence": "S",
          "niveauLibelle": "CAP, BEP et équivalents"
        }
      ],
      "id": "3641113",
      "intitule": "Métreur - Blois (Dpt 41) (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 47.575866699,
        "libelle": "41 - Loir et Cher",
        "longitude": 1.381896973
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333863947?v=3B1F6B6DABF4850AA8E4E783BEB6EFE3CB4642EE&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641113"
      },
      "qualificationCode": "X",
      "romeCode": "F1108",
      "romeLibelle": "Métré de la construction",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technicien / Technicienne agricole",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:41+01:00",
      "dateCreation": "2019-11-13T12:44:41+01:00",
      "description": "Poste à responsabilité, vous êtes en charge de la maintenance du parc et du montage des machines à traire. Votre sens de l'organisation et du contact humain garantissent votre réussite à ce poste. Vos missions : Sous la responsabilité du responsable élevage vous serez en charge des missions suivantes : - Diagnostic et réparation des matériels d'élevage - Montage et installation des matériels d'élevage - Astreintes les soirs et week-end - Selon qualification (agrément « Optitraite »), réalisation des contrôles périodiques des installations - Promotion de la vente de pièces techniquesFormation attendue : Vous êtes issu d'une formation dans la mécanique / électrotechnique. Connaissances souhaitées : Des connaissances en électricité / plomberie seraient un plus Qualités : Organisé et méthodique sont des adjectifs qui vous qualifient. Vous possédez un bon relationnel. Nous recherchons une personne qui aime travailler en équipe et qui a coeur de tenir ses engagements. Expérience : Au moins 2 ans dans le dépannage et l'installation de machine à traireTerréa, est une concession agricole John Deere, disposant de 10 bases réparties sur 7 départements. Terréa appartient au groupe Agriteam (5 concessions John Deere, 28 points de service dans le Centre et l'Est de la France). Afin de renforcer notre équipe de techniciens nous recherchons un(e) : Technicien agricole « machine à traire » (H/F)",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée de 2 An(s)",
      "formations": [],
      "id": "3641112",
      "intitule": "Technicien agricole « machine à traire » (H/F)",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.889522552,
        "libelle": "55 - Meuse",
        "longitude": 5.392706871
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865836?v=D1E5CBDB5508C7689431D0C2678EF8E6C8CC177F&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641112"
      },
      "qualificationCode": "X",
      "romeCode": "A1302",
      "romeLibelle": "Contrôle et diagnostic technique en agriculture",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Hôte / Hôtesse d'accueil",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:39+01:00",
      "dateCreation": "2019-11-13T12:44:39+01:00",
      "description": "L'accueil c'est nous, l'accueil c'est vous. S'il y a bien une chose qui vous est propre, c'est votre relationnel car votre bonne humeur est contagieuse. Vos missions: -Accueil physique : Faire vivre une expérience prestige au client. Pour ce poste d'hôte et hôtesse d'accueil en entreprise, vous saurez briller de vos compétences pour notre client spécialisé dans le domaine de l'automobile. Et puisque l'accueil n'attend pas, le poste est à pouvoir URGEMMENT dans le cadre d'un CDI à temps partiel (6h50) pour UNIQUEMENT LES SAMEDIS de 10h à 12h30 et de 14h00 à 18h00. Le poste est situé au Mans(72). Si vous souhaitez révéler le FeelGoodSpecialist et l' ImpulseurDeSourires qui est en vous, postulez en ligne ou par mail à : accueil6658.Phone-regieaprofils.org Notre entreprise est handi-accueillante. Suivez nous sur Instagram, Facebook, Linkedin et Twitter De nature dynamique, flexible et polyvalent(e), vous êtes à l'écoute des besoins du client et avez un état d'esprit positif, un vrai FeelGoodSpecialist  En bon WelcomeExpert, vous avez une excellente présentation. Vous êtes un ImpulseurDeSourires. Vous souhaitez mettre à profit votre professionnalisme et votre relationnel en rejoignant l'équipe de Phone Régie N'hésitez plus, PhoneRégie n'attend plus que vous Phone Régie est une filiale du Groupe Armonia. Prestataire de services, nous intervenons dans de nombreux secteurs d'activité : luxe, audiovisuel, communication, informatique, financeAvec 1 000 clients, plus de 4 600 salariés et 120 millions d'EUR de CA, faites confiance au leader européen : l'accueil c'est un vrai métier. Si vous souhaitez développer votre esprit de service, d'équipe et de progrès autour de belles opportunités dans une entreprise résolument humaine, votre savoir-faire et votre savoir-être sont les bienvenus. Réinventons ensemble les métiers de l'accueil",
      "dureeTravailLibelleConverti": "Non renseigné",
      "entreprise": {
        "nom": "Phone Régie"
      },
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641103",
      "intitule": "Hôte et hôtesse d'accueil - LE MANS H/F",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.026702881,
        "libelle": "72 - Sarthe",
        "longitude": 0.176558003
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1333865922?v=A6E6625DC24CD8D078881A77FDB4B53B2365444F&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641103"
      },
      "qualificationCode": "X",
      "romeCode": "M1601",
      "romeLibelle": "Accueil et renseignements",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    },
    {
      "alternance": false,
      "appellationlibelle": "Technicien / Technicienne de maintenance industrielle",
      "competences": [],
      "dateActualisation": "2019-11-13T12:44:38+01:00",
      "dateCreation": "2019-11-13T12:44:38+01:00",
      "description": "Rattaché au Responsable Maintenance, vous managez et coordonnez une équipe de 20 collaborateurs Vos missions principales sont : Assurer les opérations de maintenance correctives et préventives sur les équipements de production Participer à la préparation et à la réalisation de travaux de fiabilisation et d'amélioration des équipements de production Assurer les travaux résultants de la vérification de la conformité réglementaire sur le périmètre des moyens de production Tenir à jour la documentation \"terrain\" fonction des modifications réalisées en situation Élaborer des procédures de vérification / réglage pour les cas compliqués Tracer son activité ds la GMAO pour permettre l'analyse des phénomènes perturbants (pannes récurrentes, longues,)",
      "dureeTravailLibelleConverti": "Non renseigné",
      "experienceExige": "E",
      "experienceLibelle": "Expérience exigée",
      "formations": [],
      "id": "3641101",
      "intitule": "Technicien de maintenance H/F",
      "langues": [],
      "lieuTravail": {
        "latitude": 48.580608368,
        "libelle": "91 - Essonne",
        "longitude": 2.323250055
      },
      "natureContrat": "Contrat travail",
      "nombrePostes": 1,
      "origineOffre": {
        "origine": "2",
        "partenaires": [
          {
            "logo": "https://www.pole-emploi.fr/static/img/partenaires/adzuna80.png",
            "nom": "ADZUNA",
            "url": "https://www.adzuna.fr/details/1334074869?v=972C6BB25D84D36544FE0049457C5D48AA0D507C&utm_source=polemploi&utm_medium=organic&chnlid=126"
          }
        ],
        "urlOrigine": "https://candidat.pole-emploi.fr/offres/recherche/detail/3641101"
      },
      "qualificationCode": "X",
      "romeCode": "I1304",
      "romeLibelle": "Installation et maintenance d'équipements industriels et d'exploitation",
      "typeContrat": "CDI",
      "typeContratLibelle": "Contrat à durée indéterminée"
    }
  ]
}

module.exports = datas