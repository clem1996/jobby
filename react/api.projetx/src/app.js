const express = require('express') // on va chercher express et on l'assigne à une constante
const app = express() // notre application par défaut est maintenant express
const path = require('path')
const port = 8000
var cors = require('cors')
var morgan = require('morgan')
app.use(morgan('combined'))
app.use(cors())
const uuidv1 = require('uuid/v1');
const { check, query, header, sanitize, sanitizeQuery, sanitizeParam, validationResult } = require('express-validator');

// static views
app.get('/oAuth', (req, res, next) => { res.json(uuidv1()) })


const controlBearer =   //  je check
    [
        header('Authorization').isUUID().withMessage(`l Authorization n'est pas valide`),
   


        // gestion d'erreur
        (req, res, next) => {
            // Finds the validation errors in this request and wraps them in an object with handy functions
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({ errors: errors.array() });
            }
            return next()
        }
    ]



app.get('/adverts/:id?',
    // init 
    // control
    controlBearer,
    // get data
    (req, res, next) => {
        delete require.cache[require.resolve('./mockup/adverts')]
        const adverts = require('./mockup/adverts')
        res.locals.adverts = adverts
        return next()
    },
    // filter data
    (req, res, next) => {
        if (req.query.codePostal) {


            res.locals.adverts.resultats = res.locals.adverts.resultats.filter(resultat => {
                if (resultat.lieuTravail.codePostal === req.query.codePostal)
                    return true;
                else return false
            })


        }

        return next()

    },
     // filter data
     (req, res, next) => {
        if (req.query.id) {


            res.locals.adverts.resultats = res.locals.adverts.resultats.filter(resultat => {
                if (resultat.id === req.query.id)
                    return true;
                else return false
            })


        }

        return next()

    },

    // reponse
    (req, res, next) => {
        res.json(res.locals.adverts)
    }
)




app.listen(port, () => console.log('app running on port http://api.projetx:' + port + ' ' + process.pid)) // notre application écoute en localhost(127.0.0.1) sur le port 3000
