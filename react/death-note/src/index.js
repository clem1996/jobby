import React from 'react';
import ReactDOM from 'react-dom';
// import App from './V1/App'; // V1 une personne peut se killer
// import App from './V2/App'; // V2 un dieu peut killer une personne à coté de lui

// import App from './V3/App'; // V2 un dieu peut killer des personnes à coté de lui


 import App from './V4/App'; // V4 un dieu peut killer des personnes à coté de lui (générique)


ReactDOM.render(<App />, document.getElementById('root'));
