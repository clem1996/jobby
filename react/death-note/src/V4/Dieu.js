import React, { Component } from 'react';

class Dieu extends Component {
  // lifeCycle
  constructor(props) {
    super(props)
    console.log('constructor Dieu')
    this.state = {
      content: true
    }

  }

 

  handleToggleOnCap = () => {
    this.setState({ content: !this.state.content }) // modification d'un état par setState
    // this.state.content = !this.state.content //NE JAMAIS MODIFIER LE STATE DIRECTEMENT

  }

  render() {
    console.log('render Dieu')

    let rows = this.props.inLifes.map((inLife, index) =>
      <button
        key={`inLifes_${index}`}
        onClick={(event) => this.props.handleKill(index)}
      >
        kill
      </button>
    )

    return (
      <div>
        {!!this.state.content === true ? 'content' : 'pas content'}
        <div onClick={this.handleToggleOnCap}>{this.props.casquette}</div>
        {/* 
          les proprietes sont en lecture seul read only
        */}
        {rows}

        DIEU</div>
    )
  }
}

export default Dieu;
