import React, { Component } from 'react';
import Person from './Person';
import Dieu from './Dieu'
import   '../assets/monStyle.css'
import TreeData from './TreeData'
class App extends Component {
  // lifeCycle
  constructor(props) {
    super(props)
    console.log('constructor App')

    // l'etat est propre à un composant
    this.state = {
      inLifes: [true, true, true, true]
    }
  }
  // custom handler
  handleKill = (personNumber) => {
    console.log('handleKill', personNumber)

    // mais un composant peut changer son état
    // [false, true, true]
    let result = this.state.inLifes.map((tab, index) => {
      if (index === personNumber)
        return false;
      else
        return tab;
    }
    )


    this.setState({
      inLifes: result
    })

  }

  render() {
    console.log('render App')

    let rows = this.state.inLifes.map((inLife, index) =>
      <div key={`inLifes_${index}`}>
        <Person
          // nom={"Naruto"}
          inLifes={inLife}

        />
      </div>
    )


    return (
      <div id="coucou" className="header">
        <Dieu
          handleKill={this.handleKill}
          inLifes={this.state.inLifes}
          casquette={<img width={"20"} src="https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcQGlD-XlFFiuMI0bptkmGE3NYBvf9bGNkKqiZwEsdAcfuQwu5KPgLyFWNHLkChXwhFFcQx5F5E_Dq512lGJ9EHOUsBt0n4ItiIKlS-agMIyqtuqrsybTbwv&usqp=CAE"></img>}
        >
        </Dieu>

        {rows}
<TreeData/>

      </div>
    )
  }
}

export default App;
