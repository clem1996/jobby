import React, { Component } from 'react';
import Person from './Person';
import Dieu from './Dieu'


class App extends Component {
  // lifeCycle
  constructor(props) {
    super(props)
    console.log('constructor App')

    // l'etat est propre à un composant
    this.state = {
      inLife: true
    }
  }
  // custom handler
  handleKill = () => {
    console.log('handleKill')
    // this.props.nom="nauroto" // ====> un composant ne peut pas changer lui même ces proprietes

    // mais un composant peut changer son état
    this.setState({
      inLife: false
    })
  }

  render() {
    console.log('render App')

    return (
      <div>
        <Dieu handleKill={this.handleKill}>   </Dieu>
        <Person
          nom={"Naruto"}
          inLife={this.state.inLife}
          
        />
        {/* <Person
          nom={"Luffy"}

        />
        <Person
          nom={"L"}

        /> */}
      </div>
    )
  }
}

export default App;
