import React, { Component } from 'react';
import Person from './Person';
import Dieu from './Dieu'


class App extends Component {
  // lifeCycle
  constructor(props) {
    super(props)
    console.log('constructor App')

    // l'etat est propre à un composant
    this.state = {
      inLife: [true, true, true]
    }
  }
  // custom handler
  handleKill = (personNumber) => {
    console.log('handleKill', personNumber)

    // mais un composant peut changer son état
    // [false, true, true]
    let result = this.state.inLife.map((tab, index) => {
      if (index === personNumber)
        return false;
      else
        return tab;
    }
    )
    this.setState({
      inLife:  result
    })
  }
  render() {
    console.log('render App')

    return (
      <div>
        <Dieu
          handleKill={this.handleKill}
        >
        </Dieu>
        <Person
          nom={"Naruto"}
          inLife={this.state.inLife[0]}

        />
        <Person
          nom={"Luffy"}
          inLife={this.state.inLife[1]}

        />
        <Person
          nom={"L"}
          inLife={this.state.inLife[2]}

        />
      </div>
    )
  }
}

export default App;
