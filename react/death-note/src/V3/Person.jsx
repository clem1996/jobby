import React from 'react';

class Person extends React.Component {
  // lifeCycle
  constructor(props) {
    super(props)
    console.log('constructor Person')


  }



  // lifeCycle 
  // apres le constructor
  // aprge d'etat
  // apres que l'une des proprietes de ce composant changees que ce composant chan
  render() {
    console.log('render Person')
    let inLife = !!this.props.inLife === true ? "vivant" : "death"

    return (
      <div>
        nom : {this.props.nom}

        -

          je suis {inLife}

        
      </div>
    )
  }
}

export default Person