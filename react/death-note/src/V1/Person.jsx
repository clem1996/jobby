import React from 'react';

class Person extends React.Component {
  // lifeCycle
  constructor(props) {
    super(props)
    console.log('constructor Person')

    // l'etat est propre à un composant
    this.state = {
      inLife: true
    }
  }

  // custom handler
  handleKill = () => {
    console.log('handleKill')
    // this.props.nom="nauroto" // ====> un composant ne peut pas changer lui même ces proprietes
    
    // mais un composant peut changer son état
    this.setState({
      inLife: false
    })
  }

  // lifeCycle 
  // apres le constructor
  // apres que ce composant change d'etat
  // apres que l'une des proprietes de ce composant change
  render() {
    console.log('render Person')
    let inLife = !!this.state.inLife === true ? "vivant" : "death"

    return (
      <div>
        nom : {this.props.nom}

        -
       
          je suis {inLife}

        <button onClick={this.handleKill}>kill</button>
      </div>
    )
  }
}

export default Person