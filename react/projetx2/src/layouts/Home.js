import React from 'react';
import Menu from '../views/Menu'
import User from '../views/User'
import Adverts from '../views/Adverts'
import Pubs from '../views/Pubs'
import Maps from '../views/Maps'
import Maps2 from '../views/Maps2'
import superagent from 'superagent'
class Home extends React.Component {
    constructor(props) { // le bon moment pour initialiser les valeurs par defauts
        super(props)
        console.log('constructor Home')
        // 1
        this.state = {
            resultats: []
        }
    }

    fetchAdverts = async (codePostal)=>{
        console.log("bearerbearerbearer", this.props.bearer)

        let myApiURl = 'http://api.projetx:8000/adverts'
        let reponse = await superagent
            .get(myApiURl)
            .query({ salaireMin: 15000,
                codePostal: codePostal
            })
            .set('accept', 'application/json')
            .set('Authorization', this.props.bearer)
            // .set('x-api', this.props.bearer)
        // reponse.resultats
        this.setState({
            resultats: reponse.body.resultats
        })
    }
    componentDidMount = async () => {// le bon moment pour fetcher une premiere fois
        console.log('componenDidMount Home')
        // 3
        // let myApiURl = 'https://api.emploi-store.fr/partenaire/offresdemploi/v2/offres/search'
        // let reponse = await superagent
        //     .get(myApiURl)
        //     .query({ salaireMin: 15000 })
        //     .set('accept', 'application/json')
        //     .set('Authorization', 'Bearer 8fe106f5-2161-4b1a-8741-4a3e5376c79e')
        this.fetchAdverts()

    }
    componentDidUpdate=(prevProps,prevState)=>{
        // debugger
        // console.log(prevProps)
        // if(prevProps.bearer != this.props.bearer)
        //     this.fetchAdverts()
    }
    render() {
        return (
            <div className="Home">
                <pre>
{JSON.stringify(this.props)}

                </pre>
                Home
                <table border="1" width="100%">
                    <tbody>
                        <tr >
                            <td colSpan="3">
                                <Menu />
                            </td>
                        </tr>
                        <tr style={ {verticalAlign:"top"}}>
                            <td width="500">
                                <table border="1" width="100%">
                                    <tbody>
                                        <tr><td><User /></td></tr>
                                        <tr><td><Maps2 fetchAdverts={this.fetchAdverts}/></td></tr>
                                        <tr><td><Maps /></td></tr>
                                    </tbody>
                                </table>
                            </td>
                            <td >
                                <Adverts
                                    resultats={this.state.resultats}
                                     bidon="bidon" />
                            </td>
                            <td >
                                <Pubs />
                            </td>
                        </tr>
                    </tbody>

                </table>
            </div>
        )
    }
}

export default Home;