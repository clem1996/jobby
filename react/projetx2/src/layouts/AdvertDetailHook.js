import React, { useState, useEffect } from 'react';
import Menu from '../views/Menu'
import User from '../views/User'
import Adverts from '../views/Adverts'
import Pubs from '../views/Pubs'
import Maps from '../views/Maps'
import Maps2 from '../views/Maps2'
import superagent from 'superagent'

import {
    useParams, // hook
} from 'react-router-dom'


function AdvertDetail(props) {
    const [resultat, setResultat] = useState({})
    let { id } = useParams();
    useEffect(() => {
        console.log('componenDidMount AdvertDetail')

        fetchAdverts()
    }, [])
    const fetchAdverts = async () => {
        console.log("bearerbearerbearer", props.bearer)
     

        let myApiURl = 'http://api.projetx:8000/adverts'
        let reponse = await superagent
            .get(myApiURl)
            .query({
                id: id
            })
            .set('accept', 'application/json')
            .set('Authorization', props.bearer)
        // .set('x-api', props.bearer)
        // reponse.resultats
        setResultat(reponse.body.resultats)
    }


    return (
        <div className="AdvertDetail">
            coucou jesuis en hook
            {id}
            <pre>{JSON.stringify(props)}</pre>
        </div>
    )
}

export default AdvertDetail;