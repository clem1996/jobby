import React from 'react';
import Menu from '../views/Menu'
import User from '../views/User'
import Adverts from '../views/Adverts'
import Pubs from '../views/Pubs'
import Maps from '../views/Maps'
import Maps2 from '../views/Maps2'
import superagent from 'superagent'
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import SplitButton from '../components/SplitButton';
import Checkbox from '@material-ui/core/Checkbox'
import { Favorite, FavoriteBorder } from '@material-ui/icons'
import MyTextField from './../components/MyTextField'
import Link from '@material-ui/core/Link';
import Carousel from '../components/Carousel'
import Bidon from '../components/Bidon'
import {
    withRouter, // HOC
} from 'react-router-dom'


class AdvertDetail extends React.Component {
    constructor(props) { // le bon moment pour initialiser les valeurs par defauts
        super(props)
        console.log('constructor AdvertDetail')
        // 1
        this.state = {
            resultat: {}
        }
    }

    fetchAdverts = async () => {
        console.log("bearerbearerbearer", this.props.bearer)

        let myApiURl = 'http://api.projetx:8000/adverts/'
        let reponse = await superagent
            .get(myApiURl)
            .query({
                id: this.props.match.params.id
            })
            .set('accept', 'application/json')
            .set('Authorization', this.props.bearer)
        // .set('x-api', this.props.bearer)
        // reponse.resultats
        this.setState({
            resultat: reponse.body.resultats[0]
        })
    }
    componentDidMount = async () => {// le bon moment pour fetcher une premiere fois
        console.log('componenDidMount AdvertDetail')
        this.fetchAdverts()

    }
    handleReirectALaMano = () => {
        this.props.history.push('/')
    }

    render() {

        return (
            <Grid
                container
                direction="rows"
                justify="center"
                alignItems="center"
            >
                <Grid item xs={3}>
                    <Hidden smDown>
                        <Paper >xsUp</Paper>
                    </Hidden>
                    <ButtonGroup size="small" aria-label="small outlined button group">
                        <Button>One</Button>
                        <Button>Two</Button>
                        <Button>Three</Button>
                    </ButtonGroup>

                    <div onClick={this.handleReirectALaMano} >(faux)lien vers la home la redirection est codée en javascript </div>
                    <Maps2 fetchAdverts={this.fetchAdverts} />

                </Grid>
                <Grid item xs={12} sm={6}>
                   <Bidon
                   resultat={this.state.resultat}
                   color="bleu"
                   
                   ></Bidon>
                </Grid>
                <Grid item xs={3}>
                    <Checkbox
                        icon={<FavoriteBorder />}
                        checkedIcon={<Favorite />}
                        inputProps={{ name: "coucou" }}
                    />

                    <Button

                        variant="contained"
                        color="secondary"
                        size="large"
                    >
                        Hello World
    </Button>
                    <SplitButton label="coucou" />
                    <SplitButton label="clement" />
                </Grid>
                <Grid>

                    <Carousel/>

                    <Paper>
                        {this.state.resultat.description}

                        <MyTextField></MyTextField>
                    </Paper>
                    <Link
                        component="div"
                        variant="body2"
                        onClick={() => {
                            console.info("I'm a button.");
                        }}
                    >
                        Button Link
</Link>
                </Grid>
            </Grid>

        )
    }
}

export default withRouter(AdvertDetail);