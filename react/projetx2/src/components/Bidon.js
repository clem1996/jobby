import React from 'react';
import CardAdvert from './CardAdvert'
import Icon from '@material-ui/core/Icon'

export default function Bidon(props) {

    let {color, ...rest} = props
    return (
        <div>
            {color}
            <Icon className="fa fa-plus-circle" />

            <CardAdvert
                {...rest}

            ></CardAdvert>
        </div>
    )
}


