import React from 'react';
import {

    Link,
} from 'react-router-dom'

class Advert extends React.Component {
    render() {
        return (
            <Link to={"/detail/"+this.props.id}>
                <div className="Advert">
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    {this.props.type}

                                </td>
                                <td>
                                    {this.props.intitule}

                                </td>
                            </tr>
                            <tr>
                                <td colSpan="2">
                                    {this.props.description}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </Link>
        )
    }
}

export default Advert;