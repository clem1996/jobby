import React from 'react';
import Home from './layouts/Home'
import AdvertDetail from './layouts/AdvertDetail'
import AdvertDetailHook from './layouts/AdvertDetailHook'

import superagent from 'superagent'
import {
  Router,
  Switch,
  Route,
  Link,
  Redirect,
  withRouter,
  useParams
} from 'react-router-dom'
import { createBrowserHistory } from 'history'
const hist = createBrowserHistory();

class App extends React.Component {

  state = {
    bearer: ''
  }
  componentDidMount = async () => {

    console.log('componentDidMount App')
    let myApiURl = 'http://api.projetx:8000/oAuth'
    let reponse = await superagent
      .get(myApiURl)
    // reponse.resultats
    this.setState({
      bearer: reponse.body
    })
  }
  render() {
    console.log('render App')

    if (this.state.bearer === '')
      return (
        <div>waiting token bearer</div>
      )

    return (
      <Router history={hist}>
        <Switch>
          <Route path="/detail/:id" >
            <AdvertDetail bearer={this.state.bearer} />

          </Route>
          <Route path="/detailHook/:id" >
            <AdvertDetailHook bearer={this.state.bearer} />

          </Route>
          <Route path="/" >
            <Home bearer={this.state.bearer} />

          </Route>
          <Redirect to="/"></Redirect>
          {/* <Route path="/detail" component={<AdvertDetail bearer={this.state.bearer} />} /> */}
          {/* <PrivateRoute path="/" component={Dashboard} /> */}
        </Switch>
      </Router>
    )
  }
}

export default App;
