import React from 'react';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import "leaflet/dist/leaflet.css"
import L from "leaflet";
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});


class Maps2 extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            lat: 51.505,
            lng: -0.09,
            zoom: 7,
        }
    }


    // custom handler
    handleClickOnMap = (codePostal) => {
        console.log(codePostal)
        this.props.fetchAdverts(codePostal)
    }
    render() {
        const position = [this.state.lat, this.state.lng]
        const positionLomme = [ 51.205, -0.09]


        return (
           

                <Map center={position} zoom={this.state.zoom} style={{ height: "350px" }}>
                    <TileLayer
                        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Marker 
                        position={position} 
                        onClick={(evt)=>this.handleClickOnMap("59000")}
                        title={"Lille"}
                        fullOpacity={0.4}
                        riseOnHover={true}
                        icon={
                            L.divIcon({
                              className: 'location-pin',
                              html:
                              `<img src="http://icons.iconarchive.com/icons/iconsmind/outline/256/Map-Marker-icon.png" width="25">
                              <div class="pin"></div>
                              
                                `,
                              iconSize: [30, 30],
                              iconAnchor: [18, 30]
                            })
                          }
        
                    >
                    </Marker>
                    <Marker 
                        position={positionLomme} 
                        onClick={(evt)=>this.handleClickOnMap("59790")}
                        title={"Ronchin"}
                        fullOpacity={0.4}
                        riseOnHover={true}
                        icon={
                            L.divIcon({
                              className: 'location-pin',
                              html:
                              `<img src="http://icons.iconarchive.com/icons/iconsmind/outline/256/Map-Marker-icon.png" width="25">
                              <div class="pin"></div>
                              
                                `,
                              iconSize: [30, 30],
                              iconAnchor: [18, 30]
                            })
                          }
        
                    >
                    </Marker>
                </Map>
        )
    }
}

export default Maps2;