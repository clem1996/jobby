import React from 'react';
import Nom from '../components/Nom'
import Prenom from '../components/Prenom'
import Email from '../components/Email'

class User extends React.Component {
    render() {
        return (
            <div className="User">
                User
                <Nom/>
                <Prenom/>
                <Email/>
            </div>
        )
    }
}

export default User;