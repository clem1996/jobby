import React from 'react';


class Person extends React.Component {
  render() {
    console.log('render Person', this.props)
    return (
      <div>
          Je suis une personne
          my name is : {this.props.myName}
          my last name is : {this.props.myLastName}
        </div>
    )
  }
}

export default Person