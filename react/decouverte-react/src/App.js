import React, { Fragment, Component } from 'react';
import Person from './Person';


class App extends Component {
  render() {
    console.log('render App')
    return (
      <Fragment>
        <div>
          <Person 
            myName="kevin"
            myLastName="Bocquet"
           />
        </div>
        <pre>
          <Person myName="snoop" />
        </pre>
        <Person myName="clement" />
      </Fragment>

    )
  }
}

export default App;
