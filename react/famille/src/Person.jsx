import React from 'react';

class Person extends React.Component {
  render() {
    console.log('render Person', this.props)
    return (
      <div>
        Je suis une personne
          my name is : {this.props.prenom}
        <br />>
          my last name is : {this.props.nom}
          {this.props.avatar}
      </div>
    )
  }
}

export default Person