import React from 'react';
import ReactDOM from 'react-dom';
import Famille from './Famille';
import snoop from './assets/img/snoop.jpg'

import kevin from './assets/img/kevin.webp'
import clementine from './assets/img/clementine.webp'

ReactDOM.render(
    <Famille
        nom={"bocquet"}
        prenoms={[
            {
                label: "kevin",
                avatar: <img src={kevin} width="50" />
            }, {
                label: "snoop2",
                avatar: <img src={snoop} width="50" />
            }, {
                label: "Clementine",
                avatar: <img src={clementine} width="50" />
            }
        ]}

    />
    , document.getElementById('root'));
