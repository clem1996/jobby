import React, { Fragment, Component } from 'react';
import Person from './Person';



class Famille extends Component {
  render() {
    console.log('render Famille')

    // templating en amont
    let rows = this.props.prenoms.map((prenom, index) => {
      return (
        <div key={"prenoms" + index}>
          <Person
            prenom={prenom.label}
            nom={this.props.nom}
            avatar={prenom.avatar}
          />
        </div>
      )
    })


    return (
      <Fragment>
        {rows}
      </Fragment>
    )
  }
}

export default Famille;
