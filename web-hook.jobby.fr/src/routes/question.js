const router = require("express").Router()
const { body, sanitizeBody, validationResult } = require('express-validator');
const pool = require('../utils/dbPool')

router.post('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        res.locals.errorDetail = {}
        return next();
    },
    //  je check
    [
        body('question_categoryId').isInt().withMessage('question_categoryId must be here'),
        sanitizeBody('question_categoryId').toInt(),
        body('questionLabel').isString().isLength({ max: 100 }),
        sanitizeBody('questionLabel'),
        body('questionLabel').custom(value => {
            // pattern
            let pattern = /eval\(|delete/
            if (value.match(pattern) !== null) {
                return false
            }
            return true
        }),
    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            res.locals.errorDetail = errors.array()
            return next('route')
        }
        return next()
    },
    // traitement INSERT
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        INSERT INTO jobbyDB.question ( 
            questionLabel,
            question_categoryId
        )
        VALUES 
        (?, ? )  
        `
        // binding dynamique de parametre 
        params.push(req.body.questionLabel)
        params.push(req.body.question_categoryId)

        try {
            // lors d'une insertion la bdd renvoie la clé primaire créé
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            return next('route')
        }
        res.locals.myData = response
        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)
router.post('/',
    // retour
    (req, res, next) => {
        if (res.locals.error) {
            res.status(422)
            res.json(res.locals.errorDetail)
        } else {
            res.json(res.locals.myData)
        }
        return
    }
)



router.put('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        res.locals.errorDetail = {}
        return next();
    },
    //  je check
    [
        body('questionLabel').isString().isLength({ max: 100 }),
        sanitizeBody('questionLabel').trim(),
        body('questionLabel').custom(value => {
            // pattern
            let pattern = /eval\(|delete/
            if (value.match(pattern) !== null) {
                return false
            }
            return true
        }),
        body('questionIsArchived').isBoolean(),
        sanitizeBody('questionIsArchived').toBoolean(),

        body('questionIsMandatory').isBoolean(),
        sanitizeBody('questionIsMandatory').toBoolean(),

        body('questionHelper').isString().isLength({ max: 100 }),
        sanitizeBody('questionHelper').trim(),
        body('questionHelper').custom(value => {
            // pattern
            let pattern = /eval\(|delete/
            if (value.match(pattern) !== null) {
                return false
            }
            return true
        }),

    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            res.locals.errorDetail = errors.array()
            return next('route')
        }
        return next()
    },
    // traitement UPDATE
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        UPDATE jobbyDB.question
        SET questionLabel = ?,
            questionType = ?,
            questionIsArchived = ?,
            questionIsMandatory = ?,
            questionHelper = ?,
            questionPattern = ?
        WHERE questionId = ?
        `
        // binding dynamique de parametre 
        params.push(req.body.questionLabel)
        params.push(req.body.questionType)
        params.push(req.body.questionIsArchived)
        params.push(req.body.questionIsMandatory)
        params.push(req.body.questionHelper)
        params.push(!!req.body.questionIsMandatory === true ? req.body.questionPattern : '')
        params.push(req.body.questionId)

        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {

            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.myData = response
        return next()
    },


    (req, res, next) => {

        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.put('/',
    // retour
    (req, res, next) => {
        if (res.locals.error) {
            res.status(422)
            res.json(res.locals.errorDetail)
        } else {
            res.json(res.locals.myData)
        }


        return
    }
)

router.delete('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        res.locals.errorDetail = {}
        return next();
    },


    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            res.locals.errorDetail = errors.array()
            return next('route')
        }
        return next()
    },
    // traitement DELETE des reponses associées à la question courante
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
    DELETE FROM jobbyDB.response 
    WHERE response_questionId = ?
    
    `
        // binding dynamique de parametre 
        params.push(req.body.questionId)
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }

        return next()
    },

    // traitement DELETE
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        DELETE FROM jobbyDB.question 
        WHERE questionId = ?
        `
        // binding dynamique de parametre 
        params.push(req.body.questionId)
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.myData = response
        return next()
    },


    (req, res, next) => {

        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.delete('/',
    // retour
    (req, res, next) => {
        if (res.locals.error) {
            res.status(422)
            res.json(res.locals.errorDetail)
        } else {
            res.json(res.locals.myData)
        }


        return
    }
)



module.exports = router