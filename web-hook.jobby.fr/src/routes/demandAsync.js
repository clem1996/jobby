const router = require("express").Router()
const { body, sanitizeBody, validationResult } = require('express-validator');
// const superagent = require('superagent')
const pool = require('../utils/dbPool')
const traitementService = require('./traitementService')


router.post('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        res.locals.errorDetail = {}
        return next();
    },
    //  je check
    [
        body('categoryId').isInt().withMessage('categoryId must be here'),
        sanitizeBody('categoryId').toInt(),
        body('userId').isInt().withMessage('userId must be here'),
        sanitizeBody('userId').toInt(),
        body('userMail').isEmail().withMessage('userMail must be here'),
        body('questions.*.questionId').isInt().withMessage('questionId must be int'),
        body('questions.*.value').isString().custom((value, { req }) => {
            if (value === "eval") return false
            // Indicates the success of this synchronous custom validator
            return true;
        }).withMessage('questionId can t be eval'),
        body('responses.*.questionId').isInt().withMessage('questionId must be int'),
        body('responses.*.value').isInt().withMessage('responses can t be eval'),
        sanitizeBody('responses.*.value').toInt(),
    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            res.locals.errorDetail = errors.array()
            return next('route')
        }
        return next()
    },
    // traitement INSERT demand
    async (req, res, next) => {
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        INSERT INTO jobbyDB.demand (demandCode, demandLabel, demand_userId, demand_categoryId)
        VALUES 
        (concat("demand_", uuid()), ?, ?, ?)
        `
        // binding dynamique de parametre 
        params.push(`nouvelle demande de ${req.body.userMail} (catId:${req.body.categoryId})`)
        params.push(req.body.userId)
        params.push(req.body.categoryId)
        try {
            // lors d'une insertion la bdd renvoie la clé primaire créé
            response = await pool.query(sql, params) // execute la requete
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            return next('route')
        }
        res.locals.demandId = response.insertId
        return next()
    },
    (req, res, next) => {
        let questionIds = []
        questionIds = req.body.questions.map(question => question.questionId)
        questionIds = questionIds.concat(req.body.responses.map(response => response.questionId))
        questionIds = [...new Set(questionIds)]; // dedup les clés
        questionIds = questionIds.sort()

        res.locals.questionIds = questionIds
        return next()
    },
    // traitement INSERT demandQuestion
    async (req, res, next) => {

        // WARNING INJECTION SQL
        // sql = `
        // INSERT INTO jobbyDB.demandQuestion (demandId, questionId)
        // VALUES 
        // (5,1) ;
        // INSERT INTO jobbyDB.demandQuestion (demandId, questionId)
        // VALUES 
        // (5,1) ;
        // INSERT INTO jobbyDB.demandQuestion (demandId, questionId)
        // VALUES 
        // (5,1) ;
        // INSERT INTO jobbyDB.demandQuestion (demandId, questionId)
        // VALUES 
        // (5,1) ;
        // INSERT INTO jobbyDB.demandQuestion (demandId, questionId)
        // VALUES 
        // (5,1) ;
        // INSERT INTO jobbyDB.demandQuestion (demandId, questionId)
        // VALUES 
        // (5,1) ;

        // `
        let demandQuestion = []


        await Promise.all(
            res.locals.questionIds.map(async (questionId) => {
                let sql = ''
                let response
                let params = []
                sql = `
            INSERT INTO jobbyDB.demandQuestion (demandQuestion_demandId, demandQuestion_questionId)
            VALUES (?,?)
            `
                params.push(res.locals.demandId)
                params.push(questionId)
                try {
                    // lors d'une insertion la bdd renvoie la clé primaire créé
                    response = await pool.query(sql, params) // execute la requete
                    console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
                    console.log(`question: ${questionId} est lié à la demandQuestionId : ${response.insertId}`)
                    demandQuestion.push({ demandQuestionId: response.insertId, questionId })

                } catch (error) {
                    res.locals.error = true
                    res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
                    console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
                    return next('route')
                }


            })
        )

        res.locals.demandQuestion = demandQuestion
        return next()

    },
    // // traitement INSERT demandQuestion des valeurs simples
    // async (req, res, next) => {
    //     let sql = ''
    //     let params = []
    //     let response
    //     // WARNING INJECTION SQL
    //     // sql = `
    //     INSERT INTO jobbyDB.answer(answerLabel, answer_responseId, answer_demandQuestionId) VALUES
    //         ('huisserie', null,
    //         )`




    //     sql = `
    //     INSERT INTO jobbyDB.answer(answerLabel, answer_responseId, answer_demandQuestionId) VALUES
    //         ('je suis libre ', NULL, '10')
    //         `
    //     let sep = ''
    //     res.locals.questionIds.map(questionId => {
    //         sql += `${ sep } (?,?)`
    //         // binding dynamique de parametre 
    //         params.push(res.locals.demandId)
    //         params.push(questionId)
    //         sep = ','
    //     })
    //     try {
    //         // lors d'une insertion la bdd renvoie la clé primaire créé
    //         response = await pool.query(sql, params) // execute la requete
    //     } catch (error) {
    //         res.locals.error = true
    //         res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
    //         console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
    //         return next('route')
    //     }
    //     res.locals.demandQuestion = response
    //     return next()
    // },


    (req, res, next) => {
        res.locals.myData = {
            body: req.body,
            headers: req.headers,
            questionIds: res.locals.questionIds,
            demandQuestion: res.locals.demandQuestion,
            demandQuestionIds: res.locals.demandQuestionIds
        }
        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.post('/',
    // retour
    (req, res, next) => {
        if (res.locals.error) {
            res.status(422)
            res.json(res.locals.errorDetail)
        } else {
            res.json(res.locals.myData)
        }
        return
    }
)


module.exports = router
