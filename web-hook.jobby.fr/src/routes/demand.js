const router = require("express").Router()
const { body, sanitizeBody, validationResult } = require('express-validator');
// const superagent = require('superagent')
const pool = require('../utils/dbPool')
const traitementService = require('./traitementService')


router.post('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        res.locals.errorDetail = {}
        return next();
    },
    //  je check
    [
        body('categoryId').isInt().withMessage('categoryId must be here'),
        sanitizeBody('categoryId').toInt(),
        body('userId').isInt().withMessage('userId must be here'),
        sanitizeBody('userId').toInt(),
        body('userMail').isEmail().withMessage('userMail must be here'),
        body('questions').optional().isArray(),
        sanitizeBody('questions').toArray(),
        body('questions.*.questionId').isInt().withMessage('questionId must be int'),
        body('questions.*.value').isString().custom((value, { req }) => {
            if (value === "eval") return false
            // Indicates the success of this synchronous custom validator
            return true;
        }).withMessage('questionId can t be eval'),

        body('responses').optional().isArray(),
        sanitizeBody('responses').toArray(),
        body('responses.*.questionId').isInt().withMessage('questionId must be int'),
        body('responses.*.value').isInt().withMessage('responses can t be eval'),
        sanitizeBody('responses.*.value').toInt(),
    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            res.locals.errorDetail = errors.array()
            return next('route')
        }
        return next()
    },




    // traitement INSERT demand
    async (req, res, next) => {
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        INSERT INTO jobbyDB.demand (demandCode, demandLabel, demand_userId, demand_categoryId)
        VALUES 
        (concat("demand_", uuid()), ?, ?, ?)
        `
        // binding dynamique de parametre 
        params.push(`nouvelle demande de ${req.body.userMail} (catId:${req.body.categoryId})`)
        params.push(req.body.userId)
        params.push(req.body.categoryId)
        try {
            // lors d'une insertion la bdd renvoie la clé primaire créé
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            return next('route')
        }
        res.locals.demandId = response.insertId
        return next()
    },
    (req, res, next) => {
        let questionIds = []
        questionIds = req.body.questions.map(question => question.questionId)
        questionIds = questionIds.concat(req.body.responses.map(response => response.questionId))
        questionIds = [...new Set(questionIds)]; // dedup les clés
        questionIds = questionIds.sort()
        res.locals.questionIds = questionIds
        return next()
    },
    // traitement INSERT demandQuestion
    async (req, res, next) => {
        res.locals.demandQuestion = {}

        if (res.locals.questionIds.length === 0) return next()

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        // sql = `
        // INSERT INTO jobbyDB.demandQuestion (demandId, questionId)
        // VALUES 
        // (5,1) === 100
        // ,(5,4) === 101
        // ,(5,6) === 102
        // ,(5,2)=== 103
        // ,(5,3)=== 104
        // ,(5,5)=== 105
        // `
        sql = `
        INSERT INTO jobbyDB.demandQuestion (demandQuestion_demandId, demandQuestion_questionId)
        VALUES 
        `
        let sep = ''
        res.locals.questionIds.map(questionId => {
            sql += `${sep}(?,?)`
            // binding dynamique de parametre 
            params.push(res.locals.demandId)
            params.push(questionId)
            sep = ','
        })
        try {
            // lors d'une insertion la bdd renvoie la clé primaire créé
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            return next('route')
        }
        res.locals.demandQuestion = response
        return next()
    },
    // set demandQuestionIds
    (req, res, next) => {
        if (Object.keys(res.locals.demandQuestion).length === 0) return next() // check if a struct is empty


        let demandQuestionIds = []
        let from = res.locals.demandQuestion.insertId
        let to = res.locals.demandQuestion.insertId + res.locals.demandQuestion.affectedRows
        for (let index = from; index < to; index++) {
            demandQuestionIds.push(index)
        }
        res.locals.demandQuestionIds = demandQuestionIds
        return next()
    },
    // traitement INSERT demandQuestion des valeurs simples
    async (req, res, next) => {
        res.locals.answerSimple = []
        if (res.locals.questionIds.length === 0 || req.body.questions.length === 0) return next()
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        // sql = `
        // INSERT INTO jobbyDB.answer(answerLabel , answer_demandQuestionId) VALUES
        //     ('huisserie', '1'
        //     )`
        sql = `
            INSERT INTO jobbyDB.answer(answerLabel, answer_demandQuestionId) VALUES
        `
        let sep = ''
        res.locals.questionIds.map((questionId, index) => {
            req.body.questions.map(question => {
                if (questionId === question.questionId) {

                    sql += `${sep}(?,?)`
                    params.push(question.value)
                    let answer_demandQuestionId = res.locals.demandQuestionIds[index]
                    params.push(answer_demandQuestionId)
                    sep = ','
                }

            })
        })
        try {
            // lors d'une insertion la bdd renvoie la clé primaire créé
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            return next('route')
        }
        res.locals.answerSimple = response
        return next()
    },

    // traitement INSERT demandQuestion des valeurs multiples
    async (req, res, next) => {
        res.locals.answerMultiple = []
        if (res.locals.questionIds.length === 0 || req.body.responses.length === 0) return next()
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        // sql = `
        // INSERT INTO jobbyDB.answer(answer_responseId , answer_demandQuestionId) VALUES
        //     (1, '1'), ('5','1')
        sql = `
        INSERT INTO jobbyDB.answer(answer_responseId, answer_demandQuestionId) VALUES
    `
        let sep = ''
        res.locals.questionIds.map((questionId, index) => {
            req.body.responses.map(response => {
                if (questionId === response.questionId) {
                    sql += `${sep}(?,?)`
                    params.push(response.value) // c'est ici que l'on recupere la responseId
                    let answer_demandQuestionId = res.locals.demandQuestionIds[index]
                    params.push(answer_demandQuestionId)
                    sep = ','
                }
            })
        })
        try {
            // lors d'une insertion la bdd renvoie la clé primaire créé
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            return next('route')
        }
        res.locals.answerMultiple = response
        return next()
    },

    (req, res, next) => {
        res.locals.myData = {
            body: req.body,
            headers: req.headers,
            questionIds: res.locals.questionIds,
            demandQuestion: res.locals.demandQuestion,
            demandQuestionIds: res.locals.demandQuestionIds,
            answerSimple: res.locals.answerSimple,
            answerMultiple: res.locals.answerMultiple,
            demandId: res.locals.demandId
        }
        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.post('/',
    // retour
    (req, res, next) => {
        if (res.locals.error) {
            res.status(422)
            res.json(res.locals.errorDetail)
        } else {
            res.json(res.locals.myData)
        }
        return
    }
)


module.exports = router
