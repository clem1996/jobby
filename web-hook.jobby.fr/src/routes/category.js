const router = require("express").Router()
const { body, sanitizeBody, validationResult } = require('express-validator');
// const superagent = require('superagent')
const pool = require('../utils/dbPool')
const traitementService = require('./traitementService')


router.post('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        res.locals.errorDetail = {}
        return next();
    },
    //  je check
    [
        body('categoryLabel').isString().isLength({ max: 50 }),
        sanitizeBody('categoryLabel').trim(),
        body('categoryLabel').custom(value => {
            // pattern
            let pattern = /eval\(|delete/
            if (value.match(pattern) !== null) {
                return false
            }
            return true
        })

    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            res.locals.errorDetail = errors.array()
            return next('route')
        }
        return next()
    },
    // traitement INSERT
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        INSERT INTO jobbyDB.category (categoryCode, categoryParentId, categoryLabel, categoryImageLink, categoryPopularity, categoryIsService)
        VALUES 
        (concat("category_", uuid()), 0, ?, "", 3, 1)
        `
        // binding dynamique de parametre 
        params.push(req.body.categoryLabel)

        try {
            // lors d'une insertion la bdd renvoie la clé primaire créé

            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.myData = response
        return next()
    },


    (req, res, next) => {

        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.post('/',
    // retour
    (req, res, next) => {
        if (res.locals.error) {
            res.status(422)
            res.json(res.locals.errorDetail)
        } else {
            res.json(res.locals.myData)
        }


        return
    }
)



router.put('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        res.locals.errorDetail = {}
        return next();
    },
    //  je check
    [
        body('categoryLabel').isString().isLength({ max: 50 }),
        sanitizeBody('categoryLabel').trim(),
        body('categoryLabel').custom(value => {
            // pattern
            let pattern = /eval\(|delete/
            if (value.match(pattern) !== null) {
                return false
            }
            return true
        }),
        body('categoryParentId').optional().isInt(),
        sanitizeBody('categoryParentId').toInt(),
     
        
    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            res.locals.errorDetail = errors.array()
            return next('route')
        }
        return next()
    },
    // traitement UPDATE
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        UPDATE  jobbyDB.category 
        SET categoryLabel = ?,
            categoryImageLink = ?,
            categoryIsArchived = ?,
            categoryParentId = ?
        WHERE categoryId = ?
        `
        // binding dynamique de parametre 
        params.push(req.body.categoryLabel)
        params.push(req.body.categoryImageLink)
        params.push(req.body.categoryIsArchived)
        
        params.push(req.body.categoryParentId)
        params.push(req.body.categoryId)
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            console.log(error)
            return next('route')
        }
        res.locals.myData = response
        return next()
    },
    traitementService,

    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.put('/',
    // retour
    (req, res, next) => {
        if (res.locals.error) {
            res.status(422)
            res.json(res.locals.errorDetail)
        } else {
            res.json(res.locals.myData)
        }


        return
    }
)



router.delete('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        res.locals.errorDetail = {}
        return next();
    },
    
    
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            res.locals.errorDetail = errors.array()
            return next('route')
        }
        return next()
    },

      // traitement DELETE des reponses associées aux questions associées à la catégorie 
      async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        DELETE FROM jobbyDB.response 
        WHERE response_questionId IN (

            SELECT questionId FROM jobbyDB.question where question_categoryId = ?
        )
        `
        // binding dynamique de parametre 
        params.push(req.body.categoryId)
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
 
        return next()
    },
     // traitement DELETE des questions associées 
     async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        DELETE FROM jobbyDB.question 
        WHERE question_categoryId = ?
        `
        // binding dynamique de parametre 
        params.push(req.body.categoryId)
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
 
        return next()
    },

    // traitement DELETE
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        DELETE FROM jobbyDB.category 
        WHERE categoryId = ?
        `
        // binding dynamique de parametre 
        params.push(req.body.categoryId)
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.myData = response
        return next()
    },

    traitementService,

    (req, res, next) => {

        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.delete('/',
    // retour
    (req, res, next) => {
        if (res.locals.error) {
            res.status(422)
            res.json(res.locals.errorDetail)
        } else {
            res.json(res.locals.myData)
        }


        return
    }
)



module.exports = router
