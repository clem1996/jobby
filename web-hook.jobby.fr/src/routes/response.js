const router = require("express").Router()
const { body, sanitizeBody, validationResult } = require('express-validator');
// const superagent = require('superagent')
const pool = require('../utils/dbPool')

router.post('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        res.locals.errorDetail = {}
        return next();
    },
    //  je check
    [
        body('response_questionId').isInt().withMessage('response_questionId must be here'),
        sanitizeBody('response_questionId').toInt(), 
        body('responseLabel').isString().isLength({ max: 100 }),
        sanitizeBody('responseLabel'),
        body('responseLabel').custom(value => {
            // pattern
            let pattern = /eval\(|delete/
            if (value.match(pattern) !== null) {
                return false
            }
            return true
        }),
    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            res.locals.errorDetail = errors.array()
            return next('route')
        }
        return next()
    },
    // traitement INSERT
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        INSERT INTO jobbyDB.response ( 
            responseLabel,
            response_questionId,
            responseCode
        )
        VALUES 
        (?, ? , concat("response_", uuid()))  
        `
        // binding dynamique de parametre 
        params.push(req.body.responseLabel)
        params.push(req.body.response_questionId)

        try {
            // lors d'une insertion la bdd renvoie la clé primaire créé
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.myData = response
        return next()
    },


    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.post('/',
    // retour
    (req, res, next) => {
        if (res.locals.error) {
            res.status(422)
            res.json(res.locals.errorDetail)
        } else {
            res.json(res.locals.myData)
        }


        return
    }
)


router.put('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        res.locals.errorDetail = {}
        return next();
    },
    //  je check
    [
        body('responseLabel').isString().isLength({ max: 100 }),
        sanitizeBody('responseLabel').trim(),
        body('responseLabel').custom(value => {
            // pattern
            let pattern = /eval\(|delete/
            if (value.match(pattern) !== null) {
                return false
            }
            return true
        }),
    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            res.locals.errorDetail = errors.array()
            return next('route')
        }
        return next()
    },
    // traitement UPDATE
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        UPDATE jobbyDB.response
        SET responseLabel = ?
            
        WHERE responseId = ?
        `
        // binding dynamique de parametre 
        params.push(req.body.responseLabel)
        params.push(req.body.responseId)
        
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.myData = response
        return next()
    },


    (req, res, next) => {

        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.put('/',
    // retour
    (req, res, next) => {
        if (res.locals.error) {
            res.status(422)
            res.json(res.locals.errorDetail)
        } else {
            res.json(res.locals.myData)
        }


        return
    }
)

router.delete('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        res.locals.errorDetail = {}
        return next();
    },
    
    
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            res.locals.errorDetail = errors.array()
            return next('route')
        }
        return next()
    },
    // traitement UPDATE
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        DELETE FROM jobbyDB.response 
        WHERE responseId = ?
        `
        // binding dynamique de parametre 
        params.push(req.body.responseId)
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.myData = response
        return next()
    },


    (req, res, next) => {

        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.delete('/',
    // retour
    (req, res, next) => {
        if (res.locals.error) {
            res.status(422)
            res.json(res.locals.errorDetail)
        } else {
            res.json(res.locals.myData)
        }


        return
    }
)



module.exports = router