const pool = require('../utils/dbPool')

const traitementService = [
    // traitement service 
    // 1 je recupere les categories typés service qui ont au moins un enfant
    // je transforme le resultat pour placer les category id dans un tableau (en gros on passe de tabs=[{categoryId:1},{categoryId:25}] à tabs=[1,25])
    // je les stockes dans categoryIsServiceToUpdate
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        SELECT c1.categoryId
        FROM jobbyDB.category AS c1
        WHERE c1.categoryIsService = 1
        AND  (
                SELECT count(*)
                FROM category AS c2 
                WHERE c2.categoryParentId = c1.categoryId
            )  > 0
        `
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            console.log(error)
            return next('route')
        }

        // je transforme le resultat pour placer les category id dans un tableau
        response = response.map(item => { return item.categoryId })
        res.locals.categoryIdsIsServiceToUpdate = response
        return next()
    },

    // traitement service 
    // 2 je modifie la categoryIsService des categoryIdsIsServiceToUpdate
    async (req, res, next) => {
        if (res.locals.categoryIdsIsServiceToUpdate.length === 0) return next()
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        UPDATE jobbyDB.category
        SET categoryIsService = 0
        WHERE categoryId IN (?) 
            ;
        `
        params.push(res.locals.categoryIdsIsServiceToUpdate)
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            console.log(error)
            return next('route')
        }

        return next()
    },
    // traitement service 
    // 3 je recupere les categories non  typées service qui n'ont pas d' enfant
    // je transforme le resultat pour placer les category id dans un tableau (en gros on passe de tabs=[{categoryId:1},{categoryId:25}] à tabs=[1,25])
    // je les stockes dans categoryIsServiceToUpdate
    async (req, res, next) => {

        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        SELECT c1.categoryId
        FROM jobbyDB.category AS c1
        WHERE c1.categoryIsService = 0
        AND  (
                SELECT count(*)
                FROM category AS c2 
                WHERE c2.categoryParentId = c1.categoryId
            )  = 0
        `
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            console.log(error)
            return next('route')
        }

        // je transforme le resultat pour placer les category id dans un tableau
        response = response.map(item => { return item.categoryId })
        res.locals.categoryIdsIsServiceToUpdate = response
        return next()
    },

    // traitement service 
    // 2 je modifie la categoryIsService des categoryIdsIsServiceToUpdate
    async (req, res, next) => {
        if (res.locals.categoryIdsIsServiceToUpdate.length === 0) return next()
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        UPDATE jobbyDB.category
        SET categoryIsService = 1
        WHERE categoryId IN (?) 
            ;
        `
        params.push(res.locals.categoryIdsIsServiceToUpdate)
        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            res.locals.errorDetail = { error, query: pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, '') }
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            console.log(error)
            return next('route')
        }

        return next()
    }
]

module.exports = traitementService