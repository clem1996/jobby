const express = require('express') // on va chercher express et on l'assigne à une constante
const app = express() // notre application par défaut est maintenant express
const port = 6001
var multer = require('multer');
var upload = multer();


var bodyParser = require('body-parser');
// for parsing application/json

app.use(bodyParser.json());
// for parsing application/xwww-//form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// for parsing multipart/form-data
// app.use(upload.array()); 
var cors = require('cors')

var corsOptions = {
    origin: ['http://admin.jobby.fr:4000','http://jobby.fr:3000'],
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }
  
  app.use(cors(corsOptions))
  
app.get('/sonde',(req,res)=>res.json("ok"))



// category
app.use('/category', require('./routes/category'))
app.use('/question', require('./routes/question'))
app.use('/response', require('./routes/response'))

// app.use('/demand', upload.array(),require('./routes/demand'))
app.use('/demand', require('./routes/demand'))
app.use('/user',  require('./routes/user'))

app.listen(port, () => console.log('app running on port http://web-hook.jobby.fr:'+port +' ' +process.pid)) 