const express = require('express') // on va chercher express et on l'assigne à une constante
const app = express() // notre application par défaut est maintenant express
const path = require('path')


// static views
app.use(express.static('./views'))

app.get('/category/:categoryId', (req, res) => {
    res.sendFile(path.join(__dirname, './views/category.html'))
})
app.get('/question/:question_categoryId', (req, res) => {
    res.sendFile(path.join(__dirname, './views/question.html'))
})


app.get('/response/:responseId', (req, res) => {
    res.sendFile(path.join(__dirname, './views/response.html'))
})
// static assets
app.use('/assets',express.static('./assets'))




app.listen(4000, () => console.log('app running on port http://admin.jobby.fr:4000')) // notre application écoute en localhost(127.0.0.1) sur le port 3000
