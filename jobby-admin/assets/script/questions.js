// @algo : Generate question
// 1 : I retrieve the dom node in which i'll inject my questionsTemplate
// 2 : I declare my questionsTemplate as an empty string
// 3 : I loop over my data
// 4 : For each loop over our data we retrieve all variables from our object ( destructuring )
// 5 : An intermediate template is prepared in which data is interpolated
// 6 : For each loop over our data i concatenate the intermediate template into my questionsTemplate
// 7 : I'll inject my questionsTemplate into my menu dom node

const fetchQuestions = async () => {
    const question_categoryId = location.pathname.split('/')[2]  // choix 2 d'url

    let myApiURl = ''
    myApiURl = 'http://api.jobby.fr:5000/questions'
    let reponse = await superagent
        .get(myApiURl)
        .query({ question_categoryId: question_categoryId })
    let questionsData = reponse.body
    initializeQuestions(questionsData)
    return
}

const initializeQuestions = (questionsData) => {


    const tbody = document.querySelector('#questionList')
    let questionsTemplate = ""

    questionsData.map(question => {
        let { questionId, questionLabel, questionType, question_categoryId } = question
        let questionTemplate = `
    <tr>
        <th scope="row">
            <a href="/question/${questionId}" class="rajdhani text-dark">${questionId}</a>
        </th>
        <td>
            <a href="/question/${questionId}" class="rajdhani text-dark">${questionLabel}</a>
        </td>
        <td>
            <a href="/question/${questionId}" class="rajdhani text-dark">${questionType}</a>
        </td>
    </tr>
    `
        questionsTemplate += questionTemplate
    })
    tbody.innerHTML = questionsTemplate
}

