// @algo : Generate responses
// 1 : I retrieve the dom node in which i'll inject my responsesTemplate
// 2 : I declare my responsesTemplate as an empty string
// 3 : I loop over my data
// 4 : For each loop over our data we retrieve all variables from our object ( destructuring )
// 5 : An intermediate template is prepared in which data is interpolated
// 6 : For each loop over our data i concatenate the intermediate template into my responsesTemplate
// 7 : I'll inject my responsesTemplate into my menu dom node


const tbody = document.querySelector('tbody')
let responsesTemplate = ""

const fetchResponse = async () => {
    const response_questionId = location.pathname.split('/')[2]  // choix 2 d'url

    let myApiURl = ''
    myApiURl = 'http://api.jobby.fr:5000/responses' 
    let reponse = await superagent
        .get(myApiURl)
        .query({ response_questionId: response_questionId })
    let responsesData = reponse.body
    initializeResponses(responsesData)
    return
}
fetchResponse()

const initializeResponses = (responsesData) => {
responsesData.map(response => {
    let { responseId, responseCode, responseLabel, response_questionId} = response
    let responseTemplate = `
    <tr>
        <th scope="row">
            <a href="/response/${responseId}" class="rajdhani text-dark">${responseId}</a>
        </th>
        <td>
            <a href="/response/${responseId}" class="rajdhani text-dark">${responseLabel}</a>
        </td>
        <td>
            <a href="/response/${responseId}" class="rajdhani text-dark">${responseCode}</a>
        </td>
    </tr>
    `
    responsesTemplate += responseTemplate
})
tbody.innerHTML = responsesTemplate
}