// je recupere ma donnée
const fetchQuestion = async () => {
    const questionId = location.pathname.split('/')[2]  // choix 2 d'url
    let myApiURl = 'http://api.jobby.fr:5000/editQuestion/' + questionId
    let reponse = await superagent
        .get(myApiURl)
    console.log(reponse)
    let datasQuestion = reponse.body


    // myApiURl = 'http://api.jobby.fr:5000/category/' + datasQuestion[0].question_categoryId
    // reponse = await superagent
    //     .get(myApiURl)

    //      datasCategory = reponse.body
    //      alert(datasCategory[0].categoryLabel)
    showQuestion(datasQuestion)
    displayOrNotResponseBlock()
    displayOrNotMandatoryQuestion()
    return
}
fetchQuestion()
function displayOrNotResponseBlock(){
    let questionTypeDOM= document.querySelector('#question-type')
    let currentQuestionType = questionTypeDOM.value
    let blockQuestionListDOM= document.querySelector('#blockQuestionList')

    if (currentQuestionType === "text" || currentQuestionType === "date" || currentQuestionType === "textarea") {

        blockQuestionListDOM.classList.add("hidden");
    }
    else {
        blockQuestionListDOM.classList.remove("hidden");
    }
    
}
function displayOrNotMandatoryQuestion(){
    let questionIsMandatoryDOM = document.querySelector('#questionIsMandatory')
    let currentQuestionIsMandatory = questionIsMandatoryDOM.value
    let blockTextPatternDOM = document.querySelector('#blockTextPattern')

    if (!!parseInt(currentQuestionIsMandatory) === true) {
        blockTextPatternDOM.classList.remove("hidden")
    }
    else {
        blockTextPatternDOM.classList.add("hidden")
    }
}
function showQuestion(datas) {
    let template = ``
    datas.map(data => {
        document.querySelector('#navCourante').innerHTML = data.questionLabel
        let navPrecedente = document.querySelector('#navPrecedente')
        navPrecedente.innerHTML = data.categoryLabel
        navPrecedente.href = "/category/" + data.categoryId

        let templateQuestionIsArchived = `
        <div class="form-group">
        <label for="questionIsArchived">Question archivée</label>
                <select class="form-control" id="questionIsArchived" name="questionIsArchived">
                    <option value="1" ${!!data.questionIsArchived === true ? 'selected' : ''}>Oui</option>
                    <option value="0" ${!!data.questionIsArchived === false ? 'selected' : ''}>Non</option>
                </select>
    </div>
    `
    let templateQuestionIsMandatory = `
    <div class="form-group">
    <label for="questionIsMandatory">Obligatoire</label>
            <select onChange="displayOrNotMandatoryQuestion()" class="form-control" id="questionIsMandatory" name="questionIsMandatory">
                <option value="1" ${!!data.questionIsMandatory === true ? 'selected' : ''}>Oui</option>
                <option value="0" ${!!data.questionIsMandatory === false ? 'selected' : ''}>Non</option>
            </select>
</div>
`
    let templateQuestionHelper = `
    <div class="form-group">
            <label for="question-helper">question helper</label>
            <input type="text" class="form-control" id="questionHelper" aria-describedby="questionHelper"
            placeholder="Ex: Question 1" name="questionHelper" value="${data.questionHelper}">
    </div>
    `
    let templateQuestionPattern = `
    <div id="blockTextPattern" class="form-group">
        <label for="question-pattern">question pattern</label>
        <input type="text" class="form-control" id="questionPattern" aria-describedby="questionPattern"
        placeholder="Ex: Question 1" name="questionPattern" value="${data.questionPattern}">
    </div>
    `
        template += `
        <input type="hidden" name="questionId" id="questionId" value="${data.questionId}"/>
        <input type="hidden" name="categoryId" id="categoryId" value="${data.categoryId}"/>
    <div class="form-group">
            <label for="question-label">Label de la question</label>
            <input type="text" class="form-control" id="question-label" aria-describedby="questionLabel"
            placeholder="Ex: help 1" name="questionLabel" value="${data.questionLabel}">
    </div>
    ${templateQuestionHelper}
    <div class="form-group">
                <label for="question-img">Type de la question</label>
                <select onChange="displayOrNotResponseBlock()" class="form-control" id="question-type" name="questionType">
                    <option value="" ${data.questionType===""?"selected":""}>Sélectionner</option>
                    <option value="checkbox" ${data.questionType==="checkbox"?"selected":""}>checkbox</option>
                    <option value="date" ${data.questionType==="date"?"selected":""}>date</option>
                    <option value="radio" ${data.questionType==="radio"?"selected":""}>radio</option>
                    <option value="text" ${data.questionType==="text"?"selected":""}>text</option>
                    <option value="textarea" ${data.questionType==="textarea"?"selected":""}>textarea</option>
                    <option value="select" ${data.questionType==="select"?"selected":""}>select</option>
                </select>
    </div>
    ${templateQuestionIsArchived}
    ${templateQuestionIsMandatory}
    ${templateQuestionPattern}

    <button type="button" class="btn btn-outline-primary" onClick="updateQuestion()">MODIFIER</button>
            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#modal-delete">
                SUPPRIMER
    </button>
`


    })
    let bodyDOM = document.querySelector('#formQuestion')
    bodyDOM.innerHTML = template
}    