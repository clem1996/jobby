
// @algo : Generate categories
// 1 : I retrieve the dom node in which i'll inject my categoriesTemplate
// 2 : I declare my categoriesTemplate as an empty string
// 3 : I loop over my data
// 4 : For each loop over our data we retrieve all variables from our object ( destructuring )
// 5 : An intermediate template is prepared in which data is interpolated
// 6 : For each loop over our data i concatenate the intermediate template into my categoriesTemplate
// 7 : I'll inject my categoriesTemplate into my menu dom node

const tbody = document.querySelector('tbody')
let categoriesTemplate = ""
// je recupere ma donnée
const fetchCategories = async () => {
    let myApiURl = ''
    myApiURl = 'http://api.jobby.fr:5000/categories'
    let reponse = await superagent
        .get(myApiURl)
        .query({ modeBackOffice: true })
    let categoriesData = reponse.body
    initializeCategories(categoriesData)
    return
}

const toggleChild = async (cssSelector, categoryId) => {
    // toggle des sous categories
    document.querySelector(cssSelector).classList.toggle("hidden")
    // fecth de la data
    let myApiURl = ''
    myApiURl = 'http://api.jobby.fr:5000/categories/' + categoryId
    let reponse = await superagent
        .get(myApiURl)
    let categoriesData = reponse.body
    // construction du HTML
    let categoriesTemplate = ''
    categoriesData.map(categoryData => {
        let {
            categoryLabel,
            categoryId
        } = categoryData
        let categoryTemplate = `
        <div><a href="#categoryId${categoryId}">${categoryLabel}</a></div>
        `
        categoriesTemplate += categoryTemplate
    })
    categoriesTemplate = `<td colspan="5">${categoriesTemplate}</td>`
    document.querySelector(cssSelector).innerHTML = categoriesTemplate
}

const initializeCategories = (categoriesData) => {
    let categoriesTemplate = ''
    categoriesData.map(categoryData => {
        let {
            categoryId,
            categoryCode,
            categoryLabel,
            categoryIsArchived,
            categoryCreationDateTime,
            categoryIsService,
            nbEnfant,
            categoryImageLink
        } = categoryData


        let categoryTemplate = `
    <tr id="categoryId${categoryId}">
        <th scope="row">
            <a href="/category/${categoryId}" class="rajdhani text-dark">${categoryId}</a>
        </th>
        <td>
        <img src="${categoryImageLink}" width="50"/>
            <a href="/category/${categoryId}" class="rajdhani text-dark">${categoryLabel}
            ${!!categoryIsArchived === true ? '(archivé)' : ''} 
            </a>
        </td>
        <td>
            <a href="/category/${categoryId}" class="rajdhani text-dark">${categoryCode}</a>
        </td>
        <td>${!!categoryIsService === true ? 'oui' : 'non'}
        ${nbEnfant > 0 ? `<button onClick="toggleChild('#child${categoryId}', ${categoryId})">${nbEnfant}</button>` : ''}
        </td>
        <td>${moment.tz(categoryCreationDateTime, 'europe/paris').format('DD/MM/YYYY HH:mm')}</td>
    </tr>
    <tr id="child${categoryId}" class="hidden">
    
    </tr>
    `
        categoriesTemplate += categoryTemplate
    })
    tbody.innerHTML = categoriesTemplate
}
fetchCategories()
























const filterCategories = async (event) => {

    let categoryLabel = event.target.value
    console.log(categoryLabel)
    let response = await superagent
        .post('http://api.jobby.fr:5000/categories')
        .send({ categoryLabel: categoryLabel })
        .set('Accept', 'application/json')
    let categoriesData = response.body
    initializeCategories(categoriesData)
    return
}

const initializeFilter = () => {
    document.querySelector('input[name=categoryLabel]').addEventListener('change', filterCategories)
}
initializeFilter()