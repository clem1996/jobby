const addCategory = async () => {

    let myURLAPI = 'http://web-hook.jobby.fr:6001/category'
    let categoryLabel = document.querySelector('#exampleModal input[name="categoryLabel"]').value

    let response = await superagent
        .post(myURLAPI)
        .send({ categoryLabel: categoryLabel })
        .set('Accept', 'application/json')
    let categoriesData = response.body
    // lors d'une insertion la bdd renvoie la clé primaire créé
    let insertId = categoriesData.insertId
    // navigation 
    location.href = "/category/" + insertId
}
const updateCategory = async () => {

    let myURLAPI = 'http://web-hook.jobby.fr:6001/category'
    let categoryLabel = document.querySelector('#category-label').value
    let categoryImageLink = document.querySelector('#category-img').value
    let categoryIsArchived = document.querySelector('#category-is-archived').value
    let categoryId = document.querySelector('#categoryId').value
    let categoryParentId = document.querySelector('#categoryParentId').value


    let response = await superagent
        .put(myURLAPI)
        .send({
            categoryLabel: categoryLabel,
            categoryImageLink: categoryImageLink,
            categoryIsArchived: categoryIsArchived,
            categoryId: categoryId,
            categoryParentId: categoryParentId

        })
        .set('Accept', 'application/json')
    let categoriesData = response.body

    // emmetteur avec passage de parametre (tres puissant:)
    let evt = new CustomEvent("modificationOK", {
        detail: {
            message: categoriesData.message
        }
    })
    // envoi de l'evenement sur le dom document (toujours accessible)
    document.dispatchEvent(evt)

    fetchCategory()

}

const deleteCategory = async () => {

    let myURLAPI = 'http://web-hook.jobby.fr:6001/category'

    let categoryId = document.querySelector('#categoryId').value
    let response = await superagent
        .delete(myURLAPI)
        .send({

            categoryId: categoryId

        })
        .set('Accept', 'application/json')
    let categoriesData = response.body
    // navigation 
    location.href = "/categories.html"

}
