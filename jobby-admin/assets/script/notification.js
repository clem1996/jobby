document.addEventListener('modificationOK', (event) => {
    // on fait l'action que l'on souhaite

let templateToInject = `
<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="2000">
<div class="toast-header">
    <img src="..." class="rounded mr-2" alt="...">
    <strong class="mr-auto">Bootstrap</strong>
    <small>11 mins ago</small>
    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="toast-body">
    enregistrement effectué (${event.detail.message})
</div>
</div>

`

let bodyDOM = document.querySelector("body")
bodyDOM.innerHTML=templateToInject + bodyDOM.innerHTML

    $('.toast').toast('show')
})
