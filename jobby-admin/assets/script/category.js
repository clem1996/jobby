// je recupere ma donnée
const fetchCategory = async () => {
    const categoryId = location.pathname.split('/')[2]  // choix 2 d'url

    let myApiURl = 'http://api.jobby.fr:5000/category/' + categoryId
    let reponse = await superagent
        .get(myApiURl)
    console.log(reponse)
    let datas = reponse.body


    const otherCategories = await fetchOtherCategories()
    const children = await fetchChildren()
    show(datas, otherCategories, children)
    if (!!datas[0].categoryIsService === true)
        fetchQuestions()
    return
}
const fetchOtherCategories = async () => {
    const categoryId = location.pathname.split('/')[2]  // choix 2 d'url

    let myApiURl = 'http://api.jobby.fr:5000/categories'
    let reponse = await superagent
        .get(myApiURl)
        .query({ modeBackOffice: true })
    console.log(reponse)
    let datas = reponse.body

    datas = datas.filter(item => {
        if (item.categoryId === parseInt(categoryId))
            return false
        else
            return true
    }).sort((currentCat, nextCat) => {
        return currentCat["categoryLabel"].localeCompare(nextCat.categoryLabel);
    });


    return datas
}

const fetchChildren = async () => {
    const categoryId = location.pathname.split('/')[2]  // choix 2 d'url

    let myApiURl = 'http://api.jobby.fr:5000/categories/' + categoryId
    let reponse = await superagent
        .get(myApiURl)

    // console.log(reponse)
    let datas = reponse.body



    return datas
}


// affichage
function show(datas, otherCategories, children) {
    let template = ``
    let templateChildren = ``
    children.map(child => {
        templateChildren += `
            <a href="/category/${child.categoryId}">
                <img width="50" src="${child.categoryImageLink}">${child.categoryLabel}
            </a>`
    })

    datas.map(data => {

        // document.querySelector('#navCourante').innerHTML = data.categoryLabel
        let templateCategoryIsArchived = `
            ${new Date()}
            <input type="hidden" name="categoryId" id="categoryId" value="${data.categoryId}"/>
            <div class="form-group">
            <label for="category-is-archived">Catégorie archivée</label>
            <select class="form-control" id="category-is-archived" name="categoryIsArchived">
                <option value="1" ${!!data.categoryIsArchived === true ? 'selected' : ''}>Oui</option>
                <option value="0" ${!!data.categoryIsArchived === false ? 'selected' : ''}>Non</option>
            </select>
        </div>
        `
        let templateOtherCategories = `
            <div class="form-group">
                <label for="question-img">Parent de </label>
                    <select class="form-control" id="categoryParentId" name="categoryParentId">
                    <option value="0">-- aucun parent ---</option>
        `
        otherCategories.map(otherCategory => {
            // if(data.categoryId == 13)
            // debugger
            templateOtherCategories +=
                `
                    <option value="${otherCategory.categoryId}" ${otherCategory.categoryId === data.categoryParentId ? "selected" : ""}>${otherCategory.categoryLabel}</option>
        `
        })
        templateOtherCategories +=
            `</select>
        </div>
        `


        template += `
        <div class="form-group" id="catLabel">
            <label for="category-label">Label de la catégorie</label>
            <input type="text" class="form-control" id="category-label" aria-describedby="categoryLabel"
                placeholder="Ex: Catégorie 1" name="categoryLabel" value="${data.categoryLabel}">
        </div>
        <div class="form-group" id="catSrc">
            <label for="category-img">Image de la catégorie</label>
            <input type="text" class="form-control" id="category-img"
                placeholder="Ex: https://i.imgur.com/BZedtDL.jpg" name="categoryImageLink"
                value="${data.categoryImageLink}"
                >
        </div>
        ${templateCategoryIsArchived}
        ${templateOtherCategories}
        <div class="form-group" id="catSrc">
            <a target="_blank" href="${!!data.categoryIsService === true ? `http://jobby.fr:3000/form?categoryId=${data.categoryId}` : `http://jobby.fr:3000/categories/${data.categoryId}`}">voir la fiche</a>
        </div>
        <button type="button" class="btn btn-outline-primary" onClick="updateCategory()">MODIFIER</button>
    
        <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#modal-delete">SUPPRIMER</button>
     
        
        ${!!data.categoryIsService === false ? templateChildren : `
            <h3 class="d-flex align-items-center justify-content-center h-15 poller text-center mt-5">Consultez la liste des
            questions qui y sont associées</h3>
            <p class="d-flex align-items-center justify-content-center h-15 courgette text-center">(cliquez sur une question
                pour l'éditer)</p>

            <!-- Button trigger add question modal -->
            <button type="button" class="btn btn-outline-success mb-5" data-toggle="modal" data-target="#add-question">
                Ajouter
            </button>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">LIBELLÉ</th>
                        <th scope="col">TYPE</th>
                    </tr>

                </thead>
                <tbody id="questionList">
                    <!-- generate by question script -->
                </tbody>
            </table>
            `}
    `
    })
    let bodyDOM = document.querySelector('#formCategory')
    bodyDOM.innerHTML = template
}





fetchCategory()