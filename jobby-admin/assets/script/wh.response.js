const addResponse = async () => {

    let responseLabel = document.querySelector('#add-response input[name="responseLabel"]').value
    let questionId = document.querySelector('#questionId').value
    
    let myURLAPI = 'http://web-hook.jobby.fr:6001/response'

    let response = await superagent
        .post(myURLAPI)
        .send({
            responseLabel: responseLabel,
            response_questionId: questionId
        })
        .set('Accept', 'application/json')
    let responseData = response.body
    // lors d'une insertion la bdd renvoie la clé primaire créé
    let insertId = responseData.insertId
    // navigation 
    location.href = "/response/" + insertId
}

const updateResponse = async () => {

    let myURLAPI = 'http://web-hook.jobby.fr:6001/response'
    let responseLabel = document.querySelector('#response-label').value
    let responseId = document.querySelector('#responseId').value
    let response = await superagent
        .put(myURLAPI)
        .send({
            responseLabel: responseLabel,
            responseId: responseId
        })
        .set('Accept', 'application/json')
    let responseData = response.body
    fetchResponse()
}

const deleteResponse = async () => {
    let myURLAPI = 'http://web-hook.jobby.fr:6001/response'

    let responseId = document.querySelector('#responseId').value
    let questionId = document.querySelector('#questionId').value
    let response = await superagent
        .delete(myURLAPI)
        .send({

            responseId: responseId

        })
        .set('Accept', 'application/json')
    let responseData = response.body

    // navigation 
    location.href = "http://admin.jobby.fr:4000/question/" + questionId
}