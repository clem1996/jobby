const addQuestion = async () => {

    let questionLabel = document.querySelector('#add-question input[name="questionLabel"]').value
    let categoryId = document.querySelector('#categoryId').value

    let myURLAPI = 'http://web-hook.jobby.fr:6001/question'

    let response = await superagent
        .post(myURLAPI)
        .send({
            questionLabel: questionLabel,
            question_categoryId: categoryId
        })
        .set('Accept', 'application/json')
    let questionData = response.body
    // lors d'une insertion la bdd renvoie la clé primaire créé
    let insertId = questionData.insertId
    // navigation 
    location.href = "/question/" + insertId
}


const updateQuestion = async () => {

    let myURLAPI = 'http://web-hook.jobby.fr:6001/question'
    let questionLabel = document.querySelector('#question-label').value
    let questionType = document.querySelector('#question-type').value
    let questionId = document.querySelector('#questionId').value
    let questionIsArchived = document.querySelector('#questionIsArchived').value
    let questionIsMandatory = document.querySelector('#questionIsMandatory').value
    let questionHelper = document.querySelector('#questionHelper').value
    let questionPattern = document.querySelector('#questionPattern').value

    let response = await superagent
        .put(myURLAPI)
        .send({
            questionLabel: questionLabel,
            questionType: questionType,
            questionId: questionId,
            questionIsArchived: questionIsArchived,
            questionIsMandatory,
            questionHelper,
            questionPattern
        })
        .set('Accept', 'application/json')
    // let questionData = response.body
    fetchQuestion()
}


const deleteQuestion = async () => {
    let myURLAPI = 'http://web-hook.jobby.fr:6001/question'

    let questionId = document.querySelector('#questionId').value
    let categoryId = document.querySelector('#categoryId').value
    let response = await superagent
        .delete(myURLAPI)
        .send({

            questionId: questionId

        })
        .set('Accept', 'application/json')
    let questionData = response.body

    // navigation 
    location.href = "http://admin.jobby.fr:4000/category/" + categoryId
}