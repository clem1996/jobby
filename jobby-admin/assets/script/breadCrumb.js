// je recupere ma donnée
const fetchCategoryBreadCrumb = async () => {
    const categoryId = location.pathname.split('/')[2]  // choix 2 d'url

    let myApiURl = 'http://api.jobby.fr:5000/category/' + categoryId + '/breadCrumb'
    let reponse = await superagent
        .get(myApiURl)
    let datas = reponse.body

 
    showBreadCrumb(datas)
    return
}
// affichage
function showBreadCrumb(datas) {
    let template = `
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="/categories.html">categories</a></li>
    `
    datas.map(data => {
        template += `
    <li class="breadcrumb-item"><a href="/category/${data.categoryId}">${data.categoryLabel}</a></li>`
    })
    template += `
</ol>
    `
    let bodyDOM = document.querySelector('#breadcrumb')
    bodyDOM.innerHTML = template
}

fetchCategoryBreadCrumb()