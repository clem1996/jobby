let premierChiffre = ''
let deuxiemeChiffre = ''

clickSur = (unChiifre) => {
    if (myOperateur === "") {
        // affectation
        premierChiffre = premierChiffre + unChiifre
        // une fonction peut executer d'autres fonctions
        // affichage
        displayPremierChiffre()
        displayMuOperatorButtons()
    } else {
        deuxiemeChiffre = deuxiemeChiffre + unChiifre
        displayDeuxiemeChiffre()

    }
}
displayMuOperatorButtons = () => {
    document.querySelector("section:nth-child(2)").style = "display:inline"
}
hideMuOperatorButtons = () => {
    document.querySelector("section:nth-child(2)").style = "display:none"
}

toggleMuOperatorButtons = () => {
    if (document.querySelector("section:nth-child(2)").style.display === "none")
        displayMuOperatorButtons()
    else {
        hideMuOperatorButtons()
    }

}

displayDeuxiemeChiffre = () => {
    let displayDeuxiemeChiffreDOM = document.querySelector("#deuxiemeChiffre")
    displayDeuxiemeChiffreDOM.innerHTML = deuxiemeChiffre
}

// un algo devient facilement une fonction
// declaration 
displayPremierChiffre = () => {
    // afficher  === DOM
    // on peut manipuler le dom uniquement si celui ci est chargé
    let debugPremierChiffreDOM = document.querySelector('#debugPremierChiffre')
    debugPremierChiffreDOM.innerHTML = premierChiffre

}

// algo 
// au chargement (associer) rendre clickable les chiffres et associer notre fonction clickSur

attachClickSurDesChiffres = () => {
    // recuperer les chiffres
    document.querySelector('#maDiv0').addEventListener('click', (event) => clickSur(0))
    document.querySelector('#maDiv1').addEventListener('click', (event) => clickSur(1))
    document.querySelector('#maDiv2').addEventListener('click', (event) => clickSur(2))
    document.querySelector('#maDiv3').addEventListener('click', (event) => clickSur(3))
    document.querySelector('#maDiv4').addEventListener('click', (event) => clickSur(4))
    document.querySelector('#maDiv5').addEventListener('click', (event) => clickSur(5))
    document.querySelector('#maDiv6').addEventListener('click', (event) => clickSur(6))
    document.querySelector('#maDiv7').addEventListener('click', (event) => clickSur(7))
    document.querySelector('#maDiv8').addEventListener('click', (event) => clickSur(8))
    document.querySelector('#maDiv9').addEventListener('click', (event) => clickSur(9))

}
// console.log(document.querySelector('body'))
// document.querySelector('body').addEventListener('load', attachClickSurDesChiffres)

let myOperateur = ""

choixOperateur = (operateur) => {
    // affectation
    myOperateur = operateur
    // affichage
    operateurDOM = document.querySelector("#operateur")
    operateurDOM.innerHTML = `<b>${myOperateur}</b>`

}

attachClickSurDesOperateurs = () => {
    // recuperer les operateurs
    document.querySelector('#maDivPlus').addEventListener('click', (event) => choixOperateur("+"))
    document.querySelector('#maDivMoins').addEventListener('click', (event) => choixOperateur("-"))
    document.querySelector('#maDivMultiplier').addEventListener('click', (event) => choixOperateur("*"))
    document.querySelector('#maDivDiviser').addEventListener('click', (event) => choixOperateur("/"))
}

// algo : la touche = execute le calcul du premier chiifre et du deuxieme selon l'operateur saisie
calculateur = (event) => {
    console.log('ici')

    let resultatDuCalcul = 0
    // transtypage de string en float
    let tempPremierChiffre = parseFloat(premierChiffre)
    let tempDeuxiemeChiffre = parseFloat(deuxiemeChiffre)

    if (myOperateur === "+")
        resultatDuCalcul = tempPremierChiffre + tempDeuxiemeChiffre
    else if (myOperateur === "-")
        resultatDuCalcul = tempPremierChiffre - tempDeuxiemeChiffre
    else if (myOperateur === "*")
        resultatDuCalcul = tempPremierChiffre * tempDeuxiemeChiffre
    else if (myOperateur === "/") {
        if (tempDeuxiemeChiffre === 0)
            resultatDuCalcul = 0
        else
            resultatDuCalcul = tempPremierChiffre / tempDeuxiemeChiffre

    }
    console.log(1)
    document.querySelector('#total b').innerHTML = resultatDuCalcul
    console.log(2)


}
document.querySelector('#maDivEgale').addEventListener('click', calculateur)
document.querySelector('#maDivEgale').addEventListener('click', () => {
    premierChiffre = ''
    deuxiemeChiffre = ''
    myOperateur = ''
    displayPremierChiffre()
    displayDeuxiemeChiffre()
    operateurDOM = document.querySelector("#operateur")
    operateurDOM.innerHTML = `<b>${myOperateur}</b>`
    hideMuOperatorButtons()
})

reset = (reset) => {
    myReset = reset
    resetDOM = document.querySelector('#total b').innerHTML = ""
}

attachClickSurReset = () => {
    document.querySelector('#maDivReset').addEventListener('click', (event) => reset("rst"))
    console.log("toto")
}