
/**
 * ALGO 1:
 *  Afficher les facts de chuck sur une page 1 a 1
 * 
 */

 // 1 recupere la data
const chucks = [
        {
                "id":"144512",
                "fact":"Quand Chuck Norris fait l'épreuve des araignées dans Fort Boyard, elles sortent elles-mêmes les petits papiers et lisent à voix haute."
                ,"date":"1504726103"
                ,"vote":"545"
                ,"points":"1751"
            }
        ,{"id":"126296","fact":"Chuck Norris peut faire des ronds avec une equerre","date":"1504726086","vote":"333","points":"1286"},{"id":"136623","fact":"Chuck Norris peut écrire un traitement de texte avec la souris.","date":"1504726082","vote":"204","points":"503"},{"id":"139310","fact":"Peter Parker a été mordu par une araignée, Clark Kent a été mordu par Chuck Norris","date":"1504726078","vote":"161","points":"576"},{"id":"169729","fact":"Chuck norris se souvient très bien de son futur","date":"1498479776","vote":"335","points":"1394"},{"id":"153388","fact":"Chuck Norris peut dire Schwarzkopf en verlan.","date":"1497256302","vote":"300","points":"1115"},{"id":"163647","fact":"Rien ne sert de jouer aux échecs avec Chuck Norris, il ne connait pas l'échec.","date":"1424901109","vote":"1957","points":"7367"},{"id":"145432","fact":"Chuck Norris ne prends jamais de laxatif; personne ne fait chier Chuck Norris !","date":"1424347568","vote":"1714","points":"6774"},{"id":"165934","fact":"Le papier WC de Chuck Norris n'a pas de fin. Chuck Norris n'est jamais au bout du rouleau.","date":"1424347495","vote":"1533","points":"5984"},{"id":"134435","fact":"Chuck Norris a gagné la guerre du Golf, en 18 trous.","date":"1424347465","vote":"1542","points":"5748"}] 

// debugger
let template = ""
// 2 pour chaque ligne de data
for(let index=0; index < chucks.length; index=index+1){
    value = chucks[index]
    // 2.1 on extrait les fact
    let fact = value.fact
    let vote = value.vote
    // 2.2 on construit le template avec le fact
    template = template + "<li>" + fact + " " +vote +"</li>"
}
template = "<ul>"+ template +"</ul>"
console.log(template)

// Affiche dans le DOM
let body = document.querySelector('body')

body.innerHTML = template