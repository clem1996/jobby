

// recupere la data

const ecoles = [
    {
        "code": "ok",
        "status": 200,
        "message": "",
        "level": "",
        "data": [
            {
                "establishmentId": 27,
                "establishmentFullLabel": "Aumônerie ",
                "establishmentLabel": "Aumônerie ",
                "establishmentLogo": "https://univ.events/static/media/header-logo-catho.0d970fd1.png"
            },
            {
                "establishmentId": 17,
                "establishmentFullLabel": "DMG",
                "establishmentLabel": "DMG",
                "establishmentLogo": "https://univ.events/static/media/header-logo-catho.0d970fd1.png"
            },
            {
                "establishmentId": 11,
                "establishmentFullLabel": "EDHEC",
                "establishmentLabel": "EDHEC",
                "establishmentLogo": "https://univ-catholille.events/img/logo/1/crop/logoEdhecBusinessSchool.png"
            },
            {
                "establishmentId": 24,
                "establishmentFullLabel": "ESJ Paris Grand Lille",
                "establishmentLabel": "ESJ Paris Grand Lille",
                "establishmentLogo": "https://univ.events/img/1/establishment/logoESJParisGrandLille.png"
            },
            {
                "establishmentId": 14,
                "establishmentFullLabel": "ESPAS",
                "establishmentLabel": "ESPAS",
                "establishmentLogo": "https://univ-catholille.events/img/logo/1/crop/logoEspas.png"
            },
            {
                "establishmentId": 12,
                "establishmentFullLabel": "ESPOL",
                "establishmentLabel": "ESPOL",
                "establishmentLogo": "https://univ-catholille.events/img/logo/1/50/logoESPOL-50x50.png"
            },
            {
                "establishmentId": 15,
                "establishmentFullLabel": "ESTICE",
                "establishmentLabel": "ESTICE",
                "establishmentLogo": "https://univ-catholille.events/img/logo/1/crop/logoEstice.png"
            },
            {
                "establishmentId": 25,
                "establishmentFullLabel": "Ecole 360",
                "establishmentLabel": "Ecole 360",
                "establishmentLogo": "https://univ.events/img/1/establishment/logoEcole360.png"
            },
            {
                "establishmentId": 3,
                "establishmentFullLabel": "FD",
                "establishmentLabel": "FD",
                "establishmentLogo": "https://univ.events/img/1/establishment/logoFDD.jpg"
            },
            {
                "establishmentId": 2,
                "establishmentFullLabel": "FGES",
                "establishmentLabel": "FGES",
                "establishmentLogo": "https://univ-catholille.events/img/logo/1/crop/logoFGES.png"
            },
            {
                "establishmentId": 16,
                "establishmentFullLabel": "FLSH",
                "establishmentLabel": "FLSH",
                "establishmentLogo": "https://univ.events/img/1/establishment/logoFLSH.jpg"
            },
            {
                "establishmentId": 1,
                "establishmentFullLabel": "FMM",
                "establishmentLabel": "FMM",
                "establishmentLogo": "https://univ-catholille.events/img/logo/1/50/logoFMM-50x50.png"
            },
            {
                "establishmentId": 18,
                "establishmentFullLabel": "FT",
                "establishmentLabel": "FT",
                "establishmentLogo": "https://univ.events/img/1/establishment/logoFT.jpg"
            },
            {
                "establishmentId": 7,
                "establishmentFullLabel": "HEI",
                "establishmentLabel": "HEI",
                "establishmentLogo": "https://univ-catholille.events/img/logo/1/50/logoYncreaBMHEI-50x50.png"
            },
            {
                "establishmentId": 20,
                "establishmentFullLabel": "ICAM",
                "establishmentLabel": "ICAM",
                "establishmentLogo": "https://univ.events/img/1/establishment/logoICAM.png"
            },
            {
                "establishmentId": 9,
                "establishmentFullLabel": "IESEG",
                "establishmentLabel": "IESEG",
                "establishmentLogo": "https://univ-catholille.events/img/logo/1/crop/logoIESEG.png"
            },
            {
                "establishmentId": 23,
                "establishmentFullLabel": "IFP",
                "establishmentLabel": "IFP",
                "establishmentLogo": "https://univ.events/img/1/establishment/logoIFP.jpg"
            },
            {
                "establishmentId": 22,
                "establishmentFullLabel": "IKPO",
                "establishmentLabel": "IKPO",
                "establishmentLogo": "https://univ.events/img/1/establishment/logoIKPO.jpg"
            },
            {
                "establishmentId": 8,
                "establishmentFullLabel": "ISA",
                "establishmentLabel": "ISA",
                "establishmentLogo": "https://univ-catholille.events/img/logo/1/crop/logoISA.png"
            },
            {
                "establishmentId": 10,
                "establishmentFullLabel": "ISEN",
                "establishmentLabel": "ISEN",
                "establishmentLogo": "https://univ-catholille.events/img/logo/1/crop/logoISENLille.png"
            },
            {
                "establishmentId": 13,
                "establishmentFullLabel": "ISL",
                "establishmentLabel": "ISL",
                "establishmentLogo": "https://www.institutsociallille.fr/wp-content/uploads/2019/03/th-150x150.jpg"
            },
            {
                "establishmentId": 6,
                "establishmentFullLabel": "ISTC",
                "establishmentLabel": "ISTC",
                "establishmentLogo": "https://univ-catholille.events/img/logo/1/crop/logoISTC.png"
            },
            {
                "establishmentId": 21,
                "establishmentFullLabel": "IfSanté",
                "establishmentLabel": "IfSanté",
                "establishmentLogo": "https://univ.events/img/1/establishment/logoIFSante.png"
            },
            {
                "establishmentId": 19,
                "establishmentFullLabel": "Pôle IIID",
                "establishmentLabel": "Pôle IIID",
                "establishmentLogo": "https://univ.events/img/1/establishment/logoP3D.jpg"
            },
            {
                "establishmentId": 26,
                "establishmentFullLabel": "Université Catholique de Lille – associations non-rattachées à un seul établissement",
                "establishmentLabel": "Université Catholique de Lille – associations non-rattachées à un seul établissement",
                "establishmentLogo": "https://univ.events/static/media/header-logo-catho.0d970fd1.png"
            }
        ],
        "showMessage": false,
        "showAction": false,
        "action": null,
        "debug": null,
        "details": {
            "records": 25,
            "maxRecords": 25,
            "maxFilterRecords": 25,
            "cache": false,
            "nextOffset": null,
            "page": null,
            "previousOffset": null,
            "error": null
        },
        "extraData": {
            "locationEstablishment": [
                {
                    "establishmentId": 1,
                    "locationId": 20
                },
                {
                    "establishmentId": 2,
                    "locationId": 33
                },
                {
                    "establishmentId": 3,
                    "locationId": 33
                },
                {
                    "establishmentId": 4,
                    "locationId": 47
                },
                {
                    "establishmentId": 5,
                    "locationId": 55
                },
                {
                    "establishmentId": 6,
                    "locationId": 72
                },
                {
                    "establishmentId": 7,
                    "locationId": 76
                },
                {
                    "establishmentId": 8,
                    "locationId": 80
                },
                {
                    "establishmentId": 9,
                    "locationId": 84
                },
                {
                    "establishmentId": 10,
                    "locationId": 88
                },
                {
                    "establishmentId": 11,
                    "locationId": 92
                },
                {
                    "establishmentId": 12,
                    "locationId": 72
                },
                {
                    "establishmentId": 13,
                    "locationId": 72
                },
                {
                    "establishmentId": 14,
                    "locationId": 72
                },
                {
                    "establishmentId": 15,
                    "locationId": 72
                }
            ]
        }
    }
]
let string = ``
let something = false;
let myStyle;


for (let index = 0; index < ecoles[0].data.length; index = index + 1) {
    value = ecoles[0].data[index]
    let establishmentFullLabel = value.establishmentFullLabel
    let establishmentLogo = value.establishmentLogo
    let establishmentLabel = value.establishmentLabel

    if(something === true) {
        myStyle = "imgOne"
    }
    else {
        myStyle = "imgTwo"
    }
    let odd = index % 2 // calcul impair ou non
    let oddClass = ""
    if(odd === 1){
        oddClass ="odd"
    }


    string = string + `
        <li class=${oddClass}> ${establishmentFullLabel} 
            <img 
                src="${establishmentLogo}" 
                alt="logo du l'établissement ${establishmentLabel}" 
                class="${myStyle}">
        </li>
    `
}
// string = "<ul>" + string + "</ul>"
string = `<ul>${string}</ul>`
console.log(string)

let body = document.querySelector('body')

body.innerHTML = string

