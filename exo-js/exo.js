// // déclaration d'une constante de nom toto, de type string et de valeur (1)

// const toto = "1"

// // affichage dans la console de la valeur de toto

// console.log(toto)

// // déclaration d'un variable de nom tata, de type number et de valeur 2019

// let tata = 2019
// console.log(tata)

// // je réassigne la valeur de tata par une string (je suis tata)

// tata = "je suis tata"
// console.log(tata)


// filter
// const array = [18, 56, 72, 24, 25, 32]

// const filtered = array.filter( age => age <= 25)
// console.log(filtered)

// const array = [1, 1, 1, 1, 1, 1]
const array = ["toto", "toto", "toto", "toto", "toto", "toto"]

// const reducer = (accumulateur, currentValue) => {
//     return accumulateur + currentValue
// }
// console.log (array.reduce(reducer))


// const posts = [
//     {
//         title: 'Post one',
//         content: 'This is post one'
//     },
//     {
//         title: 'Post two',
//         content: 'This is post two '
//     }
// ]
// const getPosts = () => {
//     setTimeout(
//         () => {
//             let post = posts.map(post => {
//                 console.log(post.title, post.content)
//             })
//         }, 1000
//     )
// }
// const createPost = (post) => {
//     setTimeout(
//         () => {
//             posts.push(post)
//         }, 2000
//     )
// }
// getPosts()
// createPost({ title: 'post threee', content: 'this is post three' })

// callback solution
// const posts = [
//     {
//         title: 'Post one',
//         content: 'This is post one'
//     },
//     {
//         title: 'Post two',
//         content: 'This is post two '
//     }
// ]

// const getPosts = () => {
//     setTimeout(
//         () => {
//             let post = posts.map(post => {
//                 console.log(post.title)
//             })
//         }, 2000
//     )
// }

// const createPost = (post, callback) => {
//     setTimeout(
//         () => {
//             posts.push(post)
//             callback()
//         }, 4000
//     )
// }
// createPost(
//     {
//         title: 'Post three',
//         content: 'This is post three'
//     },
//     getPosts
// )

// const posts = [
//     {
//         title: 'Post one',
//         content: 'This is post one'
//     },
//     {
//         title: 'Post two',
//         content: 'This is post two '
//     }
// ]
// promises solution
// let output = ''
// const getPosts = () => {
//     setTimeout(
//         () => {
//             let post = posts.map(post => {
//                 console.log(post.title)
//             })
//         }, 2000
//     )
// }
// const createPost = (post) => {
//     return new Promise((resolve, reject) => {
//         setTimeout(
//             () => {
//                 posts.push(post)
//                 const err = false
//                 if (err) {
//                     reject('Error: something went wrong')
//                 }
//                 else {
//                     resolve()
//                 }
//             }, 4000
//         )
//     })
// }
// createPost(
//     {
//         title: 'Post three',
//         content: 'This is post three'
//     }
// ).then(getPosts).catch(err => console.log(err))

// const posts = [
//     {
//         title: 'Post one',
//         content: 'This is post one'
//     },
//     {
//         title: 'Post two',
//         content: 'This is post two '
//     }
// ]
// // async await solution
// let output = ''
// const getPosts = () => {
//     setTimeout(
//         () => {
//             let post = posts.map(post => {
//                 console.log(post.title)
//             })
//         }, 2000
//     )
// }
// const createPost = (post) => {
//     return new Promise((resolve, reject) => {
//         setTimeout(
//             () => {
//                 posts.push(post)
//                 const err = false
//                 if (err) {
//                     reject('Error: something went wrong')
//                 }
//                 else {
//                     resolve()
//                 }
//             }, 4000
//         )
//     })
// }

// const init = async () => {
//     await createPost(
//         {
//             title: 'Post three',
//             content: 'This is post three'
//         }
//     )
//     getPosts()
// }
// init()


// promise fetching
//  fetch("https://api.chucknorris.io/jokes/random")
// .then(response => response.json())
// .then(response => console.log(JSON.stringify(response)))
// .catch(error => alert("Erreur : " + error));


// async await fetching
let body = document.querySelector('body')
let btn = document.querySelector('button')


const getJoke = async () => {
    try {
        let res = await fetch('https://api.chucknorris.io/jokes/random')
        let data = await res.json()
        // console.log(res)
        console.log(data.value)
    }
    catch (error) {
        console.log(error)
    }
}

btn.addEventListener('click', getJoke )

// https://icanhazdadjoke.com

// const getAnotherJoke = async () => {
//     try {
//         let res = await fetch('https://jsonplaceholder.typicode.com/todos/1')
//         let data = await res.json()
//         console.log(res)
//         console.log(data)
//     }
//     catch (error) {
//         console.log(error)
//     }
// }

// getAnotherJoke()
