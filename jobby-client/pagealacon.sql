-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=
'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,
ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema jobbyDB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema jobbyDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `jobbyDB` ;
USE `jobbyDB` ;

-- -----------------------------------------------------
-- Table `jobbyDB`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`address` (
  `addressId` INT NOT NULL AUTO_INCREMENT,
  `addressCode` TINYTEXT NULL,
  `addressStreetNumber` VARCHAR(10) NULL,
  `addressStreetLabel` TINYTEXT NULL,
  `addressZipCode` INT NULL,
  `addressCityLabel` VARCHAR(100) NULL,
  `addressRegionLabel` VARCHAR(30) NULL,
  `addressDepartmentLabel` VARCHAR(30) NULL,
  `address_userId` INT NULL,
  `address_proId` INT NULL,
  `addressCreationDateTime` DATETIME NULL DEFAULT now(),
  `addressIsArchived` TINYINT(1) NULL,
  `addressIsArchivedDateTime` DATETIME NULL,
  PRIMARY KEY (`addressId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`review`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`review` (
  `reviewId` INT NOT NULL AUTO_INCREMENT,
  `reviewCode` TINYTEXT NULL,
  `reviewLabel` VARCHAR(30) NULL,
  `reviewContent` VARCHAR(45) NULL,
  `review_userId` INT NULL,
  `review_proId` INT NULL,
  `reviewCreationDateTime` DATETIME NULL DEFAULT now(),
  `reviewIsArchived` TINYINT(1) NULL,
  `reviewIsArchivedDateTime` DATETIME NULL,
  PRIMARY KEY (`reviewId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`demandQuestion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`demandQuestion` (
  `demandQuestionId` INT NOT NULL,
  `demandQuestion_demandId` INT NULL,
  `demandQuestion_questionId` INT NULL,
  PRIMARY KEY (`demandQuestionId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`demand`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`demand` (
  `demandId` INT NOT NULL AUTO_INCREMENT,
  `demandCode` TINYTEXT NULL,
  `demandLabel` TINYTEXT NULL,
  `demandCreationDateTime` DATETIME NULL DEFAULT now(),
  `demandIsArchived` TINYINT(1) NULL,
  `demandIsArchivedDateTime` DATETIME NULL,
  `demand_userId` INT NULL,
  `demand_categoryId` INT NULL,
  PRIMARY KEY (`demandId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`user` (
  `userId` INT NOT NULL AUTO_INCREMENT,
  `userCode` TINYTEXT NULL,
  `userFirstname` VARCHAR(30) NULL,
  `userLastname` VARCHAR(30) NULL,
  `userMail` VARCHAR(100) NULL,
  `userMobileNumber` INT NULL,
  `userAge` INT NULL,
  `userPassword` VARCHAR(30) NULL,
  `userCreationDateTime` DATETIME NULL DEFAULT now(),
  `userIsArchived` TINYINT(1) NULL,
  `userIsArchivedDateTime` DATETIME NULL,
  PRIMARY KEY (`userId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`answer` (
  `answerId` INT NOT NULL AUTO_INCREMENT,
  `answerCode` TINYTEXT NULL,
  `answerLabel` VARCHAR(30) NULL,
  `answerIsArchived` TINYINT(1) NULL,
  `answerIsArchivedDateTime` DATETIME NULL,
  `answer_responseId` INT NULL,
  PRIMARY KEY (`answerId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`response`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`response` (
  `responseId` INT NOT NULL AUTO_INCREMENT,
  `responseCode` TINYTEXT NULL,
  `responseLabel` VARCHAR(60) NULL,
  `responseOrder` INT NULL,
  `responseCreationDateTime` DATETIME NULL DEFAULT now(),
  `responseIsArchived` TINYINT(1) NULL,
  `responseIsArchivedDateTime` DATETIME NULL,
  `response_questionId` INT NULL,
  PRIMARY KEY (`responseId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`proProfession`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`proProfession` (
  `proProfessionId` INT NOT NULL,
  `proProfession_proId` INT NULL,
  `proProfession_professionId` INT NULL,
  PRIMARY KEY (`proProfessionId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`pro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`pro` (
  `proId` INT NOT NULL AUTO_INCREMENT,
  `proCode` TINYTEXT NULL,
  `proLabel` VARCHAR(30) NULL,
  `proFirstname` VARCHAR(30) NULL,
  `proLastname` VARCHAR(30) NULL,
  `proMail` VARCHAR(100) NULL,
  `proMobileNumber` INT NULL,
  `proPassword` VARCHAR(30) NULL,
  `proAge` VARCHAR(45) NULL,
  `proCreationDateTime` DATETIME NULL DEFAULT now(),
  `proIsArchived` TINYINT(1) NULL,
  `proIsArchivedDateTime` DATETIME NULL,
  PRIMARY KEY (`proId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`profession`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`profession` (
  `professionId` INT NOT NULL AUTO_INCREMENT,
  `professionCode` TINYTEXT NULL,
  `professionLabel` VARCHAR(30) NULL,
  `professionCreationDateTime` DATETIME NULL DEFAULT now(),
  `professionIsArchived` TINYINT(1) NULL,
  `professionIsArchivedDateTime` DATETIME NULL,
  PRIMARY KEY (`professionId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`question` (
  `questionId` INT NOT NULL AUTO_INCREMENT,
  `questionCode` TINYTEXT NULL,
  `questionLabel` VARCHAR(100) NULL,
  `questionOrder` INT NULL,
  `questionType` VARCHAR(30) NULL,
  `questionCreationDateTime` DATETIME NULL DEFAULT now(),
  `questionIsArchived` TINYINT(1) NULL,
  `questionIsArchivedDateTime` DATETIME NULL,
  `question_categoryId` INT NULL,
  PRIMARY KEY (`questionId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`category` (
  `categoryId` INT NOT NULL AUTO_INCREMENT,
  `categoryParentId` INT NULL,
  `categoryCode` TINYTEXT NULL,
  `categoryLabel` VARCHAR(30) NULL,
  `categoryImageLink` TINYTEXT NULL,
  `categoryCreationDateTime` DATETIME NULL DEFAULT now(),
  `categoryIsArchived` TINYINT(1) NULL,
  `categoryIsArchivedDateTime` DATETIME NULL,
  `categoryPopularity` INT NULL,
  `categoryIsService` TINYINT(1) NULL,
  PRIMARY KEY (`categoryId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`menu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`menu` (
  `menuId` INT NOT NULL AUTO_INCREMENT,
  `menuCode` TINYTEXT NULL,
  `menuItemLabel` VARCHAR(20) NULL,
  `menuItemLinkTo` TINYTEXT NULL,
  PRIMARY KEY (`menuId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `jobbyDB`.`footer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jobbyDB`.`footer` (
  `footerId` INT NOT NULL AUTO_INCREMENT,
  `footerCode` TINYTEXT NULL,
  `footerItemLabel` VARCHAR(30) NULL,
  PRIMARY KEY (`footerId`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;