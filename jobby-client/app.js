const express = require('express') // on va chercher express et on l'assigne à une constante
const app = express() // notre application par défaut est maintenant express

const path = require('path')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))


// // static views
// app.use(express.static('./views'))
// static assets
app.use('/assets', express.static('./assets'))



app.get('/', (req, res) => {
    // console.log(req)
    //   console.log(req.path)
    // console.log(__dirname)
    // res.send('<h1>ca marche</h1>')*
    res.sendFile(path.join(__dirname, './views/index.html'))
    // res.end()
})


app.get('/categories', (req, res) => {
    res.sendFile(path.join(__dirname, './views/categories.html'))
})
app.get('/categories/:categoryParentId', (req, res) => {
    res.sendFile(path.join(__dirname, './views/categories.html'))
})

app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname, './views/login.html'))
})

app.get('/about', (req, res) => {
    res.sendFile(path.join(__dirname, './views/about.html'))
})

app.get('/partners', (req, res) => {
    res.sendFile(path.join(__dirname, './views/partners.html'))
})

app.get('/contact', (req, res) => {
    res.sendFile(path.join(__dirname, './views/contact.html'))
})

app.get('/404', (req, res) => {
    res.sendFile(path.join(__dirname, './views/404.html'))
})

app.get('/form', (req, res) => {
    res.sendFile(path.join(__dirname, './views/form.html'))
})

app.get('/form2', (req, res) => {
    res.sendFile(path.join(__dirname, './views/form2.html'))
})

app.get('/questionForm', (req, res) => {
    // res.sendFile(path.join(__dirname, './views/questionForm.html'))
    // res.redirect('/categories')
    // res.end()
    console.log(req.query)
})
app.post('/questionForm', (req, res) => {
    // res.sendFile(path.join(__dirname, './views/questionForm.html'))
    // res.redirect('/categories')
    // res.end()
    console.log(req.query)
    console.log(req.body)
    res.json(req.body)
})

// app.get('/categories', (req, res) => {
//     // res.sendFile(path.join(__dirname, './views/questionForm.html'))
//     // res.redirect('/categories')
//     // res.end()
//     console.log(req.query)
//     console.log(req.body)
//     console.log(req.params)
// })

// app.get('/categories/:categoryId/questions/:questionId', (req, res) => {
//     // res.sendFile(path.join(__dirname, './views/questionForm.html'))
//     // res.redirect('/categories')
//     // res.end()
//     console.log(req.query)
//     console.log(req.body)
//     console.log(req.params)
// })



app.listen(3000, () => console.log('app running on port http://jobby.fr:3000')) // notre application écoute en localhost(127.0.0.1) sur le port 3000
