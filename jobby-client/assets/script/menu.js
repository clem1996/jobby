// const menuData = [
//     {
//         label: "Nos offres",
//         link: "/categories",
//     },
//     {
//         label: "Connexion",
//         link: "/login",
//     },
//     {
//         label: "À propos",
//         link: "/about",
//     },
//     {
//         label: "Contact",
//         link: "/contact",
//     },
//     {
//         label: "Nos partenaires",
//         link: "/partners",
//     }
// ]


const initializeMenu = (menuData) => {
    // @algo : Generate menu
    // 1 : I retrieve the dom node in which i'll inject my finalTemplate
    // 2 : I declare my finalTemplate as an empty string
    // 3 : I loop over my data
    // 4 : For each loop over our data we retrieve all variables from our object ( destructuring )
    // 5 : An intermediate template is prepared in which data is interpolated
    // 6 : For each loop over our data i concatenate the intermediate template into my finalTemplate
    // 7 : I'll inject my finalTemplate into my menu dom node

    const menu = document.querySelector("#menu")
    let menuTemplate = ""
    const menuItems = menuData.forEach((menuItem) => {
        let { label, link, menuTarget, menuSpecialActionOnClick } = menuItem
        let menuItemTemplate = ''
        if (link === '/login') {
            if (localStorage.isLoggedIn === "true") {
            }
            else {
                menuItemTemplate = `<a href="${link}" ${menuTarget === '' ? '' : `target=${menuTarget}`} class="menu-item">${label}</a>`
            }
        }
        else if (menuSpecialActionOnClick === 'logout') {
            if (localStorage.isLoggedIn === "true") {
                menuItemTemplate = `<a href="#" onClick="logout()" class="menu-item">${label}</a>`

                menuItemTemplate += `<div>${localStorage.userMail}</div>`
            }

        } else {
            menuItemTemplate = `<a href="${link}" ${menuTarget === '' ? '' : `target=${menuTarget}`} class="menu-item">${label}</a>`
        }
        menuTemplate += menuItemTemplate

    })
    menu.innerHTML = menuTemplate
    // @algo : toggleMenu
    // 1 : I get the dom node on which I have to listen to an event
    // 2 : I prepare a function which purpose is to toggle between menu classes
    // 3 : If my menu dom node classList contains menu-close
    // 4 : I replace the menu-close class by the menu-open class
    //  5 : Else : I replace the menu-open class by the menu-close class
    //  6 : On my menuIcon dom node i listen to a click event which triggers my toggleMenu function
    const menuIcon = document.querySelector("#menu-icon")
    let toggleMenu = () => {
        if (menu.classList.contains('menu-close')) {
            menu.classList.replace('menu-close', 'menu-open')
        }
        else {
            menu.classList.replace('menu-open', 'menu-close')
        }

    }


    menuIcon.addEventListener('click', toggleMenu)

}
// je recupere ma donnée
const fetchMenu2 = async () => {
    // let reponse = await superagent
    //     .get('http://api.jobby.fr:5000/menuGet?lang='+localStorage.lang)
    let reponse = await superagent
        .post('http://api.jobby.fr:5000/menuPost')
        .send({ lang: localStorage.lang })
        .set('Accept', 'application/json')


    let menuData = reponse.body

    initializeMenu(menuData)
    return
}


fetchMenu2()