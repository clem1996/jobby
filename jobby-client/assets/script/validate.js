const newUsernameInput = document.querySelector('#newUsername')
const newUsermailInput = document.querySelector('#newUsermail')
const newUserpasswordInput = document.querySelector('#newUserpassword')
const usernameInput = document.querySelector('#username')
const userpasswordInput = document.querySelector('#userpassword')
const signup = document.querySelector('#signup')
const signin = document.querySelector('#signin')



const usernamePattern = /[A-Za-z]{3,15}/
const usermailPattern = /(?:[a-z0-9!#$%&'*+/=?^_{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
const userPasswordPattern = /[A-Za-z0-9]{3,15}/
const addUser = async () => {
    let userPassword = document.querySelector('#newUserpassword').value
    let userMail = document.querySelector('#newUsermail').value

    if (userPassword.length > 0 && userMail.length > 0) {
        let response

        try {
            response = await superagent
                .post('http://web-hook.jobby.fr:6001/user')
                .send({ userMail, userPassword })
                .set('Accept', 'application/json')
            let status = response.status
            if (status === 200) { // ca se passe bien
                localStorage.setItem('isLoggedIn', true)
                localStorage.setItem('userMail', userMail)
                let userData = response.body

                // lors d'une insertion la bdd renvoie la clé primaire créé
                let insertId = userData.insertId
                localStorage.setItem('userId', insertId)
                location.href = sessionStorage.redirectToWhenLoggedIn
            } else {
                alert(`erreur lors de l'enregisrement`)
            }
        }
        catch (error) {
            console.log(error)
            alert('erreur reseau')
        }

    }

    return
}
const checkSignupForm = (event) => {

    // const newUsernameInputValue = event.target[0].value
    const newUsermailInputValue = document.querySelector('#newUsermail').value
    const newUserpasswordInputValue = document.querySelector('#newUserpassword').value

    // console.log(event)
    // if (!newUsernameInputValue.match(usernamePattern)) {
    //     alert('votre nom doit contenit entre 3 et 15 caractères')
    //     event.preventDefault()
    // }
    if (!newUsermailInputValue.match(usermailPattern)) {
        alert('votre email n\'est pas un email valide')
        event.preventDefault()
        return
    }
    if (!newUserpasswordInputValue.match(userPasswordPattern)) {
        alert('votre mot de passe doit contenir entre 3 et 15 caractères')
        event.preventDefault()
        return
    }
    addUser()
}
const checkSigninForm = (event) => {
    const usernameInputValue = event.target[0].value
    const userpasswordInputValue = event.target[1].value
    if (!usernameInputValue.match(usernamePattern)) {
        alert('votre nom doit contenit entre 3 et 15 caractères')
        event.preventDefault()
    }
    if (!userpasswordInputValue.match(userPasswordPattern)) {
        alert('votre mot de passe doit contenir entre 3 et 15 caractères')
        event.preventDefault()
    }
}


signup.addEventListener("submit", (event) => checkSignupForm(event))
signin.addEventListener("submit", (event) => checkSigninForm(event))