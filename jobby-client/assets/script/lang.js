// je récupère ma data
// fetch
const fetchLang = async () => {
    let langAPIUrl = "http://api.jobby.fr:5000/lang"
    let response = await superagent
        .get(langAPIUrl)
    let datas = response.body

    // avec la data je construis mon html
    let templateLang = ''
    datas.map(data => {
        templateLang += `<div data-langue="${data}">${data}</div>`
    })
    // je place le html construis dans le dom
    let langDOM = document.querySelector('#lang')
    langDOM.innerHTML = templateLang

    attachEventToLangDom()



}

fetchLang()

// j'ajoute un evenement au click sur qui ? qu'est ce qui se passe quand je click?
// sur qui => les div de langues (par eemple <div>fr</div>)
// qu'est ce qui se passe quand je click => je vais vers un listener (aka une fonction)
const attachEventToLangDom = () => {
    let domLangs = document.querySelectorAll('#lang div')

    domLangs.forEach(domLang => {
        domLang.addEventListener('click', savePrefLang)
    })
}
// cette fonction va sauvegarder à travers les futurs navigation la langue selectionné => localStorage 
const savePrefLang = (event) => {
    console.log(event)

    let lang = event.target.dataset.langue

    localStorage.setItem('lang', lang);
    fetchMenu2()
}

