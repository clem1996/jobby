
function logout() {
    localStorage.removeItem('isLoggedIn')
    localStorage.removeItem('userMail')

    localStorage.removeItem('userId')

    location.href = 'http://jobby.fr:3000'
}