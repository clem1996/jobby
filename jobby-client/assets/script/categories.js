
const initializeCategories = (categoriesData) => {
    // @algo : generate categories cards
    //  1 : I retrieve the dom node in which i'll inject my categoriesTemplate
    //  2 : I declare my categoriesTemplate as an empty string
    //  3 : I loop over my data
    //  4 : For each loop over our data we retrieve all variables from our object ( destructuring )
    //  5 : An intermediate template is prepared in which data is interpolated
    //  6 : For each loop over our data i concatenate the intermediate template into my categoriesTemplate
    //  7 : I'll inject my categoriesTemplate into my categories dom node

    const categories = document.querySelector('#card-ctn')
    let categoriesTemplate = ""

    categoriesData.forEach((categoryItem) => {
        let { categoryId, categoryLabel, categoryImageLink, categoryImgAlt, categoryIsService } = categoryItem
        // let categoryLink = !!categoryIsService === true ? '/form?categoryId=' + categoryId : '/categories?categoryParentId=' + categoryId // choix 1 d'url
        let categoryLink = !!categoryIsService === true ? '/form?categoryId=' + categoryId : '/categories/' + categoryId // choix 2 d'url

        let categoryItemTemplate = `
    <article class='card'>
        <a href='${categoryLink}'>
            <h3>${categoryLabel}</h3>
            <img src="${categoryImageLink}" alt="${categoryImgAlt}">
        </a>
    </article>
    `
        categoriesTemplate += categoryItemTemplate
    })
    categories.innerHTML = categoriesTemplate
}

// je recupere ma donnée
const fetchCategories = async () => {
    let myApiURl = ''
    if (location.href === "http://jobby.fr:3000/categories") {
        myApiURl = 'http://api.jobby.fr:5000/categories'
    }
    else {
        // const urlParams = new URLSearchParams(window.location.search); // choix 1 d'url
        // const categoryParentId = urlParams.get('categoryParentId');// choix 1 d'url

        const categoryParentId = location.pathname.split('/')[2]  // choix 2 d'url

        // technique pattern 1

        // let str = location.pathname
        // const pattern = /[^0-9]+/
        // str.replace(pattern, '')

       // technique pattern avancé 2
        

       // see back references
        // let str = location.pathname
        // const pattern =  /\/categories\/([0-9]+)/
        // str.replace(pattern, '$1')
        

        myApiURl = 'http://api.jobby.fr:5000/categories/' + categoryParentId
    }

    let reponse = await superagent
        .get(myApiURl)
    let categoriesData = reponse.body

    initializeCategories(categoriesData)
    return
}







const filterCategories = async (event) => {

    let categoryLabel = event.target.value
    console.log(categoryLabel)
    let response = await superagent
        .post('http://api.jobby.fr:5000/categories')
        .send({ categoryLabel: categoryLabel })
        .set('Accept', 'application/json')
    let categoriesData = response.body
    initializeCategories(categoriesData)
    return
}

const initializeFilter = () => {
    document.querySelector('input[name=categoryLabel]').addEventListener('change', filterCategories)
}
fetchCategories()
initializeFilter()