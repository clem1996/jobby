const partnersData = [
    {
        partnersCategoryId: 1,
        partnersCategoryCode: 123456789,
        partnersCategoryLabel: "transport",
        partners: [
            {
                partnerId: 2,
                partnerCode: 123456788,
                partnerLabel: "rapid'transport",
                partnerStars: 3
            },
            {
                partnerId: 4,
                partnerCode: 128456788,
                partnerLabel: "rapid'transport",
                partnerStars: 4
            },
            {
                partnerId: 3,
                partnerCode: 123426788,
                partnerLabel: "rapid'transport",
                partnerStars: 2
            },
            ,
            {
                partnerId: 5,
                partnerCode: 123446788,
                partnerLabel: "rapid'transport",
                partnerStars: 5
            },
        ]
    },
    {
        partnersCategoryId: 6,
        partnersCategoryCode: 123456789,
        partnersCategoryLabel: "transport",
        partners: [
            {
                partnerId: 7,
                partnerCode: 123456788,
                partnerLabel: "rapid'transport",
                partnerStars: 2
            },
            {
                partnerId: 8,
                partnerCode: 123426788,
                partnerLabel: "rapid'transport",
                partnerStars: 5
            },
            {
                partnerId: 9,
                partnerCode: 128456788,
                partnerLabel: "rapid'transport",
                partnerStars: 4
            },
            {
                partnerId: 10,
                partnerCode: 123446788,
                partnerLabel: "rapid'transport",
                partnerStars: 1
            },
        ]
    },
    {
        partnersCategoryId: 11,
        partnersCategoryCode: 123456789,
        partnersCategoryLabel: "transport",
        partners: [
            {
                partnerId: 12,
                partnerCode: 123456788,
                partnerLabel: "rapid'transport",
                partnerStars: 3
            },
            {
                partnerId: 13,
                partnerCode: 123426788,
                partnerLabel: "rapid'transport",
                partnerStars: 2
            },
            {
                partnerId: 14,
                partnerCode: 128456788,
                partnerLabel: "rapid'transport",
                partnerStars: 4
            },
            {
                partnerId: 15,
                partnerCode: 123446788,
                partnerLabel: "rapid'transport",
                partnerStars: 5
            },
        ]
    },
    {
        partnersCategoryId: 16,
        partnersCategoryCode: 123456789,
        partnersCategoryLabel: "transport",
        partners: [
            {
                partnerId: 17,
                partnerCode: 123456788,
                partnerLabel: "rapid'transport",
                partnerStars: 3
            },
            {
                partnerId: 18,
                partnerCode: 123426788,
                partnerLabel: "rapid'transport",
                partnerStars: 2
            },
            {
                partnerId: 19,
                partnerCode: 128456788,
                partnerLabel: "rapid'transport",
                partnerStars: 4
            },
            {
                partnerId: 20,
                partnerCode: 123446788,
                partnerLabel: "rapid'transport",
                partnerStars: 5
            },
        ]
    },
    {
        partnersCategoryId: 21,
        partnersCategoryCode: 123456789,
        partnersCategoryLabel: "transport",
        partners: [
            {
                partnerId: 22,
                partnerCode: 123456788,
                partnerLabel: "rapid'transport",
                partnerStars: 3
            },
            {
                partnerId: 23,
                partnerCode: 123426788,
                partnerLabel: "rapid'transport",
                partnerStars: 2
            },
            {
                partnerId: 24,
                partnerCode: 128456788,
                partnerLabel: "rapid'transport",
                partnerStars: 4
            },
            {
                partnerId: 25,
                partnerCode: 123446788,
                partnerLabel: "rapid'transport",
                partnerStars: 5
            },
        ]
    },
    {
        partnersCategoryId: 26,
        partnersCategoryCode: 123456789,
        partnersCategoryLabel: "transport",
        partners: [
            {
                partnerId: 27,
                partnerCode: 123456788,
                partnerLabel: "rapid'transport",
                partnerStars: 3
            },
            {
                partnerId: 28,
                partnerCode: 123426788,
                partnerLabel: "rapid'transport",
                partnerStars: 2
            },
            {
                partnerId: 29,
                partnerCode: 128456788,
                partnerLabel: "rapid'transport",
                partnerStars: 4
            },
            {
                partnerId: 30,
                partnerCode: 123446788,
                partnerLabel: "rapid'transport",
                partnerStars: 5
            },
        ]
    },
    {
        partnersCategoryId: 31,
        partnersCategoryCode: 123456789,
        partnersCategoryLabel: "transport",
        partners: [
            {
                partnerId: 32,
                partnerCode: 123456788,
                partnerLabel: "rapid'transport",
                partnerStars: 3
            },
            {
                partnerId: 33,
                partnerCode: 123426788,
                partnerLabel: "rapid'transport",
                partnerStars: 2
            },
            {
                partnerId: 34,
                partnerCode: 128456788,
                partnerLabel: "rapid'transport",
                partnerStars: 4
            },
            {
                partnerId: 35,
                partnerCode: 123446788,
                partnerLabel: "rapid'transport",
                partnerStars: 5
            },
        ]
    },
    {
        partnersCategoryId: 36,
        partnersCategoryCode: 123456789,
        partnersCategoryLabel: "transport",
        partners: [
            {
                partnerId: 37,
                partnerCode: 123456788,
                partnerLabel: "rapid'transport",
                partnerStars: 3
            },
            {
                partnerId: 38,
                partnerCode: 123426788,
                partnerLabel: "rapid'transport",
                partnerStars: 2
            },
            {
                partnerId: 39,
                partnerCode: 128456788,
                partnerLabel: "rapid'transport",
                partnerStars: 4
            },
            {
                partnerId: 40,
                partnerCode: 123446788,
                partnerLabel: "rapid'transport",
                partnerStars: 5
            },
        ]
    },
    {
        partnersCategoryId: 41,
        partnersCategoryCode: 123456789,
        partnersCategoryLabel: "transport",
        partners: [
            {
                partnerId: 42,
                partnerCode: 123456788,
                partnerLabel: "rapid'transport",
                partnerStars: 3
            },
            {
                partnerId: 43,
                partnerCode: 123426788,
                partnerLabel: "rapid'transport",
                partnerStars: 2
            },
            {
                partnerId: 44,
                partnerCode: 128456788,
                partnerLabel: "rapid'transport",
                partnerStars: 4
            },
            {
                partnerId: 45,
                partnerCode: 123446788,
                partnerLabel: "rapid'transport",
                partnerStars: 5
            },
        ]
    },
]
// @algo : generate partners
// 1 : I retrieve the dom node in which i'll inject my finalTemplate
// 2 : I declare my finalTemplate as an empty string
// 3 : I loop over my data to retrieve all categoriesData
// 4 : For each loop over our partenersCategoriesData we retrieve all variables from our object ( destructuring )
// 5 : An intermediate template that is left open is prepared in which partenersCategoriesData is interpolated
// 6 : For each loop over our partenersCategoriesData i concatenate the intermediate template into my finalTemplate
// 7 : I loop over my data to retrieve all partnersDetailData
// 8 : For each loop over our partnersDetailData we retrieve all variables from our object ( destructuring )
// 9 : An intermediate template that is left open is prepared in which partnersDetailData is interpolated
// 10 : I declare my allStarsTemplate as an empty string
// 11 : I declare a template for a filled stars
// 12 : I repeat my full stars template as many times as the value i retrieved in my data 
// 13 : I compute the number of empty stars which i assign to a variable
// 14 : I declare a template for a empty stars
// 15 : I repeat my empty stars template as many times as the value i computed and i concatenate it to the allStarsTemplate
// 16 : I concatenate my partnerDetailTemplate to my partnersTemplate
// 17 : I concatenate my allStarsTemplate to my partnersTemplate
// 18 : I concatenate to our partnerDetailTemplate the appropriate closing tag (</li>)
// 19 : Outside my partnersDetailsLoop, i concatenate to the end of my partnersTemplate the appropriate closing tag ('</article>') 
// 20 : I inject the partnersTemplate into the dom node i retrieved earlier


const partners = document.querySelector('.article-ctn')
let partnersTemplate = ""
const partnersCategories = partnersData.map((partnersCategory) => {
    let { partnersCategoryId, partnersCategoryCode, partnersCategoryLabel, partners } = partnersCategory
    let partnersCategoriesTemplate = `
    <article>
        <h3>${partnersCategoryLabel}</h3>
    `
    partnersTemplate += partnersCategoriesTemplate

    const partnersDetails = partners.map((partnerDetail) => {
        let { partnerId, partnerCode, partnerLabel, partnerStars } = partnerDetail
        let partnerDetailTemplate = `
        <li>
        <a href="">${partnerLabel}</a>
        `
    
        let allStarsTemplate = ""

        let filledStarTemplate = '<i class="fas fa-star"></i>'
        allStarsTemplate += filledStarTemplate.repeat(partnerStars)

        let emptyStarsNumber = 5 - partnerStars
        let emptyStarsTemplate = '<i class="far fa-star"></i>'
        allStarsTemplate += emptyStarsTemplate.repeat(emptyStarsNumber)

        partnersTemplate += partnerDetailTemplate
        partnersTemplate += allStarsTemplate
        partnerDetailTemplate += '</li>'

    })
    partnersTemplate += '</article>'
})
partners.innerHTML = partnersTemplate
