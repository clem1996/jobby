// je récupère ma data
// fetch
const fetchCategory = async () => {
    
    const urlParams = new URLSearchParams(window.location.search);
    const categoryId = urlParams.get('categoryId');

    let CategoryAPIUrl = `http://api.jobby.fr:5000/form2?categoryId=${categoryId}`
    let response = await superagent
        .get(CategoryAPIUrl)
    let datas = response.body
    // avec la data je construis mon html
    let templateCategory = ''
    datas.map(data=>{
        templateCategory += `<div data-categorie="${data.categoryLabel}">${data.categoryLabel}</div>`
    })
    // je place le html construis dans le dom
    let categoryDOM = document.querySelector('#category')
    categoryDOM.innerHTML = templateCategory
    
    // attachEventToLangDom()
}

fetchCategory()

// const attachEventToLangDom = () => {


// }