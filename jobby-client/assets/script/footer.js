// const footerData = [
//     '<i class="fab fa-facebook-f"></i>',
//     '<i class="fab fa-twitter"></i>',
//     '<i class="far fa-copyright"></i>',
//     '<i class="fas fa-cookie"></i>',
// ]


const initializeFooter = (footerData) => {
    // @algo : generate footer
    //  1 : I retrieve the dom node in which i'll inject my footerTemplate
    //  2 : I declare my footerTemplate as an empty string
    //  3 : I loop over my data
    //  4 : For each loop over my data i concatenate each footerItem to my footerTemplate
    //  5 : I'll inject my footerTemplate into my footer dom node
    const footer = document.querySelector("footer")
    let footerTemplateRS = ""
    footerData.forEach((footerItem) => {
        if (footerItem.footerType === "reseauSociaux") {
            if(!!footerItem.footerFlagIsIcon === true){
                footerTemplateRS += `<i class="${footerItem.footerIconDetail}"><a target="${footerItem.footerTarget}" href="${footerItem.footerLink}">${footerItem.footerLabel}</a></i>`
    
            } else {
            footerTemplateRS += `<a target="${footerItem.footerTarget}" href="${footerItem.footerLink}">${footerItem.footerLabel}</a>`
        
        
        }
    }
    })

    let footerTemplateBranding = ""

    footerData.forEach((footerItem) => {
        if (footerItem.footerType === "brand") {
            footerTemplateBranding += `<a target="${footerItem.footerTarget}" href="${footerItem.footerLink}">${footerItem.footerLabel}</a>`
        }
    })



    footer.innerHTML = `
<a href="http://google.fr" target="_blank">google.fr</a>

    <div><b>suivez nous sur</b>
    ${footerTemplateRS}
    </div>
    <div><b>Notre company </b>
    ${footerTemplateBranding}
    </div>
    
    `
}

// je recupere ma donnée
const fetchFooter = async () => {
    let reponse = await superagent
        .get('http://api.jobby.fr:5000/footer')
    let footerData = reponse.body

    initializeFooter(footerData)
    return
}


fetchFooter()