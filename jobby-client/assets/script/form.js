
const intializeForm = (formData) => {
    // @algo : generate form
    // 1 : I retrieve the dom node in which i'll inject my finalTemplate
    // 2 : I declare my finalTemplate as an empty string
    // 3 : I loop over my data to retrieve all questions
    // 4 : For each loop over our questionsData we retrieve all variables from our object ( destructuring )
    // 5 : An intermediate template that is left open is prepared in which questionsData is interpolated
    // 6 : For each loop over our questionsData i concatenate the intermediate template into my finalTemplate
    // 7 : I loop over my data to retrieve all responses
    // 8 : For each loop over our responsesData we retrieve all variables from our object ( destructuring )
    // 9 : An intermediate template is prepared in which responsesData is interpolated
    // 10 : For each loop over our responsesData i concatenate the intermediate template into my finalTemplate
    // 11 : We concatenate to our finalTemplate the closing tag section 
    // 12 : For question types strictly equal to text, date or textarea, 
    //      we declare an exception to concatenate to the intermediate question 
    //      template another intermediate template corresponding to the type of input we need 
    // 13 : Into the questionData loop, if the question type strictly equal to select, we declare an exception
    //      in order to concatenate a left open intermediate template
    // 14 : I loop over my data to retrieve all responses
    // 15 : For each loop over our responsesData we retrieve all variables from our object ( destructuring )
    // 16 : An intermediate template is prepared in which responsesData is interpolated
    // 17 : Into the formResponse, the question type strictly equal to select, we declare an exception
    //      in order to concatenate an empty intermediate template
    // 18 : We inject the finalTemplate into the dom node we retrieved earlier
    // 19 : At last we concatenate to the finalTemplate an intermediate template with a submit input
    const form = document.querySelector('form')
    let formTemplate = ""
    const formQuestions = formData.forEach((formQuestion) => {
        let { questionId, questionLabel, questionType, questionResponses } = formQuestion
        let formQuestionTemplate = ``
        if (questionType === "text") {
            formQuestionTemplate += `
        <input type="text" name="questionId_${questionId}" id="${questionLabel}" value="" required />
        `
        }
        else if (questionType === "date") {
            formQuestionTemplate += `
        <input type="date" name="questionId_${questionId}" id="${questionLabel}" value="" required />
        `
        }
        else if (questionType === "textarea") {
            formQuestionTemplate += `
        <textarea name="questionId_${questionId}" id="${questionLabel}"></textarea>
        `
        }
        else if (questionType === "select") {
            formQuestionTemplate += `
                <div class="question-box">
                    <select name="questionId_${questionId}_response" id="${questionLabel}">
            `
            const selectOptions = questionResponses.forEach((option) => {
                let { responseId, responseCode, responseLabel } = option
                let optionTemplate = `<option value="${responseId}">${responseLabel}</option>`
                formQuestionTemplate += optionTemplate
            })
            formQuestionTemplate += `
                </select>
            </div>
            `
        } else if (questionType === "checkbox" || questionType === "radio") {
            let flagSelected =  questionType === "radio"? "checked": ""

            const formResponses = questionResponses.forEach((formResponse) => {
                let { responseId, responseCode, responseLabel } = formResponse


                let formResponseTemplate = `
                    <div class="multiple-question-box">
                        <input ${flagSelected} type="${questionType}" name="questionId_${questionId}_response" 
                            id="${responseCode}" value="${responseId}"/>
                        <label for="${responseCode}">${responseLabel}</label>
                    </div>
                    `
                flagSelected = ""
                formQuestionTemplate += formResponseTemplate
            })
        }

        formTemplate += `
            <section class="question-ctn">
                <h4>${questionLabel}</h4>
                ${formQuestionTemplate} 
            </section>
        `

    })
    form.innerHTML = formTemplate
    form.innerHTML += `
        <section class="question-ctn submit-box">
                <div class="question-box">
                    <input type="button" value="Envoyer" id="addDemandButton" />
                </div>
        </section>
        `
}

const fetchForm = async () => {
    // @algo fetch the right category
    // 1) i declare a constant which purpose is to search for any parameters in the current window URL
    // 2) i declare a constant which value is the categoryId from the current window URL 
    // 3) i fetch my API with superagent and the good categoryId
    // 4) i declare a variable in which there is my data 
    // 5) i call my templating function with my data as a parameter 
    const urlParams = new URLSearchParams(window.location.search);
    const categoryId = urlParams.get('categoryId');
    let response = await superagent
            .get(`http://api.jobby.fr:5000/form/${categoryId}`)
    let formData = response.body
    intializeForm(formData)
    return
}

// i call my fetch function
fetchForm()





