const addDemand = async () => {
    // let responseLabel = document.querySelector('#add-response input[name="responseLabel"]').value
    // let questionId = document.querySelector('#questionId').value

    let myURLAPI = 'http://web-hook.jobby.fr:6001/demand'
    let myForm = document.getElementById('formSubmit');
    let formData = new FormData(myForm);
    let formDataJson = {}

    for (let [fieldName, fieldValue] of formData.entries()) {

        if (fieldName.match(/questionId_[0-9]*_response/)) {
            let pattern = /questionId_([0-9]*)_response/
            let questionId = fieldName.replace(pattern, '$1')
            let newValue = {
                value: fieldValue,
                questionId: questionId
            }
            if ('responses' in formDataJson) {
                formDataJson['responses'].push(newValue)
            } else {
                formDataJson['responses'] = [newValue]
            }
        }
        else if (fieldName.match(/questionId_[0-9]*/)) {
            let pattern = /questionId_([0-9]*)/
            let questionId = fieldName.replace(pattern, '$1')
            let newValue = {
                value: fieldValue,
                questionId: questionId
            }
            if ('questions' in formDataJson) {
                formDataJson['questions'].push(newValue)
            } else {
                formDataJson['questions'] = [newValue]
            }
        }
        else {
            formDataJson[fieldName] = fieldValue
        }
    }
    let response = await superagent
        .post(myURLAPI)
        .send(
            formDataJson
        )
        .set('Accept', 'application/json')
        .set('x-fuck', 'application/json')
    // .set('Content-Type', 'application/x-www-form-urlencoded')
    let responseData = response.body
    console.log(responseData)
    if(response.status===200)   {
        sessionStorage.setItem('devis', JSON.stringify(response.body.body))
        sessionStorage.setItem('demandId', JSON.stringify(response.body.demandId))

        let evt = new CustomEvent('Jobby')
        document.dispatchEvent(evt)
    }

    
}

// COMMENT DECLENCHER UNE ACTION DANS UN AUTRE FICHIER
// RECEPTEUR DE L'ENVOI (cf fichier form.js)
// écoute l'evenement customisé sur le même dom ou l'envoi est fait
document.addEventListener('bindMoiAddDemandButton', (event) => {
    // on fait l'action que l'on souhaite
    document.querySelector('#addDemandButton').addEventListener('click', addDemand)
})


