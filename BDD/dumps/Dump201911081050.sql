-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: jobbyDB
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.19.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `addressId` int(11) NOT NULL AUTO_INCREMENT,
  `addressCode` tinytext,
  `addressStreetNumber` varchar(10) DEFAULT NULL,
  `addressStreetLabel` tinytext,
  `addressZipCode` int(11) DEFAULT NULL,
  `addressCityLabel` varchar(100) DEFAULT NULL,
  `addressRegionLabel` varchar(30) DEFAULT NULL,
  `addressDepartmentLabel` varchar(30) DEFAULT NULL,
  `address_userId` int(11) DEFAULT NULL,
  `address_proId` int(11) DEFAULT NULL,
  PRIMARY KEY (`addressId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `answerId` int(11) NOT NULL AUTO_INCREMENT,
  `answerCode` tinytext,
  `answerLabel` varchar(30) DEFAULT NULL,
  `answerIsArchived` tinyint(1) DEFAULT NULL,
  `answerIsArchivedDateTime` datetime DEFAULT NULL,
  `answer_responseId` int(11) DEFAULT NULL,
  `answer_demandQuestionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`answerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryParentId` int(11) DEFAULT NULL,
  `categoryCode` tinytext,
  `categoryLabel` varchar(30) DEFAULT NULL,
  `categoryImageLink` tinytext,
  `categoryCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `categoryIsArchived` tinyint(1) DEFAULT NULL,
  `categoryIsArchivedDateTime` datetime DEFAULT NULL,
  `categoryPopularity` int(11) DEFAULT NULL,
  `categoryIsService` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`categoryId`),
  FULLTEXT KEY `index2` (`categoryLabel`,`categoryCode`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,0,'category_7231b48b-f0c9-11e9-8abb-dcf505905f9b','Bricolages','https://i.imgur.com/1XrPTg8.jpg','2019-10-17 12:32:40',0,NULL,3,1),(2,0,'category_7231b4db-f0c9-11e9-8abb-dcf505905f9b','Salle de bain','https://i.imgur.com/ZHRwHdd.jpg','2019-10-17 12:32:40',0,NULL,4,1),(3,0,'category_7231b4f8-f0c9-11e9-8abb-dcf505905f9b','Décoration','https://i.imgur.com/Mi4pngk.jpg','2019-10-17 12:32:40',NULL,NULL,1,1),(4,0,'category_7231b512-f0c9-11e9-8abb-dcf505905f9b','Électricité','https://i.imgur.com/QEbLW7T.jpg','2019-10-17 12:32:40',NULL,NULL,5,1),(5,0,'category_7231b52c-f0c9-11e9-8abb-dcf505905f9b','Portes & fenetres','https://i.imgur.com/fBLuHTd.jpg','2019-10-17 12:32:40',0,NULL,5,0),(6,0,'category_7231b561-f0c9-11e9-8abb-dcf505905f9b','Jardinage','https://i.imgur.com/qS43pNV.jpg','2019-10-17 12:32:40',NULL,NULL,2,1),(7,0,'category_7231b593-f0c9-11e9-8abb-dcf505905f9b','Soutien scolaire','https://i.imgur.com/GdSFShm.jpg','2019-10-17 12:32:40',NULL,NULL,3,1),(8,0,'category_7231b5b4-f0c9-11e9-8abb-dcf505905f9b','Peinture','https://i.imgur.com/DuxKyAi.jpg','2019-10-17 12:32:40',NULL,NULL,1,1),(9,0,'category_7231b5d3-f0c9-11e9-8abb-dcf505905f9b','Livraison','https://i.imgur.com/y1KWeES.jpg','2019-10-17 12:32:40',NULL,NULL,0,1),(10,0,'category_7231b5ed-f0c9-11e9-8abb-dcf505905f9b','Transport','https://i.imgur.com/BZedtDL.jpg','2019-10-17 12:32:40',NULL,NULL,3,1),(11,0,'category_7231b609-f0c9-11e9-8abb-dcf505905f9b','Multimedia','https://i.imgur.com/I6GSsT9.jpg','2019-10-17 12:32:40',NULL,NULL,2,1),(12,0,'category_7231b620-f0c9-11e9-8abb-dcf505905f9b','Loisirs','https://i.imgur.com/msDoqyz.jpg','2019-10-17 12:32:40',NULL,NULL,4,1),(13,5,'category_280dde56-f0cd-11e9-8abb-dcf505905f9b','Portes','https://i.imgur.com/fBLuHTd.jpg','2019-10-17 12:59:14',0,NULL,3,0),(14,5,'category_280ddea7-f0cd-11e9-8abb-dcf505905f9b','fenêtres',' https://i.imgur.com/iSBS4GY.jpg','2019-10-17 12:59:14',NULL,NULL,5,0),(15,13,'category_280ddebf-f0cd-11e9-8abb-dcf505905f9b','Montage','https://i.imgur.com/wD1g5A0.jpg','2019-10-17 12:59:14',0,NULL,1,1),(16,13,'category_280dded7-f0cd-11e9-8abb-dcf505905f9b','Démontage','https://i.imgur.com/d9MS2rQ.jpg','2019-10-17 12:59:14',NULL,NULL,2,1),(17,14,'category_280ddeeb-f0cd-11e9-8abb-dcf505905f9b','Remplacement','https://i.imgur.com/NaRauS0.png','2019-10-17 12:59:14',NULL,NULL,4,1),(18,14,'category_280ddf02-f0cd-11e9-8abb-dcf505905f9b','Isolation','https://i.imgur.com/L1iVvJT.jpg','2019-10-17 12:59:14',NULL,NULL,3,1),(19,0,'category_b6629a2a-f185-11e9-bf7f-dcf505905f9b','Bidon 1','','2019-10-18 11:00:20',1,'2019-10-18 11:00:20',3,1),(20,0,'category_b896ff9f-f185-11e9-bf7f-dcf505905f9b','bidon 2','','2019-10-18 11:00:24',1,'2019-10-18 11:00:24',3,1),(22,NULL,'zobby','zobby',NULL,'2019-10-29 16:44:05',NULL,NULL,NULL,NULL),(23,0,'category_b74f2ac2-fa6a-11e9-9176-dcf505905f9b','coucou','https://i.imgur.com/fBLuHTd.jpg','2019-10-29 17:39:46',0,NULL,3,1),(26,0,'category_0dcb1924-fb0a-11e9-972c-dcf505905f9b','tartuffe','https://s1.lmcdn.fr/multimedia/731503097677/52e15d668d22/meubles-neo.jpg?$p=hi-w1500','2019-10-30 12:40:21',0,NULL,3,1),(28,0,'category_2d656702-ff12-11e9-a92f-dcf505905f9b','casquette','','2019-11-04 15:48:34',NULL,NULL,3,1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `demand`
--

DROP TABLE IF EXISTS `demand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demand` (
  `demandId` int(11) NOT NULL AUTO_INCREMENT,
  `demandCode` tinytext,
  `demandLabel` tinytext,
  `demandCreationDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `demandIsArchived` tinyint(1) DEFAULT NULL,
  `demandIsArchivedDateTime` datetime DEFAULT NULL,
  `demand_userId` int(11) DEFAULT NULL,
  `demand_categoryId` int(11) DEFAULT NULL,
  PRIMARY KEY (`demandId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demand`
--

LOCK TABLES `demand` WRITE;
/*!40000 ALTER TABLE `demand` DISABLE KEYS */;
INSERT INTO `demand` VALUES (1,'demand_e0789aa2-f182-11e9-bf7f-dcf505905f9b','fake demand','2019-10-18 10:40:02',NULL,NULL,3,1),(2,'demand_3163ea60-f183-11e9-bf7f-dcf505905f9b','fake demand','2019-10-18 10:42:18',NULL,NULL,4,1),(5,'demand_eb69a5ae-f70c-11e9-a8c3-dcf505905f9b','fake demand','2019-10-25 11:50:47',NULL,NULL,5,1),(6,NULL,'fake demand made by clément','2019-10-25 16:47:46',NULL,NULL,3,15),(7,NULL,'fake demand made by clément','2019-10-25 16:48:15',NULL,NULL,3,15),(8,'demand_f1e8ecda-0178-11ea-9a79-dcf505905f9b','nouvelle demande de rererererer@toto.com (catId:15)','2019-11-07 17:09:15',NULL,NULL,23,15),(9,'demand_09a45aee-0179-11ea-9a79-dcf505905f9b','nouvelle demande de rererererer@toto.com (catId:15)','2019-11-07 17:09:55',NULL,NULL,23,15),(10,'demand_18f66946-0179-11ea-9a79-dcf505905f9b','nouvelle demande de rererererer@toto.com (catId:15)','2019-11-07 17:10:20',NULL,NULL,23,15),(11,'demand_272b5912-0179-11ea-9a79-dcf505905f9b','nouvelle demande de rererererer@toto.com (catId:15)','2019-11-07 17:10:44',NULL,NULL,23,15),(12,'demand_59d815f6-017c-11ea-9a79-dcf505905f9b','nouvelle demande de rererererer@toto.com (catId:15)','2019-11-07 17:33:38',NULL,NULL,23,15),(13,'demand_735a01e1-017c-11ea-9a79-dcf505905f9b','nouvelle demande de rererererer@toto.com (catId:15)','2019-11-07 17:34:21',NULL,NULL,23,15);
/*!40000 ALTER TABLE `demand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `demandQuestion`
--

DROP TABLE IF EXISTS `demandQuestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demandQuestion` (
  `demandQuestionId` int(11) NOT NULL AUTO_INCREMENT,
  `demandQuestion_demandId` int(11) DEFAULT NULL,
  `demandQuestion_questionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`demandQuestionId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demandQuestion`
--

LOCK TABLES `demandQuestion` WRITE;
/*!40000 ALTER TABLE `demandQuestion` DISABLE KEYS */;
INSERT INTO `demandQuestion` VALUES (1,7,1),(2,7,2),(3,7,3),(4,7,4),(5,7,5),(6,7,6),(7,13,1),(8,13,4),(9,13,6),(10,13,2),(11,13,3),(12,13,5);
/*!40000 ALTER TABLE `demandQuestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `footer`
--

DROP TABLE IF EXISTS `footer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footer` (
  `footerId` int(11) NOT NULL AUTO_INCREMENT,
  `footerCode` tinytext,
  `footerLabel` varchar(30) DEFAULT NULL,
  `footerIconDetail` varchar(45) DEFAULT NULL,
  `footerLink` varchar(45) DEFAULT NULL,
  `footerType` enum('reseauSociaux','brand','others') NOT NULL DEFAULT 'reseauSociaux',
  `footerAlt` tinytext,
  `footerTarget` enum('_blank','') NOT NULL,
  `footerFlagIsIcon` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`footerId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `footer`
--

LOCK TABLES `footer` WRITE;
/*!40000 ALTER TABLE `footer` DISABLE KEYS */;
INSERT INTO `footer` VALUES (1,'footer_ceeb04f2-fa2e-11e9-9176-dcf505905f9b','facebook','fab fa-facebook-f','facebook.com','reseauSociaux',NULL,'',1),(2,'footer_ceeb0535-fa2e-11e9-9176-dcf505905f9b','twitter','fab fa-twitter','twitter.com','reseauSociaux',NULL,'_blank',1),(3,'footer_ceeb0552-fa2e-11e9-9176-dcf505905f9b','copyright','fab fa-facebook-f','copyright.com','brand',NULL,'_blank',1);
/*!40000 ALTER TABLE `footer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lang`
--

DROP TABLE IF EXISTS `lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lang` (
  `langId` int(11) NOT NULL AUTO_INCREMENT,
  `langCode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`langId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lang`
--

LOCK TABLES `lang` WRITE;
/*!40000 ALTER TABLE `lang` DISABLE KEYS */;
INSERT INTO `lang` VALUES (1,'fr'),(2,'en');
/*!40000 ALTER TABLE `lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `menuId` int(11) NOT NULL AUTO_INCREMENT,
  `menuCode` tinytext,
  `menuItemLabel` varchar(20) DEFAULT NULL,
  `menuItemLinkTo` tinytext,
  `menu_langId` int(11) DEFAULT NULL,
  `menuTarget` varchar(45) NOT NULL DEFAULT '',
  `menuSpecialActionOnClick` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`menuId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'menu_b377ced6-f1b5-11e9-bf7f-dcf505905f9b','Nos offres','/categories',1,'',''),(2,'menu_b377cf37-f1b5-11e9-bf7f-dcf505905f9b','Connexion','/login',1,'',''),(3,'menu_b377cf5d-f1b5-11e9-bf7f-dcf505905f9b','À propos','/about',1,'',''),(4,'menu_b377cf85-f1b5-11e9-bf7f-dcf505905f9b','Contact','/contact',1,'',''),(5,'menu_b377cfb8-f1b5-11e9-bf7f-dcf505905f9b','Nos partenaires','/partners',1,'',''),(6,'menu_b377ced6-f1b5-11e9-bf7f-dcf505905f9C','offers','/categories',2,'',''),(7,'menu_b377ced6-f1b5-11e9-bf7f-dcf505905f9d','login','/login',2,'',''),(8,'menu_b377ced6-f1b5-11e9-bf7f-dcf505905f9e','about','/about',2,'',''),(9,'menu_b377ced6-f1b5-11e9-bf7f-dcf505905f9f','Contacts','/contact',2,'',''),(10,'menu_b377ced6-f1b5-11e9-bf7f-dcf505905faa','partners','/partners',2,'',''),(11,NULL,'admin','http://admin.jobby.fr:4000/',1,'_blank',''),(12,NULL,'admin','http://admin.jobby.fr:4000/',2,'_blank',''),(14,NULL,'logout','/logout',1,'','logout'),(15,NULL,'logout','/logout',2,'','logout');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pro`
--

DROP TABLE IF EXISTS `pro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro` (
  `proId` int(11) NOT NULL AUTO_INCREMENT,
  `proCode` tinytext,
  `proLabel` varchar(30) DEFAULT NULL,
  `proFirstname` varchar(30) DEFAULT NULL,
  `proLastname` varchar(30) DEFAULT NULL,
  `proMail` varchar(100) DEFAULT NULL,
  `proMobileNumber` int(11) DEFAULT NULL,
  `proPassword` varchar(30) DEFAULT NULL,
  `proAge` varchar(45) DEFAULT NULL,
  `proCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `proIsArchived` tinyint(1) DEFAULT NULL,
  `proIsArchivedDateTime` datetime DEFAULT NULL,
  `proNotation` int(11) DEFAULT NULL,
  PRIMARY KEY (`proId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro`
--

LOCK TABLES `pro` WRITE;
/*!40000 ALTER TABLE `pro` DISABLE KEYS */;
INSERT INTO `pro` VALUES (1,'pro_e41b64ad-f1b2-11e9-bf7f-dcf505905f9b','rapid boucherie','jack','l\'éventreur','j.kill@horreur.salace',36153615,'machette','45','2019-10-18 16:23:44',NULL,NULL,5),(2,'pro_e41b6501-f1b2-11e9-bf7f-dcf505905f9b','rapid transport','fast','&furious','f.furious@course.rally',1234567890,'bolide','35','2019-10-18 16:23:44',NULL,NULL,4),(3,'pro_e41b652d-f1b2-11e9-bf7f-dcf505905f9b','rapid intervention','spider','man','s.man@toile.avenger',999666333,'heros','20','2019-10-18 16:23:44',NULL,NULL,3);
/*!40000 ALTER TABLE `pro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proProfession`
--

DROP TABLE IF EXISTS `proProfession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proProfession` (
  `proProfessionId` int(11) NOT NULL AUTO_INCREMENT,
  `proProfession_proId` int(11) DEFAULT NULL,
  `proProfession_professionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`proProfessionId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proProfession`
--

LOCK TABLES `proProfession` WRITE;
/*!40000 ALTER TABLE `proProfession` DISABLE KEYS */;
INSERT INTO `proProfession` VALUES (1,3,3),(2,3,2),(3,1,1),(4,1,3),(5,2,2),(6,2,1);
/*!40000 ALTER TABLE `proProfession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profession`
--

DROP TABLE IF EXISTS `profession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profession` (
  `professionId` int(11) NOT NULL AUTO_INCREMENT,
  `professionCode` tinytext,
  `professionLabel` varchar(30) DEFAULT NULL,
  `professionCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `professionIsArchived` tinyint(1) DEFAULT NULL,
  `professionIsArchivedDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`professionId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profession`
--

LOCK TABLES `profession` WRITE;
/*!40000 ALTER TABLE `profession` DISABLE KEYS */;
INSERT INTO `profession` VALUES (1,'profession_8b071898-f1b3-11e9-bf7f-dcf505905f9b','boucher','2019-10-18 16:28:24',NULL,'2019-10-18 16:28:24'),(2,'profession_8b0718d5-f1b3-11e9-bf7f-dcf505905f9b','taxi','2019-10-18 16:28:24',NULL,'2019-10-18 16:28:24'),(3,'profession_8b0718ec-f1b3-11e9-bf7f-dcf505905f9b','infirmier','2019-10-18 16:28:24',NULL,'2019-10-18 16:28:24');
/*!40000 ALTER TABLE `profession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `questionId` int(11) NOT NULL AUTO_INCREMENT,
  `questionCode` tinytext,
  `questionLabel` varchar(100) DEFAULT NULL,
  `questionOrder` int(11) DEFAULT NULL,
  `questionType` varchar(30) DEFAULT NULL,
  `questionCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `questionIsArchived` tinyint(1) DEFAULT NULL,
  `questionIsArchivedDateTime` datetime DEFAULT NULL,
  `question_categoryId` int(11) DEFAULT NULL,
  `questionIsMandatory` tinyint(1) NOT NULL DEFAULT '0',
  `questionHelper` varchar(100) DEFAULT NULL,
  `questionPattern` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`questionId`),
  KEY `fk_question_1_idx` (`question_categoryId`),
  CONSTRAINT `question_categoryId` FOREIGN KEY (`question_categoryId`) REFERENCES `category` (`categoryId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'question_f22d5298-f0dc-11e9-8abb-dcf505905f9b','Désirez-vous un modèle en particulier ?',1000,'text','2019-10-17 14:52:15',0,NULL,15,0,'null',''),(2,'question_f22d5316-f0dc-11e9-8abb-dcf505905f9b','Dans quelle pièce souhaitez vous mettre une porte ?',2000,'select','2019-10-17 14:52:15',0,NULL,15,0,'null',''),(3,'question_f22d533b-f0dc-11e9-8abb-dcf505905f9b','Quel type de matériau désirez-vous ?',3000,'radio','2019-10-17 14:52:15',NULL,NULL,15,0,NULL,NULL),(4,'question_f22d5355-f0dc-11e9-8abb-dcf505905f9b','À quelle date pouvez-vous recevoir un jobbeur ?',4000,'date','2019-10-17 14:52:15',NULL,NULL,15,0,NULL,NULL),(5,'question_f22d536a-f0dc-11e9-8abb-dcf505905f9b','Quel créneau horaire vous conviendrait',5000,'checkbox','2019-10-17 14:52:15',NULL,NULL,15,0,NULL,NULL),(6,'question_f22d5385-f0dc-11e9-8abb-dcf505905f9b','Avez vous autre chose a suggérer ?',6000,'text','2019-10-17 14:52:15',NULL,NULL,15,0,NULL,NULL),(7,'question_83631a69-f0dd-11e9-8abb-dcf505905f9b','Quelle est la référence du matériel à démonter ?',1000,'text','2019-10-17 14:56:19',NULL,NULL,16,0,NULL,NULL),(8,'question_83631abf-f0dd-11e9-8abb-dcf505905f9b','Dans quelle pièce se trouve cette porte ?',2000,'select','2019-10-17 14:56:19',NULL,NULL,16,0,NULL,NULL),(9,'question_83631ade-f0dd-11e9-8abb-dcf505905f9b','Quelles sont les dimensions de la porte ?',3000,'radio','2019-10-17 14:56:19',NULL,NULL,16,0,NULL,NULL),(10,'question_83631af5-f0dd-11e9-8abb-dcf505905f9b','À quelle date pouvez-vous recevoir un jobbeur ?',4000,'date','2019-10-17 14:56:19',NULL,NULL,16,0,NULL,NULL),(11,'question_83631b0a-f0dd-11e9-8abb-dcf505905f9b','Quel créneau horaire vous conviendrait',5000,'checkbox','2019-10-17 14:56:19',NULL,NULL,16,0,NULL,NULL),(12,'question_83631b24-f0dd-11e9-8abb-dcf505905f9b','Avez vous autre chose a suggérer ?',6000,'text','2019-10-17 14:56:19',NULL,NULL,16,0,NULL,NULL),(13,'question_3bd039df-f0de-11e9-8abb-dcf505905f9b','Quelle est la référence du matériel à remplacer ?',1000,'text','2019-10-17 15:01:28',NULL,NULL,17,0,NULL,NULL),(14,'question_3bd03a45-f0de-11e9-8abb-dcf505905f9b','Dans quelle pièce se trouve ce matériel ?',2000,'select','2019-10-17 15:01:28',NULL,NULL,17,0,NULL,NULL),(15,'question_3bd03a68-f0de-11e9-8abb-dcf505905f9b','Quelles sont les dimensions de ce matériel ?',3000,'radio','2019-10-17 15:01:28',NULL,NULL,17,0,NULL,NULL),(16,'question_3bd03a82-f0de-11e9-8abb-dcf505905f9b','À quelle date pouvez-vous recevoir un jobbeur ?',4000,'date','2019-10-17 15:01:28',NULL,NULL,17,0,NULL,NULL),(17,'question_3bd03a9c-f0de-11e9-8abb-dcf505905f9b','Quel créneau horaire vous conviendrait',5000,'checkbox','2019-10-17 15:01:28',NULL,NULL,17,0,NULL,NULL),(18,'question_3bd03ab9-f0de-11e9-8abb-dcf505905f9b','Avez vous autre chose a suggérer ?',6000,'text','2019-10-17 15:01:28',NULL,NULL,17,0,NULL,NULL),(19,'question_4d4689c8-f0de-11e9-8abb-dcf505905f9b','Quelle est la référence du matériel à isoler ?',1000,'text','2019-10-17 15:01:58',NULL,NULL,18,0,NULL,NULL),(20,'question_4d468a42-f0de-11e9-8abb-dcf505905f9b','Dans quelle pièce se trouve ce matériel ?',2000,'select','2019-10-17 15:01:58',NULL,NULL,18,0,NULL,NULL),(21,'question_4d468a6d-f0de-11e9-8abb-dcf505905f9b','Quelles sont les dimensions de ce matériel ?',3000,'radio','2019-10-17 15:01:58',NULL,NULL,18,0,NULL,NULL),(22,'question_4d468a93-f0de-11e9-8abb-dcf505905f9b','À quelle date pouvez-vous recevoir un jobbeur ?',4000,'date','2019-10-17 15:01:58',NULL,NULL,18,0,NULL,NULL),(23,'question_4d468ac2-f0de-11e9-8abb-dcf505905f9b','Quel créneau horaire vous conviendrait',5000,'checkbox','2019-10-17 15:01:58',NULL,NULL,18,0,NULL,NULL),(24,'question_4d468ae6-f0de-11e9-8abb-dcf505905f9b','Avez vous autre chose a suggérer ?',6000,'text','2019-10-17 15:01:58',NULL,NULL,18,0,NULL,NULL),(25,'question_ab443180-f186-11e9-bf7f-dcf505905f9b','fake question 1',1000,'text','2019-10-18 11:07:11',1,'2019-10-18 11:07:11',NULL,0,NULL,NULL),(26,'question_ab44326e-f186-11e9-bf7f-dcf505905f9b','fake question 2',2000,'radio','2019-10-18 11:07:11',1,'2019-10-18 11:07:11',NULL,0,NULL,NULL),(27,NULL,'combien de mettre linéaire?',NULL,NULL,'2019-10-31 12:37:18',NULL,NULL,16,0,NULL,NULL),(28,NULL,'combien de mettre linéaire?',NULL,NULL,'2019-10-31 12:44:24',NULL,NULL,16,0,NULL,NULL),(29,NULL,'blabla',NULL,NULL,'2019-10-31 12:44:43',NULL,NULL,16,0,NULL,NULL),(30,NULL,'blablabla',NULL,NULL,'2019-10-31 12:45:10',NULL,NULL,16,0,NULL,NULL),(31,NULL,'combien de mettre linéaire?',NULL,NULL,'2019-10-31 12:47:45',NULL,NULL,16,0,NULL,NULL),(32,NULL,'blablabla',NULL,NULL,'2019-10-31 12:53:23',NULL,NULL,16,0,NULL,NULL),(33,NULL,'tototototo',NULL,NULL,'2019-10-31 13:09:49',NULL,NULL,16,0,NULL,NULL),(37,NULL,'tatatatat',NULL,'text','2019-10-31 14:27:50',0,NULL,1,0,'vous devez remplir ce champ',''),(38,NULL,'tetetetetetet',NULL,'checkbox','2019-10-31 14:28:30',0,NULL,1,0,'null',''),(39,NULL,'ghghghghh',NULL,NULL,'2019-10-31 14:29:26',NULL,NULL,1,0,NULL,NULL),(49,NULL,'Quelle heure est-il ?',NULL,'radio','2019-11-04 10:09:35',NULL,NULL,2,0,NULL,NULL);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response` (
  `responseId` int(11) NOT NULL AUTO_INCREMENT,
  `responseCode` tinytext,
  `responseLabel` varchar(60) DEFAULT NULL,
  `responseOrder` int(11) DEFAULT NULL,
  `responseCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `responseIsArchived` tinyint(1) DEFAULT NULL,
  `responseIsArchivedDateTime` datetime DEFAULT NULL,
  `response_questionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`responseId`),
  KEY `fk_response_1_idx` (`response_questionId`),
  CONSTRAINT `response_questionId` FOREIGN KEY (`response_questionId`) REFERENCES `question` (`questionId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response`
--

LOCK TABLES `response` WRITE;
/*!40000 ALTER TABLE `response` DISABLE KEYS */;
INSERT INTO `response` VALUES (1,'response_ec5d21fe-f0df-11e9-8abb-dcf505905f9b','chambre',1000,'2019-10-17 15:13:34',NULL,NULL,2),(2,'response_ec5d2280-f0df-11e9-8abb-dcf505905f9b','salon',2000,'2019-10-17 15:13:34',NULL,NULL,2),(3,'response_ec5d22af-f0df-11e9-8abb-dcf505905f9b','garage',3000,'2019-10-17 15:13:34',NULL,NULL,2),(4,'response_ec5d22d6-f0df-11e9-8abb-dcf505905f9b','extérieur',4000,'2019-10-17 15:13:34',NULL,NULL,2),(5,'response_ec5d22f8-f0df-11e9-8abb-dcf505905f9b','bois plein',1000,'2019-10-17 15:13:34',NULL,NULL,3),(6,'response_ec5d231f-f0df-11e9-8abb-dcf505905f9b','métal',2000,'2019-10-17 15:13:34',NULL,NULL,3),(7,'response_ec5d2346-f0df-11e9-8abb-dcf505905f9b','plastique',3000,'2019-10-17 15:13:34',NULL,NULL,3),(8,'response_ec5d236c-f0df-11e9-8abb-dcf505905f9b','contreplaqué',4000,'2019-10-17 15:13:34',NULL,NULL,3),(9,'response_ec5d23a4-f0df-11e9-8abb-dcf505905f9b','8H00-12H00',1000,'2019-10-17 15:13:34',NULL,NULL,5),(10,'response_ec5d23d0-f0df-11e9-8abb-dcf505905f9b','12H00-14H00',2000,'2019-10-17 15:13:34',NULL,NULL,5),(11,'response_ec5d23f7-f0df-11e9-8abb-dcf505905f9b','14H00-18H00',3000,'2019-10-17 15:13:34',NULL,NULL,5),(12,'response_ec5d2433-f0df-11e9-8abb-dcf505905f9b','18H00-20H00',4000,'2019-10-17 15:13:34',NULL,NULL,5),(13,'response_d012f97f-f0e0-11e9-8abb-dcf505905f9b','chambre',1000,'2019-10-17 15:19:56',NULL,NULL,8),(14,'response_d012f9b8-f0e0-11e9-8abb-dcf505905f9b','salon',2000,'2019-10-17 15:19:56',NULL,NULL,8),(15,'response_d012f9c9-f0e0-11e9-8abb-dcf505905f9b','garage',3000,'2019-10-17 15:19:56',NULL,NULL,8),(16,'response_d012f9da-f0e0-11e9-8abb-dcf505905f9b','extérieur',4000,'2019-10-17 15:19:56',NULL,NULL,8),(17,'response_d012f9e9-f0e0-11e9-8abb-dcf505905f9b','50cm X 100cm',1000,'2019-10-17 15:19:56',NULL,NULL,9),(18,'response_d012f9f6-f0e0-11e9-8abb-dcf505905f9b','100cm X 150cm',2000,'2019-10-17 15:19:56',NULL,NULL,9),(19,'response_d012fa06-f0e0-11e9-8abb-dcf505905f9b','150cm X 200cm',3000,'2019-10-17 15:19:56',NULL,NULL,9),(20,'response_d012fa14-f0e0-11e9-8abb-dcf505905f9b','200cm X 250cm',4000,'2019-10-17 15:19:56',NULL,NULL,9),(21,'response_d012fa2f-f0e0-11e9-8abb-dcf505905f9b','8H00-12H00',1000,'2019-10-17 15:19:56',NULL,NULL,11),(22,'response_d012fa3e-f0e0-11e9-8abb-dcf505905f9b','12H00-14H00',2000,'2019-10-17 15:19:56',NULL,NULL,11),(23,'response_d012fa4e-f0e0-11e9-8abb-dcf505905f9b','14H00-18H00',3000,'2019-10-17 15:19:56',NULL,NULL,11),(24,'response_d012fa65-f0e0-11e9-8abb-dcf505905f9b','18H00-20H00',4000,'2019-10-17 15:19:56',NULL,NULL,11),(25,'response_e7841cc4-f0e3-11e9-8abb-dcf505905f9b','chambre',1000,'2019-10-17 15:42:04',NULL,NULL,14),(26,'response_e7841d1e-f0e3-11e9-8abb-dcf505905f9b','salon',2000,'2019-10-17 15:42:04',NULL,NULL,14),(27,'response_e7841d3d-f0e3-11e9-8abb-dcf505905f9b','garage',3000,'2019-10-17 15:42:04',NULL,NULL,14),(28,'response_e7841d55-f0e3-11e9-8abb-dcf505905f9b','extérieur',4000,'2019-10-17 15:42:04',NULL,NULL,14),(29,'response_e7841d6c-f0e3-11e9-8abb-dcf505905f9b','50cm X 100cm',1000,'2019-10-17 15:42:04',NULL,NULL,15),(30,'response_e7841d82-f0e3-11e9-8abb-dcf505905f9b','100cm X 150cm',2000,'2019-10-17 15:42:04',NULL,NULL,15),(31,'response_e7841d96-f0e3-11e9-8abb-dcf505905f9b','150cm X 200cm',3000,'2019-10-17 15:42:04',NULL,NULL,15),(32,'response_e7841db6-f0e3-11e9-8abb-dcf505905f9b','200cm X 250cm',4000,'2019-10-17 15:42:04',NULL,NULL,15),(33,'response_e7841dce-f0e3-11e9-8abb-dcf505905f9b','8H00-12H00',1000,'2019-10-17 15:42:04',NULL,NULL,17),(34,'response_e7841dee-f0e3-11e9-8abb-dcf505905f9b','12H00-14H00',2000,'2019-10-17 15:42:04',NULL,NULL,17),(35,'response_e7841e15-f0e3-11e9-8abb-dcf505905f9b','14H00-18H00',3000,'2019-10-17 15:42:04',NULL,NULL,17),(36,'response_e7841e31-f0e3-11e9-8abb-dcf505905f9b','18H00-20H00',4000,'2019-10-17 15:42:04',NULL,NULL,17),(37,'response_20122659-f0e4-11e9-8abb-dcf505905f9b','chambre',1000,'2019-10-17 15:43:39',NULL,NULL,20),(38,'response_201226af-f0e4-11e9-8abb-dcf505905f9b','salon',2000,'2019-10-17 15:43:39',NULL,NULL,20),(39,'response_201226ca-f0e4-11e9-8abb-dcf505905f9b','garage',3000,'2019-10-17 15:43:39',NULL,NULL,20),(40,'response_201226e3-f0e4-11e9-8abb-dcf505905f9b','extérieur',4000,'2019-10-17 15:43:39',NULL,NULL,20),(41,'response_201226fc-f0e4-11e9-8abb-dcf505905f9b','50cm X 100cm',1000,'2019-10-17 15:43:39',NULL,NULL,21),(42,'response_20122712-f0e4-11e9-8abb-dcf505905f9b','100cm X 150cm',2000,'2019-10-17 15:43:39',NULL,NULL,21),(43,'response_20122728-f0e4-11e9-8abb-dcf505905f9b','150cm X 200cm',3000,'2019-10-17 15:43:39',NULL,NULL,21),(44,'response_2012273d-f0e4-11e9-8abb-dcf505905f9b','200cm X 250cm',4000,'2019-10-17 15:43:39',NULL,NULL,21),(45,'response_20122762-f0e4-11e9-8abb-dcf505905f9b','8H00-12H00',1000,'2019-10-17 15:43:39',NULL,NULL,23),(46,'response_20122779-f0e4-11e9-8abb-dcf505905f9b','12H00-14H00',2000,'2019-10-17 15:43:39',NULL,NULL,23),(47,'response_2012278d-f0e4-11e9-8abb-dcf505905f9b','14H00-18H00',3000,'2019-10-17 15:43:39',NULL,NULL,23),(48,'response_201227a3-f0e4-11e9-8abb-dcf505905f9b','18H00-20H00',4000,'2019-10-17 15:43:39',NULL,NULL,23),(49,'response_fcc02383-f186-11e9-bf7f-dcf505905f9b','fake response 1',1000,'2019-10-18 11:09:27',1,'2019-10-18 11:09:27',NULL),(50,'response_fcc02414-f186-11e9-bf7f-dcf505905f9b','fake response 2',2000,'2019-10-18 11:09:27',1,'2019-10-18 11:09:27',NULL),(58,'response_987e7b1b-feef-11e9-a92f-dcf505905f9b','il est midi',NULL,'2019-11-04 11:41:02',NULL,NULL,49),(59,'response_9c2e094f-00a1-11ea-bf78-dcf505905f9b','a midi',NULL,'2019-11-06 15:27:49',NULL,NULL,38);
/*!40000 ALTER TABLE `response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `reviewId` int(11) NOT NULL AUTO_INCREMENT,
  `reviewCode` tinytext,
  `reviewLabel` varchar(30) DEFAULT NULL,
  `reviewContent` varchar(45) DEFAULT NULL,
  `review_userId` int(11) DEFAULT NULL,
  `review_proId` int(11) DEFAULT NULL,
  `reviewCreationDateTime` datetime DEFAULT NULL,
  `reviewIsArchived` tinyint(1) DEFAULT NULL,
  `reviewIsArchivedDateT ime` datetime DEFAULT NULL,
  PRIMARY KEY (`reviewId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userCode` tinytext,
  `userFirstname` varchar(30) DEFAULT NULL,
  `userLastname` varchar(30) DEFAULT NULL,
  `userMail` varchar(100) DEFAULT NULL,
  `userMobileNumber` int(11) DEFAULT NULL,
  `userAge` int(11) DEFAULT NULL,
  `userPassword` varchar(30) DEFAULT NULL,
  `userCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `userIsArchived` tinyint(1) DEFAULT '0',
  `userIsArchivedDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (3,'user_af6ade51-f0c3-11e9-8abb-dcf505905f9b','Clément','Garland','g.c@gmail.com',239471754,23,'keepapi','2019-10-17 11:51:26',NULL,NULL),(4,'user_af6ade79-f0c3-11e9-8abb-dcf505905f9b','Kévin','Bocquet','g.c@gmail.com',239471754,36,'keepapi','2019-10-17 11:51:26',NULL,NULL),(5,'user_af6ade8f-f0c3-11e9-8abb-dcf505905f9b','Thibaut','Leroy','thib@gmail.com',239471754,37,'keepapi','2019-10-17 11:51:26',NULL,NULL),(10,NULL,'toto',NULL,NULL,NULL,NULL,NULL,'2019-10-25 16:19:38',0,NULL),(11,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'azes','2019-11-06 17:03:17',0,NULL),(12,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'sqdcsqsq','2019-11-06 17:11:59',0,NULL),(13,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'dsfdsfsdf','2019-11-06 17:19:47',0,NULL),(14,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'cxwcwxc','2019-11-06 17:20:08',0,NULL),(15,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'wcxxwc','2019-11-06 17:20:34',0,NULL),(16,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'xsqcsqc','2019-11-06 17:21:13',0,NULL),(17,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'csdvsdv','2019-11-06 17:27:56',0,NULL),(18,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'cxwcxwc','2019-11-06 17:30:10',0,NULL),(19,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'xsqdq','2019-11-06 17:33:06',0,NULL),(20,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'xw<xsq','2019-11-06 17:33:30',0,NULL),(21,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'wssqqc','2019-11-06 17:35:19',0,NULL),(22,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'kjlkj','2019-11-07 10:30:46',0,NULL),(23,NULL,NULL,NULL,'rererererer@toto.com',NULL,NULL,'qsd','2019-11-07 10:39:27',0,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'jobbyDB'
--

--
-- Dumping routines for database 'jobbyDB'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-08 10:50:34
