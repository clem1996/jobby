CREATE DATABASE  IF NOT EXISTS `jobbyDB` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `jobbyDB`;
-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: jobbyDB
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.19.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `addressId` int(11) NOT NULL AUTO_INCREMENT,
  `addressCode` tinytext,
  `addressStreetNumber` varchar(10) DEFAULT NULL,
  `addressStreetLabel` tinytext,
  `addressZipCode` int(11) DEFAULT NULL,
  `addressCityLabel` varchar(100) DEFAULT NULL,
  `addressRegionLabel` varchar(30) DEFAULT NULL,
  `addressDepartmentLabel` varchar(30) DEFAULT NULL,
  `address_userId` int(11) DEFAULT NULL,
  `address_proId` int(11) DEFAULT NULL,
  PRIMARY KEY (`addressId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `answerId` int(11) NOT NULL AUTO_INCREMENT,
  `answerCode` tinytext,
  `answerLabel` varchar(30) DEFAULT NULL,
  `answerIsArchived` tinyint(1) DEFAULT NULL,
  `answerIsArchivedDateTime` datetime DEFAULT NULL,
  `answer_responseId` int(11) DEFAULT NULL,
  PRIMARY KEY (`answerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryParentId` int(11) DEFAULT NULL,
  `categoryCode` tinytext,
  `categoryLabel` varchar(30) DEFAULT NULL,
  `categoryImageLink` tinytext,
  `categoryCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `categoryIsArchived` tinyint(1) DEFAULT NULL,
  `categoryIsArchivedDateTime` datetime DEFAULT NULL,
  `categoryPopularity` int(11) DEFAULT NULL,
  `categoryIsService` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,0,'category_7231b48b-f0c9-11e9-8abb-dcf505905f9b','Bricolage','https://i.imgur.com/1XrPTg8.jpg','2019-10-17 12:32:40',NULL,NULL,3,0),(2,0,'category_7231b4db-f0c9-11e9-8abb-dcf505905f9b','Salle de bain','https://i.imgur.com/ZHRwHdd.jpg','2019-10-17 12:32:40',NULL,NULL,4,0),(3,0,'category_7231b4f8-f0c9-11e9-8abb-dcf505905f9b','Décoration','https://i.imgur.com/Mi4pngk.jpg','2019-10-17 12:32:40',NULL,NULL,1,0),(4,0,'category_7231b512-f0c9-11e9-8abb-dcf505905f9b','Électricité','https://i.imgur.com/QEbLW7T.jpg','2019-10-17 12:32:40',NULL,NULL,5,0),(5,0,'category_7231b52c-f0c9-11e9-8abb-dcf505905f9b','Portes & fenêtres','https://i.imgur.com/mq22efW.jpg','2019-10-17 12:32:40',NULL,NULL,5,0),(6,0,'category_7231b561-f0c9-11e9-8abb-dcf505905f9b','Jardinage','https://i.imgur.com/qS43pNV.jpg','2019-10-17 12:32:40',NULL,NULL,2,0),(7,0,'category_7231b593-f0c9-11e9-8abb-dcf505905f9b','Soutien scolaire','https://i.imgur.com/GdSFShm.jpg','2019-10-17 12:32:40',NULL,NULL,3,0),(8,0,'category_7231b5b4-f0c9-11e9-8abb-dcf505905f9b','Peinture','https://i.imgur.com/DuxKyAi.jpg','2019-10-17 12:32:40',NULL,NULL,1,0),(9,0,'category_7231b5d3-f0c9-11e9-8abb-dcf505905f9b','Livraison','https://i.imgur.com/y1KWeES.jpg','2019-10-17 12:32:40',NULL,NULL,0,0),(10,0,'category_7231b5ed-f0c9-11e9-8abb-dcf505905f9b','Transport','https://i.imgur.com/BZedtDL.jpg','2019-10-17 12:32:40',NULL,NULL,3,0),(11,0,'category_7231b609-f0c9-11e9-8abb-dcf505905f9b','Multimedia','https://i.imgur.com/I6GSsT9.jpg','2019-10-17 12:32:40',NULL,NULL,2,0),(12,0,'category_7231b620-f0c9-11e9-8abb-dcf505905f9b','Loisirs','https://i.imgur.com/msDoqyz.jpg','2019-10-17 12:32:40',NULL,NULL,4,0),(13,5,'category_280dde56-f0cd-11e9-8abb-dcf505905f9b','Portes','https://i.imgur.com/fBLuHTd.jpg','2019-10-17 12:59:14',NULL,NULL,3,0),(14,5,'category_280ddea7-f0cd-11e9-8abb-dcf505905f9b','fenêtres',' https://i.imgur.com/iSBS4GY.jpg','2019-10-17 12:59:14',NULL,NULL,5,0),(15,13,'category_280ddebf-f0cd-11e9-8abb-dcf505905f9b','Montage','https://i.imgur.com/wD1g5A0.jpg','2019-10-17 12:59:14',NULL,NULL,1,1),(16,13,'category_280dded7-f0cd-11e9-8abb-dcf505905f9b','Démontage','https://i.imgur.com/d9MS2rQ.jpg','2019-10-17 12:59:14',NULL,NULL,2,1),(17,14,'category_280ddeeb-f0cd-11e9-8abb-dcf505905f9b','Remplacement','https://i.imgur.com/NaRauS0.png','2019-10-17 12:59:14',NULL,NULL,4,1),(18,14,'category_280ddf02-f0cd-11e9-8abb-dcf505905f9b','Isolation','https://i.imgur.com/L1iVvJT.jpg','2019-10-17 12:59:14',NULL,NULL,3,1),(19,0,'category_b6629a2a-f185-11e9-bf7f-dcf505905f9b','Bidon 1','','2019-10-18 11:00:20',1,'2019-10-18 11:00:20',3,0),(20,0,'category_b896ff9f-f185-11e9-bf7f-dcf505905f9b','bidon 2','','2019-10-18 11:00:24',1,'2019-10-18 11:00:24',3,0);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `demand`
--

DROP TABLE IF EXISTS `demand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demand` (
  `demandId` int(11) NOT NULL AUTO_INCREMENT,
  `demandCode` tinytext,
  `demandLabel` tinytext,
  `demandCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `demandIsArchived` tinyint(1) DEFAULT NULL,
  `demandIsArchivedDateTime` datetime DEFAULT NULL,
  `demand_userId` int(11) DEFAULT NULL,
  `demand_categoryId` int(11) DEFAULT NULL,
  PRIMARY KEY (`demandId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demand`
--

LOCK TABLES `demand` WRITE;
/*!40000 ALTER TABLE `demand` DISABLE KEYS */;
INSERT INTO `demand` VALUES (1,'demand_e0789aa2-f182-11e9-bf7f-dcf505905f9b','fake demand','2019-10-18 10:40:02',NULL,NULL,3,1),(2,'demand_3163ea60-f183-11e9-bf7f-dcf505905f9b','fake demand','2019-10-18 10:42:18',NULL,NULL,4,1),(3,'demand_3163eab4-f183-11e9-bf7f-dcf505905f9b','fake demand','2019-10-18 10:42:18',NULL,NULL,5,1);
/*!40000 ALTER TABLE `demand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `demandQuestion`
--

DROP TABLE IF EXISTS `demandQuestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demandQuestion` (
  `demandQuestionId` int(11) NOT NULL,
  `demandQuestion_demandId` int(11) DEFAULT NULL,
  `demandQuestion_questionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`demandQuestionId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demandQuestion`
--

LOCK TABLES `demandQuestion` WRITE;
/*!40000 ALTER TABLE `demandQuestion` DISABLE KEYS */;
/*!40000 ALTER TABLE `demandQuestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `footer`
--

DROP TABLE IF EXISTS `footer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footer` (
  `footerId` int(11) NOT NULL AUTO_INCREMENT,
  `footerCode` tinytext,
  `footerItemLabel` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`footerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `footer`
--

LOCK TABLES `footer` WRITE;
/*!40000 ALTER TABLE `footer` DISABLE KEYS */;
/*!40000 ALTER TABLE `footer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `menuId` int(11) NOT NULL AUTO_INCREMENT,
  `menuCode` tinytext,
  `menuItemLabel` varchar(20) DEFAULT NULL,
  `menuItemLinkTo` tinytext,
  PRIMARY KEY (`menuId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pro`
--

DROP TABLE IF EXISTS `pro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro` (
  `proId` int(11) NOT NULL AUTO_INCREMENT,
  `proCode` tinytext,
  `proLabel` varchar(30) DEFAULT NULL,
  `proFirstname` varchar(30) DEFAULT NULL,
  `proLastname` varchar(30) DEFAULT NULL,
  `proMail` varchar(100) DEFAULT NULL,
  `proMobileNumber` int(11) DEFAULT NULL,
  `proPassword` varchar(30) DEFAULT NULL,
  `proAge` varchar(45) DEFAULT NULL,
  `proCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `proIsArchived` tinyint(1) DEFAULT NULL,
  `proIsArchivedDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`proId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro`
--

LOCK TABLES `pro` WRITE;
/*!40000 ALTER TABLE `pro` DISABLE KEYS */;
/*!40000 ALTER TABLE `pro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proProfession`
--

DROP TABLE IF EXISTS `proProfession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proProfession` (
  `proProfessionId` int(11) NOT NULL,
  `proProfession_proId` int(11) DEFAULT NULL,
  `proProfession_professionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`proProfessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proProfession`
--

LOCK TABLES `proProfession` WRITE;
/*!40000 ALTER TABLE `proProfession` DISABLE KEYS */;
/*!40000 ALTER TABLE `proProfession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profession`
--

DROP TABLE IF EXISTS `profession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profession` (
  `professionId` int(11) NOT NULL AUTO_INCREMENT,
  `professionCode` tinytext,
  `professionLabel` varchar(30) DEFAULT NULL,
  `professionCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `professionIsArchived` tinyint(1) DEFAULT NULL,
  `professionIsArchivedDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`professionId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profession`
--

LOCK TABLES `profession` WRITE;
/*!40000 ALTER TABLE `profession` DISABLE KEYS */;
/*!40000 ALTER TABLE `profession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `questionId` int(11) NOT NULL AUTO_INCREMENT,
  `questionCode` tinytext,
  `questionLabel` varchar(100) DEFAULT NULL,
  `questionOrder` int(11) DEFAULT NULL,
  `questionType` varchar(30) DEFAULT NULL,
  `questionCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `questionIsArchived` tinyint(1) DEFAULT NULL,
  `questionIsArchivedDateTime` datetime DEFAULT NULL,
  `question_categoryId` int(11) DEFAULT NULL,
  PRIMARY KEY (`questionId`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'question_f22d5298-f0dc-11e9-8abb-dcf505905f9b','Désirez-vous un modèle en particulier ?',1000,'text','2019-10-17 14:52:15',NULL,NULL,15),(2,'question_f22d5316-f0dc-11e9-8abb-dcf505905f9b','Dans quelle pièce souhaitez vous mettre une porte ?',2000,'select','2019-10-17 14:52:15',NULL,NULL,15),(3,'question_f22d533b-f0dc-11e9-8abb-dcf505905f9b','Quel type de matériau désirez-vous ?',3000,'radio','2019-10-17 14:52:15',NULL,NULL,15),(4,'question_f22d5355-f0dc-11e9-8abb-dcf505905f9b','À quelle date pouvez-vous recevoir un jobbeur ?',4000,'date','2019-10-17 14:52:15',NULL,NULL,15),(5,'question_f22d536a-f0dc-11e9-8abb-dcf505905f9b','Quel créneau horaire vous conviendrait',5000,'checkbox','2019-10-17 14:52:15',NULL,NULL,15),(6,'question_f22d5385-f0dc-11e9-8abb-dcf505905f9b','Avez vous autre chose a suggérer ?',6000,'text','2019-10-17 14:52:15',NULL,NULL,15),(7,'question_83631a69-f0dd-11e9-8abb-dcf505905f9b','Quelle est la référence du matériel à démonter ?',1000,'text','2019-10-17 14:56:19',NULL,NULL,16),(8,'question_83631abf-f0dd-11e9-8abb-dcf505905f9b','Dans quelle pièce se trouve cette porte ?',2000,'select','2019-10-17 14:56:19',NULL,NULL,16),(9,'question_83631ade-f0dd-11e9-8abb-dcf505905f9b','Quelles sont les dimensions de la porte ?',3000,'radio','2019-10-17 14:56:19',NULL,NULL,16),(10,'question_83631af5-f0dd-11e9-8abb-dcf505905f9b','À quelle date pouvez-vous recevoir un jobbeur ?',4000,'date','2019-10-17 14:56:19',NULL,NULL,16),(11,'question_83631b0a-f0dd-11e9-8abb-dcf505905f9b','Quel créneau horaire vous conviendrait',5000,'checkbox','2019-10-17 14:56:19',NULL,NULL,16),(12,'question_83631b24-f0dd-11e9-8abb-dcf505905f9b','Avez vous autre chose a suggérer ?',6000,'text','2019-10-17 14:56:19',NULL,NULL,16),(13,'question_3bd039df-f0de-11e9-8abb-dcf505905f9b','Quelle est la référence du matériel à remplacer ?',1000,'text','2019-10-17 15:01:28',NULL,NULL,17),(14,'question_3bd03a45-f0de-11e9-8abb-dcf505905f9b','Dans quelle pièce se trouve ce matériel ?',2000,'select','2019-10-17 15:01:28',NULL,NULL,17),(15,'question_3bd03a68-f0de-11e9-8abb-dcf505905f9b','Quelles sont les dimensions de ce matériel ?',3000,'radio','2019-10-17 15:01:28',NULL,NULL,17),(16,'question_3bd03a82-f0de-11e9-8abb-dcf505905f9b','À quelle date pouvez-vous recevoir un jobbeur ?',4000,'date','2019-10-17 15:01:28',NULL,NULL,17),(17,'question_3bd03a9c-f0de-11e9-8abb-dcf505905f9b','Quel créneau horaire vous conviendrait',5000,'checkbox','2019-10-17 15:01:28',NULL,NULL,17),(18,'question_3bd03ab9-f0de-11e9-8abb-dcf505905f9b','Avez vous autre chose a suggérer ?',6000,'text','2019-10-17 15:01:28',NULL,NULL,17),(19,'question_4d4689c8-f0de-11e9-8abb-dcf505905f9b','Quelle est la référence du matériel à isoler ?',1000,'text','2019-10-17 15:01:58',NULL,NULL,18),(20,'question_4d468a42-f0de-11e9-8abb-dcf505905f9b','Dans quelle pièce se trouve ce matériel ?',2000,'select','2019-10-17 15:01:58',NULL,NULL,18),(21,'question_4d468a6d-f0de-11e9-8abb-dcf505905f9b','Quelles sont les dimensions de ce matériel ?',3000,'radio','2019-10-17 15:01:58',NULL,NULL,18),(22,'question_4d468a93-f0de-11e9-8abb-dcf505905f9b','À quelle date pouvez-vous recevoir un jobbeur ?',4000,'date','2019-10-17 15:01:58',NULL,NULL,18),(23,'question_4d468ac2-f0de-11e9-8abb-dcf505905f9b','Quel créneau horaire vous conviendrait',5000,'checkbox','2019-10-17 15:01:58',NULL,NULL,18),(24,'question_4d468ae6-f0de-11e9-8abb-dcf505905f9b','Avez vous autre chose a suggérer ?',6000,'text','2019-10-17 15:01:58',NULL,NULL,18),(25,'question_ab443180-f186-11e9-bf7f-dcf505905f9b','fake question 1',1000,'text','2019-10-18 11:07:11',1,'2019-10-18 11:07:11',NULL),(26,'question_ab44326e-f186-11e9-bf7f-dcf505905f9b','fake question 2',2000,'radio','2019-10-18 11:07:11',1,'2019-10-18 11:07:11',NULL);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response` (
  `responseId` int(11) NOT NULL AUTO_INCREMENT,
  `responseCode` tinytext,
  `responseLabel` varchar(60) DEFAULT NULL,
  `responseOrder` int(11) DEFAULT NULL,
  `responseCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `responseIsArchived` tinyint(1) DEFAULT NULL,
  `responseIsArchivedDateTime` datetime DEFAULT NULL,
  `response_questionId` int(11) DEFAULT NULL,
  PRIMARY KEY (`responseId`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response`
--

LOCK TABLES `response` WRITE;
/*!40000 ALTER TABLE `response` DISABLE KEYS */;
INSERT INTO `response` VALUES (1,'response_ec5d21fe-f0df-11e9-8abb-dcf505905f9b','chambre',1000,'2019-10-17 15:13:34',NULL,NULL,2),(2,'response_ec5d2280-f0df-11e9-8abb-dcf505905f9b','salon',2000,'2019-10-17 15:13:34',NULL,NULL,2),(3,'response_ec5d22af-f0df-11e9-8abb-dcf505905f9b','garage',3000,'2019-10-17 15:13:34',NULL,NULL,2),(4,'response_ec5d22d6-f0df-11e9-8abb-dcf505905f9b','extérieur',4000,'2019-10-17 15:13:34',NULL,NULL,2),(5,'response_ec5d22f8-f0df-11e9-8abb-dcf505905f9b','bois plein',1000,'2019-10-17 15:13:34',NULL,NULL,3),(6,'response_ec5d231f-f0df-11e9-8abb-dcf505905f9b','métal',2000,'2019-10-17 15:13:34',NULL,NULL,3),(7,'response_ec5d2346-f0df-11e9-8abb-dcf505905f9b','plastique',3000,'2019-10-17 15:13:34',NULL,NULL,3),(8,'response_ec5d236c-f0df-11e9-8abb-dcf505905f9b','contreplaqué',4000,'2019-10-17 15:13:34',NULL,NULL,3),(9,'response_ec5d23a4-f0df-11e9-8abb-dcf505905f9b','8H00-12H00',1000,'2019-10-17 15:13:34',NULL,NULL,5),(10,'response_ec5d23d0-f0df-11e9-8abb-dcf505905f9b','12H00-14H00',2000,'2019-10-17 15:13:34',NULL,NULL,5),(11,'response_ec5d23f7-f0df-11e9-8abb-dcf505905f9b','14H00-18H00',3000,'2019-10-17 15:13:34',NULL,NULL,5),(12,'response_ec5d2433-f0df-11e9-8abb-dcf505905f9b','18H00-20H00',4000,'2019-10-17 15:13:34',NULL,NULL,5),(13,'response_d012f97f-f0e0-11e9-8abb-dcf505905f9b','chambre',1000,'2019-10-17 15:19:56',NULL,NULL,8),(14,'response_d012f9b8-f0e0-11e9-8abb-dcf505905f9b','salon',2000,'2019-10-17 15:19:56',NULL,NULL,8),(15,'response_d012f9c9-f0e0-11e9-8abb-dcf505905f9b','garage',3000,'2019-10-17 15:19:56',NULL,NULL,8),(16,'response_d012f9da-f0e0-11e9-8abb-dcf505905f9b','extérieur',4000,'2019-10-17 15:19:56',NULL,NULL,8),(17,'response_d012f9e9-f0e0-11e9-8abb-dcf505905f9b','50cm X 100cm',1000,'2019-10-17 15:19:56',NULL,NULL,9),(18,'response_d012f9f6-f0e0-11e9-8abb-dcf505905f9b','100cm X 150cm',2000,'2019-10-17 15:19:56',NULL,NULL,9),(19,'response_d012fa06-f0e0-11e9-8abb-dcf505905f9b','150cm X 200cm',3000,'2019-10-17 15:19:56',NULL,NULL,9),(20,'response_d012fa14-f0e0-11e9-8abb-dcf505905f9b','200cm X 250cm',4000,'2019-10-17 15:19:56',NULL,NULL,9),(21,'response_d012fa2f-f0e0-11e9-8abb-dcf505905f9b','8H00-12H00',1000,'2019-10-17 15:19:56',NULL,NULL,11),(22,'response_d012fa3e-f0e0-11e9-8abb-dcf505905f9b','12H00-14H00',2000,'2019-10-17 15:19:56',NULL,NULL,11),(23,'response_d012fa4e-f0e0-11e9-8abb-dcf505905f9b','14H00-18H00',3000,'2019-10-17 15:19:56',NULL,NULL,11),(24,'response_d012fa65-f0e0-11e9-8abb-dcf505905f9b','18H00-20H00',4000,'2019-10-17 15:19:56',NULL,NULL,11),(25,'response_e7841cc4-f0e3-11e9-8abb-dcf505905f9b','chambre',1000,'2019-10-17 15:42:04',NULL,NULL,14),(26,'response_e7841d1e-f0e3-11e9-8abb-dcf505905f9b','salon',2000,'2019-10-17 15:42:04',NULL,NULL,14),(27,'response_e7841d3d-f0e3-11e9-8abb-dcf505905f9b','garage',3000,'2019-10-17 15:42:04',NULL,NULL,14),(28,'response_e7841d55-f0e3-11e9-8abb-dcf505905f9b','extérieur',4000,'2019-10-17 15:42:04',NULL,NULL,14),(29,'response_e7841d6c-f0e3-11e9-8abb-dcf505905f9b','50cm X 100cm',1000,'2019-10-17 15:42:04',NULL,NULL,15),(30,'response_e7841d82-f0e3-11e9-8abb-dcf505905f9b','100cm X 150cm',2000,'2019-10-17 15:42:04',NULL,NULL,15),(31,'response_e7841d96-f0e3-11e9-8abb-dcf505905f9b','150cm X 200cm',3000,'2019-10-17 15:42:04',NULL,NULL,15),(32,'response_e7841db6-f0e3-11e9-8abb-dcf505905f9b','200cm X 250cm',4000,'2019-10-17 15:42:04',NULL,NULL,15),(33,'response_e7841dce-f0e3-11e9-8abb-dcf505905f9b','8H00-12H00',1000,'2019-10-17 15:42:04',NULL,NULL,17),(34,'response_e7841dee-f0e3-11e9-8abb-dcf505905f9b','12H00-14H00',2000,'2019-10-17 15:42:04',NULL,NULL,17),(35,'response_e7841e15-f0e3-11e9-8abb-dcf505905f9b','14H00-18H00',3000,'2019-10-17 15:42:04',NULL,NULL,17),(36,'response_e7841e31-f0e3-11e9-8abb-dcf505905f9b','18H00-20H00',4000,'2019-10-17 15:42:04',NULL,NULL,17),(37,'response_20122659-f0e4-11e9-8abb-dcf505905f9b','chambre',1000,'2019-10-17 15:43:39',NULL,NULL,20),(38,'response_201226af-f0e4-11e9-8abb-dcf505905f9b','salon',2000,'2019-10-17 15:43:39',NULL,NULL,20),(39,'response_201226ca-f0e4-11e9-8abb-dcf505905f9b','garage',3000,'2019-10-17 15:43:39',NULL,NULL,20),(40,'response_201226e3-f0e4-11e9-8abb-dcf505905f9b','extérieur',4000,'2019-10-17 15:43:39',NULL,NULL,20),(41,'response_201226fc-f0e4-11e9-8abb-dcf505905f9b','50cm X 100cm',1000,'2019-10-17 15:43:39',NULL,NULL,21),(42,'response_20122712-f0e4-11e9-8abb-dcf505905f9b','100cm X 150cm',2000,'2019-10-17 15:43:39',NULL,NULL,21),(43,'response_20122728-f0e4-11e9-8abb-dcf505905f9b','150cm X 200cm',3000,'2019-10-17 15:43:39',NULL,NULL,21),(44,'response_2012273d-f0e4-11e9-8abb-dcf505905f9b','200cm X 250cm',4000,'2019-10-17 15:43:39',NULL,NULL,21),(45,'response_20122762-f0e4-11e9-8abb-dcf505905f9b','8H00-12H00',1000,'2019-10-17 15:43:39',NULL,NULL,23),(46,'response_20122779-f0e4-11e9-8abb-dcf505905f9b','12H00-14H00',2000,'2019-10-17 15:43:39',NULL,NULL,23),(47,'response_2012278d-f0e4-11e9-8abb-dcf505905f9b','14H00-18H00',3000,'2019-10-17 15:43:39',NULL,NULL,23),(48,'response_201227a3-f0e4-11e9-8abb-dcf505905f9b','18H00-20H00',4000,'2019-10-17 15:43:39',NULL,NULL,23),(49,'response_fcc02383-f186-11e9-bf7f-dcf505905f9b','fake response 1',1000,'2019-10-18 11:09:27',1,'2019-10-18 11:09:27',NULL),(50,'response_fcc02414-f186-11e9-bf7f-dcf505905f9b','fake response 2',2000,'2019-10-18 11:09:27',1,'2019-10-18 11:09:27',NULL);
/*!40000 ALTER TABLE `response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `reviewId` int(11) NOT NULL AUTO_INCREMENT,
  `reviewCode` tinytext,
  `reviewLabel` varchar(30) DEFAULT NULL,
  `reviewContent` varchar(45) DEFAULT NULL,
  `review_userId` int(11) DEFAULT NULL,
  `review_proId` int(11) DEFAULT NULL,
  `reviewCreationDateTime` datetime DEFAULT NULL,
  `reviewIsArchived` tinyint(1) DEFAULT NULL,
  `reviewIsArchivedDateT ime` datetime DEFAULT NULL,
  PRIMARY KEY (`reviewId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userCode` tinytext,
  `userFirstname` varchar(30) DEFAULT NULL,
  `userLastname` varchar(30) DEFAULT NULL,
  `userMail` varchar(100) DEFAULT NULL,
  `userMobileNumber` int(11) DEFAULT NULL,
  `userAge` int(11) DEFAULT NULL,
  `userPassword` varchar(30) DEFAULT NULL,
  `userCreationDateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `userIsArchived` tinyint(1) DEFAULT NULL,
  `userIsArchivedDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (3,'user_af6ade51-f0c3-11e9-8abb-dcf505905f9b','Clément','Garland','g.c@gmail.com',239471754,23,'keepapi','2019-10-17 11:51:26',NULL,NULL),(4,'user_af6ade79-f0c3-11e9-8abb-dcf505905f9b','Kévin','Bocquet','g.c@gmail.com',239471754,36,'keepapi','2019-10-17 11:51:26',NULL,NULL),(5,'user_af6ade8f-f0c3-11e9-8abb-dcf505905f9b','Thibaut','Leroy','thib@gmail.com',239471754,37,'keepapi','2019-10-17 11:51:26',NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-18 14:11:05
