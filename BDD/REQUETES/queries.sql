-- concat("user_", uuid())
-- create user 
INSERT INTO jobbyDB.user (userCode, userFirstname, userLastname, userMail, userMobileNumber, userAge, userPassword)
VALUES (concat("user_", uuid()), "Clément", "Garland", "g.c@gmail.com", "0239471754", "23", "keepapi");
-- read user
SELECT * FROM jobbyDB.user;
-- delete user where userId = 1
DELETE FROM jobbyDB.user WHERE userId = 2;
-- create users 
INSERT INTO jobbyDB.user (userCode, userFirstname, userLastname, userMail, userMobileNumber, userAge, userPassword)
VALUES 
(concat("user_", uuid()), "Clément", "Garland", "g.c@gmail.com", "0239471754", "23", "keepapi"),
(concat("user_", uuid()), "Kévin", "Bocquet", "g.c@gmail.com", "0239471754", "36", "keepapi"),
(concat("user_", uuid()), "Thibaut", "Leroy", "g.c@gmail.com", "0239471754", "37", "keepapi");
-- update user where userId = 5
UPDATE jobbyDB.user SET userMail = "thib@gmail.com" WHERE userId = 5;
-- count users (number of tuple)
SELECT COUNT(*) FROM jobbyDB.user;
-- count users (number of tuple) with an alias
SELECT COUNT(*) AS AllMyUsers FROM jobbyDB.user;

-- create categories 
INSERT INTO jobbyDB.category (categoryCode, categoryParentId, categoryLabel, categoryImageLink, categoryPopularity, categoryIsService)
VALUES 
(concat("category_", uuid()), 0, "Bricolage", "https://i.imgur.com/1XrPTg8.jpg", 3, 0),
(concat("category_", uuid()), 0, "Salle de bain", "https://i.imgur.com/ZHRwHdd.jpg", 4, 0),
(concat("category_", uuid()), 0, "Décoration", "https://i.imgur.com/Mi4pngk.jpg", 1, 0),
(concat("category_", uuid()), 0, "Électricité", "https://i.imgur.com/QEbLW7T.jpg", 5, 0),
(concat("category_", uuid()), 0, "Portes & fenêtres", "https://i.imgur.com/mq22efW.jpg", 5, 0),
(concat("category_", uuid()), 0, "Jardinage", "https://i.imgur.com/qS43pNV.jpg", 2, 0),
(concat("category_", uuid()), 0, "Soutien scolaire", "https://i.imgur.com/GdSFShm.jpg", 3, 0),
(concat("category_", uuid()), 0, "Peinture", "https://i.imgur.com/DuxKyAi.jpg", 1, 0),
(concat("category_", uuid()), 0, "Livraison", "https://i.imgur.com/y1KWeES.jpg", 0, 0),
(concat("category_", uuid()), 0, "Transport", "https://i.imgur.com/BZedtDL.jpg", 3, 0),
(concat("category_", uuid()), 0, "Multimedia", "https://i.imgur.com/I6GSsT9.jpg", 2, 0),
(concat("category_", uuid()), 0, "Loisirs", "https://i.imgur.com/msDoqyz.jpg", 4, 0);
-- read categories
SELECT * FROM jobbyDB.category;
-- update categories with restriction
UPDATE jobbyDB.category SET categoryIsArchivedDateTime = null WHERE categoryIsArchivedDateTime IS NOT NULL;

-- create subcategories 
INSERT INTO jobbyDB.category (categoryCode, categoryParentId, categoryLabel, categoryImageLink, categoryPopularity, categoryIsService)
VALUES 
(concat("category_", uuid()), 5, "Portes", "https://i.imgur.com/fBLuHTd.jpg", 3, 0),
(concat("category_", uuid()), 5, "fenêtres", " https://i.imgur.com/iSBS4GY.jpg", 5, 0),
(concat("category_", uuid()), 13, "Montage", "https://i.imgur.com/wD1g5A0.jpg", 1, 1),
(concat("category_", uuid()), 13, "Démontage", "https://i.imgur.com/d9MS2rQ.jpg", 2, 1),
(concat("category_", uuid()), 14, "Remplacement", "https://i.imgur.com/NaRauS0.png", 4, 1),
(concat("category_", uuid()), 14, "Isolation", "https://i.imgur.com/L1iVvJT.jpg", 3, 1);

-- create question for service number 15
INSERT INTO jobbyDB.question (questionCode, questionLabel, questionOrder, questionType, question_categoryId)
VALUES
(concat("question_", uuid()), "Désirez-vous un modèle en particulier ?", 1000, "text", 15),
(concat("question_", uuid()), "Dans quelle pièce souhaitez vous mettre une porte ?", 2000, "select", 15),
(concat("question_", uuid()), "Quel type de matériau désirez-vous ?", 3000, "radio", 15),
(concat("question_", uuid()), "À quelle date pouvez-vous recevoir un jobbeur ?", 4000, "date", 15),
(concat("question_", uuid()), "Quel créneau horaire vous conviendrait", 5000, "checkbox", 15),
(concat("question_", uuid()), "Avez vous autre chose a suggérer ?", 6000, "text", 15);

SELECT * FROM jobbyDB.question;

INSERT INTO jobbyDB.question (questionCode, questionLabel, questionOrder, questionType, question_categoryId)
VALUES
(concat("question_", uuid()), "Quelle est la référence du matériel à démonter ?", 1000, "text", 16),
(concat("question_", uuid()), "Dans quelle pièce se trouve cette porte ?", 2000, "select", 16),
(concat("question_", uuid()), "Quelles sont les dimensions de la porte ?", 3000, "radio", 16),
(concat("question_", uuid()), "À quelle date pouvez-vous recevoir un jobbeur ?", 4000, "date", 16),
(concat("question_", uuid()), "Quel créneau horaire vous conviendrait", 5000, "checkbox", 16),
(concat("question_", uuid()), "Avez vous autre chose a suggérer ?", 6000, "text", 16);


INSERT INTO jobbyDB.question (questionCode, questionLabel, questionOrder, questionType, question_categoryId)
VALUES
(concat("question_", uuid()), "Quelle est la référence du matériel à remplacer ?", 1000, "text", 17),
(concat("question_", uuid()), "Dans quelle pièce se trouve ce matériel ?", 2000, "select", 17),
(concat("question_", uuid()), "Quelles sont les dimensions de ce matériel ?", 3000, "radio", 17),
(concat("question_", uuid()), "À quelle date pouvez-vous recevoir un jobbeur ?", 4000, "date", 17),
(concat("question_", uuid()), "Quel créneau horaire vous conviendrait", 5000, "checkbox", 17),
(concat("question_", uuid()), "Avez vous autre chose a suggérer ?", 6000, "text", 17);

INSERT INTO jobbyDB.question (questionCode, questionLabel, questionOrder, questionType, question_categoryId)
VALUES
(concat("question_", uuid()), "Quelle est la référence du matériel à isoler ?", 1000, "text", 18),
(concat("question_", uuid()), "Dans quelle pièce se trouve ce matériel ?", 2000, "select", 18),
(concat("question_", uuid()), "Quelles sont les dimensions de ce matériel ?", 3000, "radio", 18),
(concat("question_", uuid()), "À quelle date pouvez-vous recevoir un jobbeur ?", 4000, "date", 18),
(concat("question_", uuid()), "Quel créneau horaire vous conviendrait", 5000, "checkbox", 18),
(concat("question_", uuid()), "Avez vous autre chose a suggérer ?", 6000, "text", 18);

INSERT INTO jobbyDB.response (responseCode, responseLabel, responseOrder, response_questionId)
VALUES
(concat("response_", uuid()), "chambre", 1000, 2),
(concat("response_", uuid()), "salon", 2000, 2),
(concat("response_", uuid()), "garage", 3000, 2),
(concat("response_", uuid()), "extérieur", 4000, 2),
(concat("response_", uuid()), "bois plein", 1000, 3),
(concat("response_", uuid()), "métal", 2000, 3),
(concat("response_", uuid()), "plastique", 3000, 3),
(concat("response_", uuid()), "contreplaqué", 4000, 3),
(concat("response_", uuid()), "8H00-12H00", 1000, 5),
(concat("response_", uuid()), "12H00-14H00", 2000, 5),
(concat("response_", uuid()), "14H00-18H00", 3000, 5),
(concat("response_", uuid()), "18H00-20H00", 4000, 5);



INSERT INTO jobbyDB.response (responseCode, responseLabel, responseOrder, response_questionId)
VALUES
(concat("response_", uuid()), "chambre", 1000, 8),
(concat("response_", uuid()), "salon", 2000, 8),
(concat("response_", uuid()), "garage", 3000, 8),
(concat("response_", uuid()), "extérieur", 4000, 8),
(concat("response_", uuid()), "50cm X 100cm", 1000, 9),
(concat("response_", uuid()), "100cm X 150cm", 2000, 9),
(concat("response_", uuid()), "150cm X 200cm", 3000, 9),
(concat("response_", uuid()), "200cm X 250cm", 4000, 9),
(concat("response_", uuid()), "8H00-12H00", 1000, 11),
(concat("response_", uuid()), "12H00-14H00", 2000, 11),
(concat("response_", uuid()), "14H00-18H00", 3000, 11),
(concat("response_", uuid()), "18H00-20H00", 4000, 11);

-- update wrong foreign key values for a response between 5 and 8
UPDATE jobbyDB.response SET response_questionId = 3 WHERE responseId BETWEEN 5 AND 8;
UPDATE jobbyDB.response SET response_questionId = 5 WHERE responseId BETWEEN 9 AND 12;

INSERT INTO jobbyDB.response (responseCode, responseLabel, responseOrder, response_questionId)
VALUES
(concat("response_", uuid()), "chambre", 1000, 14),
(concat("response_", uuid()), "salon", 2000, 14),
(concat("response_", uuid()), "garage", 3000, 14),
(concat("response_", uuid()), "extérieur", 4000, 14),
(concat("response_", uuid()), "50cm X 100cm", 1000, 15),
(concat("response_", uuid()), "100cm X 150cm", 2000, 15),
(concat("response_", uuid()), "150cm X 200cm", 3000, 15),
(concat("response_", uuid()), "200cm X 250cm", 4000, 15),
(concat("response_", uuid()), "8H00-12H00", 1000, 17),
(concat("response_", uuid()), "12H00-14H00", 2000, 17),
(concat("response_", uuid()), "14H00-18H00", 3000, 17),
(concat("response_", uuid()), "18H00-20H00", 4000, 17);

INSERT INTO jobbyDB.response (responseCode, responseLabel, responseOrder, response_questionId)
VALUES
(concat("response_", uuid()), "chambre", 1000, 20),
(concat("response_", uuid()), "salon", 2000, 20),
(concat("response_", uuid()), "garage", 3000, 20),
(concat("response_", uuid()), "extérieur", 4000, 20),
(concat("response_", uuid()), "50cm X 100cm", 1000, 21),
(concat("response_", uuid()), "100cm X 150cm", 2000, 21),
(concat("response_", uuid()), "150cm X 200cm", 3000, 21),
(concat("response_", uuid()), "200cm X 250cm", 4000, 21),
(concat("response_", uuid()), "8H00-12H00", 1000, 23),
(concat("response_", uuid()), "12H00-14H00", 2000, 23),
(concat("response_", uuid()), "14H00-18H00", 3000, 23),
(concat("response_", uuid()), "18H00-20H00", 4000, 23);


SELECT * FROM jobbyDB.response;

-- GET ALL RESPONSES FOR THE QUESTION WITH A RESPONSE_QUESTIONiD OF 23
SELECT * FROM jobbyDB.response WHERE response_questionId = 23;

-- GET ALL RESPONSES FOR THE QUESTION WITH A QUESTIONiD OF 4000 or 23
SELECT * FROM jobbyDB.response WHERE response_questionId = 4000 OR response_questionId = 23;

-- GET ALL RESPONSES FOR THE QUESTION WITH A QUESTIONiD OF 21 or 23
SELECT * FROM jobbyDB.response WHERE response_questionId = 21 OR response_questionId = 23;

-- GET ALL RESPONSES FOR THE QUESTION WITH A RESPONSE_QUESTIONID EQUAL TO THE QUESTION ID WHERE QUESTIONCODE = question_4d468ac2-f0de-11e9-8abb-dcf505905f9b
SELECT * FROM jobbyDB.response WHERE response_questionId IN (
SELECT questionId from jobbyDB.question WHERE questionCode = "question_4d468ac2-f0de-11e9-8abb-dcf505905f9b");

-- GET ALL RESPONSES FOR ALL QUESTIONS RELATED TO THE CATEGORY WHERE CATEGORYCODE = category_280ddebf-f0cd-11e9-8abb-dcf505905f9b
SELECT * FROM jobbyDB.response WHERE response_questionId IN (
SELECT questionId from jobbyDB.question WHERE question_categoryId IN (
SELECT categoryId from jobbyDB.category WHERE categoryCode = "category_280ddebf-f0cd-11e9-8abb-dcf505905f9b")); 


-- GET ALL RESPONSES FOR ALL QUESTIONS RELATED TO ALL CATEGORIES RELATED TO ALL ITS PARENTS RELATED TO ITS PARENT (SUB QUERIES)
SELECT responseLabel FROM jobbyDB.response WHERE response_questionId IN (
SELECT questionId from jobbyDB.question WHERE question_categoryId IN (
SELECT categoryId FROM jobbyDB.category WHERE categoryParentId IN (
SELECT categoryId from jobbyDB.category WHERE categoryParentId IN (
SELECT categoryId from jobbyDB.category WHERE categoryCode = "category_7231b52c-f0c9-11e9-8abb-dcf505905f9b"))));


SELECT * FROM  jobbyDB.response AS r
INNER JOIN
jobbyDB.question AS q
ON r.response_questionId = q.questionId
WHERE q.questionId IN (
SELECT questionId WHERE questionCode = "question_4d468ac2-f0de-11e9-8abb-dcf505905f9b");

-- recuperer tout ce qui est en commun entre la table question et la table category avec le code la category :
-- category_280ddebf-f0cd-11e9-8abb-dcf505905f9b

SELECT * FROM jobbyDB.question AS q
INNER JOIN 
jobbyDB.category AS c
ON q.question_categoryId = c.categoryId
WHERE c.categoryId IN (
SELECT categoryId WHERE categoryCode = "category_280ddebf-f0cd-11e9-8abb-dcf505905f9b");

-- select all question and read in order (desc)
SELECT * FROM jobbyDB.question
ORDER BY questionId DESC;

SELECT questionOrder, questionLabel FROM jobbyDB.question
ORDER BY questionOrder DESC, questionLabel ASC;

SELECT * FROM jobbyDB.category
WHERE categoryLabel LIKE '%age';

SELECT * FROM jobbyDB.category
WHERE categoryLabel LIKE 'mul%';

SELECT * FROM jobbyDB.category
WHERE categoryLabel LIKE 'po%es';

SELECT categoryLabel FROM jobbyDB.category
UNION
SELECT categoryCode FROM jobbyDB.category; 

SELECT * FROM jobbyDB.question
WHERE questionType = 'text' AND questionOrder = 1000;

INSERT INTO jobbyDB.category (categoryCode, categoryParentId, categoryLabel, categoryImageLink, categoryIsArchived, categoryIsArchivedDateTime, categoryPopularity, categoryIsService)
VALUES 
(concat("category_", uuid()), 0, "Bidon 1", "", 1, now(), 3, 0);

SELECT * FROM jobbyDB.category;

UPDATE jobbyDB.category SET categoryLabel = "bidon 2" WHERE categoryId = 20;

INSERT INTO jobbyDB.demand (demandCode, demandLabel, demand_userId, demand_categoryId)
VALUES
(concat("demand_",uuid()), "fake demand", 3, 1);

SELECT * FROM jobbyDB.demand;

ALTER TABLE `jobbyDB`.`demand` 
CHANGE COLUMN `demandIsArchivedDateTime` `demandIsArchivedDateTime` DATETIME NULL ;

INSERT INTO jobbyDB.demand (demandCode, demandLabel, demand_userId, demand_categoryId)
VALUES
(concat("demand_",uuid()), "fake demand", 4, 1),
(concat("demand_",uuid()), "fake demand", 5, 1);

UPDATE jobbyDB.demand SET demandIsArchivedDateTime = null WHERE demand_userId IN (
SELECT userId FROM jobbyDB.user WHERE userCode = "user_af6ade51-f0c3-11e9-8abb-dcf505905f9b");

INSERT INTO jobbyDB.question (questionCode, questionLabel, questionOrder, questionType, questionIsArchived, questionIsArchivedDateTime)
VALUES
(concat("question_", uuid()), "fake question 1", 1000, "text", 1, now()),
(concat("question_", uuid()), "fake question 2", 2000, "radio", 1, now());

SELECT * FROM jobbyDB.question;

INSERT INTO jobbyDB.response (responseCode, responseLabel, responseOrder, responseIsArchived, responseIsArchivedDateTime)
VALUES
(concat("response_", uuid()), "fake response 1", 1000, 1, now()),
(concat("response_", uuid()), "fake response 2", 2000, 1, now());

SELECT * FROM jobbyDB.response;

ALTER TABLE `jobbyDB`.`response` 
CHANGE COLUMN `responseIsArchivedDateTime` `responseIsArchivedDateTime` DATETIME NULL ;

UPDATE jobbyDB.response SET responseIsArchivedDateTime = null WHERE response_questionId IS NOT NULL;



-- récupérer toutes les catégories qui ont des questions et toutes les questions qui ont une catégorie
SELECT * FROM jobbyDB.category AS c 
inner JOIN jobbyDB.question AS q
ON c.categoryId = q.question_categoryId;

-- recuperer toutes les categories qui ont des questions, toutes les categories qui n'ont pas de questions et toutes les questions qui ont une catégorie
SELECT * FROM jobbyDB.category AS c 
LEFT JOIN jobbyDB.question AS q
ON c.categoryId = q.question_categoryId;

-- récupérer toutes les catégories qui ont des questions, toutes les catégories qui n'ont pas de questions et toutes les questions qui n'ont pas de catégorie
SELECT * FROM jobbyDB.category AS c 
LEFT JOIN jobbyDB.question AS q
ON q.question_categoryId IS NULL;
-- focalise les requêtes sur jobbyDB
USE jobbyDB;

-- récupérer toutes les questions qui ont des réponses et toutes les réponses qui n'ont pas de questions
SELECT * FROM question AS q
RIGHT JOIN response AS r
ON q.questionId = r.response_questionId;

-- récupérer toutes les questions et toutes les réponses qui n'ont pas de question
SELECT * FROM question AS q
LEFT JOIN response AS r
ON r.response_questionId IS NULL;
 
-- récupérer toutes les catégrories qui ont des questions et toutes les questions qui ont des réponses
SELECT * FROM category AS c
INNER JOIN question AS q
ON c.categoryId = q.question_categoryId
INNER JOIN response as r
ON q.questionId = r.response_questionId;

-- récupérer toutes les catégories, toutes les questions qui ont une catégorie et une réponse ainsi que toutes les réponses qui ont des questions
SELECT * FROM category AS c
LEFT JOIN question AS q
ON c.categoryId = q.question_categoryId
LEFT JOIN response AS r
ON q.questionId = r.response_questionId;

-- récupérer toutes les catégories qui ont des questions, toutes les questions qui ont une catégorie et une réponse ainsi que toutes les réponses
SELECT * FROM  category AS c
INNER JOIN question AS q
ON c.categoryId = q.question_categoryId
RIGHT JOIN response AS r
ON q.questionId = r.response_questionId;

SELECT * FROM demand;

INSERT INTO pro (proCode, proLabel, proFirstname, proLastname, proMail, proMobileNumber, proPassword, proAge, proNotation)
VALUES
(concat("pro_", uuid()), "rapid boucherie", "jack", "l éventreur", "j.kill@horreur.salace", 36153615, "machette", 45, 5),
(concat("pro_", uuid()), "rapid transport", "fast", "&furious", "f.furious@course.rally", 1234567890, "bolide", 35, 4),
(concat("pro_", uuid()), "rapid intervention", "spider", "man", "s.man@toile.avenger", 999666333, "heros", 20, 3);

SELECT * FROM pro;

INSERT INTO profession (professionCode, professionLabel)
VALUES
(concat("profession_", uuid()), "boucher"),
(concat("profession_", uuid()), "taxi"),
(concat("profession_", uuid()), "infirmier");

SELECT * FROM profession;
-- insert multiple or bulk insert
INSERT INTO proProfession (proProfession_proId, proProfession_professionId)
VALUES
(3, 3),
(3, 2),
(1, 1),
(1, 3),
(2, 2),
(2, 1);

SELECT * FROM proProfession;

SELECT * FROM menu;

INSERT INTO menu (menuCode, menuItemLabel, menuItemLinkTo)
VALUES 
(concat("menu_", uuid()), "Nos offres", "/categories"),
(concat("menu_", uuid()), "Connexion", "/login"),
(concat("menu_", uuid()), "À propos", "/about"),
(concat("menu_", uuid()), "Contact", "/contact"),
(concat("menu_", uuid()), "Nos partenaires", "/partners");

select * from response
where response_questionId in (
select questionId from question where question_categoryId = 15);
select * from demand;
select * from user;
-- user_af6ade8f-f0c3-11e9-8abb-dcf505905f9b
UPDATE demand SET demandLabel = 'fake demand made by user number 5'
WHERE demand_userId IN (
SELECT userId FROM user
WHERE userCode = 'user_af6ade8f-f0c3-11e9-8abb-dcf505905f9b');

DELETE FROM demand 
WHERE demand_userId IN (
SELECT userId FROM user
WHERE userCode = 'user_af6ade8f-f0c3-11e9-8abb-dcf505905f9b');

INSERT INTO demand ( demandCode, demandLabel, demand_userId, demand_categoryId)
VALUES
(concat('demand_', uuid()), 'fake demand', 5, 1);

SELECT questionLabel, categoryLabel, responseLabel FROM question AS q
inner JOIN category AS c
ON q.question_categoryId = c.categoryId
inner JOIN response AS r
ON q.questionId = response_questionId;

SELECT questionLabel, categoryLabel FROM question AS q
LEFT JOIN category AS c
ON q.question_categoryId = c.categoryId;
 
SELECT questionLabel, categoryLabel FROM category AS c
LEFT JOIN question AS q
ON q.question_categoryId = c.categoryId; 

SELECT questionLabel, categoryLabel, responseLabel FROM category AS c
LEFT JOIN question AS q
ON c.categoryId = q.question_categoryId
LEFT JOIN response AS r
ON q.questionId = response_questionId;


SET @toto = 1;
select @toto;

select @tata := 2 as tata;


-- intégrité référentielle des données entre la table question et la table réponse
ALTER TABLE `jobbyDB`.`response` 
ADD INDEX `fk_response_1_idx` (`response_questionId` ASC);
;
ALTER TABLE `jobbyDB`.`response` 
ADD CONSTRAINT `response_questionId`
  FOREIGN KEY (`response_questionId`)
  REFERENCES `jobbyDB`.`question` (`questionId`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  
INSERT INTO question (questionLabel)
VALUES
('cascade question');
SELECT * FROM question;

INSERT INTO response (responseLabel, response_questionId)
VALUES
('cascade response', 27);  
SELECT * FROM response;

delete from question where questionId = 27;

-- IRD 
ALTER TABLE `jobbyDB`.`question` 
ADD INDEX `fk_question_1_idx` (`question_categoryId` ASC);
;
ALTER TABLE `jobbyDB`.`question` 
ADD CONSTRAINT `question_categoryId`
  FOREIGN KEY (`question_categoryId`)
  REFERENCES `jobbyDB`.`category` (`categoryId`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  
INSERT INTO category (categoryLabel)
VALUES
('cascade category');
SELECT * FROM category;  

INSERT INTO question (questionLabel)
VALUES
('cascade question');
SELECT * FROM question;

INSERT INTO response (responseLabel)
VALUES
('cascade response');
SELECT * FROM response;
DELETE FROM category WHERE categoryId = 21;


-- ! erreur
-- récupérer tous les professionnels qui preatique telle profession :
-- profession_8b0718ec-f1b3-11e9-bf7f-dcf505905f9b
-- SELECT * FROM pro
-- WHERE proId IN (
-- SELECT proProfessionId FROM proProfession
-- WHERE proProfession_proId IN (
-- select professionId from profession
-- where professionCode = 'profession_8b0718ec-f1b3-11e9-bf7f-dcf505905f9b'));


-- récupérer tous les professionnels qui pratique telle profession :
-- profession_8b0718ec-f1b3-11e9-bf7f-dcf505905f9b
SELECT * FROM pro
WHERE proId IN (
SELECT proProfession_proId FROM proProfession
WHERE proProfession_professionId IN (
select professionId from profession
where professionCode = 'profession_8b0718ec-f1b3-11e9-bf7f-dcf505905f9b'));


-- pro_e41b64ad-f1b2-11e9-bf7f-dcf505905f9b
SELECT * FROM  profession
WHERE professionId IN (
SELECT proProfession_professionId FROM proProfession
WHERE proProfession_proId IN (
select proId from pro
where proCode = 'pro_e41b64ad-f1b2-11e9-bf7f-dcf505905f9b'));


-- récupérer tous les labels des pros et tous les labels des professions qu'ils pratiquent
SELECT proLabel, professionLabel FROM pro AS p
inner JOIN proProfession AS pp
ON p.proId = pp.proProfession_proId
inner JOIN profession AS pf
ON pp.proProfession_professionId = pf.professionId;


select * from user;
commit;
-- autocommit = 1

set autocommit = 0;
select * from user;

start transaction;
insert into user (userFirstname) values ('toto');
rollback;
-- set autocommit = 1;

start transaction;
insert into user (userFirstname) values ('toto');
SAVEPOINT newUserInsert;
update user set userFirstname = 'tata' where userFirstname = 'toto';
update user set userLastname = 'tata' where userFirstname = 'tata';
rollback to newUserInsert;


-- @algo
-- 1 démarrer une transaction
-- 2 insérer dans la table demand, dans les champs demandLabel, demand_userId, demand_categoryId les valeurs 'fake demand made by clément', 3, 15
-- 3 assigner à une variable nommé @lastDemandId la dernière id insérée dans la table demande une seule fois
-- 4 insérer dans la table demandQuestion, dans le champs demandQuestion_questionId les questionId liées à la catégorie concernée par la demande en cours
-- 5 mettre à jour la table demandQuestion pour que les champs demandQuestion_demandId qui sont null correspondent bien à la demandId en cours
-- 6 selectionner en union et avec l'alias demandInfos les champs demandLabel et questionLabel des tables correspondantes
--   grâce aux ids respectives de la table demandQuestion pour la demand en cours
-- 7 valider la transaction

start transaction;
INSERT INTO demand (demandLabel, demand_userId, demand_categoryId)
VALUES ('fake demand made by clément', 3, 15);
SET @lastDemandId = (select last_insert_id() from demand limit 1);
insert into demandQuestion(demandQuestion_questionId)(select questionId from question where question_categoryId in (
select demand_categoryId from demand where demandId = @lastDemandId));
update demandQuestion set demandQuestion_demandId = @lastDemandId where demandQuestion_demandId is null;
commit;
select demandLabel as demandInfos from demand where demandId in (
select demandQuestion_demandId from demandQuestion where demandQuestion_demandId = @lastDemandId )
union
select questionLabel from question where questionId in (
select demandQuestion_questionId from demandQuestion where demandQuestion_demandId = @lastDemandId);



-- duplicate
INSERT INTO menu(menuItemLabel, menuItemLinkTo)
SELECT menuItemLabel, menuItemLinkTo 
FROM menu AS m2






