

#######
# from clement


# chemin relatif = chemin qui depend de l'endroit ou tu te situes ex
cd ..
cd ../../../../../
cd ../../bin/../bin/../
# cd .. remonte un dossier
cd .
# reste dans le dossier courant => c'est egalement des chemins relatifs


# chemin absolu
# les chemins absolues commencent par / ex;
cd /home/clement/



# hosts sous linux
/etc/hosts

127.0.0.1	localhost
127.0.1.1	clement-ASUS
127.0.0.1   clement.com

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters


# npmnpm
# créer un fichier package.json (utiliser par npm)
npm init 

# install le package edbug a sa version 3.2.6
npm install debug@3.2.6
# idem
npm i debug@3.2.6
# install debug a sa version latest
npm install debug

# tout reinstaller en fonction de ce qui il y a dans le package.json
npm install

# installer en dev dependencies (pas pour la prod)
npm install --save-dev nodemon
npm i --save-dev swagger-jsdoc


# how to find a used port
sudo lsof -n -i :5000 | grep LISTEN

# how to kill a process by process Id
kill 11111111
# how to kill a process by name
pkill node
# lancer un script avec le mot cle run
npm run dev
par exemple avec ce fichier
{
  "name": "jobby-api",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "dev": "nodemon app.js"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "debug": "^3.2.6",
    "express": "^4.17.1",
    "express-validator": "^6.2.0",
    "winston": "^3.2.1"
  }
}





# git

git status
# index le fichier pour un futur commit (met le fichier dans l'enveloppe)
git add aFile.js
# ecrit un message sur l'enveloppe
git commit -m"mise en place du debuuger"
# envoyer l'enveloppe
git push
# synchroniser une branch
git pull

# git stash (la planque) pour mettre en attente des fichiers non commité

# vi
vi readme.md
# PASSER EN MODE INSERTION
ESC i
# ENREGISTRER
ESC : w
# quitter
ESC : q

# ENREGISTRER forcer
ESC : w!
# quitter forcer
ESC : q!
# ENREGISTRER et QUITTER
ESC : wq
# extension ou alternative à vi
- vim
- nano
- emacs


# bash enchainer des commandes avec doubles esperluettes
ls && cd ..


# lancer un processus en background avec un &
ping -i 20 google.fr &
# voir les processus 
ps
# voir tous les processus 
ps -a
# voir tous les jobs 
jobs

# placer un processus en background
ping -i 20 google.fr 
# ctrl+z pour freezer le processus
# passer en background
bg

# placer un processus en foreground
ping -i 20 google.fr &
# ctrl+z pour freezer le processus
# passer en background
fg

# placer un processus précis en foreground
# ctrl+z pour freezer le processus
# passer en background avec 2 l'id du job
fg 2




# stop a service for exemple stop docker
service docker stop


# stop a service for exemple stop docker
service mysql restart


# docker

docker run hello-world

docker run ubuntu -it bash

# list les processus du demon docker (aka la baleine)
docker ps


cd docker

# force remove a container by container id
docker rm e14337ca92ed -f
# force remove a container by container name
docker rm nifty_edison -f

# create a file name dockerfile with content
cat <<'EOF' > dockerfile
FROM node

RUN npm i -g nodemon

CMD ["ping", "google.fr"]
EOF

# build an image
docker build . -t nodeclement

# run my new image
docker run nodeclement


# enter a container (avec fd7037e2c932 l'id du container)
docker exec -ti fd7037e2c932 bash





docker run -p 8090:8080 nodeclement


# persistance des données
cd docker2
docker build . -t jobbyapi
docker run -p 8090:5000 --mount type=bind,source=/home/clement/clement-garland/jobby-api,destination=/home/ jobbyapi

# enter the container

# a retenir :
- il y a une ligne de commande qui s'appelle docker
- cette commande peut parler au daemon docker (baleine)
- la baleine a une bibliotheque perso d'image
- si elle n'a pas l'image en stock elle va le chercher dans des registries (docker.hub)
- la baleine sait a partir d'une image créer un ou plusieurs container 
- un container porte un processus de base
- si ce processus meurt le container meurt
- vis à vis du host (ton ordinateur) il est possible de faire des liaisons avec ces containers
- via du port forwarding (2000:5000 le port 2000 sur ton poste est lié au port 5000 dans le container)
- et/ou du volume binding (le volume du container est lié à ton disque dur) => principe de la persistance des données
- Tu peux construire tes propres images en les buildant


# notion de semver (dependencies HELL)

# changer les permissions d'un fichier
chmod 644 bootstrap-project && ll bootstrap-project 
# give execution to a group
chmod g+x bootstrap-project 


# bootstrap a project
#!/bin/bash
cd ~
mkdir translate.clement.com
cd translate.clement.com
mkdir src
# création d'un fichier vide
touch src/app.js
npm init -y
npm i express cors

# creation d'un script sous linux IL FAUT LUI DONNER LES DROITS D'EXECUTION
chmod g+x bootstrap-project
# lancer le script
bash bootstrap-project


#!/usr/bin/env node
 const exec = require('child_process').exec;
 function puts(error, stdout, stderr) { console.log(stdout) }
 exec('ls -l', puts);










# create a file name startAll with content
cat <<'EOF' > ~/clement-garland/bash/startAll.sh
cd ~/clement-garland/jobby-admin/
npm start &

cd ~/clement-garland/jobby-clientOldFashion/
npm start &

cd ~/clement-garland/jobby-api/
npm run dev &

cd ~/clement-garland/web-hook.jobby.fr/
npm start &
EOF
# rend executable le fichier
chmod +x ~/clement-garland/bash/startAll.sh
# pour executer le fichier
bash ~/clement-garland/bash/startAll.sh