var mysql = require('mysql')
var util = require('util')
// const secrets = require('./docker-secrets')
var connectionLimit = process.env.DB_CONNECTION_LIMIT || 100
var host = process.env.DB_HOST || '127.0.0.1'
var port = process.env.DB_PORT || '3306'
var user = process.env.DB_USER || 'root'
var database = process.env.DB_DATABASE || 'jobbyDB'
var password = process.env.DB_PASSWORD || 'root'
var options = {
    connectionLimit: connectionLimit,
    host: host,
    port: port,
    user: user,
    password: password,
    database: database,

    supportBigNumbers: true,
    debug: false,
    trace: true,
    multipleStatements: true,
    acquireTimeout: 1000000,
    charset: 'utf8mb4'
}
//    debug: ['ComQueryPacket'],
var pool = mysql.createPool(options)

pool.query = util.promisify(pool.query) // for async await purpose
pool.format = mysql.format // requete en console

module.exports = pool