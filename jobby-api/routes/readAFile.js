const router = require("express").Router()
const fs = require('fs')
const fsPromises = require('fs').promises


const path = require('path')

const winston = require('winston');
const logger1 = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log` 
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({ filename: 'combined1.log' })
    ]
});
const logger2 = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log` 
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({ filename: 'combined2.log' })
    ]
});
const logger3 = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log` 
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({ filename: 'combined3.log' })
    ]
});
// 107
router.get('/sync',
    (req, res, next) => {
        let filePath = path.join(__dirname, 'start.html');
        // console.log("1")


        let content = fs.readFileSync(filePath)
        // logger1.info(content.toString())
        // console.log("2")

        res.send('ok')
    }
)

// 250
router.get('/notSync',
    (req, res, next) => {
        let filePath = path.join(__dirname, 'start.html');
        // console.log("1")

        fs.readFile(filePath, (err, content) => {
            // console.log("2")

            // console.log("4")
            // logger2.info(content.toString())
            // console.log(content.toString())
        })
        // console.log("5")
        res.send('ok')


    }
)

router.get('/async',
    async (req, res, next) => {
        let filePath = path.join(__dirname, 'start.html');
        // console.log("1")
        let content = await fsPromises.readFile(filePath)
        // console.log("2")
        // logger3.info(content.toString())




        res.send('ok')
    }
)

module.exports = router