const express = require("express")
const app = express()
const router = express.Router()
const { sanitizeParam, param, body, query, sanitizeBody, validationResult } = require('express-validator');
const superagent = require('superagent')
const pool = require('../utils/dbPool')


// optionale req.params
router.get('/:responseId',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        return next();
    },
    // check 
    [
        // categoryId must be an isInt
        param('responseId').optional().isInt().withMessage('nous attentons un entier SVP questionId'),
        sanitizeParam('responseId').toInt(),
    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },
    async (req, res, next) => {
        // je recupere la data
        let sql = ''; let params = []; let response;
        // WARNING INJECTION SQL
        sql = `
        SELECT  r.responseId,
                r.responseLabel,
                r.response_questionId,
                q.questionLabel,
                q.questionId,
                categoryId,
                c.categoryLabel
        FROM response AS r
            INNER JOIN 
                question AS q
                ON r.response_questionId = q.questionId
            INNER JOIN 
                category AS c
                ON q.question_categoryId = c.categoryId
        WHERE r.responseId = ?
        `
        params.push(req.params.responseId)
        try {
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            console.log(error, pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            return next('route')
        }
        res.locals.responses = response
        return next()
    },
    (req, res, next) => {
        res.locals.myData = res.locals.responses
        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }

)


router.get('/:responseId',

    // retour
    (req, res, next) => {
        if (res.locals.error)
            res.status(422)
        res.json(res.locals.myData)
        return
    }
)



module.exports = router