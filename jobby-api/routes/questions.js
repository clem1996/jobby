const express = require("express")
const app = express()
const router = express.Router()
const { sanitizeParam, param, body, query, sanitizeBody, validationResult } = require('express-validator');
const superagent = require('superagent')
const pool = require('../utils/dbPool')


router.get('/',
    async (req, res, next) => {
        let sql = ''
        let params = []
        let response

        sql = `
    SELECT  q.questionId, 
            q.questionLabel,
            q.questionType

    FROM question AS q 
    where q.question_categoryId = ?
    `
        params.push(req.query.question_categoryId)
        try {
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            console.log(error)
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            // return next() // mode SOLID
            return next(error)
        }
        res.json(response)
    }
)

// router.get('/:questionId', (req, res) => {
//     let {questionId} = req.params
//     // let categoryId = req.params.categoryId
//     res.send(`je récupère la question avec l'id ${questionId}`)
//     // res.send(req.params)
// })




module.exports = router