const express = require("express")
const router = express.Router()
const pool = require('../utils/dbPool')


// const lang = ["fr", "en"]
// [
//     {
//         langCode: "FR",
//     },
//     {
//         langCode: "EN",
//     },
// ]

router.get('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        return next();
    },
    //  je check

    // recuperation de data

    async (req, res, next) => {
        // je recupere la data
        let sql = ''
        let params = []
        let response
        // sql injection 
        sql = `
        SELECT langCode FROM lang
        
        `
        // params.push(req.query.monZob)
        try {
            let format = pool.format(sql, params) // 
            // console.log(format);
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            return next('route')
        }

        res.locals.lang = response
        return next()
    },
    // traitement (filtre, modification de la data....)
    (req, res, next) => {
        res.locals.lang = res.locals.lang.map(lang => {
            return lang.langCode
        })
        return next()
    },
    (req, res, next) => {
        res.locals.myData =  res.locals.lang
        return next();
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)


router.get('/',
    // retour
    (req, res, next) => {
        if (res.locals.error)
            res.status(422)
        res.json(res.locals.myData)
        return
    }
)

module.exports = router

