const express = require("express")
const router = express.Router()
const { check, query, body, sanitize, sanitizeQuery, sanitizeBody, validationResult } = require('express-validator');
const pool = require('../utils/dbPool')

router.post('/',

    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        return next();
    },
    //  je check
    [
        body('lang').isIn(["Fr", "En", "fr", "en"]).withMessage(`la langue choisis n'est pas valide`),
        sanitizeBody('lang').customSanitizer(value => {

            if (value === 'Fr')
                return 'fr'
            else if (value === 'En')
                return 'en'
            return value
        })
    ],

    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            return next('route')
            // return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },
    // recuperation de data
    async (req, res, next) => {
        // je recupere la data
        // requete en base 
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        SELECT m.menuItemLabel AS label, m.menuItemLinkTo AS link, m.menuTarget, m.menuSpecialActionOnClick
        FROM menu AS m
        WHERE m.menu_langId	 = (
            SELECT l.langId FROM lang AS l WHERE l.langCode = ?
        )
        `
        // binding dynamique de parametre 
        params.push(req.body.lang)

        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            console.log(pool.format(sql,params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.menu2 = response
        return next()
    },
    (req, res, next) => {
        res.locals.myData = res.locals.menu2
        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)


router.post('/',
    // retour
    (req, res, next) => {
        if (res.locals.error)
            res.status(422)
        res.json(res.locals.myData)
        return
    }
)



module.exports = router




// const express = require("express")
// const router = express.Router()
// const { check, query, body, sanitizeQuery, validationResult } = require('express-validator');


// const menuFr = [
//     {
//         label: "Nos offres",
//         link: "/categories",
//     },
//     {
//         label: "Connexion",
//         link: "/login",
//     },
//     {
//         label: "À propos",
//         link: "/about",
//     },
//     {
//         label: "Contact",
//         link: "/contact",
//     },
//     {
//         label: "Nos partenaires",
//         link: "/partners",
//     }
// ]
// const menuEn = [
//     {
//         label: "offers",
//         link: "/categories",
//     },
//     {
//         label: "sign-in",
//         link: "/login",
//     },
//     {
//         label: "about",
//         link: "/about",
//     },
//     {
//         label: "Contact",
//         link: "/contact",
//     },
//     {
//         label: "partners",
//         link: "/partners",
//     }
// ]
// const menuAll = [
//     {
//         labelFr: "nos offres",
//         labelEn: "offers",
//         link: "/categories",
//     }
// ]
// const menuAllBis = [
//     {
//         trads: [{
//             fr: "nos offres",
//             en: "offers"
//         }],
//         link: "/categories",
//     },
//     {
//         trads: [{
//             fr: "contact",
//             en: "contact"
//         }],
//         link: "/contact",
//     }
// ]
// const menuAllBisOther = [
//     {
//         label: "nos offres",
//         lang: "fr",
//         link: "/categories",
//     },
//     {
//         label: "offers",
//         lang: "en",
//         link: "/categories",
//     }
// ]
// router.get('/',
//     //  je check
//     [
//         query('lang').optional().isIn(["Fr", "En", "fr", "en"]).withMessage(`la langue choisis n'est pas valide`),
//         sanitizeQuery('lang').customSanitizer(value => {

//             if(value === 'Fr')
//                 return 'fr'
//             else if(value === 'En')
//                  return 'en'
//             return value
//           })
//     ],

//     // gestion d'erreur
//     (req, res, next) => {
//         // Finds the validation errors in this request and wraps them in an object with handy functions
//         const errors = validationResult(req);
//         if (!errors.isEmpty()) {
//             return res.status(422).json({ errors: errors.array() });
//         }
//         return next()
//     },


//     (req, res, next) => {
//         // je recupere la data
//         // requet en base 
//         // 
//         if (req.query.lang === 'fr')
//             res.locals.menu2 = menuFr
//         else if (req.query.lang === 'en')
//             res.locals.menu2 = menuEn
//         else
//             res.locals.menu2 = menuFr
//         return next()
//     },
//     (req, res, next) => {
//         // je renvoie la réponse
//         res.json(res.locals.menu2)
//         return
//     }
// )


// module.exports = router