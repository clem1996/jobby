const express = require("express")
const app = express()
const router = express.Router()
const { sanitizeParam, param, sanitizeQuery, body, query, sanitizeBody, validationResult } = require('express-validator');
const superagent = require('superagent')
const pool = require('../utils/dbPool')



// optionale req.params
router.get('/:categoryParentId?',

    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        return next();
    },
    // check 
    [
        // categoryId must be an isInt
        param('categoryParentId').optional().isInt().withMessage('nous attentons un entier SVP'),
        sanitizeParam('categoryParentId').toInt(),
        query('modeBackOffice').optional().isBoolean(),
        sanitizeQuery('modeBackOffice').toBoolean(),

    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },

    async (req, res, next) => {
        // je recupere la data
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        // back office
        if (req.query.modeBackOffice === true) {
            sql = `
            SELECT c1.categoryLabel,
                c1.categoryImageLink , 
                c1.categoryId, 
                c1.categoryIsService,
                c1.categoryCode,
                c1.categoryIsArchived,
                c1.categoryCreationDateTime,
                c1.categoryParentId,
                c1.categoryIsService,
                (
                    SELECT count(*)
                    FROM category AS c2 
                    WHERE c2.categoryParentId = c1.categoryId
                ) AS nbEnfant
            FROM category AS c1
                    
            `

            // http://api.jobby.fr:5000/categories?modeBackOffice=true&sort=categoryImageLink&sortSens=DESC
            // if (req.query.sort && req.query.sortSens) {
            //     sql += ' ORDER BY ' + req.query.sort + ' ' + req.query.sortSens
            // }

        } else {
            // front-office
            sql = `
            SELECT c.categoryLabel,
                c.categoryImageLink , 
                c.categoryId, 
                c.categoryIsService,
                c.categoryCode
            FROM category AS c
            WHERE 1=1
            AND (
                categoryIsArchived IS NULL
                OR 
                categoryIsArchived = 0
                )

            `
            if (req.params.categoryParentId) {
                sql += ' AND  categoryParentId  = ?'
                params.push(req.params.categoryParentId)
            }
            else {
                sql += ' AND  categoryParentId  = 0'
            }
        }

        try {
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            console.log(error)
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.categories = response
        return next()
    },
    (req, res, next) => {
        res.locals.myData = res.locals.categories
        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.get('/:categoryParentId?',

    // retour
    (req, res, next) => {
        if (res.locals.error)
            res.status(422)
        res.json(res.locals.myData)
        return
    }
)






router.post('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        return next();
    },
    //  je check
    [
        body('categoryLabel').isString().isLength({ max: 50 }),
        sanitizeBody('categoryLabel').trim(),
        body('categoryLabel').custom(value => {
            // pattern
            let pattern = /eval\(|delete/
            if (value.match(pattern) !== null) {
                return false
            }
            return true
        }),

        // sanitizeBody('categoryLabel').customSanitizer( async(value) => {
        //     let monApiDeTraduction = "http://translate.clement.com:2000/translate"
        //     let response = await superagent
        //         .post(monApiDeTraduction)
        //         .set('Accept', 'application/json')
        //         .send({text:value})
        //      console.log(response.body)
        //     let response2= await response.body
        //     return response2.translate



        //   }),

    ],


    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },
    async (req, res, next) => {
        // je recupere la data
        let sql = ''
        let params = []
        let response
        // sql injection 
        sql = `
          SELECT c1.categoryLabel,
                    c1.categoryImageLink , 
                    c1.categoryId, 
                    c1.categoryIsService,
                    c1.categoryCode,
                    c1.categoryIsArchived,
                    c1.categoryCreationDateTime,
                    c1.categoryIsService,
                    (
                        SELECT count(*)
                        FROM category AS c2 
                        WHERE c2.categoryParentId = c1.categoryId
                ) AS nbEnfant
        FROM jobbyDB.category AS c1
             
        WHERE c1.categoryLabel LIKE ?;
        `
        //    WHERE MATCH(c1.categoryLabel) AGAINST(?)   cf fulltext https://dev.mysql.com/doc/refman/8.0/en/fulltext-natural-language.html


        // binding dynamique de parametre 
        params.push(req.body.categoryLabel + '%')
        try {
            let format = pool.format(sql, params) // 
            console.log(format);
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            res.locals.error = true
            return next('route')
        }


        res.locals.categories = response
        return next()
    },

    // je filtre
    // (req, res, next) => {
    //     res.locals.categories = res.locals.categories.filter(category => {
    //         if (category.categoryLabel.includes(req.body.categoryLabel))
    //             return true
    //         else return false
    //     })
    //     return next()
    // },
    (req, res, next) => {
        res.locals.myData = res.locals.categories
        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }

)

router.post('/',
    // retour
    (req, res, next) => {
        if (res.locals.error)
            res.status(422)
        res.json(res.locals.myData)
        return
    }
)


module.exports = router
