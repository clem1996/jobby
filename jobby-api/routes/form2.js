const express = require("express")
const router = express.Router()
const { check, query, body, sanitizeQuery, validationResult } = require('express-validator');
const superagent = require('superagent')

router.get('/',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        return next();
    },
    // 1 checkers
    query('categoryId').isInt(),
    sanitizeQuery("categoryId").toInt(),
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            return next('route')
            // return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },
    // 2 recuperation de data
    async (req, res, next) => {
        let apiCategories = 'http://api.jobby.fr:5000/categories'
        //fetch
        let response = await superagent
            .get(apiCategories)
            .query({modeBackOffice:true})
            
        res.locals.myData = response.body
        return next()
    },
    // 3 traitement (filtre, modification de la data....) elag la masse
    (req, res, next) => {
        res.locals.myData = res.locals.myData.filter(data => {
            if (data.categoryId === req.query.categoryId)
                return true
            return false
        })
        return next()
    },
    // 4 traitement (filtre, modification de la data....) restreint sur certain champs
    (req, res, next) => {
        let macategory = res.locals.myData[0]
        // spread omitting
        let { categoryLabel, ...toto } = macategory
        res.locals.myData = [{ categoryLabel }]
        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)
router.get('/',
    // retour
    (req, res, next) => {
        if (res.locals.error)
            res.status(422)
        res.json(res.locals.myData)
        return
    }
)


module.exports = router