

const express = require("express")
const router = express.Router()
const { check, query, body, sanitizeQuery, validationResult } = require('express-validator');


const menuFr = [
    {
        label: "Nos offres",
        link: "/categories",
    },
    {
        label: "Connexion",
        link: "/login",
    },
    {
        label: "À propos",
        link: "/about",
    },
    {
        label: "Contact",
        link: "/contact",
    },
    {
        label: "Nos partenaires",
        link: "/partners",
    }
]
const menuEn = [
    {
        label: "offers",
        link: "/categories",
    },
    {
        label: "sign-in",
        link: "/login",
    },
    {
        label: "about",
        link: "/about",
    },
    {
        label: "Contact",
        link: "/contact",
    },
    {
        label: "partners",
        link: "/partners",
    }
]
const menuAll = [
    {
        labelFr: "nos offres",
        labelEn: "offers",
        link: "/categories",
    }
]
const menuAllBis = [
    {
        trads: [{
            fr: "nos offres",
            en: "offers"
        }],
        link: "/categories",
    },
    {
        trads: [{
            fr: "contact",
            en: "contact"
        }],
        link: "/contact",
    }
]
const menuAllBisOther = [
    {
        label: "nos offres",
        lang: "fr",
        link: "/categories",
    },
    {
        label: "offers",
        lang: "en",
        link: "/categories",
    }
]
/**
 * @swagger
 *
 * /menuGet:
 *   get:
 *     description: get all langs or specific lang
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: lang
 *         description: lang.
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *         description: the langs
 */
router.get('/',
    //  je check
    [
        query('lang').optional().isIn(["Fr", "En", "fr", "en"]).withMessage(`la langue choisis n'est pas valide`),
        sanitizeQuery('lang').customSanitizer(value => {
            
            if(value === 'Fr')
                return 'fr'
            else if(value === 'En')
                return 'en'
            return value
        })
    ],

    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },


    (req, res, next) => {
        // je recupere la data
        // requet en base 
        // 
        if (req.query.lang === 'fr')
            res.locals.menu2 = menuFr
        else if (req.query.lang === 'en')
            res.locals.menu2 = menuEn
        else
            res.locals.menu2 = menuFr
        return next()
    },
    (req, res, next) => {
        // je renvoie la réponse
        res.json(res.locals.menu2)
        return
    }
)


module.exports = router