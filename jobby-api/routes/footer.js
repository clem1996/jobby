const express = require("express")
const router = express.Router()
const pool = require('../utils/dbPool')

// const footer = [
//     '<i class="fab fa-facebook-f"></i>',
//     '<i class="fab fa-twitter"></i>',
//     '<i class="far fa-copyright"></i>',
//     '<i class="fas fa-cookie"></i>',
// ]


// const footer = [{
//     type: 'reseauSociaux',
//     title: 'facebook',
//     link: 'facebook.com',
//     alt: '',
//     target:"",
//     apaye:true,
//     icon:true,
//     iconDetail:'fab fa-facebook-f'

// },
// {
//     type: 'reseauSociaux',
//     title: 'twitter',
//     link: 'twitter.com',
//     alt: '',
//     target:"_blank",
//     icon:true,
//     iconDetail:'fab fa-twitter'
// },
// {
//     type: 'brand',
//     title: 'copyright',
//     link: 'copyright.com',
//     alt: '',

//     target:"_blank",
//     icon:true,
//     iconDetail:'fab fa-facebook-f'
// },
// {
//     type: 'brand',
//     title: 'cookie',
//     link: 'cookie.com',
//     alt: '',
//     icon:true,
//     iconDetail:'fab fa-facebook-f'
// }
// ,
// {
//     type: 'others',
//     title: 'keepapi.com',
//     link: 'keepapi.com',
//     alt: 'trop bien',
//     icon:false,
// }
// ]


router.get('/',

    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        return next();
    },
    //  je check

    async (req, res, next) => {
        // je recupere la data
        // requete en base 
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `SELECT 
        f.footerLabel
        ,f.footerIconDetail
        ,f.footerLink
        ,f.footerType
        ,f.footerAlt
        ,f.footerTarget
        ,f.footerFlagIsIcon

        FROM footer AS f
        `

        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.footer = response
        return next()
    },
    (req, res, next) => {
        res.locals.myData = res.locals.footer
        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.get('/',
    // retour
    (req, res, next) => {
        if (res.locals.error)
            res.status(422)
        res.json(res.locals.myData)
        return
    }
)


module.exports = router