const express = require("express")
const app = express()
const router = express.Router()
const { sanitizeParam, param, body, query, sanitizeBody, validationResult } = require('express-validator');
const superagent = require('superagent')
const pool = require('../utils/dbPool')


router.get('/',
    async (req, res, next) => {
        
        let sql = ''
        let params = []
        let response
        sql = `
    SELECT  r.responseId, 
            r.responseLabel,
            r.responseCode

    FROM response AS r 
    where r.response_questionId = ?
    `
        params.push(req.query.response_questionId)
        try {
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            console.log(error)
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            // return next() // mode SOLID
            return next(error)
        }
        res.json(response)
    }
)


module.exports = router
