const express = require("express")
const app = express()
const router = express.Router()
const { sanitizeParam, param, body, query, sanitizeBody, validationResult } = require('express-validator');
const superagent = require('superagent')
const pool = require('../utils/dbPool')


router.get('/:categoryId/breadCrumb',

    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        return next();
    },
    // check 
    [
        // categoryId must be an isInt
        param('categoryId').isInt().withMessage('nous attentons un entier SVP'),
        sanitizeParam('categoryId').toInt(),

    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },
    async (req, res, next) => {
        let parents = []
        let gardeFou =0

        var searchParent = async (categoryId) => {
            gardeFou++
            if(gardeFou > 100) return

            // je recupere la data
            let sql = ''
            let params = []
            let response
            // WARNING INJECTION SQL
            sql = `
            SELECT  c1.categoryLabel, 
                c1.categoryId,
                c1.categoryParentId
            FROM category AS c1
            WHERE c1.categoryId =?
            `
            params.push(categoryId)
            try {
                console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
                response = await pool.query(sql, params) // execute la requete
            } catch (error) {
                res.locals.error = true
                console.log(error)
                console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

                return next('route')
            }
            let categoryCurrent = response[0]
            parents.unshift(categoryCurrent)

            if (categoryCurrent.categoryParentId > 0 ) {

                return searchParent(categoryCurrent.categoryParentId);
            }
        };
        await searchParent(req.params.categoryId);


        res.locals.myData = parents
        return next()
    },
    // (req, res, next) => {
    //     res.locals.myData =
    //         [
    //             { categoryId: 1, categoryLabel: "portes & fenetres" },
    //             { categoryId: 2, categoryLabel: "fenetre" },
    //             { categoryId: 4, categoryLabel: "isolation" }
    //         ]


    //     return next()
    // },
    // retour
    (req, res, next) => {
        return next('route')
    }
)

router.get('/:categoryId/breadCrumb',

    // retour
    (req, res, next) => {
        if (res.locals.error)
            res.status(422)
        res.json(res.locals.myData)
        return
    }
)






// optionale req.params
router.get('/:categoryId',
    async (req, res, next) => {
        // je recupere la data
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        SELECT  c1.categoryLabel, 
                c1.categoryImageLink,
                c1.categoryIsArchived, 
                c1.categoryId,
                c1.categoryIsService,
                c1.categoryParentId
        FROM category AS c1
        WHERE c1.categoryId =?
        `
        params.push(req.params.categoryId)
        try {
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            console.log(error)
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.json(response)

    },

)


module.exports = router
