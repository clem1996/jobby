const express = require("express")
const router = express.Router()
const { check, query, param, sanitize, sanitizeQuery, sanitizeParam, validationResult } = require('express-validator');
const pool = require('../utils/dbPool')

router.get('/:demandId',

    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        return next();
    },
    //  je check
    [
        param('demandId').isInt().withMessage(`la langue choisis n'est pas valide`),
        sanitizeParam('demandId').toInt()
    ],

    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            return next('route')
            // return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },
    // recuperation de data
    async (req, res, next) => {
        // je recupere la data
        // requete en base 
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `
        SELECT demandLabel FROM demand WHERE demandId = ?
        `
        // binding dynamique de parametre 
        params.push(req.params.demandId)

        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.demand = response[0]
        return next()
    },

    // INNER JOIN 
    // question AS q
    //        on q.questionId  = dq.demandQuestion_questionId
    //        INNER JOIN 
    //        response AS r
    //               on r.responseId  = a.answer_responseId   
    // recuperation de data
    async (req, res, next) => {
        // je recupere la data
        // requete en base 
        let sql = ''
        let params = []
        let response
        // WARNING INJECTION SQL
        sql = `

    SELECT a.answerLabel , q.questionLabel, r.responseLabel
    FROM answer AS a 
        INNER JOIN 
            demandQuestion AS dq
            on a.answer_demandQuestionId = dq.demandQuestionId
        INNER JOIN
            question AS q
                ON q.questionId = dq.demandQuestion_questionId
        LEFT JOIN
            response AS r
                ON r.responseId = a.answer_responseId



    WHERE answer_demandQuestionId  in (SELECT demandQuestionId FROM demandQuestion WHERE demandQuestion_demandId = ?)
    ORDER by q.questionId
   
    `
        // binding dynamique de parametre 
        params.push(req.params.demandId)

        try {
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

            return next('route')
        }
        res.locals.demand.answers = response
        return next()
    },

    // async (req, res, next) => {
    //     // je recupere la data
    //     // requete en base 
    //     let sql = ''
    //     let params = []
    //     let response
    //     // WARNING INJECTION SQL
    //     sql = `
    // SELECT a.* 
    // FROM answer AS a 
    //     INNER JOIN
    //         demandQuestion AS dq
    //             on a.answer_demandQuestionId = dq.demandQuestionId
   
    // WHERE demandQuestionId = ?
    // `
    //     // binding dynamique de parametre 
    //     params.push(req.params.demandId)

    //     try {
    //         response = await pool.query(sql, params) // execute la requete
    //     } catch (error) {
    //         res.locals.error = true
    //         console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))

    //         return next('route')
    //     }
    //     res.locals.demand[0].answer = response
    //     return next()
    // },

    (req, res, next) => {
        res.locals.myData = res.locals.demand
        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)


router.get('/:demandId',
    // retour
    (req, res, next) => {
        if (res.locals.error)
            res.status(422)
        res.json(res.locals.myData)
        return
    }
)



module.exports = router



