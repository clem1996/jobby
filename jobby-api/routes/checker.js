const express = require("express")
const router = express.Router()
const { check, query, body, sanitizeQuery, validationResult } = require('express-validator');




const stubCategory = [
    { id: 1, label: "electricite", year: 2018 },
    { id: 2, label: 'plomberie', year: 2000 },
    { id: 500, label: 'peinture', year: 2000 },

]


router.post('/',
    // check 
    [
        // categoryId must be an isInt
        body('categoryId').optional().isInt().withMessage('nous attentons u  entier SVP'),
        body('sort').optional().isIn(["DESC", "ASC"]).withMessage(`si vous souhiatez trier merci d'ecrire ?sort=DESC ou sort=ASC`),
        body('email').isEmail().withMessage('nous attentons un email valide  SVP'),
        body('test_contain').contains( 'toto'),
        body('test_boolean').isBoolean(), // true false 0 1
        body('aString').isLength({min:2, max:5} ), // chaine entre 2 et 5 caracteres
        

    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },

    // traitement
    (req, res, next) => {
        // 1 je vais en base de donnee chercher la data
        res.locals.categories = stubCategory
        return next()
    },

    (req, res, next) => {
        // filtrer ma data par rapport à la category demandé
        if (req.body.categoryId) {
            let myCategory = res.locals.categories.filter((categorie) => {
                if (categorie.id == req.body.categoryId)
                    return true
            })
            res.locals.myCategory = myCategory
        } else {
            res.locals.myCategory = res.locals.categories
        }

        return next()
    },
    (req, res, next) => {
        if (req.body.sort) {
            let myCategory = res.locals.myCategory.sort((categorieCourante, categorieSuivante) => {
                let sort = categorieCourante.id - categorieSuivante.id

                if (req.body.sort === "ASC")
                    return sort > 0 ? 1 : -1
                else
                    return sort > 0 ? -1 : 1
            })
            res.locals.myCategory = myCategory
        }
        return next()

    },


    (req, res, next) => {
        // je renvoie la data
        res.json(res.locals.myCategory)
        return
    },

)


router.get('/',
    // check 
    [
        // categoryId must be an isInt
        query('categoryId').optional().isInt().withMessage('nous attentons u  entier SVP'),
        sanitizeQuery('categoryId').toInt(),
        query('categoryLabel').optional().isString().withMessage('nous attentons une string SVP'),
        sanitizeQuery('categoryLabel').trim(),
        query('sort').optional().isIn(["DESC", "ASC"]).withMessage(`si vous souhiatez trier merci d'ecrire ?sort=DESC ou sort=ASC`)

    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },

    // traitement
    (req, res, next) => {
        // 1 je vais en base de donnee chercher la data
        res.locals.categories = stubCategory
        return next()
    },

    (req, res, next) => {
        // filtrer ma data par rapport à la category demandé
        if (req.query.categoryId) {
            let myCategory = res.locals.categories.filter((categorie) => {
                if (categorie.id === req.query.categoryId)
                    return true
            })
            res.locals.myCategory = myCategory
        } else {
            res.locals.myCategory = res.locals.categories
        }

        return next()
    },
    (req, res, next) => {
        if (req.query.sort) {
            let myCategory = res.locals.myCategory.sort((categorieCourante, categorieSuivante) => {
                let sort = categorieCourante.id - categorieSuivante.id

                if (req.query.sort === "ASC")
                    return sort > 0 ? 1 : -1
                else
                    return sort > 0 ? -1 : 1
            })
            res.locals.myCategory = myCategory
        }
        return next()

    },
    // filtre sur le label
    (req, res, next) => {
        if (req.query.categoryLabel) {
            let myCategory = res.locals.myCategory.filter((categorie) => {
                if (categorie.label === req.query.categoryLabel)
                    return true
            })
            res.locals.myCategory = myCategory
        }
        return next()

    },

    (req, res, next) => {
        // je renvoie la data
        res.json(res.locals.myCategory)
        return
    },

)






// router.get('/:categoryId?',
//     (req, res, next) => {
//         // 1 je vais en base de donnee chercher la data
//         res.locals.categories = stubCategory
//         return next()
//     },

//     (req, res, next) => {
//         // filtrer ma data par rapport à la category demandé
//         let myCategory = res.locals.categories.filter((categorie)=> {
//             if(categorie.id == req.params.categoryId)
//                 return true
//         })
//         res.locals.myCategory = myCategory
//         return next()
//     },
//     (req, res, next) => {
//         // je renvoie la data
//         res.json(res.locals.myCategory)
//     },

// )

















// router.post('/', (req, res) => {
//     res.send('toto')
// })


module.exports = router

