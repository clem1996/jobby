const express = require("express")
const app = express()
const router = express.Router()
const { sanitizeParam, param, body, query, sanitizeBody, validationResult } = require('express-validator');
const superagent = require('superagent')
const pool = require('../utils/dbPool')


// optionale req.params
router.get('/:questionId',
    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        return next();
    },
    // check 
    [
        // categoryId must be an isInt
        param('questionId').optional().isInt().withMessage('nous attentons un entier SVP questionId'),
        sanitizeParam('questionId').toInt(),
    ],
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },
    async (req, res, next) => {
        // je recupere la data
        let sql = ''; let params = []; let response;
        // WARNING INJECTION SQL
        sql = `
        SELECT  q.questionLabel,
                q.questionId,
                q.question_categoryId
        FROM question AS q
        WHERE q.questionId = ?
        `
        params.push(req.params.questionId)
        try {
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            console.log(error, pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            return next('route')
        }
        res.locals.questions = response
        return next()
    },
    async (req, res, next) => {
        // je recupere la data
        let sql = ''; let params = []; let response;
        // WARNING INJECTION SQL
        sql = `
        SELECT  c.categoryLabel
        FROM category AS c
        WHERE c.categoryId = ?
        `
        params.push(res.locals.questions[0].question_categoryId)
        try {
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            response = await pool.query(sql, params) // execute la requete
        } catch (error) {
            res.locals.error = true
            console.log(error, pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            return next('route')
        }
        res.locals.categories = response
        return next()
    },
    // consolider
    (req, res, next) => {
        res.locals.myData = res.locals.questions.map(question => {
            question.categoryLabel = res.locals.categories[0].categoryLabel
            return question

        })

        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }

)


router.get('/:questionId',

    // retour
    (req, res, next) => {
        if (res.locals.error)
            res.status(422)
        res.json(res.locals.myData)
        return
    }
)



module.exports = router






// sql = `
        // SELECT  q.questionLabel,
        //         q.questionId,
        //         q.question_categoryId,
        //         c.categoryLabel
        // FROM question AS q
        //     INNER JOIN 
        //         category AS c
        //         on q.question_categoryId = c.categoryId
        // WHERE q.questionId = ?
        // `