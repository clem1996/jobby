const express = require("express")
const router = express.Router()
const pool = require('../utils/dbPool')

const { param, sanitizeParam, validationResult } = require('express-validator');

// http://api.jobby.com/questionResponse/5
router.get('/:categoryId',


    // initialisation des variables de retour
    (req, res, next) => {
        res.locals.myData = []
        res.locals.error = false
        return next();
    },
    // 1 checkers
    param('categoryId').isInt().withMessage('categoryId must be provided'),
    sanitizeParam('categoryId').toInt(),
    // gestion d'erreur
    (req, res, next) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.locals.error = true
            return next('route')
            // return res.status(422).json({ errors: errors.array() });
        }
        return next()
    },
    // 2 recuperation de data
    async (req, res, next) => {
        let sql = ''
        let params = []
        let datas
        // WARNING INJECTION SQL
        sql = `
        SELECT  q.questionId,
                q.questionLabel 
        FROM question AS q
        WHERE q.question_categoryId	 = ?
        `
        // binding dynamique de parametre 
        params.push(req.params.categoryId)

        try {
            datas = await pool.query(sql, params) // execute la requete
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
        } catch (error) {
            res.locals.error = true
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            return next('route')
        }
        res.locals.questions = datas
        return next()


    },
    (req, res, next) => {
        res.locals.questionsIds = res.locals.questions.map(item => { return item.questionId })
        return next()
    },

    // 2 recuperation de data
    async (req, res, next) => {

        let sql = ''
        let params = []
        let datas
        // WARNING INJECTION SQL
        sql = `
        SELECT *
        FROM response AS r
			WHERE r.response_questionId  IN  (?);
    `
        // binding dynamique de parametre 
        params.push(res.locals.questionsIds)

        try {
            datas = await pool.query(sql, params) // execute la requete
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
        } catch (error) {
            res.locals.error = true
            console.log(pool.format(sql, params).replace(/(\r\n|\n|\r)/gm, ''))
            return next('route')
        }
        res.locals.responses = datas
        return next()

    },

    // 3 traitement (filtre, modification de la data....) elag la masse
    (req, res, next) => {
        let questions = res.locals.questions
        let responses = res.locals.responses

        let questionsResponses = questions.map(question => {
            question.questionResponses = []


            responses.map(response => {
                if (question.questionId === response.response_questionId) {
                    question.questionResponses.push(response)

                }

            })
            return question
        })

        res.locals.questionsResponses = questionsResponses
        return next()
    },

    // 3 traitement ajout de la clé categoryId à la structure.
    (req, res, next) => {
        let questionsResponses = res.locals.questionsResponses
        let datas = questionsResponses.map(questionResponses => {
            questionResponses.categoryId = req.params.categoryId
            return questionResponses
        })

        res.locals.myData = datas
        return next()
    },
    // retour
    (req, res, next) => {
        return next('route')
    }
)
router.get('/:questionId',
    // retour
    (req, res, next) => {
        if (res.locals.error)
            res.status(422)
        res.json(res.locals.myData)
        return
    }
)


module.exports = router