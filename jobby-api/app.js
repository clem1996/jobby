const express = require("express")
const app = express()
const port = 5000
var cors = require('cors')
const swaggerUi = require('swagger-ui-express');
// const swaggerDocument = require('./swagger.json'); // a la mano
const swaggerJSDoc = require('swagger-jsdoc');

const options = {
  swaggerDefinition: {
    swagger: "2.0",
    info: {
      title: "Jobby API",
      description: "API description in Markdown.",
      version: "1.0.0"
    },
    host: `api.jobby.fr:${port}`,
    basePath: "/",
    schemes: [
      "http",
      "https"
    ],
  },
  apis: ['/home/clement/clement-garland/jobby-api/routes/*.js'], // <-- not in the definition, but in the options
};

const swaggerSpec = swaggerJSDoc(options);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


var corsOptions = {
  origin: ['http://jobby.fr:3000', 'http://admin.jobby.fr:4000','http://clement-asus:8080'],
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions))


app.get('/toto', (req, res) => {
  res.send(`coucou`)
})

app.use('/answer', require('./routes/answer'))

app.use('/form2', require('./routes/form2'))
app.use('/form', require('./routes/form'))
// app.use('/categoryQuestionResponse', require('./routes/categoryQuestionResponse'))

app.use('/lang', require('./routes/lang'))

app.use('/menuGet', require('./routes/menuGet'))
app.use('/menuPost', require('./routes/menuPost'))
app.use('/footer', require('./routes/footer'))

app.use('/readAFile', require('./routes/readAFile'))
app.use('/category', require('./routes/category'))
app.use('/categories', require('./routes/categories'))

app.use('/questions', require('./routes/questions'))
app.use('/editQuestion', require('./routes/editQuestion'))
app.use('/responses', require('./routes/responses'))



app.use('/addResponse', require('./routes/addResponses'))






// 404 && global error handler (ramasse miette / garbage error collector)  && error avec promises (assync/await)

app.use('*', (req, res, next) => {

  res.status(404).send('Sorry cant find that!');
});
// error handling ======================================================================
process.on('unhandledRejection', (reason, promise) => {
  console.log({ message: `Unhandled Rejection at: ${promise}`, reason: `${reason}`, stackTrace: reason.stack })
  throw new Error(reason)
}
)
process.on('uncaughtException', (reason) => {
  console.log({ message: 'uncaughtException', stackTrace: reason.stack })
  throw new Error(reason)
}
)
// seul middleware avec 4 parametres!!!
app.use(function (err, req, res, next) {
  console.error(err);
  res.status(500).json(err);
});


const server = app.listen(port, () => console.log(`app running on port http://api.jobby.fr:${port} and process id is :${process.pid}`))

// process.once('SIGUSR2', function () {
//     server.close(function () {
//       process.kill(process.pid, 'SIGUSR2')
//     })
//   })
// const cluster = require('cluster');

// const numCPUs = require('os').cpus().length;

// if (cluster.isMaster) {
//   console.log(`Master ${process.pid} is running`);

//   // Fork workers.
//   for (let i = 0; i < numCPUs; i++) {
//     cluster.fork();
//   }

//   cluster.on('exit', (worker, code, signal) => {
//     console.log(`worker ${worker.process.pid} died`);
//   });
// } else {
//     app.listen(5000, () => console.log('app running on port 5000'))

//   console.log(`Worker ${process.pid} started`);
// }